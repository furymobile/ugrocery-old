//
//  IKOLocationManager.h
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <MapKit/MapKit.h>

@class CLLocation;

@interface IKOLocationManager : NSObject

+ (IKOLocationManager *)sharedInstance;

- (void)startUpdatingLocation;

- (void)stopUpdatingLocation;

- (void)resumeUpdatingLocation;

- (CLLocation *)getCurrentLocation;

- (void)getPlacemark:(void (^)(NSError *error, CLPlacemark *placemark))completion;

- (BOOL)isEnabled;


@end
