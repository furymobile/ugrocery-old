//
//  IKOFLBHelper.m
//  uGrocery
//
//  Created by Duane Schleen on 8/22/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFLBHelper.h"
#import "IKOProduct.h"
#import "IKOServerResponse.h"
#import "NSArray+Additions.h"

@implementation IKOFLBHelper {
    IKOServerResponse *_response;
}

- (id)initWithServerResponse:(IKOServerResponse *)response {
    self = [super init];
    if (self) {
        _response = response;
        _flbData = ([response.payload isKindOfClass:[NSDictionary class]]) ? response.payload :[NSDictionary new];
    }
    return self;
}

- (BOOL)hasValidPayload {
    if (!_flbData || ([_flbData allKeys] && ((NSArray *)[_flbData allKeys]).count == 0))
        return NO;

    NSDictionary *categoriesNode = _flbData[@"Categories"];
    if (categoriesNode && categoriesNode.count == 0)
        return NO;

    return YES;
}

- (NSUInteger)countOfProducts {
    NSDictionary *productsNode = _flbData[@"Products"];
    return (productsNode) ?[productsNode allKeys].count : 0;
}

- (NSArray *)storesFor:(NSNumber *)productId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *productsNode = _flbData[@"Products"];

    if (productsNode && ![productsNode isKindOfClass:[NSArray class]] && [productsNode allKeys].count > 0) {
        NSDictionary *productNode = productsNode[[productId stringValue]];
        if (![productNode isKindOfClass:[NSNull class]]) {
            NSMutableArray *arr = [NSMutableArray arrayWithArray:[[productNode allKeys]sortedArray]];
            [arr removeObject:@"Product"];
            return arr;
        }
    }
    return nil;
}

- (NSDictionary *)productNodeForProductId:(NSNumber*)productId {
    if (![self hasValidPayload])
        return nil;
    
    NSDictionary *productsNode = _flbData[@"Products"];
    
    if (productsNode && ![productsNode isKindOfClass:[NSArray class]] && [productsNode allKeys].count > 0) {
        NSDictionary *productNode = productsNode[[productId stringValue]];
        if (![productNode isKindOfClass:[NSNull class]])
            return [productNode objectForKey:@"Product"];
    }
    return nil;
}


- (NSString *)priceForProductId:(NSNumber *)productId inStore:(NSString *)storeId {
    if ([productId integerValue] == 0)
        return nil;

    if (![self hasValidPayload])
        return @"N/A";

    NSDictionary *productsNode = _flbData[@"Products"];
    NSDictionary *productNode = productsNode[productId];
    if (!productNode)
        productNode = productsNode[[productId stringValue]];  //this is hacky... but sometimes the damn parser thinks the keys are strings
    if (productNode && ![productNode isKindOfClass:[NSNull class]] && [productNode allKeys].count > 0) {
        NSDictionary *storePriceData = productNode[storeId];
        if (![storePriceData isKindOfClass:[NSNull class]]) {
            NSString *total =  storePriceData[@"FinalPrice"];
            BOOL isArtificial = [storePriceData[@"HasArtificialPrice"] boolValue];
            return [NSString stringWithFormat:@"$%.2lf%@", [total doubleValue], (isArtificial) ? @"*" : @""];
        }
    }
    return @"NC";
}

- (BOOL)hasSubstitutes {
    BOOL __block hasSubstitutes = NO;

    if (![self hasValidPayload])
        return NO;

    NSDictionary *productsNode = _flbData[@"Products"];
    [productsNode enumerateKeysAndObjectsUsingBlock:^(id key, id products, BOOL *stop1) {
        if ([products isKindOfClass:[NSDictionary class]]) {
            NSDictionary *dict = (NSDictionary *)products;
            [dict enumerateKeysAndObjectsUsingBlock:^(id k, NSDictionary *detail, BOOL *stop2) {
                if ([detail[@"HasArtificialPrice"] boolValue]) {
                    hasSubstitutes = YES;
                    *stop2 = YES;
                    *stop1 = YES;
                }
            }];
        }
    }];
    return hasSubstitutes;
}

- (BOOL)isWinnerForProductId:(NSNumber *)productId inStore:(NSString *)storeId {
    if (![self hasValidPayload])
        return NO;

    NSDictionary *productsNode = _flbData[@"Products"];
    NSDictionary *productNode = productsNode[[productId stringValue]];
    if (productNode) {
        NSDictionary *storePriceData = productNode[storeId];
        if (![storePriceData isKindOfClass:[NSNull class]]) {
            NSNumber *isWinner = [storePriceData objectForKey:@"IsWinner"];
            return [isWinner boolValue];
        }
    }
    return NO;
}

#pragma mark - Summary

- (NSArray *)summaryStores {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *summaryNode = _flbData[@"Summary"];
    return [[summaryNode allKeys]sortedArray];
}

- (NSString *)summaryWinner {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *summaryNode = _flbData[@"Summary"];
    NSString __block *winner;
    [summaryNode enumerateKeysAndObjectsUsingBlock:^(id key, NSDictionary *obj, BOOL *stop) {
        if ([(NSNumber *)obj[@"IsWinner"] isEqual:@1]) {
            winner = key;
            *stop = YES;
        }
    }];
    return winner;
}

- (NSString *)savingsDelta {
    if (![self hasValidPayload])
        return 0;

    NSMutableArray *prices = [NSMutableArray new];
    NSDictionary *summaryNode = _flbData[@"Summary"];
    [summaryNode enumerateKeysAndObjectsUsingBlock:^(id key, NSDictionary *obj, BOOL *stop) {
        NSNumber *temp = [NSNumber numberWithDouble:[SF(@"%@", obj[@"FinalPrice"])doubleValue]];
        [prices addObject:temp];
    }];
    NSArray *sorted = [prices sortedArray];
    double diff =  [sorted[sorted.count-1] doubleValue] - [sorted[0] doubleValue];
    return [NSString stringWithFormat:@"$%.2lf", diff];
}

- (NSString *)summaryPriceForStore:(NSString *)storeId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *summaryNode = _flbData[@"Summary"];
    NSDictionary *storeSummary = summaryNode[storeId];
    if (![storeSummary isKindOfClass:[NSNull class]]) {
        NSString *total =  storeSummary[@"FinalPrice"];
        return [NSString stringWithFormat:@"$%.2lf", [total doubleValue]];
    }
    return @"N/A";
}

#pragma mark - Coupons and Deals

- (NSDictionary *)saleDataForProductId:(NSNumber *)productId atStoreId:(NSString *)storeId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *productsNode = _flbData[@"Products"];
    NSDictionary *productNode = productsNode[[productId stringValue]];
    if (!productNode)
        productNode = productsNode[[productId stringValue]];  //this is hacky... but sometimes the damn parser thinks the keys are strings
    if (productNode && ![productNode isKindOfClass:[NSNull class]] && [productNode allKeys].count > 0) {
        NSDictionary *storePriceData = productNode[storeId];
        if (![storePriceData isKindOfClass:[NSNull class]]) {
            if ([storePriceData[@"SaleDetails"] isKindOfClass:[NSNull class]])
                return nil;

            NSMutableDictionary *saleDetails =  [NSMutableDictionary dictionaryWithDictionary:storePriceData[@"SaleDetails"]];
            if (storePriceData[@"SalePrice"])
                [saleDetails setObject:storePriceData[@"SalePrice"] forKey:@"SalePrice"];
            if (storePriceData[@"BasePrice"])
                [saleDetails setObject:storePriceData[@"BasePrice"] forKey:@"BasePrice"];
            if (storePriceData[@"MaxSavings"])
                [saleDetails setObject:storePriceData[@"MaxSavings"] forKey:@"MaxSavings"];
            return saleDetails;
        }
    }

    return nil;
}

- (NSArray *)couponsDataForProductId:(NSNumber *)productId atStoreId:(NSString *)storeId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *productsNode = _flbData[@"Products"];
    NSDictionary *productNode = productsNode[[productId stringValue]];
    if (!productNode)
        productNode = productsNode[[productId stringValue]];  //this is hacky... but sometimes the damn parser thinks the keys are strings
    if (productNode && ![productNode isKindOfClass:[NSNull class]] && [productNode allKeys].count > 0) {
        NSDictionary *storePriceData = productNode[storeId];
        if (![storePriceData isKindOfClass:[NSNull class]]) {
            if ([storePriceData[@"CouponDetails"] isKindOfClass:[NSNull class]])
                return nil;
            NSArray *couponData = storePriceData[@"CouponDetails"];

            return (couponData && couponData.count > 0) ? couponData : nil;
        }
    }

    return nil;
}

#pragma mark - Deals

//- (IKOProduct *)productFor:(NSNumber *)productId {
//    if (![self hasValidPayload])
//        return nil;
//    
//    NSDictionary *productsNode = _flbData[@"Products"];
//    
//    if (productsNode && ![productsNode isKindOfClass:[NSArray class]] && [productsNode allKeys].count > 0) {
//        NSDictionary *productNode = productsNode[[productId stringValue]];
//        if (![productNode isKindOfClass:[NSNull class]]) {
//            return [IKOProduct objectFromJSON:productNode[@"Product"]];
//        }
//    }
//    return nil;
//}

- (NSArray *)categoriesForDeals {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *categoriesNode = _flbData[@"Categories"];
    return [categoriesNode allKeys];
}

- (NSArray *)productsForDealsCategory:(NSNumber *)categoryId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *categoriesNode = _flbData[@"Categories"];
    NSDictionary *category = categoriesNode[categoryId];
    NSMutableArray *products = [NSMutableArray new];
    
    [category enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, NSDictionary*  _Nonnull obj, BOOL * _Nonnull stop) {
        NSDictionary *productNode = obj[@"Product"];
        if (![productNode isKindOfClass:[NSNull class]]) {
            [products addObject:[IKOProduct objectFromJSON:productNode]];
        }
    }];
    
    return products;
}

- (NSDictionary *)productNodeForCategoryId:(NSNumber *)categoryId andProductId:(NSNumber *)productId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *categoriesNode = _flbData[@"Categories"];
    NSDictionary *category = categoriesNode[categoryId];
    if (!category)
        category = categoriesNode[[categoryId stringValue]];
    return category[SF(@"%@",productId)];
}

- (NSString *)maxSavingsFor:(NSNumber *)productId atStoreId:(NSString *)storeId {
    if (![self hasValidPayload])
        return nil;

    NSDictionary *productsNode = _flbData[@"Products"];
    NSDictionary *productNode = productsNode[productId];
    if (!productNode)
        productNode = productsNode[[productId stringValue]];  //this is hacky... but sometimes the damn parser thinks the keys are strings
    if (productNode && ![productNode isKindOfClass:[NSNull class]] && [productNode allKeys].count > 0) {
        NSDictionary *storePriceData = productNode[storeId];
        if (![storePriceData isKindOfClass:[NSNull class]]) {
            if (storePriceData[@"MaxSavings"])
                return storePriceData[@"MaxSavings"];
        }
    }

    return nil;
}

@end
