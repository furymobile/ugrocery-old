//
//  IKOProductCategoryDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOProductCategory.h"
#import "IKOProductCategoryJoin.h"

extern NSString *const productCategoryDbName;
extern NSString *const productCategoryJoinsDbName;

@interface IKOProductCategoryDataStore : IKODataStore

- (IKOProductCategory *)getProductCategory:(NSNumber *)productCategoryId;
- (IKOProductCategory *)getL1ForProductCategoryId:(NSNumber *)categoryId;
- (IKOProductCategory *)categoryFromTerm:(NSString *)term;
- (NSArray *)categoriesForLevel:(NSNumber *)level;
- (NSArray *)childrentForCategory:(NSNumber *)productCategoryId;

- (void)seedCategories:(IKOSeedType)seedType completion:(void (^)())completionBlock;

@end
