//
//  IKODataStorageManager.h
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager.h"

@implementation IKODataStorageManager
{
    NSMutableDictionary *_services;
}

+ (IKODataStorageManager *)sharedInstance {
    static IKODataStorageManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc]init];
    });
    return sharedInstance;
}

- (id)init {
    self = [super init];
    if (self) {
        _services = [NSMutableDictionary new];
    }
    return self;
}

- (instancetype)storeForClass:(Class)class {
    id <IKODataStoreProtocol> service = [_services objectForKey:NSStringFromClass(class)];
    return service;
}

- (void)addService:(id <IKODataStoreProtocol>)service {
    [_services setObject:service forKey:NSStringFromClass([service targetClass])];
    if ([service respondsToSelector:@selector(openDatabase)]) {
        [service openDatabase];
    }
}

- (void)removeServiceForClass:(Class)class {
    id <IKODataStoreProtocol> service = [self storeForClass:class];
    [_services removeObjectForKey:NSStringFromClass(class)];
    if ([service respondsToSelector:@selector(closeDatabase)]) {
        [service closeDatabase];
    }
}

- (void)closeDatabases {
    @synchronized(self) {
        DDLogVerbose(@"Closing Databases");
        for (id <IKODataStoreProtocol> service in _services) {
            if ([service respondsToSelector:@selector(closeDatabase)]) {
                [service closeDatabase];
            }
        }
    }
}

- (void)flushDatabases {
    @synchronized(self) {
        DDLogVerbose(@"Closing Databases");
        for (id <IKODataStoreProtocol> service in _services) {
            if ([service respondsToSelector:@selector(flush)]) {
                [service flush];
            }
        }
    }
}

@end
