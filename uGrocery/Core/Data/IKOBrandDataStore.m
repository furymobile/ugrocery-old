//
//  IKOBrandDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "NSData+Base64.h"
#import "NSArray+Additions.h"
#import "NSString+Additions.h"
#import "IKOCachedArray.h"
#import "IKOSDK.h"

NSString *const brandDbName = @"ugbdb.tct";

@interface IKOBrandDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKOBrandDataStore

- (Class)targetClass {
    return [IKOBrand class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:brandDbName];
    [super seedDatabaseNamed:brandDbName];
    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumNumberValue];
    [_database setType:HeliumNumberValue forColumn:@"StoreBrand"];

    if (![self lastUpdatedTimestamp]) {
        NSString *path = [[NSBundle mainBundle] pathForResource:@"Seed" ofType:@"plist"];
        NSDictionary *dict = [[NSDictionary alloc] initWithContentsOfFile:path];
        [self updateTimestamp:[dict objectForKey:@"DATA_SEED"]];
    }
    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveBrand:(IKOBrand *)brand {
    BOOL saved = [_database setObject:[brand JSONRepresentation] forKey:brand.brandId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removeBrand:(NSString *)brandId {
    id obj = [_database objectForKey:brandId];

    BOOL removed = [_database removeObjectForKey:obj];
    [self scheduleFlush];
    return removed;
}

- (IKOBrand *)getBrand:(NSNumber *)brandId {
    return [IKOBrand objectFromJSON:[_database objectForKey:brandId]];
}

- (IKOBrand *)brandFromTerm:(NSString *)term {
    IKOBrand *brand = [[self allObjects] firstElementMatchingBlock:^BOOL (IKOBrand *s) {
        return [[[s.name uppercaseString] stripPunctuation] isEqualToString:[[term uppercaseString] stripPunctuation]];
    }];
    return brand;
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    [query order:HeliumQueryAscendingOrder byColumn:@"Name"];
    NSArray *brandIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOBrand objectFromJSON:[self.database objectForKey:brandIds[index]]];
    }
           getCount:^{
        return brandIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)seedBrands:(IKOSeedType)seedType completion:(void (^)())completionBlock {
    IKOSearchDataStore *searchDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOSearchItem class]];
    
    NSString *timestamp = (seedType == IKOSeedTypeIncremental) ? [self lastUpdatedTimestamp] : nil;

    [[IKOSDK new] fetchBulkData:[super nameForSeedType:seedType] type:@"brands" timestamp:timestamp callback:^(IKOError *error, IKOServerResponse *response) {
        
        if (error.responseErrorDescription) {
            if (completionBlock)
                completionBlock();
        }
        NSArray *jsonArray = [response.payload allValues];
        [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            IKOBrand *brand = [IKOBrand objectFromJSON:obj];
            BOOL brandExists = [self getBrand:brand.brandId] != nil;
            [self saveBrand:brand];
            
            IKOSearchItem *searchItem = nil;
            if (!brandExists) {
                searchItem = [[IKOSearchItem alloc]initWithBrand:brand];
            } else {
                searchItem = [[IKOSearchItem alloc]initWithBrand:brand];
                IKOSearchItem *temp = [searchDataStore getSearchItem:brand.brandId forNonZeroColumn:@"BrandId"];
                searchItem.searchItemId = temp.searchItemId;
            }
            
            [searchDataStore addSearchItem:searchItem];
        }];
        if (completionBlock)
            completionBlock();
       
    }];
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
