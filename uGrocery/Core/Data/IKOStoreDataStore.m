//
//  IKOStoreDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOStoreDataStore.h"
#import "NSArray+Additions.h"
#import "IKOAuthenticationManager.h"

@interface IKOStoreDataStore ()
@property (nonatomic) NSArray *stores;
@property (nonatomic) NSArray *publicStores;
@end

@implementation IKOStoreDataStore

- (Class)targetClass {
    return [IKOStore class];
}

- (BOOL)openDatabase {
    _stores = @[
        [[IKOStore alloc]initWithId:@2 andName:@"King Soopers"],
        [[IKOStore alloc]initWithId:@4 andName:@"Safeway"],
        [[IKOStore alloc]initWithId:@5 andName:@"Target"],
        [[IKOStore alloc]initWithId:@7 andName:@"Walmart"],
        [[IKOStore alloc]initWithId:@6 andName:@"Walgreens"],
        [[IKOStore alloc]initWithId:@8 andName:@"Whole Foods"],
        [[IKOStore alloc]initWithId:@10 andName:@"Natural Grocers"],
        [[IKOStore alloc]initWithId:@19 andName:@"Sprouts"],
        [[IKOStore alloc]initWithId:@11 andName:@"Harris Teeter"],
        [[IKOStore alloc]initWithId:@16 andName:@"Fry's"],
        [[IKOStore alloc]initWithId:@17 andName:@"Safeway - AZ"],
        [[IKOStore alloc]initWithId:@18 andName:@"Walmart - AZ"],
    ];
    _publicStores = @[
        [[IKOStore alloc]initWithId:@2 andName:@"King Soopers"],
        [[IKOStore alloc]initWithId:@4 andName:@"Safeway"],
        [[IKOStore alloc]initWithId:@5 andName:@"Target"],
        [[IKOStore alloc]initWithId:@7 andName:@"Walmart"],
        [[IKOStore alloc]initWithId:@6 andName:@"Walgreens"],
        [[IKOStore alloc]initWithId:@8 andName:@"Whole Foods"],
        [[IKOStore alloc]initWithId:@10 andName:@"Natural Grocers"],
        [[IKOStore alloc]initWithId:@19 andName:@"Sprouts"]
    ];
    return YES;
}

- (void)closeDatabase {
    _stores = nil;
}

- (NSArray *)allObjects {
    if ([[IKOAuthenticationManager sharedInstance]isUpdater]) {
        return _stores;
    } else {
        return _publicStores;
    }
}

+ (NSArray *)allStoreIds {
    return @[@"2",@"4",@"5",@"7"];
}

- (NSString *)storeNameFor:(NSNumber *)storeId {
    IKOStore *store = [_stores firstElementMatchingBlock:^BOOL (IKOStore *s) {
        return [s.storeId isEqualToNumber:storeId];
    }];
    return store.name;
}

- (UIImage *)storeImageFor:(NSNumber *)storeId {
    if ([storeId isEqualToNumber:@2]) {
        return [UIImage imageNamed:@"logo-kingsoopers"];
    } else if ([storeId isEqualToNumber:@4]) {
        return [UIImage imageNamed:@"logo-safeway"];
    } else if ([storeId isEqualToNumber:@5]) {
        return [UIImage imageNamed:@"logo-target"];
    } else if ([storeId isEqualToNumber:@7]) {
        return [UIImage imageNamed:@"logo-walmart"];
    }
    return nil;
}

- (UIImage *)largerStoreImageFor:(NSNumber *)storeId {
    if ([storeId isEqualToNumber:@2]) {
        return [UIImage imageNamed:@"logo-kingsoopers-large"];
    } else if ([storeId isEqualToNumber:@4]) {
        return [UIImage imageNamed:@"logo-safeway-large"];
    } else if ([storeId isEqualToNumber:@5]) {
        return [UIImage imageNamed:@"logo-target-large"];
    } else if ([storeId isEqualToNumber:@7]) {
        return [UIImage imageNamed:@"logo-walmart-large"];
    }
    return nil;
}

@end
