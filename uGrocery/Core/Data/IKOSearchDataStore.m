//
//  IKOSearchDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSearchDataStore.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"
#import "NSArray+Additions.h"
#import "NSString+Additions.h"

NSString *const searchDbName = @"ugschdb.tct";

@interface IKOSearchDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKOSearchDataStore

- (Class)targetClass {
    return [IKOSearchItem class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:searchDbName];
    [super seedDatabaseNamed:searchDbName];

    [super addSkipBackupAttributeToItemAtPath:dbPath];
    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];
    [_database setType:HeliumNumberValue forColumn:@"BrandId"];
    [_database setType:HeliumNumberValue forColumn:@"CategoryId"];
    [_database setType:HeliumNumberValue forColumn:@"StoreBrand"];

    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"SearchName"];

    [_database optimize];
    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)addSearchItem:(IKOSearchItem *)searchItem {
    if ([searchItem.searchName hasPrefix:@"PLACEHOLDER"])
        return NO;
    BOOL saved = [_database setObject:[searchItem JSONRepresentation] forKey:searchItem.searchItemId];
    [self scheduleFlush];
    return saved;
}

- (NSArray *)search:(NSString *)term {

    term = [[term uppercaseString]stripPunctuation];
    HeliumQuery *query = [_database newQuery];
    [query order:HeliumQueryAscendingOrder byColumn:@"SearchName"];
    [query column:@"SearchName" searchPhrase:term];

    NSArray *searchItemIds = query.allKeys;
    NSMutableArray *results =  [[[IKOCachedArray alloc]
      initWithLoadObject:^(NSUInteger index) {
          NSDictionary *dict = [self.database objectForKey:searchItemIds[index]];
          return [IKOSearchItem objectFromJSON:dict];
    }
      getCount:^{
        return searchItemIds.count;
    }
      cacheSize:40
      context:nil]mutableCopy];
    
    
    NSArray *left = [NSArray new];
    NSArray *right = [NSArray new];

    [results splitWhere:^BOOL (IKOSearchItem *element, NSUInteger idx, BOOL *stop) {
        return [[[element.searchName uppercaseString]stripPunctuation]hasPrefix:term];
    } left:&left right:&right];

    NSMutableArray *array = [NSMutableArray arrayWithArray:right];
    [array addObjectsFromArray:left];
    
    return [array filteredArrayUsingBlock:^BOOL(IKOSearchItem *element) {
        return ![[[element.searchName uppercaseString]stripPunctuation]hasPrefix:@"PLACEHOLDER"];
    }];
}

- (NSArray *)search:(NSString *)term forNonZeroColumn:(NSString *)columnName {
    HeliumQuery *query = [_database newQuery];
    [query order:HeliumQueryAscendingOrder byColumn:@"SearchName"];
    [query column:columnName greaterThan:@0];
    [query column:@"SearchName" searchPhrase:[[term uppercaseString]stripPunctuation]];

    NSArray *searchItemIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOSearchItem objectFromJSON:[self.database objectForKey:searchItemIds[index]]];
    }
           getCount:^{
        return searchItemIds.count;
    }
           cacheSize:40
           context:nil];
}

- (NSArray *)allObjectsForNonZeroColumn:(NSString *)columnName {
    HeliumQuery *query = [_database newQuery];
    [query order:HeliumQueryAscendingOrder byColumn:@"SearchName"];
    [query column:columnName greaterThan:@0];
    NSArray *searchItemIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOSearchItem objectFromJSON:[self.database objectForKey:searchItemIds[index]]];
    }
           getCount:^{
        return searchItemIds.count;
    }
           cacheSize:40
           context:nil];
}

- (IKOSearchItem *)getSearchItem:(NSNumber *)identifier forNonZeroColumn:(NSString *)columnName {
    HeliumQuery *query = [_database newQuery];
    [query column:columnName equals:identifier];
    
    NSArray *searchItemIds = query.allKeys;
    
    NSArray *array = [[IKOCachedArray alloc]
            initWithLoadObject:^(NSUInteger index) {
                return [IKOSearchItem objectFromJSON:[self.database objectForKey:searchItemIds[index]]];
            }
            getCount:^{
                return searchItemIds.count;
            }
            cacheSize:40
            context:nil];
    
    return array.firstObject;
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    NSArray *searchItemIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOSearchItem objectFromJSON:[self.database objectForKey:searchItemIds[index]]];
    }
           getCount:^{
        return searchItemIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
