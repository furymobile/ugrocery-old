//
//  IKOShoppingListDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/15/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "IKOAuthenticationManager.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"
#import "NSString+Additions.h"

NSString *const shoppingListDbName = @"ugpsldb.tct";
NSString *const IKONotificationShoppingListChanged = @"IKONotificationShoppingListChanged";

@interface IKOShoppingListDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKOShoppingListDataStore

- (Class)targetClass {
    return [IKOShoppingListItem class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:shoppingListDbName];

    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];
    [_database setType:HeliumNumberValue forColumn:@"ProductId"];
    [_database setType:HeliumNumberValue forColumn:@"CategoryId"];
    [_database setType:HeliumNumberValue forColumn:@"BrandId"];
    [_database setType:HeliumNumberValue forColumn:@"L1Id"];
    [_database setType:HeliumNumberValue forColumn:@"Quantity"];
    [_database setType:HeliumNumberValue forColumn:@"CrossedOff"];
    [_database setType:HeliumNumberValue forColumn:@"IsAdhoc"];
    [_database setType:HeliumNumberValue forColumn:@"Comparable"];

    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveShoppingListItem:(IKOShoppingListItem **)item {
    NSString *categoryId = SF(@"%@", (*item).categoryId);
    if ([categoryId isEqualToString:@"99998"]) {
        (*item).categoryId = @0;
    }
    if ([(*item).productId intValue] != 0) {
        IKOShoppingListItem *existingItem = [self getShoppingListItemForProductId:(*item).productId];
        if (existingItem) {
            existingItem.quantity = (*item).quantity;
            existingItem.crossedOff = (*item).crossedOff;
            (*item) = existingItem;
        }
    } else {
        IKOShoppingListItem *existingItem = [self getShoppingListItemByDescription:(*item).listItemDescription];
        if (existingItem)
            return YES;
    }

    if (!(*item).l1Id && (*item).categoryId) {
        IKOProductCategoryDataStore *categoryStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
        (*item).l1Id = [categoryStore getL1ForProductCategoryId:(*item).categoryId].productCategoryId;
    }

    (*item).userId = [IKOAuthenticationManager currentUser].userId;
    BOOL saved = [_database setObject:[(*item) JSONRepresentation] forKey:(*item).shoppingListItemId];
    [[NSNotificationCenter defaultCenter]postNotificationName:IKONotificationShoppingListChanged object:self];
    [self scheduleFlush];
    return saved;
}

- (BOOL)deleteList {
    [[NSNotificationCenter defaultCenter]postNotificationName:IKONotificationShoppingListChanged object:self];

    BOOL success = YES;
    for (IKOShoppingListItem *item in [self allObjects]) {
        success &= [self removeShoppingListItem:item];
    }
    return success;
}

- (BOOL)crossOffList:(BOOL)isCrossed {
    BOOL __block saved;
    [[self allObjects]enumerateObjectsUsingBlock:^(IKOShoppingListItem *obj, NSUInteger idx, BOOL *stop) {
        obj.crossedOff = [NSNumber numberWithBool:isCrossed];
        saved &= [self saveShoppingListItem:&obj];
    }];
    [[NSNotificationCenter defaultCenter]postNotificationName:IKONotificationShoppingListChanged object:self];
    return saved;
}

- (BOOL)removeShoppingListItem:(IKOShoppingListItem *)item {
    BOOL removed = [_database removeObjectForKey:item.shoppingListItemId];
    [[NSNotificationCenter defaultCenter]postNotificationName:IKONotificationShoppingListChanged object:self];
    [self scheduleFlush];
    return removed;
}

- (IKOShoppingListItem *)getShoppingListItemByDescription:(NSString *)productDescription {
    HeliumQuery *query = [_database newQuery];
    [query column:@"ItemDescription" includes:productDescription];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    NSArray *ids = query.allKeys;
    IKOCachedArray *array = [[IKOCachedArray alloc]
      initWithLoadObject:^(NSUInteger index) {
        return [IKOShoppingListItem objectFromJSON:[self.database objectForKey:ids[index]]];
    }
      getCount:^{
        return ids.count;
    }
      cacheSize:40
      context:nil];

    if (array)
        return array[0];
    return nil;
}

- (IKOShoppingListItem *)getShoppingListItemForProductId:(NSNumber *)productId {
    HeliumQuery *query = [_database newQuery];
    [query column:@"ProductId" equals:productId];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    NSArray *ids = query.allKeys;
    IKOCachedArray *array = [[IKOCachedArray alloc]
      initWithLoadObject:^(NSUInteger index) {
        return [IKOShoppingListItem objectFromJSON:[self.database objectForKey:ids[index]]];
    }
      getCount:^{
        return ids.count;
    }
      cacheSize:40
      context:nil];

    if (array)
        return array[0];
    return nil;
}

- (BOOL)removeProductFromShoppingList:(IKOProduct *)product {
    return [self removeShoppingListItem:[self getShoppingListItemForProductId:product.productId]];
}

- (BOOL)isOnShoppingList:(NSNumber *)productId {
    return [self getShoppingListItemForProductId:productId] != nil;
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    NSArray *ids = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOShoppingListItem objectFromJSON:[self.database objectForKey:ids[index]]];
    }
           getCount:^{
        return ids.count;
    }
           cacheSize:40
           context:nil];
}

- (NSUInteger)countofUncrossedItems {
    HeliumQuery *query = [_database newQuery];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    [query column:@"CrossedOff" lessThanOrEquals:@0];
    return query.allKeys.count;
}

- (BOOL)hasComparables {
    HeliumQuery *query = [_database newQuery];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    [query column:@"Comparable" greaterThanOrEquals:@1];
    return query.allKeys.count > 0;
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
