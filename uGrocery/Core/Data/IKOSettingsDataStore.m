//
//  IKOSettingsDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"
#import "IKOSettingsDataStore.h"
#import "NSData+Base64.h"
#import "NSEnumerator+Contains.h"

NSString *const settingsDbName = @"ugsdb.tct";

NSString *const kLastDeals = @"com.ugrocery.lastdeals";
NSString *const kLastDealsStore = @"com.ugrocery.lastdealsstore";
NSString *const kLastDealsUpdatedOn = @"com.ugrocery.lastdealsupdatedon";
NSString *const kLastPriceCaptureExpanded = @"com.ugrocery.lastpricecaptureexpanded";
NSString *const kLastTabIndex = @"com.ugrocery.lasttabindex";
NSString *const kLastUsedBrandKey = @"com.ugrocery.lastusedbrandkey";
NSString *const kLastUsedCategoryKey = @"com.ugrocery.lastusedcategorykey";
NSString *const kLastUsedPrice = @"com.ugrocery.lastusedprice";
NSString *const kLastUsedSaleDescription = @"com.ugrocery.lastusedsaledescription";
NSString *const kLastUsedSaleEndDate = @"com.ugrocery.lastusedsaleenddate";
NSString *const kLastUsedSalePrice = @"com.ugrocery.lastusedsaleprice";
NSString *const kLastUsedStoreKey = @"com.ugrocery.lastusedstorekey";
NSString *const kLastUsedUpdatedOn = @"com.ugrocery.lastusedupdatedon";
NSString *const kShowAdvancedPriceCapture = @"com.ugrocery.showadvancedpricecapture";
NSString *const kShowCoupons = @"com.ugrocery.showCoupons";
NSString *const kShowStoreBrands = @"com.ugrocery.showstorebrands";

@interface IKOSettingsDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKOSettingsDataStore

- (Class)targetClass {
    return [IKOObject class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:settingsDbName];

    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];
    [_database setType:HeliumStringValue forColumn:@"data"];
    
//    Uncomment to default store brands to on by default
//    if (![self hasSettingForKey:kShowStoreBrands])
//        [self setObject:@1 forKey:kShowStoreBrands];
    
    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)hasSettingForKey:(NSString *)key {
    return [[_database allKeys]contains:key];
}

- (BOOL)setString:(NSString *)string forKey:(NSString *)key {
    BOOL returnValue = [_database setObject:@{@"data": string } forKey:key];
    [self scheduleFlush];
    return returnValue;
}

- (BOOL)setObject:(id<NSCoding>)object forKey:(NSString *)key {
    NSData *data = [NSKeyedArchiver archivedDataWithRootObject:object];
    BOOL returnValue = [_database setObject:@{@"data": [data base64EncodedString] } forKey:key];
    [self scheduleFlush];
    return returnValue;
}

- (BOOL)setImage:(UIImage *)image forKey:(NSString *)key {
    NSData *data = UIImagePNGRepresentation(image);
    BOOL returnValue = [_database setObject:@{@"data": [data base64EncodedString] } forKey:key];
    [self scheduleFlush];
    return returnValue;
}

- (id)objectForKey:(NSString *)key {
    id obj = [self.database objectForKey:key];

    if (!obj)
        return nil;
    return [NSKeyedUnarchiver unarchiveObjectWithData:[NSData dataFromBase64String:obj[@"data"]]];
}

- (NSString *)stringForKey:(NSString *)key {
    NSDictionary *dict = [self.database objectForKey:key];
    return dict[@"data"];
}

- (UIImage *)imageForKey:(NSString *)key {
    id obj = [self.database objectForKey:key];

    if (!obj)
        return nil;

    return [UIImage imageWithData:[NSData dataFromBase64String:obj[@"data"]]];
}

- (BOOL)removeObjectForKey:(NSString *)key {
    return [_database removeObjectForKey:key];
}

#pragma mark - Flushing


- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
