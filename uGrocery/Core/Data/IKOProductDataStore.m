//
//  IKOProductDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"
#import "NSString+Additions.h"
#import "IKOSDK.h"
#import "IKOAuthenticationManager.h"
#import "IKONetworkManager.h"
#import "IKOPagingInfo.h"
#import "OrderedDictionary.h"

NSString *const productDbName = @"ugpdb.tct";

@interface IKOProductDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKOProductDataStore

- (Class)targetClass {
    return [IKOProduct class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:productDbName];

    [super addSkipBackupAttributeToItemAtPath:dbPath];
    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    // [_database optimize];
    [_database setType:HeliumNumberValue forColumn:@"BrandId"];
    [_database setType:HeliumNumberValue forColumn:@"CategoryId"];
    [_database setType:HeliumNumberValue forColumn:@"Comparable"];
    [_database setType:HeliumNumberValue forColumn:@"Id"];
    [_database setType:HeliumNumberValue forColumn:@"StoreBrand"];
    [_database setType:HeliumStringValue forColumn:@"SearchName"];

    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"BrandId"];
    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"CategoryId"];
    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"Id"];
    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"SearchName"];
    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"Upc"];
    [_database configureIndex:HeliumTableCreateLexicalIndex | HeliumTableKeepIndex forColumn:@"UpcE"];

    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveProduct:(IKOProduct *)product {
    if (!product)
        return NO;
    BOOL saved = [_database setObject:[product JSONRepresentation] forKey:product.productId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removeProduct:(NSString *)productId {
    id obj = [_database objectForKey:productId];

    BOOL removed = [_database removeObjectForKey:obj];
    [self scheduleFlush];
    return removed;
}

- (IKOProduct *)getProduct:(NSNumber *)productId {
    return [IKOProduct objectFromJSON:[_database objectForKey:productId]];
}

- (void)getProductForUPC:(NSString *)upcCode callback:(void (^)(IKOError *error, IKOProduct *product, IKOServerResponse *response))completion {
    if (upcCode.length == 13) {
        upcCode = [upcCode substringFromIndex:1];
    }
    if (![IKONetworkManager connectedToNetwork]){
        IKOProduct *prod = [self getOfflineProductForUPC:upcCode];
        completion(nil, prod , nil);
    }
    
    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc] initWithUpc:upcCode];
    payload.flbPath =  @"IKOBarcodeScanner";
    [[IKOSDK new] findLowestBillsUpcDetailWithPayload:[payload JSONRepresentation] storeIds:[IKOStoreDataStore allStoreIds] user:[IKOAuthenticationManager currentUser] callback:^(IKOError *e, IKOServerResponse *response) {
        if (e)
            completion(e,nil,nil);
        
        IKOProduct *p = nil;
        NSDictionary *products = response.payload[@"Products"];
        if (products && products[[products allKeys][0]]) {
            NSDictionary *pdict = products[[products allKeys][0]];
            if (![[NSNull null] isEqual:pdict]){
                NSDictionary *pvalue = [pdict objectForKey:@"Product"];
                if (pvalue)
                    p = [IKOProduct objectFromJSON:pvalue];
            }
        }
        [self saveProduct:p];
        completion(nil, p ?: [self getOfflineProductForUPC:upcCode], response);
    }];
}

- (IKOProduct *)getOfflineProductForUPC:(NSString *)upcCode {
    NSString *queryColumn = @"Upc";
    if (upcCode.length == 13) {
        upcCode = [upcCode substringFromIndex:1];
    } else {
        queryColumn = @"UpcE";
    }
    // upcCode = @"4610000764";
    HeliumQuery *query = [_database newQuery];
    [query column:queryColumn includes:upcCode];
    NSArray *productIds = query.allKeys;

    if (productIds.count > 0) {
        return [self getProduct:productIds[0]];
    }
    return nil;
}

- (void)allProductsWithPagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion {
    
    if (![IKONetworkManager connectedToNetwork]){
        NSArray *prods = [self getOfflineProductsForCategory:0];
        completion(nil, prods , nil);
        return;
    }
    
    IKOSettingsDataStore *settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    
    BOOL showStoreBrands = [[settingsDataStore objectForKey:kShowStoreBrands] boolValue];
    BOOL withCoupons = [[settingsDataStore objectForKey:kShowCoupons] boolValue];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[IKOSDK new] searchProducts:@"" storeIds:[IKOStoreDataStore allStoreIds] l1CatIds:@[] l3CatIds:@[] brandIds:@[] withCoupons:withCoupons withStoreBrands:showStoreBrands page:pagingInfo.currentPage limit:pagingInfo.limit user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
            if (error.responseErrorDescription) {
                completion(error,nil,nil);
                return;
            }
            
            IKOProduct __block *p = nil;
            NSMutableArray *prods = [NSMutableArray new];
            NSDictionary *products = response.payload[@"Products"];
            NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
            
            [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *pp = products[productId][@"Product"];
                if (pp) {
                    p = [IKOProduct objectFromJSON:pp];
                    [self maxSavingsForProduct:p inStores:products[productId]];
                }
                [self saveProduct:p];
                [prods addObject:p];
            }];
            
            
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, prods , response);
                });
            }
            
        }];
    });
    
}

- (void)productsForL1:(NSNumber *)categoryId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion {
    
    if (![IKONetworkManager connectedToNetwork]){
        NSArray *prods = [self getOfflineProductsForCategory:categoryId];
        completion(nil, prods , nil);
        return;
    }
    
    IKOSettingsDataStore *settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    
    BOOL showStoreBrands = [[settingsDataStore objectForKey:kShowStoreBrands] boolValue];
    BOOL withCoupons = [[settingsDataStore objectForKey:kShowCoupons] boolValue];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
        [[IKOSDK new] searchProducts:@"" storeIds:[IKOStoreDataStore allStoreIds] l1CatIds:@[categoryId] l3CatIds:@[] brandIds:@[] withCoupons:withCoupons withStoreBrands:showStoreBrands page:pagingInfo.currentPage limit:pagingInfo.limit user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
            if (error.responseErrorDescription) {
                completion(error,nil,nil);
                return;
            }
            
            IKOProduct __block *p = nil;
            NSMutableArray *prods = [NSMutableArray new];
            NSDictionary *products = response.payload[@"Products"];
            NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
            
            [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
                NSDictionary *pp = products[productId][@"Product"];
                if (pp) {
                    p = [IKOProduct objectFromJSON:pp];
                    [self maxSavingsForProduct:p inStores:products[productId]];
                }
                [self saveProduct:p];
                [prods addObject:p];
            }];
            
            
            if (completion) {
                dispatch_async(dispatch_get_main_queue(), ^{
                    completion(nil, prods , response);
                });
            }
            
        }];
    });
    
}

- (void)productsForCategory:(NSNumber *)categoryId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion {

    if (![IKONetworkManager connectedToNetwork]){
        NSArray *prods = [self getOfflineProductsForCategory:categoryId];
        completion(nil, prods , nil);
        return;
    }
    
    IKOSettingsDataStore *settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    
    BOOL showStoreBrands = [[settingsDataStore objectForKey:kShowStoreBrands] boolValue];
    BOOL withCoupons = [[settingsDataStore objectForKey:kShowCoupons] boolValue];
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
        
    [[IKOSDK new] searchProducts:@"" storeIds:[IKOStoreDataStore allStoreIds] l1CatIds:@[] l3CatIds:@[categoryId] brandIds:@[] withCoupons:withCoupons withStoreBrands:showStoreBrands page:pagingInfo.currentPage limit:pagingInfo.limit user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
        if (error.responseErrorDescription) {
            completion(error,nil,nil);
            return;
        }
        
        IKOProduct __block *p = nil;
        NSMutableArray *prods = [NSMutableArray new];
        NSDictionary *products = response.payload[@"Products"];
        NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
        
        [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *pp = products[productId][@"Product"];
            if (pp) {
                p = [IKOProduct objectFromJSON:pp];
                [self maxSavingsForProduct:p inStores:products[productId]];
            }
            [self saveProduct:p];
            if (p)
                [prods addObject:p];
        }];
        

        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, prods , response);
            });
        }
        
    }];
    });

}

- (NSArray *)getOfflineProductsForCategory:(NSNumber *)categoryId {
    HeliumQuery *query = [_database newQuery];
    [query column:@"CategoryId" equals:categoryId];
    [query column:@"HasPrices" equals:@1];
    [query order:HeliumQueryDescendingOrder byColumn:@"HaveImage"];
    NSArray *productIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProduct objectFromJSON:[self.database objectForKey:productIds[index]]];
    }
           getCount:^{
        return productIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)productsForBrand:(NSNumber *)brandId pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion {
    
    if (![IKONetworkManager connectedToNetwork]){
        NSArray *prods = [self getOfflineProductsForBrand:brandId];
        completion(nil, prods , nil);
        return;
    }
    
    IKOSettingsDataStore *settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    
    BOOL showStoreBrands = [[settingsDataStore objectForKey:kShowStoreBrands] boolValue];
    BOOL withCoupons = [[settingsDataStore objectForKey:kShowCoupons] boolValue];
    
     dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
         
    [[IKOSDK new] searchProducts:@"" storeIds:[IKOStoreDataStore allStoreIds] l1CatIds:@[] l3CatIds:@[] brandIds:@[brandId] withCoupons:withCoupons withStoreBrands:showStoreBrands page:pagingInfo.currentPage limit:pagingInfo.limit user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
        if (error.responseErrorDescription) {
            completion(error,nil,nil);
            return;
        }
        
        IKOProduct __block *p = nil;
        NSMutableArray *prods = [NSMutableArray new];
        NSDictionary *products = response.payload[@"Products"];
        NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
        
        [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *pp = products[productId][@"Product"];
            if (pp) {
                p = [IKOProduct objectFromJSON:pp];
                [self maxSavingsForProduct:p inStores:products[productId]];
            }
            [self saveProduct:p];
            [prods addObject:p];
        }];
        
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, prods , response);
            });
        }
        
    }];
     });

    
}

- (void)maxSavingsForProduct:(IKOProduct *)product inStores:(NSDictionary*)stores {
    [stores enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, NSDictionary*  _Nonnull obj, BOOL * _Nonnull stop) {
            if (![key isEqualToString:@"Product"]) {
                if (( obj[@"IsWinner"] && [obj[@"IsWinner"] boolValue] && [obj[@"MaxSavings"] isEqualToNumber:@0] ) || [obj[@"SaleDetails"] isKindOfClass:[NSDictionary class]]) {
                    product.maxSavings = obj[@"MaxSavings"];
                    product.hasCoupons = obj[@"HasCoupons"];
                    product.winningStore = [NSNumber numberWithInteger:[key integerValue]];
                    product.isOnSale = obj[@"IsOnSale"];
                    *stop = YES;
                }
            }
    }];
}

- (NSArray *)getOfflineProductsForBrand:(NSNumber *)brandId {
    HeliumQuery *query = [_database newQuery];
    [query column:@"BrandId" equals:brandId];
    [query column:@"HasPrices" equals:@1];
    [query order:HeliumQueryDescendingOrder byColumn:@"HaveImage"];
    NSArray *productIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProduct objectFromJSON:[self.database objectForKey:productIds[index]]];
    }
           getCount:^{
        return productIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)searchProducts:(NSString *)term pagingInfo:(IKOPagingInfo *)pagingInfo callback:(void (^)(IKOError *error, NSArray *products, IKOServerResponse *response))completion {
    
    if (![IKONetworkManager connectedToNetwork]){
        NSArray *prods = [self searchProductsOffline:term];
        completion(nil, prods , nil);
        return;
    }
    
    IKOSettingsDataStore *settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    
    BOOL showStoreBrands = [[settingsDataStore objectForKey:kShowStoreBrands] boolValue];
    BOOL withCoupons = [[settingsDataStore objectForKey:kShowCoupons] boolValue];
    
    WEAK_SELF(weakSelf);
    
    
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
    
    [[IKOSDK new] searchProducts:term storeIds:[IKOStoreDataStore allStoreIds] l1CatIds:@[] l3CatIds:@[] brandIds:@[] withCoupons:withCoupons withStoreBrands:showStoreBrands page:pagingInfo.currentPage limit:pagingInfo.limit user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
        if (error.responseErrorDescription) {
            completion(error,nil,nil);
            return;
        }
        
        IKOProduct __block *p = nil;
        NSMutableArray *prods = [NSMutableArray new];
        NSDictionary *products = response.payload[@"Products"];
        NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
        
        [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *pp = products[productId][@"Product"];
            if (pp) {
                p = [IKOProduct objectFromJSON:pp];
                [weakSelf maxSavingsForProduct:p inStores:products[productId]];
            }
            [weakSelf saveProduct:p];
            [prods addObject:p];
        }];
        
        
        if (completion) {
            dispatch_async(dispatch_get_main_queue(), ^{
                completion(nil, prods , response);
            });
        }
        
    }];
});
}

- (NSArray *)searchProductsOffline:(NSString *)term {
    if (!term || term.length ==0)
        return nil;

    HeliumQuery *query = [_database newQuery];
    NSArray *searchTokens = [[term uppercaseString]arrayBySplittingIntoWords];
    [query column:@"HasPrices" equals:@1];
    [query column:@"SearchName" searchCompoundExpression:[searchTokens componentsJoinedByString:@" " ]];

    NSArray *productIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProduct objectFromJSON:[self.database objectForKey:productIds[index]]];
    }
           getCount:^{
        return productIds.count;
    }
           cacheSize:40
           context:nil];
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    NSArray *productIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProduct objectFromJSON:[self.database objectForKey:productIds[index]]];
    }
           getCount:^{
        return productIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)seedProducts:(void (^)())completionBlock {
    
   
    [[IKOSDK new] fetchBulkData:@"admin" type:@"products" timestamp:nil callback:^(IKOError *error, IKOServerResponse *response) {
            if (error.responseErrorDescription) {
                if (completionBlock)
                    completionBlock();
            }
     
    
             NSArray *jsonArray = [response.payload allValues];
        
            if (jsonArray.count == 0)
                completionBlock();
                
            [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
                IKOProduct *product = [IKOProduct objectFromJSON:obj];
                [self saveProduct:product];
            }];
            
            
  
        }];
        

    
}

- (void)scheduleFlush {
    [self.database flush];
}

@end
