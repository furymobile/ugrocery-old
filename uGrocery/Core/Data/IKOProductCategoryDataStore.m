//
//  IKOProductCategoryDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 7/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "NSArray+Additions.h"
#import "NSString+Additions.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"
#import "IKOSDK.h"

NSString *const productCategoryDbName = @"ugpcdb.tct";
NSString *const productCategoryJoinsDbName = @"ugpcjdb.tct";

@interface IKOProductCategoryDataStore ()

@property (nonatomic, readonly) HeliumTable *database;
@property (nonatomic, readonly) HeliumTable *joinsDatabase;

@end

@implementation IKOProductCategoryDataStore

- (Class)targetClass {
    return [IKOProductCategory class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:productCategoryDbName];
    NSString *joinsDbPath = [[NSSearchPathForDirectoriesInDomains(NSCachesDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:productCategoryJoinsDbName];

    [super seedDatabaseNamed:productCategoryDbName];
    [super seedDatabaseNamed:productCategoryJoinsDbName];

    [super addSkipBackupAttributeToItemAtPath:dbPath];
    [super addSkipBackupAttributeToItemAtPath:joinsDbPath];

    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    _joinsDatabase = [[HeliumTable alloc]initWithPath:joinsDbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", joinsDbPath, error);
        return NO;
    }
    DDLogVerbose(@"Databases Opened");

    [_database setDefaultKeyType:HeliumNumberValue];
    [_database setType:HeliumNumberValue forColumn:@"Id"];

    [_joinsDatabase setDefaultKeyType:HeliumStringValue];

    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveProductCategory:(IKOProductCategory *)productCategory {
    BOOL saved = [_database setObject:[productCategory JSONRepresentation] forKey:productCategory.productCategoryId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removeProductCategory:(NSNumber *)productCategoryId {
    id obj = [_database objectForKey:productCategoryId];

    BOOL removed = [_database removeObjectForKey:obj];
    //TODO:  Remove Joins!!!

    [self scheduleFlush];
    return removed;
}

- (IKOProductCategory *)categoryFromTerm:(NSString *)term {
    IKOProductCategory *category = [[self allObjects] firstElementMatchingBlock:^BOOL (IKOProductCategory *s) {
        return [[[s.name uppercaseString] stripPunctuation] isEqualToString:[[term uppercaseString] stripPunctuation]];
    }];
    return ([SF(@"%@",category.level) isEqualToString:@"3"]) ? category : nil;
}

- (IKOProductCategory *)getProductCategory:(NSNumber *)productCategoryId {
    return [IKOProductCategory objectFromJSON:[_database objectForKey:productCategoryId]];
}

- (NSArray *)categoriesForLevel:(NSNumber *)level {
    HeliumQuery *query = [_database newQuery];
    [query column:@"Level" equals:level];
    [query order:HeliumQueryAscendingOrder byColumn:@"SortValue"];
    
    NSArray *categories = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProductCategory objectFromJSON:[self.database objectForKey:categories[index]]];
    }
           getCount:^{
        return categories.count;
    }
           cacheSize:40
           context:nil];
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    NSArray *productCategoryIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProductCategory objectFromJSON:[self.database objectForKey:productCategoryIds[index]]];
    }
           getCount:^{
        return productCategoryIds.count;
    }
           cacheSize:40
           context:nil];
}

- (NSArray *)allJoinObjects {
    HeliumQuery *query = [_joinsDatabase newQuery];
    NSArray *productCategoryJoinIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOProductCategory objectFromJSON:[self.database objectForKey:productCategoryJoinIds[index]]];
    }
           getCount:^{
        return productCategoryJoinIds.count;
    }
           cacheSize:40
           context:nil];
}

- (IKOProductCategory *)getParentForProductCategoryId:(NSNumber *)categoryId {
    HeliumQuery *query = [_joinsDatabase newQuery];
    [query column:@"ChildId" equals:categoryId];
    NSArray *productCategoryJoinIds = query.allKeys;
    if (productCategoryJoinIds.count > 0) {
        IKOProductCategoryJoin *join = [IKOProductCategoryJoin objectFromJSON:[self.joinsDatabase objectForKey:productCategoryJoinIds[0]]];
        return [self getProductCategory:join.parentId];
    }
    return nil;
}

- (NSArray *)childrentForCategory:(NSNumber *)productCategoryId {
    NSMutableArray *results = [NSMutableArray new];
    HeliumQuery *query = [_joinsDatabase newQuery];
    [query column:@"ParentId" equals:productCategoryId];
    NSArray *productCategoryJoinIds = query.allKeys;
    
    for (id key in productCategoryJoinIds) {
        IKOProductCategoryJoin *join = [IKOProductCategoryJoin objectFromJSON:[self.joinsDatabase objectForKey:key]];
        id obj = [self getProductCategory:join.childId];
        if (obj)
            [results addObject:obj];
    }
    return results;
}

- (IKOProductCategory *)getL1ForProductCategoryId:(NSNumber *)categoryId {
    IKOProductCategory *currentCategory;
    IKOProductCategory *lastCategory;

    NSNumber *currentId = categoryId;

    do {
        currentCategory = [self getParentForProductCategoryId:currentId];
        if (currentCategory) {
            lastCategory = currentCategory;
            currentId = lastCategory.productCategoryId;
        }
    } while (currentCategory != nil);

    return lastCategory;
}

- (void)seedCategories:(IKOSeedType)seedType completion:(void (^)())completionBlock {
    IKOSearchDataStore *searchDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOSearchItem class]];
    NSString *timestamp = (seedType == IKOSeedTypeIncremental) ? [self lastUpdatedTimestamp] : nil;
    [[IKOSDK new] fetchBulkData:[super nameForSeedType:seedType] type:@"categories" timestamp:timestamp callback:^(IKOError *error, IKOServerResponse *response) {
        
        if (error.responseErrorDescription) {
            if (completionBlock)
                completionBlock();
        }
        NSArray *jsonArray = [response.payload allValues];
        [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            IKOProductCategory *productCategory = [IKOProductCategory objectFromJSON:obj];
             BOOL categoryExists = [self getProductCategory:productCategory.productCategoryId] != nil;
            [self saveProductCategory:productCategory];
            
            IKOSearchItem *searchItem = nil;
            if (!categoryExists && [productCategory.level isEqualToNumber:@3]) {
                IKOSearchItem *searchItem = [[IKOSearchItem alloc]initWithProductCategory:productCategory];
                [searchDataStore addSearchItem:searchItem];
            } else if ([productCategory.level isEqualToNumber:@3]) {
                searchItem = [[IKOSearchItem alloc]initWithProductCategory:productCategory];
                IKOSearchItem *temp = [searchDataStore getSearchItem:productCategory.productCategoryId forNonZeroColumn:@"CategoryId"];
                searchItem.searchItemId = temp.searchItemId;
                if (searchItem.searchItemId)
                    [searchDataStore addSearchItem:searchItem];
            }
            
        }];
        
        [self seedCategoryJoins:seedType completion:completionBlock];
    }];
    
}

- (void)seedCategoryJoins:(IKOSeedType)seedType completion:(void (^)())completionBlock {
    NSString *timestamp = (seedType == IKOSeedTypeIncremental) ? [self lastUpdatedTimestamp] : nil;
    
    [[IKOSDK new] fetchBulkData:[super nameForSeedType:seedType] type:@"category_joins" timestamp:timestamp callback:^(IKOError *error, IKOServerResponse *response) {
        
        if (error.responseErrorDescription) {
            if (completionBlock)
                completionBlock();
        }
        NSArray *jsonArray = [response.payload allValues];
        [jsonArray enumerateObjectsUsingBlock:^(NSDictionary *obj, NSUInteger idx, BOOL *stop) {
            IKOProductCategoryJoin *productCategoryJoin = [IKOProductCategoryJoin objectFromJSON:obj];
            productCategoryJoin.uuid = [[NSUUID UUID]UUIDString];
            [_joinsDatabase setObject:[productCategoryJoin JSONRepresentation] forKey:productCategoryJoin.uuid];
            [self scheduleFlush];
        }];
        if (completionBlock)
            completionBlock();

    }];
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
