//
//  IKOShoppingListDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/15/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOShoppingListItem.h"

extern NSString *const IKONotificationShoppingListChanged;

@interface IKOShoppingListDataStore : IKODataStore

- (BOOL)saveShoppingListItem:(IKOShoppingListItem **)item;
- (IKOShoppingListItem *)getShoppingListItemForProductId:(NSNumber *)productId;
- (BOOL)removeShoppingListItem:(IKOShoppingListItem *)item;
- (BOOL)removeProductFromShoppingList:(IKOProduct *)product;
- (BOOL)isOnShoppingList:(NSNumber *)productId;
- (NSArray *)allObjects;
- (BOOL)deleteList;
- (BOOL)crossOffList:(BOOL)isCrossed;
- (NSUInteger)countofUncrossedItems;
- (BOOL)hasComparables;

@end
