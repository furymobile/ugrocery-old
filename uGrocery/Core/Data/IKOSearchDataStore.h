//
//  IKOSearchDataStore.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStore.h"
#import "IKOSearchItem.h"

extern NSString *const searchDbName;

@interface IKOSearchDataStore : IKODataStore

- (BOOL)addSearchItem:(IKOSearchItem *)searchItem;
- (NSArray *)search:(NSString *)term;
- (NSArray *)search:(NSString *)term forNonZeroColumn:(NSString *)columnName;
- (NSArray *)allObjectsForNonZeroColumn:(NSString *)columnName;

- (IKOSearchItem *)getSearchItem:(NSNumber *)identifier forNonZeroColumn:(NSString *)columnName;

@end
