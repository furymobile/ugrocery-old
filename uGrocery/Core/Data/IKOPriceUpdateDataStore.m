//
//  IKOPriceUpdateDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 11/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOPriceUpdateDataStore.h"
#import "NSData+Base64.h"
#import "IKOObject.h"
#import "IKOSDK.h"
#import "IKOCachedArray.h"
#import "FTimer.h"
#import "IKOAuthenticationManager.h"

NSString *const priceUpdateDbName = @"ugpudb.tct";

@interface IKOPriceUpdateDataStore ()

@property (nonatomic, readonly) HeliumTable *database;
@property (nonatomic) FTimer *timer;

@end

@implementation IKOPriceUpdateDataStore

- (Class)targetClass {
    return [IKOPriceUpdate class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:priceUpdateDbName];

    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];

    _timer = [[FTimer alloc]initWithTimeInterval:300000 andBlock:^{
        [self retryResubmissions];
    }];
    [_timer fire];
    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
    [_timer invalidate];
    _timer = nil;
}

#pragma mark - Storage and Retrieval methods

- (void)sendPriceUpdate:(IKOPriceUpdate *)priceUpdate {
    DDLogVerbose(@"Sending Price Submission: %@", [priceUpdate JSONRepresentation]);
    [[IKOSDK new]submitUPC:priceUpdate user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
        if (!response.success) {
            [self savePriceUpdate:priceUpdate];
        }
    }];
}

- (BOOL)savePriceUpdate:(IKOPriceUpdate *)priceUpdate {
    BOOL saved = [_database setObject:[priceUpdate JSONRepresentation] forKey:priceUpdate.priceUpdateId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removePriceUpdate:(NSString *)priceUpdateId {
    BOOL removed = [_database removeObjectForKey:priceUpdateId];
    [self scheduleFlush];
    return removed;
}

- (IKOPriceUpdate *)getPriceUpdate:(NSString *)priceUpdateId {
    return [IKOPriceUpdate objectFromJSON:[_database objectForKey:priceUpdateId]];
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    NSArray *productIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOPriceUpdate objectFromJSON:[self.database objectForKey:productIds[index]]];
    }
           getCount:^{
        return productIds.count;
    }
           cacheSize:40
           context:nil];
}

#pragma mark - Resubmission

- (void)retryResubmissions {
    if ([self allObjects].count == 0)
        return;

    DDLogVerbose(@"Retrying submissions");

    NSUInteger indexKey = 0U;
    NSMutableArray *priceUpdateIdsToDelete = [NSMutableArray new];
    NSMutableDictionary *updateDictionary = [NSMutableDictionary new];

    for (IKOPriceUpdate *priceUpdate in [self allObjects]) {
        // Build the request payload
        [updateDictionary setObject:[priceUpdate submissionPayload] forKey:[NSString stringWithFormat:@"%lu", (unsigned long)indexKey++]];
        [priceUpdateIdsToDelete addObject:priceUpdate.priceUpdateId];
    }

    [[IKOSDK new]submitUPCBulk:updateDictionary callback:^(IKOError *error, IKOServerResponse *response) {
        // Mark the lot as sent on success
        if (response.success) {
            [priceUpdateIdsToDelete enumerateObjectsUsingBlock:^(NSString *priceUpdateId, NSUInteger idx, BOOL *stop) {
                [self removePriceUpdate:priceUpdateId];
            }];
        }
    }];
}

#pragma mark - Flushing

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
