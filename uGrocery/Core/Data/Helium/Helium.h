//
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <Foundation/Foundation.h>
#import "tcadb.h"

@class HeliumQuery;

typedef enum {
    HeliumStringValue,
    HeliumNumberValue,
    HeliumDateValue,
    HeliumUInt64Value,
    HeliumUInt32Value,
    HeliumPlistValue,
    HeliumDataValue,
} HeliumValueType;


extern NSString *const HeliumDomain;

@interface Helium : NSObject {
    TCADB *_db;
}

@property (nonatomic,assign) HeliumValueType defaultKeyType;
@property (nonatomic,assign) HeliumValueType defaultObjectType;
@property (nonatomic) NSError *lastError;

- (BOOL)flush;
- (BOOL)optimize;
- (BOOL)removeAllObjects;
- (BOOL)removeObjectForKey:(id)key;
- (BOOL)setObject:(id)value forKey:(id)key;
- (NSArray *)keysWithPrefix:(id)prefix;
- (NSDictionary *)allObjects;
- (NSEnumerator *)allKeys;
- (NSError *)dbError;
- (NSString *)dbPath;
- (NSUInteger)count;
- (TCADB *)db;
- (id)initWithPath:(NSString *)path error:(NSError **)error;
- (id)objectForKey:(id)key;
- (void)close;

@end

@interface HeliumKeyEnumerator : NSEnumerator {
    Helium *_database;
    HeliumValueType _type;
    BOOL _init;
}

- (id)initWithDatabase:(Helium *)database type:(HeliumValueType)type;

@end

@interface HeliumListArray : NSArray {
    Helium *_database;
    TCLIST *_list;
    HeliumValueType _type;
}

- (id)initWithDatabase:(Helium *)database list:(TCLIST *)list type:(HeliumValueType)type;

@end

typedef enum {
    HeliumTableCreateLexicalIndex = TDBITLEXICAL,
    HeliumTableCreateDecimalIndex = TDBITDECIMAL,
    HeliumTableCreateTokenIndex = TDBITTOKEN,
    HeliumTableCreateQgramIndex = TDBITQGRAM,
    HeliumTableOptimizeIndex = TDBITOPT,
    HeliumTableRemoveIndex = TDBITVOID,
    HeliumTableKeepIndex = TDBITKEEP
} HeliumIndexOperation;


@interface HeliumTable : Helium
{
    NSMutableDictionary *_columnTypes;
}

- (BOOL)configureIndex:(HeliumIndexOperation)indexOperation forColumn:(NSString *)columnName;
- (HeliumQuery *)newQuery;
- (HeliumValueType)typeForColumn:(NSString *)columnName;
- (NSNumber *)generateUniqueID;
- (id)initWithPath:(NSString *)path options:(NSString *)options error:(NSError **)error;
- (void)setType:(HeliumValueType)type forColumn:(NSString *)columnName;

@end

typedef enum {
    HeliumQueryAscendingOrder,
    HeliumQueryDescendingOrder
} HeliumQueryOrder;


@interface HeliumQueryCondition : NSObject

@property (nonatomic, assign) int condition;
@property (nonatomic) NSString *columnName;
@property (nonatomic) NSString *expr;

- (id)initWithColumnName:(NSString *)columnName condition:(int)condition expr:(NSString *)expr;

- (HeliumQueryCondition *)negate;
- (HeliumQueryCondition *)negate:(BOOL)negate;
- (HeliumQueryCondition *)noIndex;

@end

@interface HeliumQuery : NSObject {
    HeliumTable *_database;
    NSMutableArray *_conditions;
    NSString *_orderByColumnName;
    int _orderType;
}

- (HeliumQueryCondition *)column:(NSString *)columnName between:(NSNumber *)number other:(NSNumber *)otherNumber;
- (HeliumQueryCondition *)column:(NSString *)columnName equals:(id)stringOrNumber;
- (HeliumQueryCondition *)column:(NSString *)columnName equalsAny:(NSArray *)stringsOrNumbers;
- (HeliumQueryCondition *)column:(NSString *)columnName greaterThan:(NSNumber *)number;
- (HeliumQueryCondition *)column:(NSString *)columnName greaterThanOrEquals:(NSNumber *)number;
- (HeliumQueryCondition *)column:(NSString *)columnName hasPrefix:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName hasSuffix:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName includes:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName includesAllTokens:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName includesAnyToken:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName lessThan:(NSNumber *)number;
- (HeliumQueryCondition *)column:(NSString *)columnName lessThanOrEquals:(NSNumber *)number;
- (HeliumQueryCondition *)column:(NSString *)columnName matchesRegex:(NSString *)regex;
- (HeliumQueryCondition *)column:(NSString *)columnName searchAllTokens:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName searchAnyToken:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName searchCompoundExpression:(NSString *)string;
- (HeliumQueryCondition *)column:(NSString *)columnName searchPhrase:(NSString *)string;
- (NSArray *)allKeys;
- (NSArray *)keysWithLimit:(int)limit skip:(int)skip;
- (id)firstKey;
- (id)initWithDatabase:(HeliumTable *)database;
- (void)order:(HeliumQueryOrder)order byColumn:(NSString *)columnName;

@end
