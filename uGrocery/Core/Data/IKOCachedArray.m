#import "IKOCachedArray.h"

@interface IKOCachedArray ()

@property (nonatomic) NSCache *cache;

@end

@implementation IKOCachedArray {
    id (^_loadObject)(NSUInteger);
    NSUInteger (^_getCount)();
}

- (id)initWithLoadObject:(id (^)(NSUInteger))loadObject
  getCount:(NSUInteger (^)())getCount
  cacheSize:(NSUInteger)cacheSize
  context:(id)context {
    self = [super init];
    if (self) {
        _loadObject = [loadObject copy];
        _getCount = [getCount copy];
        _cache = [[NSCache alloc]init];
        _cache.countLimit = cacheSize;
        self.context = context;
    }
    return self;
}

- (id)objectAtIndex:(NSUInteger)index {
    NSNumber *key = @(index);
    id obj = [_cache objectForKey:key];
    if (!obj) {
        obj = _loadObject(index);
        if (obj) {
            [_cache setObject:obj forKey:key];
        }
    }
    return obj;
}

- (NSUInteger)count {
    return _getCount();
}

@end
