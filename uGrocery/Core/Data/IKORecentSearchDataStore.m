//
//  IKORecentSearchDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 1/15/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"

NSString *const recentSearchDbName = @"ugrsdb.tct";

@interface IKORecentSearchDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end

@implementation IKORecentSearchDataStore

- (Class)targetClass {
    return [IKORecentSearchItem class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:recentSearchDbName];
    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];
    [_database setType:HeliumNumberValue forColumn:@"BrandId"];
    [_database setType:HeliumNumberValue forColumn:@"CategoryId"];
    [_database setType:HeliumNumberValue forColumn:@"LastSearchDateLong"];


    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveRecentSearchItem:(IKORecentSearchItem *)recentSearchItem {
    NSMutableArray *recentSearchItems = [NSMutableArray arrayWithArray:[self allObjects]];
    if (recentSearchItems.count + 1 == 10) {
        //delete last item
        [self removeRecentSearchItem:[recentSearchItems lastObject]];
    }
    BOOL saved = [_database setObject:[recentSearchItem JSONRepresentation] forKey:recentSearchItem.recentSearchItemId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)updateTimestamp:(IKORecentSearchItem *)recentSearchItem {
    recentSearchItem.lastSearchDateLong = [NSNumber numberWithDouble:[NSDate timeIntervalSinceReferenceDate]];
    BOOL saved = [_database setObject:[recentSearchItem JSONRepresentation] forKey:recentSearchItem.recentSearchItemId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removeRecentSearchItem:(IKORecentSearchItem *)recentSearchItem {
    BOOL removed = [_database removeObjectForKey:recentSearchItem.recentSearchItemId];
    [self scheduleFlush];
    return removed;
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    [query order:HeliumQueryDescendingOrder byColumn:@"LastSearchDateLong"];
    NSArray *searchItemIds = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKORecentSearchItem objectFromJSON:[self.database objectForKey:searchItemIds[index]]];
    }
           getCount:^{
        return searchItemIds.count;
    }
           cacheSize:40
           context:nil];
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
