//
//  IKOMyProductsDataStore.m
//  uGrocery
//
//  Created by Duane Schleen on 8/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "IKOAuthenticationManager.h"
#import "NSData+Base64.h"
#import "IKOCachedArray.h"

NSString *const myProductsDbName = @"ugmpdb.tct";

@interface IKOFavoritesDataStore ()

@property (nonatomic, readonly) HeliumTable *database;

@end


@implementation IKOFavoritesDataStore

- (Class)targetClass {
    return [IKOFavoriteItem class];
}

- (BOOL)openDatabase {
    NSString *dbPath = [[NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES)lastObject]stringByAppendingPathComponent:myProductsDbName];

    NSError *error;
    _database = [[HeliumTable alloc]initWithPath:dbPath options:@"mode=rwc#xmsiz=10485760" error:&error];
    if (error) {
        DDLogError(@"Cannot open database %@: %@", dbPath, error);
        return NO;
    }
    DDLogVerbose(@"Database Opened");

    [_database setDefaultKeyType:HeliumStringValue];
    [_database setType:HeliumNumberValue forColumn:@"ProductId"];
    [_database setType:HeliumNumberValue forColumn:@"L1Id"];

    return YES;
}

- (void)closeDatabase {
    [self.database close];
    _database = nil;
}

#pragma mark - Storage and Retrieval methods

- (BOOL)saveMyProductItem:(IKOFavoriteItem **)item {
    if ((*item).productId != 0) {
        IKOFavoriteItem *existingItem = [self getMyProductItemForProductId:(*item).productId];

        if (existingItem) {
            existingItem.isFavorite = (*item).isFavorite;
            *item = existingItem;
        }
    }

    if (!(*item).l1Id && (*item).categoryId) {
        IKOProductCategoryDataStore *categoryStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
        (*item).l1Id = [categoryStore getL1ForProductCategoryId:(*item).categoryId].productCategoryId;
    }

    (*item).userId = [IKOAuthenticationManager currentUser].userId;
    BOOL saved = [_database setObject:[(*item)JSONRepresentation] forKey:(*item).myProductItemId];
    [self scheduleFlush];
    return saved;
}

- (BOOL)removeMyProductItem:(IKOFavoriteItem *)item {
    BOOL removed = [_database removeObjectForKey:item.myProductItemId];
    [self scheduleFlush];
    return removed;
}

- (IKOFavoriteItem *)getMyProductItemForProductId:(NSNumber *)productId {
    HeliumQuery *query = [_database newQuery];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    [query column:@"ProductId" equals:productId];
    NSArray *ids = query.allKeys;
    IKOCachedArray *array = [[IKOCachedArray alloc]
      initWithLoadObject:^(NSUInteger index) {
        return [IKOFavoriteItem objectFromJSON:[self.database objectForKey:ids[index]]];
    }
      getCount:^{
        return ids.count;
    }
      cacheSize:40
      context:nil];

    if (array)
        return array[0];
    return nil;
}

- (NSArray *)allObjects {
    HeliumQuery *query = [_database newQuery];
    [query column:@"UserId" equals:[IKOAuthenticationManager currentUser].userId];
    [query order:HeliumQueryAscendingOrder byColumn:@"IsFavorite"];
    NSArray *ids = query.allKeys;

    return [[IKOCachedArray alloc]
           initWithLoadObject:^(NSUInteger index) {
        return [IKOFavoriteItem objectFromJSON:[self.database objectForKey:ids[index]]];
    }
           getCount:^{
        return ids.count;
    }
           cacheSize:40
           context:nil];
}

- (void)scheduleFlush {
    // TODO Reduce memory pressure by instead of rescheduling immediately, update local timestamp and reschedule in callback if needed.
    //	[_timer dispose];
    //	_timer = [self.scheduler afterDelay:FlushDelay schedule:^{
    [self.database flush];
    //	}];
}

@end
