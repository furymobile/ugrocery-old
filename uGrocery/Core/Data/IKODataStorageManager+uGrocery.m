//
//  FCStorageManager+CardReader.m
//  card-reader-ios
//
//  Created by Duane Schleen on 3/10/14.
//  Copyright (c) 2014 FullContact. All rights reserved.
//

#import "IKODataStorageManager+uGrocery.h"
#import "IKONetworkManager.h"

NSString *const kStoredBuildNumber = @"kStoredBuildNumber";

@implementation IKODataStorageManager (uGrocery)

+ (void)initialize:(void (^)())completionBlock {
#if TARGET_IPHONE_SIMULATOR
    DDLogInfo(@"Documents Directory: %@", [[[NSFileManager defaultManager]URLsForDirectory:
      NSDocumentDirectory inDomains:NSUserDomainMask]lastObject]);
#endif

    ONCE(^{
        [[self sharedInstance]addService:[IKOSearchDataStore new]];
        [[self sharedInstance]addService:[IKOSettingsDataStore new]];
        [[self sharedInstance]addService:[IKOProductDataStore new]];
        [[self sharedInstance]addService:[IKOProductCategoryDataStore new]];
        [[self sharedInstance]addService:[IKOShoppingListDataStore new]];
        [[self sharedInstance]addService:[IKOBrandDataStore new]];
        [[self sharedInstance]addService:[IKOFavoritesDataStore new]];
        [[self sharedInstance]addService:[IKOStoreDataStore new]];
        [[self sharedInstance]addService:[IKOPriceUpdateDataStore new]];
        [[self sharedInstance]addService:[IKORecentSearchDataStore new]];

#ifdef SEED
        DDLogInfo(@"Starting background L3 and Brand refresh...");
        IKOBrandDataStore *brandDataStore = [[self sharedInstance]storeForClass:[IKOBrand class]];
        [brandDataStore seedBrands:IKOSeedTypePublic completion:^{
            IKOProductCategoryDataStore *productCategoryDataStore = [[self sharedInstance]storeForClass:[IKOProductCategory class]];
            [productCategoryDataStore seedCategories:IKOSeedTypePublic completion:completionBlock];
            DDLogInfo(@"Finished background L3 and Brand refresh...");
            if (completionBlock)
                completionBlock();
        }];
#else
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), ^{
            DDLogInfo(@"Starting incremental background L3 and Brand refresh...");
            IKOBrandDataStore *brandDataStore = [[self sharedInstance]storeForClass:[IKOBrand class]];
            [brandDataStore seedBrands:IKOSeedTypeIncremental completion:^{
                IKOProductCategoryDataStore *productCategoryDataStore = [[self sharedInstance]storeForClass:[IKOProductCategory class]];
                [productCategoryDataStore seedCategories:IKOSeedTypeIncremental completion:completionBlock];
                DDLogInfo(@"Finished incremental background L3 and Brand refresh...");
            }];
        });

#endif



    });
}

@end
