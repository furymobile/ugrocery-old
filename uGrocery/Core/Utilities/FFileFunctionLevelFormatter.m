//
//  FFileFunctionLevelFormatter.m
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "FFileFunctionLevelFormatter.h"

@implementation FFileFunctionLevelFormatter


- (NSString *)formatLogMessage:(DDLogMessage *)logMessage {
    NSString *logFlag = nil;
    switch (logMessage->logFlag) {
        case LOG_FLAG_ERROR:
            logFlag = @"E";
            break;
        case LOG_FLAG_WARN:
            logFlag = @"W";
            break;
        case LOG_FLAG_INFO:
            logFlag = @"I";
            break;
        default:
            logFlag = @"V";
            break;
    }

    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
    [dateFormatter setDateFormat:@"yyyyMMdd'T'HH:mm:ss:SSSS"];
    NSString *formattedDateString = [dateFormatter stringFromDate:[NSDate date]];

    return [NSString stringWithFormat:@"[%@][%@][%@ %@][Line %d] [Thread %@] %@",
           formattedDateString,
           logFlag,
           logMessage.fileName,
           logMessage.methodName,
           logMessage->lineNumber,
           logMessage.threadID,
           logMessage->logMsg];
}

@end
