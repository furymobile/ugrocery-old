//
//  FMemCache.m
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "FMemCache.h"

static FMemCache *sharedCache = nil;

@interface FMemCache ()

@property (nonatomic) NSMutableDictionary *cache;

@end

@implementation FMemCache

+ (FMemCache *)sharedCache {
    if(!sharedCache)
        sharedCache = [[FMemCache alloc]init];

    return sharedCache;
}

- (id)init {
    self = [super init];
    if (self != nil) {
        NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
        [center addObserver:self
        selector:@selector(clearCache)
        name:UIApplicationDidReceiveMemoryWarningNotification
        object:nil];
        [center addObserver:self
        selector:@selector(clearCache)
        name:UIApplicationWillResignActiveNotification
        object:nil];
        _lifetime = 60.0f;
    }
    return self;
}

- (void)clearCache {
    [_cache removeObjectsForKeys:[_cache allKeys]];
    _cache = nil;
}

- (void)dealloc {
    NSNotificationCenter *center = [NSNotificationCenter defaultCenter];
    [center removeObserver:self
    name:UIApplicationDidReceiveMemoryWarningNotification
    object:nil];
    [center removeObserver:self
    name:UIApplicationWillResignActiveNotification
    object:nil];
}

- (void)setValue:(id)value forKey:(NSString *)key {
    if (!_cache)
        _cache = [[NSMutableDictionary alloc]init];

    [_cache setValue:value forKey:key];
    [NSObject cancelPreviousPerformRequestsWithTarget:_cache selector:@selector(removeObjectForKey:) object:key];
    [_cache performSelector:@selector(removeObjectForKey:) withObject:key afterDelay:_lifetime];
}

- (id)valueForKey:(NSString *)key {
    return [_cache valueForKey:key];
}

- (BOOL)hasKey:(NSString *)key {
    return [[_cache allKeys]containsObject:key];
}

@end
