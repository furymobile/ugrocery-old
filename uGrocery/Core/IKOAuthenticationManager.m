//
//  IKOAuthenticationManager.m
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOAuthenticationManager.h"
#import "IKOServerResponse.h"
#import "IKOUser.h"
#import "NSData+Base64.h"
#import "PDKeychainBindings.h"
#import "UserVoice.h"
#import "IAPHelper.h"
#import "IKONetworkManager.h"
#import "IAPShare.h"

NSString *const IKO1MonthSubscription = @"com.ugrocery.subscription.1month";
NSString *const IKO3MonthSubscription = @"com.ugrocery.subscription.3months";
NSString *const IKO6MonthSubscription = @"com.ugrocery.subscription.6months";
NSString *const IKO1YearSubscription = @"com.ugrocery.subscription.1year";

NSString *const kSubscriptionExpirationDateKey = @"ExpirationDate";

const NSString *kIKOUser = @"ikou";
const NSString *kSubKey = @"ikosub";
const NSString *kFirstLaunchKey = @"ikoflk";
#define kNumberOfTrialDays 604800

NSString *const IKOAuthenticationManagerDidAuthenticate = @"IKOAuthenticationManagerDidAuthenticate";
NSString *const IKOAuthenticationManagerDidSignOut = @"IKOAuthenticationManagerDidSignOut";

@interface IKOAuthenticationManager ()

@property (nonatomic) NSDate *firstLaunch;

@end

@implementation IKOAuthenticationManager

static IKOUser *currentUser;

+ (IKOAuthenticationManager *)sharedInstance {
    static IKOAuthenticationManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc]init];
        [[self class]currentUser];
        DDLogInfo(@"Started Authentication Manager");
        DDLogInfo(@"Purchased Products: %@",[IAPShare sharedHelper].iap.purchasedProducts);
        [sharedInstance isInTrialPeriod];

        if(![IAPShare sharedHelper].iap) {
            
            NSSet *dataSet = [[NSSet alloc]initWithArray:@[ALL_PRODUCT_IDS]];
            [IAPShare sharedHelper].iap = [[IAPHelper alloc]initWithProductIdentifiers:dataSet];
#ifdef DEBUG
            [IAPShare sharedHelper].iap.production = NO;
            [[PDKeychainBindings sharedKeychainBindings]removeObjectForKey:(NSString *)kSubscriptionExpirationDateKey];
         //   [[PDKeychainBindings sharedKeychainBindings]removeObjectForKey:(NSString *)kFirstLaunchKey];
#else
            [IAPShare sharedHelper].iap.production = YES;
#endif
            [[IAPShare sharedHelper].iap requestProductsWithCompletion:nil];
        }

        sharedInstance.firstLaunch = [sharedInstance firstLaunchDate];
        DDLogInfo(@"First Launch Date: %@", sharedInstance.firstLaunch);
    });
    return sharedInstance;
}

+ (IKOUser *)currentUser {
    if (currentUser)
        return currentUser;

    NSData *data = [NSData dataFromBase64String:[[PDKeychainBindings sharedKeychainBindings]objectForKey:(NSString *)kIKOUser]];
    currentUser = [IKOUser objectFromJSON:[NSKeyedUnarchiver unarchiveObjectWithData:data]];
    return currentUser;
}

- (void)authenticatedUser:(IKOServerResponse *)response {
    IKOUser *user = nil;
    if (response.payload)
        user = [IKOUser objectFromJSON:response.payload];
    if (user) {
        NSData *data = [NSKeyedArchiver archivedDataWithRootObject:[user JSONRepresentation]];
        [[PDKeychainBindings sharedKeychainBindings]setObject:[data base64EncodedString] forKey:(NSString *)kIKOUser];
        currentUser = user;
        [[NSNotificationCenter defaultCenter]postNotificationName:IKOAuthenticationManagerDidAuthenticate object:user];
        ANALYTIC(@"Signed In");
    }
}

- (NSDate *)firstLaunchDate {
    NSString *firstLaunchString = (NSString *)[[PDKeychainBindings sharedKeychainBindings]objectForKey:(NSString *)kFirstLaunchKey];
    if (!firstLaunchString) {
        [self initiateTrial:7];
        NSDate *date = [NSDate date];
        [[PDKeychainBindings sharedKeychainBindings]setObject:SF(@"%f", [date timeIntervalSince1970]) forKey:(NSString *)kFirstLaunchKey];
        self.showFirstLaunchDialog = YES;
        return date;
    }
    return [NSDate dateWithTimeIntervalSince1970:[firstLaunchString doubleValue]];
}

- (BOOL)isInTrialPeriod {
    if([self daysRemainingOnSubscription] > 0) {
        if (![self hasAppStoreReceipt] && [self trialPeriodRemaining] > 0) {
            return YES;
        } else if ([self hasAppStoreReceipt]) {
            return NO;
        }
    }

    NSDate *date2 = [self firstLaunchDate];
    NSTimeInterval difference = [[NSDate date]timeIntervalSinceDate:date2];
    if(difference > kNumberOfTrialDays) {
        return NO;
    } else {
        return YES;
    }
}

- (long)trialPeriodRemaining {
    NSDate *dt1 = [NSDate date];
    NSDate *dt2 = [[self firstLaunchDate] dateByAddingTimeInterval:kNumberOfTrialDays];
    NSUInteger unitFlags = NSCalendarUnitDay;
    NSCalendar *calendar = [[NSCalendar alloc]initWithCalendarIdentifier:NSGregorianCalendar];
    NSDateComponents *components = [calendar components:unitFlags fromDate:dt1 toDate:dt2 options:0];
    return [components day]+1;
}

- (BOOL)isAuthenticated {
    return ([[self class]currentUser].oauthToken) ? YES : NO;
}

- (BOOL)isAdministrator {
    return [currentUser.userTypeId isEqualToNumber:[NSNumber numberWithInt:IKORoleAdministrator]];
}

- (BOOL)isUpdater {
    return [currentUser.userTypeId isEqualToNumber:[NSNumber numberWithInt:IKORoleAdministrator]] || [currentUser.userTypeId isEqualToNumber:[NSNumber numberWithInt:IKORoleUpdater]] || [currentUser.userTypeId isEqualToNumber:[NSNumber numberWithInt:IKORoleuGroceryUpdater]];
}

- (BOOL)hasAppStoreReceipt {
    NSURL *receiptURL = [[NSBundle mainBundle] appStoreReceiptURL];
    NSData *receipt = [NSData dataWithContentsOfURL:receiptURL];
    return receipt != nil;
}

- (BOOL)isPremium {
    if([self daysRemainingOnSubscription] > 0) {
        return YES;
    }

    if ([self isInTrialPeriod])
        return YES;

    switch ([currentUser.userTypeId intValue]) {
        case IKORoleFreeMember:
        case IKORoleMember:
        case IKORoleUpdater:
        case IKORoleAdministrator:
        case IKORoleuGroceryUpdater:
            return YES;
            break;
        default:
            return NO;
            break;
    }
}

- (void)signOut {
    currentUser = nil;
    [[PDKeychainBindings sharedKeychainBindings]removeObjectForKey:(NSString *)kIKOUser];
    [[NSNotificationCenter defaultCenter]postNotificationName:IKOAuthenticationManagerDidSignOut object:nil];
    ANALYTIC(@"Signed Out");
}

- (UVConfig *)userVoiceConfig {
    IKOUser *user = [[self class]currentUser];
    UVConfig *config = [UVConfig configWithSite:@"ugrocery.uservoice.com"];
    if ([self isAuthenticated])
        [config identifyUserWithEmail:user.loginEmail name:SF(@"%@ %@", user.firstName, user.lastName) guid:SF(@"%@", user.userId)];
    return config;
}

#pragma mark - Subscription Handling

- (int)daysRemainingOnSubscription {
    NSDate *expiration = [self expirationDate];
    NSTimeInterval timeInt = [expiration timeIntervalSinceDate:[NSDate date]];
    int days = timeInt / 60 / 60 / 24;
    return (days > 0) ? days : 0;
}

- (NSString *)getExpirationDateString {
    if ([self daysRemainingOnSubscription] > 0) {
        NSDate *expiration = [self expirationDate];
        NSDateFormatter *dateFormat = [NSDateFormatter new];
        [dateFormat setDateFormat:@"MMM dd, yyyy"];
        return SF(@"Subscribed to uGrocery Premium! \nExpires: %@ (%i Days)",[dateFormat stringFromDate:expiration],[self daysRemainingOnSubscription]);
    } else {
        return @"Not Subscribed";
    }
}

- (void)initiateTrial:(int)days {
    NSDate *originDate = ([self daysRemainingOnSubscription] > 0) ? [self expirationDate] : [NSDate date];
    NSDateComponents *dateComp = [NSDateComponents new];
    [dateComp setDay:days];
    
    NSDate *expirationDate = [[NSCalendar currentCalendar] dateByAddingComponents:dateComp toDate:originDate options:0];

    [[PDKeychainBindings sharedKeychainBindings]setObject:SF(@"%f", [expirationDate timeIntervalSince1970]) forKey:(NSString *)kSubscriptionExpirationDateKey];
}

- (NSDate *)getExpirationDateForMonths:(int)months {
    NSDate *originDate = ([self daysRemainingOnSubscription] > 0) ? [self expirationDate] : [NSDate date];
    NSDateComponents *dateComp = [NSDateComponents new];
    [dateComp setMonth:months];
    [dateComp setDay:1];
    
    return [[NSCalendar currentCalendar] dateByAddingComponents:dateComp
                                                         toDate:originDate
                                                         options:0];
}

- (void)purchaseSubscriptionWithMonths:(int)months {

    NSDate * expirationDate = [self getExpirationDateForMonths:months];
    [[PDKeychainBindings sharedKeychainBindings]setObject:SF(@"%f", [expirationDate timeIntervalSince1970]) forKey:(NSString *)kSubscriptionExpirationDateKey];
    NSLog(@"Subscription Complete!");
}

- (NSDate *)expirationDate {
    NSString *dateString = (NSString *)[[PDKeychainBindings sharedKeychainBindings]objectForKey:(NSString *)kSubscriptionExpirationDateKey];
    return [NSDate dateWithTimeIntervalSince1970:[dateString doubleValue]] ?: nil;
}


@end
