//
//  FMemCache.h
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//


#import <Foundation/Foundation.h>

@interface FMemCache : NSObject

@property (nonatomic) float lifetime;

+ (FMemCache *)sharedCache;

- (void)setValue:(id)value forKey:(NSString *)key;
- (id)valueForKey:(NSString *)key;
- (void)clearCache;
- (BOOL)hasKey:(NSString *)key;

@end
