//
//  IKONetworkManager.m
//  uGrocery
//
//  Created by Duane Schleen on 3/18/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKONetworkManager.h"
#import "Reachability.h"

@interface IKONetworkManager ()

@property (nonatomic) Reachability *hostReach;
@property (nonatomic) Reachability *currentReach;

@end

@implementation IKONetworkManager

+ (IKONetworkManager *)sharedInstance {
    static IKONetworkManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc]init];
        [[NSNotificationCenter defaultCenter]addObserver:sharedInstance selector:@selector(reachabilityChanged:) name:kReachabilityChangedNotification object:nil];

        sharedInstance.hostReach = [Reachability reachabilityWithHostName:@"api.ugrocery.com"];
        [sharedInstance.hostReach startNotifier];
    });

    return sharedInstance;
}

+ (BOOL)connectedToNetwork {
    Reachability *r = [[self class]sharedInstance].hostReach;
    return [r currentReachabilityStatus] == NotReachable ? NO : YES;
}

//Called by Reachability whenever status changes.
- (void)reachabilityChanged:(NSNotification *)note {
    DDLogVerbose(@"Reachability Changed: notification object type = %@", [note object]);
    Reachability *curReach = [note object];
    _currentReach = curReach;
}

@end
