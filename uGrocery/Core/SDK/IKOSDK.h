#import <Foundation/Foundation.h>
#import "IKOServerResponse.h"
#import "IKOFLBRequestPayload.h"
#import "IKOError.h"

@class IKOError;
@class IKOServerResponse;
@class IKOPriceUpdate;
@class IKOUser;

@interface IKOSDK : NSObject

- (void)registerNewUser:(NSString *)loginEmail
              firstName:(NSString *)firstName
               lastName:(NSString *)lastName
               password:(NSString *)password
             postalCode:(NSString *)postalCode
              promoCode:(NSString *)promoCode
            phoneNumber:(NSString *)phoneNumber
             newsletter:(BOOL)newsletter
             referredBy:(NSNumber *)referredBy
               callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)loginUser:(NSString *)loginEmail
         password:(NSString *)password
         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)forgotPassword:(NSString *)emailAddress
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion;

- (void)changePassword:(NSString *)oldPassword
           newPassword:(NSString *)newPassword
                  user:(IKOUser *)user
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion;

- (void)premiumUpgrade:(IKOUser *)user
            expiration:(NSDate *)expirationDate
               receipt:(NSString *)receipt
      isSandboxReceipt:(BOOL)isSandboxReceipt
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion;

- (void)findLowestBill:(NSArray *)productIds
              storeIds:(NSArray *)storeIds
                  user:(IKOUser *)user
              callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)findLowestBillsDetailWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion;

- (void)findLowestBillsUpcDetailWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion;

- (void)findLowestBillMyDealsWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion;

- (void)findDealsTopWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion;

- (void)findLowestBillWithPayload:(NSDictionary *)payload
                         storeIds:(NSArray *)storeIds
                             user:(IKOUser *)user
                         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
                        andMethod:(NSString*)method;

- (void)findDealsWithPayload:(NSDictionary *)payload
                    storeIds:(NSArray *)storeIds
                        user:(IKOUser *)user
                    callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
                   andMethod:(NSString*)method;

- (void) search:(NSString *)search
       storeIds:(NSArray *)storeIds
       l1CatIds:(NSArray *)l1CatIds
       l3CatIds:(NSArray *)l3CatIds
       brandIds:(NSArray *)brandIds
    withCoupons:(BOOL)withCoupons
withStoreBrands:(BOOL)withStoreBrands
           page:(NSNumber *)page
          limit:(NSNumber *)limit
           user:(IKOUser *)user
       callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
      andMethod:(NSString*)method;

- (void)searchProducts:(NSString *)search
              storeIds:(NSArray *)storeIds
              l1CatIds:(NSArray *)l1CatIds
              l3CatIds:(NSArray *)l3CatIds
              brandIds:(NSArray *)brandIds
           withCoupons:(BOOL)withCoupons
       withStoreBrands:(BOOL)withStoreBrands
                  page:(NSNumber *)page
                 limit:(NSNumber *)limit
                  user:(IKOUser *)user
              callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)searchOffers:(NSString *)search
            storeIds:(NSArray *)storeIds
            l1CatIds:(NSArray *)l1CatIds
            l3CatIds:(NSArray *)l3CatIds
            brandIds:(NSArray *)brandIds
         withCoupons:(BOOL)withCoupons
     withStoreBrands:(BOOL)withStoreBrands
                page:(NSNumber *)page
               limit:(NSNumber *)limit
                user:(IKOUser *)user
            callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)submitUPC:(IKOPriceUpdate *)upcSubmission
             user:(IKOUser *)user
         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)submitUPCBulk:(NSDictionary *)bulkUpcSubmission
             callback:(void (^)(IKOError *error, IKOServerResponse *response))completion;

- (void)getApiVersion:(void(^)(IKOError *error, IKOServerResponse *response))completion;

- (void)fetchBulkData:(NSString *)target
                 type:(NSString *)type
            timestamp:(NSString *)timestamp
             callback:(void (^)(IKOError *, IKOServerResponse *))completion;

@end
