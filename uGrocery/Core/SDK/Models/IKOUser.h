//
//  IKOUser.h
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOUser : IKOObject

@property (nonatomic) NSNumber *userId;
@property (nonatomic) NSNumber *userTypeId;
@property (nonatomic) NSString *loginEmail;
@property (nonatomic) NSString *firstName;
@property (nonatomic) NSString *lastName;
@property (nonatomic) NSString *zipCode;
@property (nonatomic) NSString *phone;
@property (nonatomic) NSString *oauthToken;
@property (nonatomic) NSString *oauthSecret;

@end
