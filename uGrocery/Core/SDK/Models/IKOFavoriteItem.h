//
//  IKOMyProductItem.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"
#import "IKOProduct.h"

@interface IKOFavoriteItem : IKOObject

@property (nonatomic) NSString *myProductItemId;
@property (nonatomic) NSNumber *userId;
@property (nonatomic) NSNumber *productId;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSNumber *l1Id;
@property (nonatomic) NSString *productDescription;
@property (nonatomic) NSString *searchName;
@property (nonatomic) NSNumber *isFavorite;
@property (nonatomic) NSString *imageUri;
@property (nonatomic) NSNumber *comparable;
@property (nonatomic) NSString *measureText;

- (instancetype)initWithProduct:(IKOProduct *)product;


@end
