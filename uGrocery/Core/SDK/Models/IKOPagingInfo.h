//
//  IKOPagingInfo.h
//  uGrocery
//
//  Created by Duane Schleen on 9/17/15.
//  Copyright © 2015 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface IKOPagingInfo : NSObject

@property (nonatomic, strong) NSNumber *currentPage;
@property (nonatomic, strong) NSNumber *limit;
@property (nonatomic, strong) NSNumber *totalPages;

-(id)initWithPage:(NSNumber *)page andLimit:(NSNumber *)limit;
-(id)initWithPage:(NSNumber *)page andLimit:(NSNumber *)limit andTotal:(NSNumber *)total;

-(BOOL)hasMorePages;
-(void)nextPage;
-(void)previousPage;

@end
