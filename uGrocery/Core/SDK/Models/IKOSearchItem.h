//
//  IKOSearchItem.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@class IKOBrand;
@class IKOProductCategory;
@class IKOShoppingListItem;
@class IKORecentSearchItem;

@interface IKOSearchItem : IKOObject

@property (nonatomic) NSString *searchItemId;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSString *itemDescription;
@property (nonatomic) NSString *searchName;
@property (nonatomic) NSString *measureText;
@property (nonatomic) NSNumber *isStoreBrand;

- (instancetype)initWithBrand:(IKOBrand *)brand;
- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory;
- (instancetype)initWithShoppingListItem:(IKOShoppingListItem *)item;
- (instancetype)initWithRecentSearchItem:(IKORecentSearchItem *)item;

@end
