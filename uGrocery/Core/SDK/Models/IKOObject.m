//
//  IKOObject.m
//  uGrocery
//
//  Created by Duane Schleen on 7/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

typedef NS_ENUM (NSInteger, FMapType) {
    FMapTypeSimple,
    FMapTypeObject,
    FMapTypeCollection,
};


@interface FMap ()
@property (nonatomic) FMapType type;
@end


@implementation FMap

- (instancetype)initWithPropertyName:(NSString *)name
  andJsonKey:(NSString *)jsonKey {
    if ((self = [super init])) {
        _keyPath = name;
        _jsonKey = jsonKey;
    }
    return self;
}

+ (instancetype)mapPropertyName:(NSString *)name
  toJsonKey:(NSString *)jsonKey {
    return [[self alloc]initWithPropertyName:name andJsonKey:jsonKey];
}

+ (instancetype)mapPropertyName:(NSString *)name {
    return [[self alloc]initWithPropertyName:name andJsonKey:name];
}

- (instancetype)convertNumberToString:(BOOL)convertNumberToString {
    _convertNumberToString = convertNumberToString;
    return self;
}

- (instancetype)class:(Class)class {
    NSParameterAssert(class != nil);
    NSParameterAssert([class isSubclassOfClass:[IKOObject class]]);
    _klass = class;
    _type = FMapTypeObject;
    return self;
}

- (instancetype)collectionClass:(Class)class {
    NSParameterAssert(class != nil);
    NSParameterAssert([class isSubclassOfClass:[IKOObject class]]);
    _klass = class;
    _type = FMapTypeCollection;
    return self;
}

@end


@implementation IKOObject

+ (instancetype)objectFromJSON:(NSDictionary *)json {
    if (![json isKindOfClass:[NSDictionary class]]) return nil;

    IKOObject *object = [[self alloc]init];
    for (FMap *mapping in [self maps]) {
        id value = json[mapping.jsonKey];
        if (!value || value == [NSNull null]) continue;

        switch (mapping.type) {
            case FMapTypeSimple:
                if (mapping.convertNumberToString && [value isKindOfClass:[NSNumber class]]) {
                    value = [(NSNumber *)value stringValue];
                }
                break;
            case FMapTypeObject:
                value = [mapping.klass objectFromJSON:value];
                break;
            case FMapTypeCollection:
                value = [self collectionWithClass:mapping.klass fromJSON:value];
                break;
        }

        if (!value) continue;
        [object setValue:value forKeyPath:mapping.keyPath];
    }
    ;
    return object;
}

+ (NSArray *)collectionWithClass:(Class)class fromJSON:(NSArray *)json {
    if (![json isKindOfClass:[NSArray class]]) return nil;
    NSMutableArray *collection = [NSMutableArray arrayWithCapacity:json.count];
    for (NSDictionary *objectJson in json) {
        IKOObject *object = [class objectFromJSON:objectJson];
        if (object)
            [collection addObject:object];
    }
    return collection;
}

- (NSDictionary *)JSONRepresentation {
    NSArray *mappings = [[self class]maps];
    NSMutableDictionary *json = [NSMutableDictionary dictionaryWithCapacity:mappings.count];
    for (FMap *mapping in mappings) {
        id value = [self valueForKeyPath:mapping.keyPath];

        switch (mapping.type) {
            case FMapTypeObject:
                if (![value isKindOfClass:[IKOObject class]]) continue;
                value = [(IKOObject *)value JSONRepresentation];
                break;
            case FMapTypeCollection:
                value = [[self class]JSONFromCollection:value];
                break;
            case FMapTypeSimple:
                break;
        }

        if (!value) continue;

        json[mapping.jsonKey] = value;
    }
    return json;
}

+ (NSArray *)JSONFromCollection:(NSArray *)collection {
    if (![collection isKindOfClass:[NSArray class]]) return nil;
    NSMutableArray *json = [NSMutableArray arrayWithCapacity:collection.count];
    for (IKOObject *object in collection) {
        if (![object isKindOfClass:[IKOObject class]]) continue;
        [json addObject:[object JSONRepresentation]];
    }
    return json;
}

+ (NSArray *)maps {
    NSString *reason = [NSString stringWithFormat:@"Expected subclass %@ to define method %@", NSStringFromClass(self), NSStringFromSelector(_cmd)];
    @throw [NSException exceptionWithName:@"Abstract method not implemented" reason:reason userInfo:nil];
}

#pragma mark - NSCoding

- (void)encodeWithCoder:(NSCoder *)coder {
    for (FMap *mapping in [[self class] maps]) {
        [coder encodeObject:[self valueForKeyPath:mapping.keyPath] forKey:mapping.keyPath];
    }
}

- (id)initWithCoder:(NSCoder *)coder {
    if ((self = [super init])) {
        for (FMap *mapping in [[self class] maps]) {
            [self setValue:[coder decodeObjectForKey:mapping.keyPath] forKeyPath:mapping.keyPath];
        }
    }
    return self;
}

- (NSString *)description {
    return [[self JSONRepresentation]description];
}

@end
