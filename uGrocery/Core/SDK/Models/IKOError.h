//
//  IKOError.h
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@class IKOServerResponse;

@interface IKOError : IKOObject

@property (nonatomic) NSNumber *code;
@property (nonatomic) NSString *errorDescription;
@property (nonatomic) NSString *responseErrorDescription;
@property (nonatomic) NSError *error;

+ (IKOError *)errorFromResponse:(IKOServerResponse *)response andError:(NSError *)error;

- (id)initWithCode:(NSNumber*)code;

@end
