//
//  IKOBrand.h
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOBrand : IKOObject

@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSString *name;
@property (nonatomic) NSNumber *storeId;
@property (nonatomic) NSNumber *isStoreBrand;

@end
