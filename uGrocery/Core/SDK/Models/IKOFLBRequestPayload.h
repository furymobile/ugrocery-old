//
//  IKOFLBRequestPayload.h
//  uGrocery
//
//  Created by Duane Schleen on 8/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@interface IKOFLBRequestPayload : IKOObject

@property (nonatomic) NSArray *products;
@property (nonatomic) NSNumber *withCoupons;
@property (nonatomic) NSString *flbPath;

- (instancetype)initWithShoppingListItems:(NSArray *)shoppingListItems;
- (instancetype)initWithProducts:(NSArray *)products;
- (instancetype)initWithMyProducts:(NSArray *)products;
- (instancetype)initWithUpc:(NSString *)upc;

@end
