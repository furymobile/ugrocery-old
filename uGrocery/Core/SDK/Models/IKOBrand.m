//
//  IKOBrand.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOBrand.h"

@implementation IKOBrand

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"brandId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"name" toJsonKey:@"Name"],
        [FMap mapPropertyName:@"storeId" toJsonKey:@"StoreId"],
        [FMap mapPropertyName:@"isStoreBrand" toJsonKey:@"StoreBrand"]
    ];
}

@end
