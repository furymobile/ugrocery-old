//
//  IKOMyProductItem.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFavoriteItem.h"

@implementation IKOFavoriteItem

- (instancetype)initWithProduct:(IKOProduct *)product {
    self = [super init];
    if (self) {
        self.myProductItemId = [[NSUUID UUID]UUIDString];
        self.productId = product.productId;
        self.categoryId = product.categoryId;
        self.brandId = product.brandId;
        self.productDescription = product.productDescription;
        self.searchName = product.searchName;
        if ([product.hasImage integerValue] == 1)
            self.imageUri = product.imageUri;
        self.isFavorite = @0;
        self.comparable = product.comparable;
        self.measureText = product.measureText;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"myProductItemId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"userId" toJsonKey:@"UserId"],
        [FMap mapPropertyName:@"productId" toJsonKey:@"ProductId"],
        [FMap mapPropertyName:@"categoryId" toJsonKey:@"CategoryId"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"l1Id" toJsonKey:@"L1Id"],
        [FMap mapPropertyName:@"productDescription" toJsonKey:@"ProductDescription"],
        [FMap mapPropertyName:@"searchName" toJsonKey:@"SearchName"],
        [FMap mapPropertyName:@"isFavorite" toJsonKey:@"IsFavorite"],
        [FMap mapPropertyName:@"imageUri" toJsonKey:@"ImageUri"],
        [FMap mapPropertyName:@"comparable" toJsonKey:@"Comparable"],
        [FMap mapPropertyName:@"measureText" toJsonKey:@"MeasureText"]
    ];
}

@end
