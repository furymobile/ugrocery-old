//
//  IKOServerResponse.h
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOObject.h"
#import "IKOPagingInfo.h"

@interface IKOServerResponse : IKOObject

@property (nonatomic) NSString *type;
@property (nonatomic) BOOL success;
@property (nonatomic) id payload;
@property (nonatomic) NSNumber *limit;
@property (nonatomic) NSNumber *page;
@property (nonatomic) NSNumber *total;

@property (nonatomic, readonly, getter=getPagingInfo) IKOPagingInfo *pagingInfo;

@end
