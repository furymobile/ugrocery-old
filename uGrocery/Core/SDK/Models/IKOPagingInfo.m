//
//  IKOPagingInfo.m
//  uGrocery
//
//  Created by Duane Schleen on 9/17/15.
//  Copyright © 2015 ikonomo. All rights reserved.
//

#import "IKOPagingInfo.h"

@implementation IKOPagingInfo

-(id)initWithPage:(NSNumber *)page andLimit:(NSNumber *)limit andTotal:(NSNumber *)total {
    self = [super init];
    if (self) {
        self.currentPage = page;
        self.limit = limit;
        self.totalPages = total;
    }
    return self;
}

-(id)initWithPage:(NSNumber *)page andLimit:(NSNumber *)limit {
    return [self initWithPage:page andLimit:limit andTotal:nil];
}

-(BOOL)hasMorePages {
    return ([_currentPage intValue] < [_totalPages intValue]);
}

-(void)nextPage {
    if ([_currentPage intValue] == [_totalPages intValue])
        return;
    
    _currentPage = [NSNumber numberWithLong:[_currentPage longValue] + 1];
    NSLog(@"Current Page: %@", _currentPage);
}

-(void)previousPage {
    if ([_currentPage intValue] == 0)
        return;

    _currentPage = [NSNumber numberWithLong:[_currentPage longValue] - 1];
}

@end
