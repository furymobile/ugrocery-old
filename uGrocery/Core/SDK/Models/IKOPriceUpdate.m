//
//  IKOPriceUpdate.m
//  uGrocery
//
//  Created by Duane Schleen on 9/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOPriceUpdate.h"
#import "IKOUser.h"
#import "IKOProduct.h"

@implementation IKOPriceUpdate

- (instancetype)initWithUPC:(NSString *)upc
  andUser:(IKOUser *)user {
    self = [super init];
    if (self) {
        self.priceUpdateId = [[NSUUID UUID]UUIDString];
        self.userId = user.userId;
        self.captureDate = [NSNumber numberWithInt:[[NSDate date]timeIntervalSince1970]];
        self.upc = upc;
        self.ppfSafe = @0;
    }
    return self;
}

- (instancetype)initWithProduct:(IKOProduct *)product
  andUser:(IKOUser *)user {
    self = [super init];
    if (self) {
        self.priceUpdateId = [[NSUUID UUID]UUIDString];
        self.userId = user.userId;
        self.captureDate = [NSNumber numberWithInt:[[NSDate date]timeIntervalSince1970]];
        self.upc = product.upc;
        self.productId = product.productId;
        self.l3Id = product.categoryId;
        self.brandId = product.brandId;
        self.productDescription = product.productDescription;
        self.ppfSafe = @0;
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"priceUpdateId"],
        [FMap mapPropertyName:@"upc" toJsonKey:@"Upc"],
        [FMap mapPropertyName:@"captureDate" toJsonKey:@"CapturedAt"],
        [FMap mapPropertyName:@"userId" toJsonKey:@"UserId"],
        [FMap mapPropertyName:@"latitude" toJsonKey:@"Lat"],
        [FMap mapPropertyName:@"longitude" toJsonKey:@"Long"],
        [FMap mapPropertyName:@"l3Id" toJsonKey:@"L3Id"],
        [FMap mapPropertyName:@"storeId" toJsonKey:@"StoreId"],
        [FMap mapPropertyName:@"productDescription" toJsonKey:@"ProductDesc"],
        [FMap mapPropertyName:@"size" toJsonKey:@"Size"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"brandName" toJsonKey:@"BrandName"],
        [FMap mapPropertyName:@"price" toJsonKey:@"Price"],
        [FMap mapPropertyName:@"ppfSafe" toJsonKey:@"PpfSafe"],
        [FMap mapPropertyName:@"productId" toJsonKey:@"ProductId"],
        [FMap mapPropertyName:@"salePrice" toJsonKey:@"SalePrice"],
        [FMap mapPropertyName:@"saleEndDate" toJsonKey:@"SaleEndDate"],
        [FMap mapPropertyName:@"saleDescription" toJsonKey:@"SaleDesc"]
    ];
}

- (NSDictionary *)submissionPayload {
    NSMutableDictionary *payload = [[self JSONRepresentation]mutableCopy];
    [payload removeObjectForKey:@"priceUpdateId"];

    if ([self.saleEndDate isEqual:@0])
        [payload removeObjectForKey:@"SaleEndDate"];

    if ([self.salePrice isEqual:@0])
        [payload removeObjectForKey:@"SalePrice"];

    return payload;
}

@end
