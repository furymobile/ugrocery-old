//
//  IKORecentSearchItem.h
//  uGrocery
//
//  Created by Duane Schleen on 1/15/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKOObject.h"

@class IKOBrand;
@class IKOProductCategory;
@class IKOSearchItem;


@interface IKORecentSearchItem : IKOObject

@property (nonatomic) NSString *recentSearchItemId;
@property (nonatomic) NSNumber *brandId;
@property (nonatomic) NSNumber *categoryId;
@property (nonatomic) NSString *termDescription;
@property (nonatomic) NSNumber *lastSearchDateLong;

- (instancetype)initWithBrand:(IKOBrand *)brand;
- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory;
- (instancetype)initWithSearchItem:(IKOSearchItem *)searchItem;

@end
