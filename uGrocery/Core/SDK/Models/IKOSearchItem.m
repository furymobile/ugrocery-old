//
//  IKOSearchItem.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSearchItem.h"
#import "IKOBrand.h"
#import "IKOProductCategory.h"
#import "IKOShoppingListItem.h"
#import "IKORecentSearchItem.h"
#import "NSString+Additions.h"

@implementation IKOSearchItem

- (instancetype)initWithBrand:(IKOBrand *)brand {
    self = [super init];
    if (self) {
        self.searchItemId = [[NSUUID UUID]UUIDString];
        self.brandId = brand.brandId;
        self.categoryId = 0;
        self.itemDescription = brand.name;
        self.searchName = [[brand.name uppercaseString]stripPunctuation];
        self.measureText = nil;
        self.isStoreBrand = brand.isStoreBrand;
    }
    return self;
}

- (instancetype)initWithProductCategory:(IKOProductCategory *)productCategory {
    self = [super init];
    if (self) {
        self.searchItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = productCategory.productCategoryId;
        self.brandId = 0;
        self.itemDescription = productCategory.name;
        self.searchName = [[productCategory.name uppercaseString]stripPunctuation];
        self.measureText = nil;
        self.isStoreBrand = @0;
    }
    return self;
}

- (instancetype)initWithShoppingListItem:(IKOShoppingListItem *)item {
    self = [super init];
    if (self) {
        self.searchItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = item.categoryId;
        self.brandId = item.brandId;
        self.itemDescription = item.listItemDescription;
        self.searchName = [[item.listItemDescription uppercaseString]stripPunctuation];
        self.measureText = item.measureText;
    }
    return self;
}

- (instancetype)initWithRecentSearchItem:(IKORecentSearchItem *)item {
    self = [super init];
    if (self) {
        self.searchItemId = [[NSUUID UUID]UUIDString];
        self.categoryId = item.categoryId;
        self.brandId = item.brandId;
        self.itemDescription = item.termDescription;
        self.searchName = [[item.termDescription uppercaseString]stripPunctuation];
    }
    return self;
}

+ (NSArray *)maps {
    return @[
        [FMap mapPropertyName:@"searchItemId" toJsonKey:@"Id"],
        [FMap mapPropertyName:@"brandId" toJsonKey:@"BrandId"],
        [FMap mapPropertyName:@"categoryId" toJsonKey:@"CategoryId"],
        [FMap mapPropertyName:@"itemDescription" toJsonKey:@"ItemDescription"],
        [FMap mapPropertyName:@"searchName" toJsonKey:@"SearchName"],
        [FMap mapPropertyName:@"measureText" toJsonKey:@"MeasureText"],
        [FMap mapPropertyName:@"isStoreBrand" toJsonKey:@"StoreBrand"],
    ];
}

@end
