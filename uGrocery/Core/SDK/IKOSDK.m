#import "IKOError.h"
#import "IKOPriceUpdate.h"
#import "IKOSDK.h"
#import "IKOServerResponse.h"
#import "IKOUser.h"
#import "NSString+URLEncoding.h"

#include "hmac.h"
#include "Base64Transcoder.h"

//------------------------------------------------------------------------------------
// @note For now, we're configuring the callback as 'oob' for out of band per the spec
//       however, we'll want to support it for third party clients
// @note Sometimes the callback has to be the same as the registered app callback url
//------------------------------------------------------------------------------------
#define ACCESS_TOKEN_URL     @"oauth/access-token-explicit.json"
#define AUTHENTICATE_URL     @"application/authorize.json"
#define CONSUMER_KEY         @"91050360b7e938c980e4e69d7cb79006157b1b2c"
#define CONSUMER_SECRET      @"1dd03ec461489e6435ceef3694924ee05381dde4"
#define OAUTH_CALLBACK       @"oob"
#define REQUEST_TOKEN_URL    @"oauth/request-token.json"

//#if PROD
#define AUTH_URL             @"https://api.ugrocery.com/v1/"
#define API_URL              @"https://api.ugrocery.com/v1/"
//#elif LOCAL_DEV
  //  #define AUTH_URL             @"http://api.dev.ugrocery.com/v1/"
  //  #define API_URL              @"http://api.dev.ugrocery.com/v1/"
//#elif UAT
//#define AUTH_URL             @"https://api.uat.ugrocery.com/v1/"
//#define API_URL              @"https://api.uat.ugrocery.com/v1/"
//#elif TARGET_IPHONE_SIMULATOR
//    #define AUTH_URL             @"https://api.staging.ugrocery.com/v1/"
//    #define API_URL              @"https://api.staging.ugrocery.com/v1/"
//#else
//    #define AUTH_URL             @"https://api.staging.ugrocery.com/v1/"
//    #define API_URL              @"https://api.staging.ugrocery.com/v1/"
//#endif

//------------------------------------------------------------
// Scope is implemented in an odd way by LinkedIn...
// They've hacked the protocol to pass along scope parameters
// with the OAuth parameters. For uGrocery scope is set with
// the app during registration
//------------------------------------------------------------
#define OAUTH_SCOPE_PARAM    @""


#define REQUEST_TOKEN_METHOD @"GET"
#define ACCESS_TOKEN_METHOD  @"POST"

#define NETWORK_TIMEOUT 120000


//--- The part below is from AFNetworking---
static NSString * CHPercentEscapedQueryStringPairMemberFromStringWithEncoding(NSString *string, NSStringEncoding encoding) {
    static NSString * const kCHCharactersToBeEscaped = @":/?&=;+!@#$()~";
    static NSString * const kCHCharactersToLeaveUnescaped = @"[].";
    
	return (__bridge_transfer  NSString *)CFURLCreateStringByAddingPercentEscapes(kCFAllocatorDefault, (__bridge CFStringRef)string, (__bridge CFStringRef)kCHCharactersToLeaveUnescaped, (__bridge CFStringRef)kCHCharactersToBeEscaped, CFStringConvertNSStringEncodingToEncoding(encoding));
}

#pragma mark -

@interface CHQueryStringPair : NSObject
@property (readwrite, nonatomic, strong) id field;
@property (readwrite, nonatomic, strong) id value;

- (id)initWithField:(id)field value:(id)value;

- (NSString *)URLEncodedStringValueWithEncoding:(NSStringEncoding)stringEncoding;
@end

@implementation CHQueryStringPair
@synthesize field = _field;
@synthesize value = _value;


- (id)initWithField:(id)field value:(id)value {
    self = [super init];
    if (!self) {
        return nil;
    }
    
    self.field = field;
    self.value = value;
    
    return self;
}

- (NSString *)URLEncodedStringValueWithEncoding:(NSStringEncoding)stringEncoding {
    if (!self.value || [self.value isEqual:[NSNull null]]) {
        return CHPercentEscapedQueryStringPairMemberFromStringWithEncoding([self.field description], stringEncoding);
    } else {
        return [NSString stringWithFormat:@"%@=%@", CHPercentEscapedQueryStringPairMemberFromStringWithEncoding([self.field description], stringEncoding), CHPercentEscapedQueryStringPairMemberFromStringWithEncoding([self.value description], stringEncoding)];
    }
}

@end

#pragma mark -

extern NSArray * CHQueryStringPairsFromDictionary(NSDictionary *dictionary);
extern NSArray * CHQueryStringPairsFromKeyAndValue(NSString *key, id value);

NSString * CHQueryStringFromParametersWithEncoding(NSDictionary *parameters, NSStringEncoding stringEncoding) {
    NSMutableArray *mutablePairs = [NSMutableArray array];
    for (CHQueryStringPair *pair in CHQueryStringPairsFromDictionary(parameters)) {
        [mutablePairs addObject:[pair URLEncodedStringValueWithEncoding:stringEncoding]];
    }
    
    return [mutablePairs componentsJoinedByString:@"&"];
}

NSArray * CHQueryStringPairsFromDictionary(NSDictionary *dictionary) {
    return CHQueryStringPairsFromKeyAndValue(nil, dictionary);
}

NSArray * CHQueryStringPairsFromKeyAndValue(NSString *key, id value) {
    NSMutableArray *mutableQueryStringComponents = [NSMutableArray array];
    
    if([value isKindOfClass:[NSDictionary class]]) {
        // Sort dictionary keys to ensure consistent ordering in query string, which is important when deserializing potentially ambiguous sequences, such as an array of dictionaries
        NSSortDescriptor *sortDescriptor = [NSSortDescriptor sortDescriptorWithKey:@"description" ascending:YES selector:@selector(caseInsensitiveCompare:)];
        [[[value allKeys] sortedArrayUsingDescriptors:[NSArray arrayWithObject:sortDescriptor]] enumerateObjectsUsingBlock:^(id nestedKey, NSUInteger idx, BOOL *stop) {
            id nestedValue = [value objectForKey:nestedKey];
            if (nestedValue) {
                [mutableQueryStringComponents addObjectsFromArray:CHQueryStringPairsFromKeyAndValue((key ? [NSString stringWithFormat:@"%@[%@]", key, nestedKey] : nestedKey), nestedValue)];
            }
        }];
    } else if([value isKindOfClass:[NSArray class]]) {
        [value enumerateObjectsUsingBlock:^(id nestedValue, NSUInteger idx, BOOL *stop) {
            [mutableQueryStringComponents addObjectsFromArray:CHQueryStringPairsFromKeyAndValue([NSString stringWithFormat:@"%@[]", key], nestedValue)];
        }];
    } else {
        [mutableQueryStringComponents addObject:[[CHQueryStringPair alloc] initWithField:key value:value]];
    }
    
    return mutableQueryStringComponents;
}

//--- The part above is from AFNetworking---

@interface IKOSDK ()

@property (nonatomic, strong) NSNumber *userId;
@property (nonatomic, strong) NSString *accessTokenKey;
@property (nonatomic, strong) NSString *accessTokenSecret;

@end

static NSString *cachedRequestTokenKey;
static NSString *cachedRequestTokenSecret;
static double requestTokenCacheTime = 0.0;

@implementation IKOSDK


//================================================================================
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//                             High Level API methods
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//================================================================================

// User methods ------------------------------------------------------------

/**
 * Register a new user for uGrocery.
 */
- (void)registerNewUser:(NSString *)loginEmail
              firstName:(NSString *)firstName
               lastName:(NSString *)lastName
               password:(NSString *)password
             postalCode:(NSString *)postalCode // XXX Maybe better as NSNumber ??
              promoCode:(NSString *)promoCode
            phoneNumber:(NSString *)phoneNumber
             newsletter:(BOOL)newsletter
             referredBy:(NSNumber *)referredBy
               callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    // Package the registration parameters in a call to the generic protected access request method
    NSDictionary *requestParams = [NSMutableDictionary dictionary];
    
    [requestParams setValue:loginEmail forKey:@"LoginEmail"];
    [requestParams setValue:firstName forKey:@"FirstName"];
    [requestParams setValue:lastName forKey:@"LastName"];
    [requestParams setValue:password forKey:@"Password"];
    [requestParams setValue:postalCode forKey:@"PostalCode"];
    [requestParams setValue:promoCode forKey:@"PromoCode"];
    [requestParams setValue:phoneNumber forKey:@"Phone"];
    [requestParams setValue:[NSNumber numberWithBool:newsletter] forKey:@"Newsletter"];
    [requestParams setValue:referredBy forKey:@"ReferredBy"];
    
    // ---------------------------------------------------------------------------------------------
    // @todo Implementing the same validation from the server will help conserve requests to the API
    // ---------------------------------------------------------------------------------------------
    // --- REQUIRED Registration Parameters ---
    //       * FirstName & LastName min length of 2 chars
    //       * Password strength check min length of 8 characters, 1 number, 1 upper case letter, 1 special character
    //       * Postal code 5 digits
    // --- OPTIONAL Registration Parameters ---
    //       * PromoCode can only be FFREE right now (some day should have a list / endpoint for them)
    //       * Phone number validation (server would like something such as 888-888-8888, no leading 1)
    //       * Newsletter boolean
    //       * RefferedBy Integer (existing user id)
    [self requestProtectedAccessResource:@"user/create.json" requestParams:requestParams callback:^(IKOError *error, IKOServerResponse *response)
     {
         if (completion)
             completion(error, response);
     }];
}

/**
 * Log an existing user into the API.
 * This is similar to requesting Protected Access resources, in that we start by obtaining a Request Token
 * However here, we use the Request Token to obtain an Access Token
 * @TODO There should be some sort of check to a local database for a stored access token first...
 */
- (void)loginUser:(NSString *)loginEmail
         password:(NSString *)password
         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    // Request Request Token
    __block IKOSDK *blocksafeSelf = self;
    
    // Acquire a Request Token, then use it to request an Access Token
    [self requestRequestToken:^(NSError *error, NSDictionary *responseParams)
     {
         [blocksafeSelf requestAccessToken:loginEmail
                                  password:password
                                oauthToken:[responseParams valueForKey:@"oauth_token"]
                          oauthTokenSecret:[responseParams valueForKey:@"oauth_token_secret"]
                             oauthVerifier:nil
                                completion:^(NSError *error, NSDictionary *response)
          {
              IKOServerResponse *r = [IKOServerResponse objectFromJSON:response];
              IKOError *e = [IKOError errorFromResponse:r andError:error];
              
              if (completion)
                  completion(e, r);
          }];
     }];
}

/**
 * When a user has forgotten their password, they can provide their email address.
 * The server will reset the password if the email is found and send the user an email
 * with a temporary password.
 */
- (void)forgotPassword:(NSString *)emailAddress
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:@{}];
    requestParams[@"LoginEmail"] = emailAddress;

    [self requestProtectedAccessResource:SF(@"user/forgot-password.json") requestParams:requestParams callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

/**
 * Additionally, once a password has been reset, the user may wish to change it
 * once the log in again.
 */
- (void)changePassword:(NSString *)oldPassword
           newPassword:(NSString *)newPassword
                  user:(IKOUser *)user
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:@{}];
    requestParams[@"OldPassword"]      = oldPassword;
    requestParams[@"NewPassword"]      = newPassword;

    [self requestPrivateAccessResource:SF(@"user/change-password.json")
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                         requestParams:requestParams
                              callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

/**
 * Once a user pays for a premium subscription we need to tell the server.
 * This will change their user_type to 'ugrocery-premium' on the server.
 */
- (void)premiumUpgrade:(IKOUser *)user
            expiration:(NSDate *)expirationDate
               receipt:(NSString *)receipt
      isSandboxReceipt:(BOOL)isSandboxReceipt
              callback:(void(^)(IKOError *error, IKOServerResponse *response))completion
{
    // Package the registration parameters in a call to the generic protected access request method
    NSDictionary *requestParams = [NSMutableDictionary dictionary];
    
    [requestParams setValue:receipt forKey:@"Receipt"];
    [requestParams setValue:[NSNumber numberWithBool:isSandboxReceipt] forKey:@"IsSandboxReceipt"];
    [requestParams setValue:[NSNumber numberWithDouble:[expirationDate timeIntervalSince1970]] forKey:@"ExpDate"];

    [self requestPrivateAccessResource:SF(@"user/premium-upgrade.json")
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                         requestParams:requestParams
                              callback:^(IKOError *error, IKOServerResponse *response) {
                                  if (completion)
                                      completion(error, response);
                              }];
}

// Find Lowest Bill methods ------------------------------------------------------------

- (void)findLowestBill:(NSArray *)productIds
              storeIds:(NSArray *)storeIds
                  user:(IKOUser *)user
              callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:@{@"ProductIds": productIds}];

    [self requestPrivateAccessResource:@"find-lowest-bill.json"
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                         requestParams:requestParams
                              callback:^(IKOError *error, IKOServerResponse *response)
     {
         if (completion)
             completion(error, response);
     }];
}

- (void)findLowestBillsDetailWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion {
    [self findLowestBillWithPayload:payload storeIds:storeIds user:user callback:completion andMethod:@"detail"];
}

- (void)findLowestBillsUpcDetailWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion {
    [self findLowestBillWithPayload:payload storeIds:storeIds user:user callback:completion andMethod:@"detail/upc"];
}

- (void)findLowestBillMyDealsWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion {
    [self findLowestBillWithPayload:payload storeIds:storeIds user:user callback:completion andMethod:@"deals"];
}

- (void)findDealsTopWithPayload:(NSDictionary *)payload storeIds:(NSArray *)storeIds user:(IKOUser *)user callback:(void (^)(IKOError *, IKOServerResponse *))completion
{
    [self findDealsWithPayload:payload storeIds:storeIds user:user callback:completion andMethod:@"top"];
}

- (void)findLowestBillWithPayload:(NSDictionary *)payload
              storeIds:(NSArray *)storeIds
                           user:(IKOUser *)user
                         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
                        andMethod:(NSString*)method
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:payload];
    requestParams[@"StoreIds"] = storeIds;
    
    [self requestPrivateAccessResource:SF(@"find-lowest-bill/%@.json", method)
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                           requestParams:requestParams
                                callback:^(IKOError *error, IKOServerResponse *response)
     {
         if (completion)
             completion(error, response);
     }];
}

// Deals methods ------------------------------------------------------------

- (void)findDealsWithPayload:(NSDictionary *)payload
                    storeIds:(NSArray *)storeIds
                        user:(IKOUser *)user
                    callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
                   andMethod:(NSString*)method
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:payload];
    requestParams[@"StoreIds"] = storeIds;
    
    [self requestPrivateAccessResource:SF(@"deals/%@.json", method)
                                tokenKey:user.oauthToken
                             tokenSecret:user.oauthSecret
                           requestParams:requestParams
                                callback:^(IKOError *error, IKOServerResponse *response)
     {
         if (completion)
             completion(error, response);
     }];
}

// UPC Capture methods ------------------------------------------------------------

- (void)submitUPC:(IKOPriceUpdate *)upcSubmission
             user:(IKOUser *)user
         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    [self requestPrivateAccessResource:SF(@"capture/upc.json")
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                         requestParams:[upcSubmission submissionPayload]
                              callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

- (void)submitUPCBulk:(NSDictionary *)bulkUpcSubmission
         callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    [self requestProtectedAccessResource:SF(@"capture/upc/bulk.json") requestParams:bulkUpcSubmission callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

// Search methods ------------------------------------------------------------

/**
 * Base search method, similar to findLowestBillWithPayload.
 * Essentially a private method...≈ß
 */
- (void) search:(NSString *)search
       storeIds:(NSArray *)storeIds
       l1CatIds:(NSArray *)l1CatIds
       l3CatIds:(NSArray *)l3CatIds
       brandIds:(NSArray *)brandIds
    withCoupons:(BOOL)withCoupons
withStoreBrands:(BOOL)withStoreBrands
           page:(NSNumber *)page
          limit:(NSNumber *)limit
           user:(IKOUser *)user
       callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
      andMethod:(NSString*)method
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:@{}];

    requestParams[@"StoreIds"]        = storeIds;
    requestParams[@"L1CatIds"]        = l1CatIds;
    requestParams[@"L3CatIds"]        = l3CatIds;
    requestParams[@"BrandIds"]        = brandIds;
    requestParams[@"WithCoupons"]     = withCoupons == YES ? @1 : @0;
    requestParams[@"WithStoreBrands"] = withStoreBrands == YES ? @1 : @0;
    requestParams[@"Page"]            = page;
    requestParams[@"Limit"]           = limit;
    requestParams[@"Search"]          = search;
    
    [self requestPrivateAccessResource:SF(@"search/%@.json", method)
                              tokenKey:user.oauthToken
                           tokenSecret:user.oauthSecret
                         requestParams:requestParams
                              callback:^(IKOError *error, IKOServerResponse *response)
     {
         if (completion)
             completion(error, response);
     }];
}

/**
 * Search for products. The search string may be emtpy.
 * Products will be returned in the same format as the FLB calls.
 * Products are sorted by L1-ASC on the server, so the order of the results
 * will be different across the pages from the client perspective than when using the offers endpoint.
 */
- (void)searchProducts:(NSString *)search
              storeIds:(NSArray *)storeIds
              l1CatIds:(NSArray *)l1CatIds
              l3CatIds:(NSArray *)l3CatIds
              brandIds:(NSArray *)brandIds
           withCoupons:(BOOL)withCoupons
       withStoreBrands:(BOOL)withStoreBrands
                  page:(NSNumber *)page
                 limit:(NSNumber *)limit
                  user:(IKOUser *)user
              callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    [self search:search
        storeIds:storeIds
        l1CatIds:l1CatIds
        l3CatIds:l3CatIds
        brandIds:brandIds
     withCoupons:withCoupons
 withStoreBrands:withStoreBrands
            page:page
           limit:limit
            user:user
        callback:completion
       andMethod:@"products"];
}

/**
 * XXX Work in progress .... not ready for use...
 *
 * Search for offers. The search string can be empty.
 * Really the main difference from searchProducts is the order of products among the pages.
 * with this endpoint, the results will be based on savings descending.
 */
- (void)searchOffers:(NSString *)search
            storeIds:(NSArray *)storeIds
            l1CatIds:(NSArray *)l1CatIds
            l3CatIds:(NSArray *)l3CatIds
            brandIds:(NSArray *)brandIds
         withCoupons:(BOOL)withCoupons
     withStoreBrands:(BOOL)withStoreBrands
                page:(NSNumber *)page
               limit:(NSNumber *)limit
                user:(IKOUser *)user
            callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    [self search:search
        storeIds:storeIds
        l1CatIds:l1CatIds
        l3CatIds:l3CatIds
        brandIds:brandIds
     withCoupons:withCoupons
 withStoreBrands:withStoreBrands
            page:page
           limit:limit
            user:user
        callback:completion
       andMethod:@"offers"];
}

// API version methods ------------------------------------------------------------
/**
 * A minimal version service. The server will return a payload with 2 fields, one is an integer
 * with the version on the server, the other a boolean indicating if an upgrade is required.
 */
- (void)getApiVersion:(void(^)(IKOError *error, IKOServerResponse *response))completion
{
    NSMutableDictionary *requestParams = [NSMutableDictionary dictionaryWithDictionary:@{}];
    [self requestProtectedAccessResource:SF(@"version.json") requestParams:requestParams callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

// Data export methods ------------------------------------------------------------
/**
 * Fetch data from the server in bulk.
 */

- (void)fetchBulkData:(NSString *)target
                 type:(NSString *)type
            timestamp:(NSString *)timestamp
             callback:(void (^)(IKOError *, IKOServerResponse *))completion
{
    if (timestamp) {
        [self fetchBulkData:@{ @"Target" : target, @"Type" : type, @"Timestamp" : timestamp } callback:completion];
    } else {
        [self fetchBulkData:@{ @"Target" : target, @"Type" : type } callback:completion];

    }
}

- (void)fetchBulkData:(NSDictionary *)requestParams // 'public' or 'admin' // One of brands, categories, category_joins, products, zips
             callback:(void(^)(IKOError *error, IKOServerResponse *response))completion
{
    [self requestProtectedAccessResource:SF(@"data/fetch.json") requestParams:requestParams callback:^(IKOError *error, IKOServerResponse *response) {
        if (completion)
            completion(error, response);
    }];
}

//================================================================================
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//                             Mid Level API methods
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//================================================================================

/**
 * Request a Protected Access resource from the API.
 *
 * This method makes a request for a Request Token,
 * then asynchronously makes another request for a Protected Access resource.
 */
- (void)requestProtectedAccessResource:(NSString *)requestURI
                         requestParams:(NSDictionary *)requestParams
                              callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    // Check the current timestamp against the Request Token cache timestamp
    double now = [[NSDate date] timeIntervalSince1970];
    
    // If the cached Request Token is stale (or we don't have one)
    // Acquire a Request Token, then use it to request a Protected Access resource
    if(requestTokenCacheTime == 0.0 || now - requestTokenCacheTime > 500.0) { // Slightly less than timeout on server to help with clock skew
        
        __block NSDictionary *blocksafeRequestParams = requestParams;

        // Acquire a Request Token
        [self requestRequestToken:^(NSError *error, NSDictionary *responseParams)
         {
             requestTokenCacheTime    = now;
             cachedRequestTokenKey    = [responseParams valueForKey:@"oauth_token"];
             cachedRequestTokenSecret = [responseParams valueForKey:@"oauth_token_secret"];
             
            // Send an API call against a Protected Access resource
            [self doRequestProtectedAccessResource:requestURI
                                     requestParams:blocksafeRequestParams
                                          tokenKey:cachedRequestTokenKey
                                       tokenSecret:cachedRequestTokenSecret
                                          callback:completion];
         }];
    }
    // Otherwise just go ahead and fetch the Protected Access resource, using the cached Request Token
    else {
        // Send an API call against a Protected Access resource
        [self doRequestProtectedAccessResource:requestURI
                                 requestParams:requestParams
                                      tokenKey:cachedRequestTokenKey
                                   tokenSecret:cachedRequestTokenSecret
                                      callback:completion];
    }
}

- (void)doRequestProtectedAccessResource:(NSString *)requestURI
                           requestParams:(NSDictionary *)requestParams
                                tokenKey:(NSString *)tokenKey
                             tokenSecret:(NSString *)tokenSecret
                                callback:(void (^)(IKOError *error, IKOServerResponse *response))completion
{
    //------------------------------------------------------------
    // Build the request
    //------------------------------------------------------------
    // Slice out the OAuth params so we can stuff the rest directly into the POST body
    NSMutableDictionary *payloadParams = [NSMutableDictionary dictionaryWithDictionary:requestParams];
    [payloadParams removeObjectForKey:@"oauth_token"];
    [payloadParams removeObjectForKey:@"oauth_token_secret"];
    
    NSMutableURLRequest *request =
    [IKOSDK buildApiOAuthRequest:[AUTH_URL stringByAppendingString:requestURI]
                  consumerSecret:CONSUMER_SECRET
                        tokenKey:tokenKey
                     tokenSecret:tokenSecret
                   oauthVerifier:nil
                 includeCallback:NO
                      httpMethod:@"POST"
                  requestPayload:payloadParams];
    request.timeoutInterval = NETWORK_TIMEOUT;
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [request addValue:version forHTTPHeaderField:@"x-application-version"];
    
    DDLogVerbose(@"Request URL: %@", [request URL]);
    
    //------------------------------------------------------------------------------
    // Finally, request a Protected Access resource --------------------------------
    // Send a follow-up request but do a POST request and pass the previously
    // acquired Request Token to access a 'Protected Access' resource on the server
    //------------------------------------------------------------------------------
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *r, NSData *responseData, NSError *error)
     {
         IKOServerResponse *serverResponse = [IKOServerResponse objectFromJSON:[IKOSDK jsonDecodeData:responseData]];
         IKOError *e = [IKOError errorFromResponse:serverResponse andError:error];
         completion(e, serverResponse);
     }];
}

/**
 * Request a Private Access resource from the API using an existing Access Token
 */
- (void)requestPrivateAccessResource:(NSString *)requestURI
                            tokenKey:(NSString *)tokenKey
                         tokenSecret:(NSString *)tokenSecret
                       requestParams:(NSDictionary *)requestParams
                            callback:(void (^)(IKOError *error, IKOServerResponse *responseParams))completion
{
    //------------------------------------------------------------
    // Build the request
    //------------------------------------------------------------
    // Slice out the OAuth params so we can stuff the rest directly into the POST body
    NSMutableDictionary *payloadParams = [NSMutableDictionary dictionaryWithDictionary:requestParams];
    
    NSMutableURLRequest *request =
    [IKOSDK buildApiOAuthRequest:[AUTH_URL stringByAppendingString:requestURI]
                  consumerSecret:CONSUMER_SECRET
                        tokenKey:tokenKey
                     tokenSecret:tokenSecret
                   oauthVerifier:nil
                 includeCallback:NO
                      httpMethod:@"POST"
                  requestPayload:payloadParams];
    
    NSString *version = [[[NSBundle mainBundle] infoDictionary] objectForKey:@"CFBundleShortVersionString"];
    [request addValue:version forHTTPHeaderField:@"x-application-version"];
    
    DDLogVerbose(@"Request URL: %@", [request URL]);
    request.timeoutInterval = NETWORK_TIMEOUT;
    
    //----------------------------------------------------------------------
    // Send a POST request to the server passing the previously tokenKey and
    // tokenSecret associated with the logged in user
    //----------------------------------------------------------------------
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *r, NSData *responseData, NSError *error)
     {
         NSDictionary *dictionary = [IKOSDK jsonDecodeData:responseData];
         IKOServerResponse *serverResponse = [IKOServerResponse objectFromJSON:dictionary];
         
         IKOError *e = [IKOError errorFromResponse:serverResponse andError:error];
         
         //this checks to see if the response data was a server error and fills the IKOError object appropriately
         NSDictionary *errorDict = dictionary[@"error"];
         if (errorDict) {
             e.errorDescription = errorDict[@"message"];
             e.responseErrorDescription = e.errorDescription;
         }
        
         completion(e, serverResponse);
     }];
}


/**
 * Build an Authorization header and apply it to a request.
 * XXX This method has not been scrubbed since the customization effort...
 */
+ (void)buildAuthHeader:(NSDictionary *)parameters request:(NSMutableURLRequest *)request
{
    NSMutableArray *parameterPairs = [NSMutableArray array];
    for (NSString *name in parameters) {
        NSString *aPair = [name stringByAppendingFormat:@"=\"%@\"", [parameters[name] utf8AndURLEncode]];
        [parameterPairs addObject:aPair];
    }
    NSString *oAuthHeader = [@"OAuth " stringByAppendingFormat:@"%@", [parameterPairs componentsJoinedByString:@", "]];
    [request setValue:oAuthHeader forHTTPHeaderField:@"Authorization"];
}

//================================================================================
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//                             Low Level API methods
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//================================================================================
/**
 * Request a Request Token from the server.
 * This can be subsequently be used to request a Protected Access resource or an Access Token
 */
- (void)requestRequestToken:(void(^)(NSError * error, NSDictionary *responseParams))completion
{
    //----------------------------------------
    // Build the request
    //----------------------------------------
    NSMutableURLRequest *request =
    [IKOSDK buildApiOAuthRequest:[AUTH_URL stringByAppendingString:REQUEST_TOKEN_URL]
                            consumerSecret:CONSUMER_SECRET
                                  tokenKey:nil
                               tokenSecret:nil
                             oauthVerifier:nil
                           includeCallback:YES
                                httpMethod:@"GET"
                            requestPayload:nil
     ];
    
    //-------------------------------------------------------------------------
    // Send the request for a Request Token delegating handling of the response
    //-------------------------------------------------------------------------
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error)
     {
         NSDictionary *responseParams = [IKOSDK jsonDecodeData:responseData];
         completion(error, responseParams);
     }];
}

/**
 * Using an existing Request Token, request an Access Token from the server.
 * The Access Token may then be used to request Private Access resources.
 * @note Currently this is hardcoded to operate against only the expclit Access Token resource
 */
- (void)requestAccessToken:(NSString *)loginEmail
                  password:(NSString *)password
                oauthToken:(NSString *)oauthToken
          oauthTokenSecret:(NSString *)oauthTokenSecret
             oauthVerifier:(NSString *)oauthVerifier
                completion:(void (^)(NSError *error, NSDictionary *responseParams))completion
{
    //----------------------------------------
    // Build the request w/ login credentials
    //----------------------------------------
    NSMutableDictionary *credentials = [NSMutableDictionary dictionary];
    [credentials setValue:loginEmail forKey:@"username"];
    [credentials setValue:password forKey:@"password"];
    
    NSMutableURLRequest * request =
    [IKOSDK buildApiOAuthRequest:[AUTH_URL stringByAppendingString:ACCESS_TOKEN_URL]
                            consumerSecret:CONSUMER_SECRET
                                  tokenKey:oauthToken
                               tokenSecret:oauthTokenSecret
                             oauthVerifier:oauthVerifier
                           includeCallback:NO
                                httpMethod:@"POST"
                            requestPayload:credentials
     ];
    
    //-------------------------------------------------------------------------
    // Send the request for an Access Token delegating handling of the response
    //-------------------------------------------------------------------------
    [NSURLConnection
     sendAsynchronousRequest:request
     queue:[NSOperationQueue mainQueue]
     completionHandler:^(NSURLResponse *response, NSData *responseData, NSError *error)
     {
         NSDictionary *responseParams = [IKOSDK jsonDecodeData:responseData];
         completion(error, responseParams);
     }];
}

//================================================================================
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//          Static methods that belong on a dedicated OAuthUtils class
//||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||||
//================================================================================
/**
 * A static method to encapsulate the boilerplate of request generation
 */
+ (NSMutableURLRequest *)buildApiOAuthRequest:(NSString *)apiUrl
                               consumerSecret:(NSString *)consumerSecret
                                     tokenKey:(NSString *)tokenKey
                                  tokenSecret:(NSString *)tokenSecret
                                oauthVerifier:(NSString *)oauthVerifier
                              includeCallback:(BOOL)includeCallback
                                   httpMethod:(NSString *)httpMethod
                               requestPayload:(NSDictionary *)requestPayload
{
    NSString *secretString = nil;
    
    // If the token secret is not nil then include it in the secret string
    if(tokenSecret == nil) {
        secretString = [consumerSecret.utf8AndURLEncode stringByAppendingString:@"&"];
    } else {
        secretString = [consumerSecret.utf8AndURLEncode stringByAppendingFormat:@"&%@", tokenSecret.utf8AndURLEncode];
    }
    
    // Start out with 'standard' oauth parameter set, conditionally including oauth_callback
    NSMutableDictionary *oauthParameters = [self.class standardOauthParameters:includeCallback];
    
    // Add the verifier if one has been supplied
    if(oauthVerifier != nil) {
        [oauthParameters setValue:oauthVerifier forKey:@"oauth_verifier"];
    }
    
    // Add the token key if one has been provided
    if(tokenKey != nil) {
        [oauthParameters setValue:tokenKey forKey:@"oauth_token"];
    }
    
    // Build the parameters as they will be sent over the wire
    NSString *parametersString = CHQueryStringFromParametersWithEncoding(oauthParameters, NSUTF8StringEncoding);
    
    // Build up the base string to sign the request
    NSString *baseString = [httpMethod stringByAppendingFormat:@"&%@&%@", apiUrl.utf8AndURLEncode, parametersString.utf8AndURLEncode];
    
    // Sign the request
    NSString *oauthSignature = [self.class signClearText:baseString withSecret:secretString];
    
    // Add the signature to the oauth parameters
    [oauthParameters setValue:oauthSignature forKey:@"oauth_signature"];
    
//    parametersString = CHQueryStringFromParametersWithEncoding(oauthParameters, NSUTF8StringEncoding);
    
    //-----------------------------------------
    // Create the request & set the HTTP method
    //-----------------------------------------
    NSMutableURLRequest *request = [NSMutableURLRequest requestWithURL:[NSURL URLWithString:apiUrl]];
    request.HTTPMethod = httpMethod;
    [request setValue:@"gzip" forHTTPHeaderField:@"Accept-Encoding"];
    
    //-----------------------------------------------------------------
    // Build the Authorization header
    //-----------------------------------------------------------------
    [IKOSDK buildAuthHeader:oauthParameters request:request];
    
    //---------------------------------------
    // Build the request payload
    //---------------------------------------
    if(requestPayload != nil) {
        NSData *jsonData = [self jsonEncodeDictionary:requestPayload];
        [request setHTTPBody:jsonData];
    }
    
    return request;
}

/**
 * A static method to convert NSData (presumably a Dictionary inside)
 * to an NSString and NSData representations of JSON
 */
+ (void) jsonEncodeData:(NSData *)jsonData
        jsonEncodedData:(NSData *)jsonEncodedData
             jsonString:(NSString *)jsonString
{
    NSError *error = nil;
    jsonEncodedData = [NSJSONSerialization dataWithJSONObject:jsonData
                                                      options:0
                                                        error:&error];
    
    // TODO Add some error handling if we fail to parse the JSON
    if(jsonData) {
        jsonString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    }
}


/**
 * A static method to convert NSDictionary to an NSData and NSString of JSON
 */
+ (NSData *) jsonEncodeDictionary:(NSDictionary *)jsonParameters
{
    NSError *error = nil;
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:jsonParameters
                                                       options:0
                                                         error:&error];
    
    if(!jsonData) {
        NSLog(@"Got an error: %@", error);
    }
    return jsonData;
}

/**
 * A static method to convert NSData of JSON to NSDictionary
 */
+ (NSDictionary *) jsonDecodeData:(NSData *)jsonData
{
    if (!jsonData)
        return nil;

    NSError *jsonError;
    id jsonResponseObject = [NSJSONSerialization JSONObjectWithData:jsonData
                                                            options:0
                                                              error:&jsonError];
    // TODO Error parsing / handling in case the serer responded with an error
    if([jsonResponseObject isKindOfClass: [NSDictionary class]]) {
        return (NSDictionary *)jsonResponseObject;
    }
    return nil;
}

+ (NSMutableDictionary *)standardOauthParameters:(BOOL)includeCallback
{
    NSString *oauth_timestamp = [NSString stringWithFormat:@"%lu", (unsigned long)[NSDate.date timeIntervalSince1970]];
    NSString *oauth_nonce = [NSString getNonce];
    NSString *oauth_consumer_key = CONSUMER_KEY;
    NSString *oauth_signature_method = @"HMAC-SHA1";
    NSString *oauth_version = @"1.0";
    NSString *oauth_callback = OAUTH_CALLBACK;
    
    NSMutableDictionary *standardParameters = [NSMutableDictionary dictionary];
    if(includeCallback == YES) {
        [standardParameters setValue:oauth_callback         forKey:@"oauth_callback"];
    }
    [standardParameters setValue:oauth_consumer_key     forKey:@"oauth_consumer_key"];
    [standardParameters setValue:oauth_nonce            forKey:@"oauth_nonce"];
    [standardParameters setValue:oauth_signature_method forKey:@"oauth_signature_method"];
    [standardParameters setValue:oauth_timestamp        forKey:@"oauth_timestamp"];
    [standardParameters setValue:oauth_version          forKey:@"oauth_version"];
    
    return standardParameters;
}

+ (NSString *)URLStringWithoutQueryFromURL:(NSURL *) url
{
    NSArray *parts = [[url absoluteString] componentsSeparatedByString:@"?"];
    return [parts objectAtIndex:0];
}


#pragma mark -
+ (NSString *)signClearText:(NSString *)text withSecret:(NSString *)secret
{
    NSData *secretData = [secret dataUsingEncoding:NSUTF8StringEncoding];
    NSData *clearTextData = [text dataUsingEncoding:NSUTF8StringEncoding];
    
    unsigned char result[20];
    hmac_sha1((unsigned char *)[clearTextData bytes], [clearTextData length], (unsigned char *)[secretData bytes], [secretData length], result);
    
    //Base64 Encoding
    char base64Result[32];
    size_t theResultLength = 32;
    Base64EncodeData(result, 20, base64Result, &theResultLength);
    NSData *theData = [NSData dataWithBytes:base64Result length:theResultLength];
    
    return [NSString.alloc initWithData:theData encoding:NSUTF8StringEncoding];
}

@end
