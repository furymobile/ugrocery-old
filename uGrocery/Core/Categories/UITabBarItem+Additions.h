//
//  UITabBarItem+Additions.h
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <UIKit/UIKit.h>

@interface UITabBarItem (Additions)

-(void)setCustomBadgeValue:(NSString *)value;

-(void)setCustomBadgeValue:(NSString *)value
                  withFont:(UIFont *)font
              andFontColor:(UIColor *)color
        andBackgroundColor:(UIColor *)backColor;

@end
