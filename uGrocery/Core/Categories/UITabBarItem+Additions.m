//
//  UITabBarItem+CustomBadge.m

#import "UITabBarItem+Additions.h"

NSUInteger const CUSTOM_BADGE_TAG = 99;

@implementation UITabBarItem (CustomBadge)

-(void)setCustomBadgeValue:(NSString *)value
{
    [self setCustomBadgeValue:value
                     withFont:[UIFont systemFontOfSize:13.0]
                 andFontColor:[UIColor whiteColor]
           andBackgroundColor:[UIColor lightGrayColor]];
}

-(void)setCustomBadgeValue:(NSString *)value
                  withFont:(UIFont *)font
              andFontColor:(UIColor *)color
        andBackgroundColor:(UIColor *)backColor
{
    UIView *v = [self valueForKey:@"view"];
    
    [self setBadgeValue:value];
    
    for(UIView *sv in v.subviews)
    {
        if(sv.tag == CUSTOM_BADGE_TAG)
        {
            [sv removeFromSuperview];
            continue;
        }
    }
    
    for(UIView *sv in v.subviews)
    {
        if([NSStringFromClass([sv class]) isEqualToString:@"_UIBadgeView"])
        {
            UILabel *l = [[UILabel alloc] initWithFrame:CGRectMake(0, 0, sv.frame.size.width, sv.frame.size.height)];
            
            [l setFont:font];
            [l setText:value];
            [l setBackgroundColor:backColor];
            [l setTextColor:color];
            [l setTextAlignment:NSTextAlignmentCenter];
            
            l.layer.cornerRadius = l.frame.size.height/2;
            l.layer.masksToBounds = YES;
            
            sv.layer.borderWidth = 1;
            sv.layer.borderColor = [backColor CGColor];
            sv.layer.cornerRadius = sv.frame.size.height/2;
            sv.layer.masksToBounds = YES;
            
            [sv addSubview:l];
            l.tag = CUSTOM_BADGE_TAG;
        }
    }
}




@end
