//
//  UIFont+uGrocery.m
//  uGrocery
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "UIFont+uGrocery.h"

@implementation UIFont (uGrocery)

+ (void)logAvailableFonts
{
	NSArray *familyNames = [[NSArray alloc] initWithArray:[UIFont familyNames]];
	for (NSUInteger indFamily = 0; indFamily < [familyNames count]; ++indFamily) {
		NSLog(@"Family name: %@", [familyNames objectAtIndex:indFamily]);
		NSArray *fontNames = [[NSArray alloc] initWithArray:
				[UIFont fontNamesForFamilyName:
						[familyNames objectAtIndex:indFamily]]];
		for (NSUInteger indFont = 0; indFont < [fontNames count]; ++indFont) {
			NSLog(@"    Font name: %@", [fontNames objectAtIndex:indFont]);
		}
	}
}

+ (UIFont*)IKOLightFontOfSize:(CGFloat)fontSize
{
	return [UIFont fontWithName:@"Roboto-Thin" size:fontSize];
}

+ (UIFont*)IKOMediumFontOfSize:(CGFloat)fontSize
{
	return [UIFont fontWithName:@"Roboto-Light" size:fontSize];
}

+ (UIFont*)IKOHeavyFontOfSize:(CGFloat)fontSize
{
	return [UIFont fontWithName:@"Roboto-Regular" size:fontSize];
}

+ (UIFont*)IKOItalicFontOfSize:(CGFloat)fontSize
{
	return [UIFont fontWithName:@"HelveticaNeue-LightItalic" size:86];
}


@end
