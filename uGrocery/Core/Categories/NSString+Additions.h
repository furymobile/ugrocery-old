//
//  NSString+Additions.h
//  uGrocery
//
//  Created by Duane Schleen on 1/22/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Additions)

- (NSString *)stripPunctuation;
- (NSArray *)arrayBySplittingIntoWords;

@end
