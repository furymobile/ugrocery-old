//
//  UIScreen+Additions.m
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "UIScreen+Additions.h"

@implementation UIScreen (Additions)

+ (CGSize)currentSize
{
    return [[UIScreen mainScreen] bounds].size;
}

+ (CGFloat)currentScale
{
    return [UIScreen mainScreen].scale;
}

@end
