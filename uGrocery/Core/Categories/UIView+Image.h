//
//  UIView+Image.h
//  Copyright (c) 2013 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <Foundation/Foundation.h>

@interface UIView (Image)

- (UIImage *)renderAsImage;

@end
