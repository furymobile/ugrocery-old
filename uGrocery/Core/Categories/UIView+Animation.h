//
//  UIView+Animation.h
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import <Foundation/Foundation.h>

@interface UIView (Animation)

- (void)spinWithDuration:(CGFloat)duration
               rotations:(CGFloat)rotations
                  repeat:(float)repeat;

@end
