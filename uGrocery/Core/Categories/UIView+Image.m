//
//  UIView+Image.m
//  Copyright (c) 2013 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "UIView+Image.h"

@implementation UIView (Image)

- (UIImage *)renderAsImage
{
	UIGraphicsBeginImageContextWithOptions(self.bounds.size, NO, 0.0);
	[self.layer renderInContext:UIGraphicsGetCurrentContext()];
	UIImage *img = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	return img;
}


@end
