//
//  UIViewController+Utilities.m
//  fury-utilities
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//

#import "UIViewController+Utilities.h"
#import "UIColor+uGrocery.h"

@implementation UIViewController (Utilities)

- (void)setupViewController
{
    
	UIBarButtonItem *backItem = [[UIBarButtonItem alloc] initWithTitle:@"back"
	                                                             style:UIBarButtonItemStylePlain
		                                                        target:nil
			                                                    action:nil];
	backItem.accessibilityLabel = @"<";
	[self.navigationItem setBackBarButtonItem:backItem];
    self.navigationController.navigationBar.tintColor = [UIColor UGBlue];
    self.view.backgroundColor = [UIColor UGReallyLtGrey];
}

@end
