//
//  IKOFLBHelper.h
//  uGrocery
//
//  Created by Duane Schleen on 8/22/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IKOServerResponse;
@class IKOProduct;

@interface IKOFLBHelper : NSObject

@property (nonatomic) NSDictionary *flbData;

- (id)initWithServerResponse:(IKOServerResponse *)response;

- (NSArray *)storesFor:(NSNumber *)productId;
- (NSString *)priceForProductId:(NSNumber *)productId inStore:(NSString *)storeId;
- (BOOL)isWinnerForProductId:(NSNumber *)productId inStore:(NSString *)storeId;
- (BOOL)hasSubstitutes;
- (NSUInteger)countOfProducts;

#pragma mark - Summary

- (NSArray *)summaryStores;
- (NSString *)summaryWinner;
- (NSString *)savingsDelta;
- (NSString *)summaryPriceForStore:(NSString *)storeId;
- (NSDictionary *)productNodeForProductId:(NSNumber*)productId;

#pragma mark - Coupons and Deals

- (NSDictionary *)saleDataForProductId:(NSNumber *)productId atStoreId:(NSString *)storeId;
- (NSArray *)couponsDataForProductId:(NSNumber *)productId atStoreId:(NSString *)storeId;

#pragma mark - Deals

- (NSArray *)categoriesForDeals;
- (NSArray *)productFor:(NSNumber *)productId;
- (NSArray *)productsForDealsCategory:(NSNumber *)categoryId;
- (NSDictionary *)productNodeForCategoryId:(NSNumber *)categoryId andProductId:(NSNumber *)productId;
//- (NSArray*)storesForProductNodeForCategoryId:(NSNumber*)categoryId andProductId:(NSNumber*)productId;
- (NSString *)maxSavingsFor:(NSNumber *)productId atStoreId:(NSString *)storeId;

@end
