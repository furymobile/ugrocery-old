//
//  IKOAnalytics.m
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

//#ifdef DEBUG
#define kFlurryAPIKey @"GMN2BSJNSQ5GSG3NVJ5J"  //dev
//#else
//#define kFlurryAPIKey @"RKK8YF2SF94K8PQ6PTKG"  //prod
//#endif

#import "IKOAnalytics.h"
#import "Flurry.h"
#import <CoreLocation/CoreLocation.h>
#import "ATConnect.h"


@implementation IKOAnalytics

#pragma mark - Singleton methods

+ (IKOAnalytics *)sharedInstance {
    static IKOAnalytics *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc]init];
        [Flurry setCrashReportingEnabled:YES];
        [Flurry startSession:kFlurryAPIKey];
    });

    return sharedInstance;
}

+ (void)setLocation:(CLLocation *)location {
    [IKOAnalytics sharedInstance];
    [Flurry setLatitude:location.coordinate.latitude
    longitude:location.coordinate.longitude
    horizontalAccuracy:(float) location.horizontalAccuracy
    verticalAccuracy:(float) location.verticalAccuracy];
}

+ (void)logEvent:(NSString *)eventName
  withParameters:(NSDictionary *)parameters {
    NSAssert(eventName, @"eventName cannot be nil");
    [IKOAnalytics sharedInstance];

    NSMutableDictionary *params = [NSMutableDictionary dictionaryWithDictionary:parameters];
    [params setObject:@"uGrocery iOS" forKey:@"Product Name"];

    [Flurry logEvent:eventName withParameters:params];
}

#pragma mark - Errors and Exceptions

+ (void)logException:(NSString *)eventName
  withMessage:(NSString *)message
  andException:(NSException *)exception {
    NSAssert(eventName, @"eventName cannot be nil");
    NSAssert(message, @"message cannot be nil");
    NSAssert(exception, @"exception cannot be nil");

    DDLogError(@"*FATAL* Uncaught Exception: %@, Event: %@, Reason: %@", exception.name, eventName, exception.reason);

    [IKOAnalytics sharedInstance];

    [Flurry logError:eventName message:message exception:exception];
}

+ (void)logError:(NSString *)eventName
  withMessage:(NSString *)message
  andError:(NSError *)error {
    NSAssert(eventName, @"eventName cannot be nil");
    [IKOAnalytics sharedInstance];

    [Flurry logError:eventName message:message error:error];
}

+ (void)logError:(NSString *)description
  withErrorActivity:(NSString *)errorActivity {
    NSAssert(description, @"description cannot be nil");
    NSAssert(errorActivity, @"errorActivity cannot be nil");

    [IKOAnalytics logEvent:@"Activity" withParameters:@{@"Description": description, @"Error Activity": errorActivity}];
}

#pragma mark - One Time Events

+ (void)logOneTimeEvent:(NSString *)eventName
  withParameters:(NSDictionary *)parameters {
//	NSAssert(eventName, @"eventName cannot be nil");
//	NSAssert(parameters, @"parameters cannot be nil");
//
////	SettingsStorageService *settingsService = [[StorageManager sharedInstance] serviceForClass:[NSObject class]];
//
//	NSString *description = [parameters valueForKey:@"Description"];
//
//	NSAssert(description, @"parameters needs to contain a key named Description");
//
//	NSUInteger eventKey = [[NSString stringWithFormat:@"EVENT:%@%@", eventName, description] hash];
//
//	NSString *key = [NSString stringWithFormat:@"%d", eventKey];
//
//	if ([settingsService hasSettingForKey:key])
//		return;
//
//	[IKOAnalytics logEventNew:eventName withParameters:parameters];
//	[settingsService setString:@"YES" forKey:key];
}

+ (void)logApptentiveEvent:(NSString *)eventName fromViewController:viewController {
   [[ATConnect sharedConnection] engage:eventName fromViewController:viewController];
}

#pragma mark - Activated Event

+ (void)logActivatedEvent {
    [IKOAnalytics logOneTimeEvent:@"Activated" withParameters:@{@"Description": @"Activated app"}];
}

@end
