//
//  IKOUtilities.h
//  uGrocery
//
//  Created by Duane Schleen on 9/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@class IKOProduct;

@interface IKOUtilities : NSObject

+ (NSAttributedString *)attributedLabelFor:(NSString *)description
  andMeasure:(NSString *)measure
  titleSize:(int)size;

+ (void)animateAddToCartInView:(UIView *)targetView
  imageviewToAnimate:(UIImageView *)imageView
  animateToPoint:(CGPoint)endPoint;

+ (CGRect)frameForTabInTabBar:(UITabBar *)tabBar
  withIndex:(NSUInteger)index
  inView:(UIView *)targetView;

+ (BOOL)hasCameraPermission;

+ (UIImage *)blurView:(UIView *)view;

+ (BOOL)notConnectedAlert;

@end
