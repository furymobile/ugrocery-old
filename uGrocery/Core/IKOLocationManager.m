//
//  IKOLocationManager.m
//  uGrocery
//
//  Created by Duane Schleen on 6/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOLocationManager.h"

@interface IKOLocationManager ()

@property (nonatomic) CLLocationManager *locationManager;

@end

@implementation IKOLocationManager

+ (IKOLocationManager *)sharedInstance {
    static IKOLocationManager *sharedInstance = nil;
    static dispatch_once_t oncePredicate;
    dispatch_once(&oncePredicate, ^{
        sharedInstance = [[self alloc]init];
        sharedInstance.locationManager = [[CLLocationManager alloc]init];
        // Check for iOS 8. Without this guard the code will crash with "unknown selector" on iOS 7.
        if ([sharedInstance.locationManager respondsToSelector:@selector(requestWhenInUseAuthorization)]) {
            [sharedInstance.locationManager requestWhenInUseAuthorization];
        }
        [sharedInstance.locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
    });
    return sharedInstance;
}

- (void)startUpdatingLocation {
    [_locationManager startUpdatingLocation];
    [IKOAnalytics setLocation:[[IKOLocationManager sharedInstance]getCurrentLocation]];
    DDLogVerbose(@"Started/Resumed tracking location");
}

- (void)stopUpdatingLocation {
    if (_locationManager)
        [_locationManager stopUpdatingLocation];
    DDLogVerbose(@"Stopped tracking location");
}

- (void)resumeUpdatingLocation {
    if (_locationManager) {
        [_locationManager startUpdatingLocation];
        DDLogVerbose(@"Started/Resumed tracking location");
    } else {
        [self startUpdatingLocation];
    }
}

- (CLLocation *)getCurrentLocation {
    return (_locationManager) ? _locationManager.location : nil;
}

- (void)getPlacemark:(void (^)(NSError *error, CLPlacemark *placemark))completion {
    CLGeocoder *geocoder = [[CLGeocoder alloc]init];
    [geocoder reverseGeocodeLocation:[self getCurrentLocation]
    completionHandler:^(NSArray *placemarks, NSError *error)
    {
        if (error) {
            DDLogError(@"Geocode failed with error: %@", error);
            if (completion) completion(error, nil);
            return;
        }
        if (completion) completion(nil, [placemarks objectAtIndex:0]);
    }];
}

- (BOOL)isEnabled {
    if([CLLocationManager locationServicesEnabled] &&
      [CLLocationManager authorizationStatus] != kCLAuthorizationStatusDenied) {
        return YES;
    }
    return NO;
}

@end
