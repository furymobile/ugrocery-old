//
//  IKORegex.h
//  uGrocery
//
//  Created by Duane Schleen on 8/6/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#ifndef uGrocery_IKORegex_h
#define uGrocery_IKORegex_h

#define kValidateEmailRegex @"^[^\\s]+@[^\\.][^\\s]{0,}\\.[A-Za-z]{1,10}$"

#endif
