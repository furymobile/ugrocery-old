//
//  IKOAddToShoppingListProtocol.h
//  uGrocery
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>

@protocol IKOAddToShoppingListProtocol <NSObject>

@required

- (void)showToastWithImageView:(UIImageView *)image andMessage:(NSString *)message;

@optional

- (void)addToShoppingList:(id)object;
- (void)quantityChanged:(NSNumber *)quantity;
- (void)markAsFavorite:(id)object isFavorite:(BOOL)favorite;

@end
