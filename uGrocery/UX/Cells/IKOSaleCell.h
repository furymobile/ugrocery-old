//
//  IKOSaleCell.h
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOSaleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UILabel *expiration;
@property (weak, nonatomic) IBOutlet UILabel *regularPrice;
@property (weak, nonatomic) IBOutlet UILabel *salesPrice;
@property (weak, nonatomic) IBOutlet UILabel *minimumQuantity;
@property (weak, nonatomic) IBOutlet UILabel *saleDescription;

@end
