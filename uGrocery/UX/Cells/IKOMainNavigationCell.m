//
//  IKOMainNavigationCell.m
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import "IKOMainNavigationCell.h"

@implementation IKOMainNavigationCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
