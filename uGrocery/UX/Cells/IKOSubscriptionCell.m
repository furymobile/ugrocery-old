//
//  IKOSubscriptionCell.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKOSubscriptionCell.h"
#import "IAPHelper.h"
#import "IAPShare.h"

@interface IKOSubscriptionCell ()

@property (nonatomic) SKProduct* product;
@end
@implementation IKOSubscriptionCell

- (void)configureCellWith:(SKProduct *)product {
    self.product = product;
    self.productName.text = product.localizedDescription;
    self.price.text = [self formattedPrice:product];
}

- (IBAction)makePurchase:(id)sender {
    [_delegate purchaseProduct:_product];
}

- (NSString *)formattedPrice:(SKProduct *)product {
    NSNumberFormatter *numberFormatter = [[NSNumberFormatter alloc]init];
    [numberFormatter setFormatterBehavior:NSNumberFormatterBehavior10_4];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setLocale:product.priceLocale];
    return [numberFormatter stringFromNumber:product.price];
}

-(void)prepareForReuse {
    _product = nil;
    _productName.text = @"";
    _price.text = @"";
}

@end
