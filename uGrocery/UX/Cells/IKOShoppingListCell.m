//
//  IKOShoppingListCell.m
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOShoppingListCell.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "Haneke.h"
#import "IKOUtilities.h"
#import "IKOProductDataStore.h"
#import "IKODataStorageManager+uGrocery.h"

@implementation IKOShoppingListCell {
    IKOShoppingListItem *_item;
    UILabel *_descriptionLabel;
}

//- (instancetype)initWithCoder:(NSCoder *)coder
//{
//    self = [super initWithCoder:coder];
//    if (self) {
//        if (NSFoundationVersionNumber <= NSFoundationVersionNumber_iOS_7_1)
//        {
//
//            UIView *cellContentView = self.contentView;
//            cellContentView.translatesAutoresizingMaskIntoConstraints = NO;
//
//            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"H:|[cellContentView]|"
//                                                                         options:0
//                                                                         metrics:0
//                                                                           views:NSDictionaryOfVariableBindings(cellContentView)]];
//            [self addConstraints:[NSLayoutConstraint constraintsWithVisualFormat:@"V:|[cellContentView]|"
//                                                                         options:0
//                                                                         metrics:0
//                                                                           views:NSDictionaryOfVariableBindings(cellContentView)]];
//
//        }
//    }
//    return self;
//}


- (void)configureWith:(IKOShoppingListItem *)item {
    _item = item;

    [self drawCountText:[item.quantity stringValue]];

    if (item.imageUri)
        [_productImage hnk_setImageFromURL:[NSURL URLWithString:SF(kProductImageUrl, item.imageUri, item.productId)]
        placeholder:[UIImage imageNamed:@"noproductimage"]];
    [self configureWithPrice:nil];
    self.selectionStyle = UITableViewCellSelectionStyleGray;
    self.accessoryType = UITableViewCellAccessoryDisclosureIndicator;

    if ([item.isAdhoc integerValue] == 1) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }

    if ([item.crossedOff integerValue] == 1) {
        self.accessoryType = UITableViewCellAccessoryNone;
        self.selectionStyle = UITableViewCellSelectionStyleNone;
    }
}

- (void)buildDescriptionLabelWithText:(NSString *)text {
    [_descriptionLabel removeFromSuperview];
    _descriptionLabel = [[UILabel alloc]initWithFrame:CGRectMake(101, 6, _productDescription.frame.size.width, 78)];
    _descriptionLabel.textColor = [UIColor IKODarkGrayColor];
    _descriptionLabel.font = [UIFont IKOMediumFontOfSize:13];
    _descriptionLabel.text = text;
    _descriptionLabel.numberOfLines = 4;
    [self.contentView addSubview:_descriptionLabel];
}

- (void)configureWithPrice:(NSString *)price {
    if (_item.productId && [_item.productId integerValue] == 0) {
        _productDescription.text = nil;
        [self buildDescriptionLabelWithText:_item.listItemDescription];
    } else {
        _productDescription.text = _item.listItemDescription;
    }
    _measure.text = _item.measureText;

    _price.hidden = [_item.productId integerValue] == 0;
    _price.text = price;

    if ([price isEqualToString:@"NC"]) {
        _notComparable.hidden = NO;
        _price.hidden = YES;
    } else {
        _notComparable.hidden = YES;
        _price.hidden = NO;
    }
}

- (void)drawCountText:(NSString *)text {
    UIView *countView = [[UIView alloc]init];
    countView.frame = CGRectMake(0, 0, 30, 30);
    countView.backgroundColor = [UIColor clearColor];

    UILabel *count = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    count.text = text;
    count.textAlignment = NSTextAlignmentCenter;
    count.font = [UIFont IKOMediumFontOfSize:14];
    count.textColor = [UIColor UGBlue];

    UIImageView *circleImage = [[UIImageView alloc]initWithFrame:countView.frame];
    circleImage.image = [_item.crossedOff boolValue] ?[UIImage imageNamed:@"checkbox-checked"] :[UIImage imageNamed:@"checkbox-unchecked"];

    [countView addSubview:circleImage];
    if (![_item.crossedOff boolValue] && ([_item.quantity intValue]  > 1))
        [countView addSubview:count];
    [_quantityView addSubview:countView];
}

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (void)prepareForReuse {
    _item = nil;
    _productImage.image = [UIImage imageNamed:@"noproductimage"];
    [_descriptionLabel removeFromSuperview];
    _descriptionLabel = nil;
    _price.text = nil;
    _price.hidden = NO;
    _notComparable.hidden = YES;
    [_quantityView.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
}

- (IBAction)crossOff:(id)sender {
    [_quantityView.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    _item.crossedOff = [NSNumber numberWithBool:![_item.crossedOff boolValue]];
    [UIView animateWithDuration:0.5f animations:^{
        [self drawCountText:[_item.quantity stringValue]];
    } completion:^(BOOL finished) {
        [_crossOffDelegate crossOff:_item crossed:![_item.crossedOff boolValue] sender:self];
    }];
}

@end
