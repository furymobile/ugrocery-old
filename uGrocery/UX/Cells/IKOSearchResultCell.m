//
//  IKOSearchResultCell.m
//  uGrocery
//
//  Created by Duane Schleen on 7/24/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSearchResultCell.h"
#import "IKOProduct.h"
#import "IKOProductCategory.h"
#import "IKOUtilities.h"
#import "UIFont+uGrocery.h"
#import "Haneke.h"
#import "UIColor+uGrocery.h"
#import "IKODataStorageManager+uGrocery.h"

@interface IKOSearchResultCell ()

@property (nonatomic) UIView* containerView;

@end

@implementation IKOSearchResultCell {
    id _model;
    BOOL _selected;
    UIImageView *_circleImage;
    BOOL _isMarkedFavorite;
}

- (void)configureWithSearchItem:(IKOSearchItem *)item {
    _model = item;
    _descriptionText.text = item.itemDescription;
    _measure.text = item.measureText;
    [_addToShoppingList addTarget:self action:@selector(addToShoppingList:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configureWithSearchItem:(IKOSearchItem *)item andCurrentSearchTerm:(NSString *)term {
    [self configureWithSearchItem:item];
    NSMutableAttributedString *mutable = [[NSMutableAttributedString alloc]initWithString:[item.itemDescription lowercaseString]];
    [mutable addAttribute:NSFontAttributeName value:[UIFont IKOHeavyFontOfSize:16] range:[[item.itemDescription uppercaseString]rangeOfString:[term uppercaseString]]];
    _descriptionText.attributedText = mutable;
}

- (void)configureWithProduct:(IKOProduct *)product {
    _model = product;
    if (product.imageUri && [product.hasImage integerValue] == 1)
        [_resultImage hnk_setImageFromURL:[NSURL URLWithString:SF(kProductImageUrl, product.imageUri, product.productId)] placeholder:[UIImage imageNamed:@"noproductimage"]];
    _descriptionText.text = product.productDescription;
    _measure.text = product.measureText;

#if TARGET_IPHONE_SIMULATOR
    _productId.text = [product.productId stringValue];
   // _productId.hidden = NO;
#endif
    
    if ([product.maxSavings intValue] > 0) {
        self.maxSavings.hidden = NO;
        self.storeLogo.hidden = NO;
        
        IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOStore class]];
        
        NSMutableAttributedString *saleText = [[NSMutableAttributedString alloc]initWithString:SF(@"SAVE UP TO %d%%", [product.maxSavings intValue])];
        NSMutableParagraphStyle *paraStyle = [[NSMutableParagraphStyle alloc]init];
        paraStyle.alignment = NSTextAlignmentRight;
        [saleText addAttribute:NSParagraphStyleAttributeName value:paraStyle range:NSMakeRange(0, saleText.length)];
        [saleText addAttribute:NSForegroundColorAttributeName value:[UIColor IKODarkGrayColor] range:NSMakeRange(0,10)];
        [saleText addAttribute:NSForegroundColorAttributeName value:[UIColor UGGreen] range:NSMakeRange(11,saleText.length-11)];
        [saleText addAttribute:NSFontAttributeName value:[UIFont IKOMediumFontOfSize:15] range:NSMakeRange(0,10)];
        [saleText addAttribute:NSFontAttributeName value:[UIFont IKOMediumFontOfSize:18] range:NSMakeRange(11,saleText.length-11)];

        self.maxSavings.attributedText = saleText;
        self.storeLogo.image = [storeDataStore largerStoreImageFor:product.winningStore];
        
    }
    
    [_addToShoppingList addTarget:self action:@selector(addToShoppingList:) forControlEvents:UIControlEventTouchUpInside];
}

- (void)configurewWithAdhocProduct:(IKOProductCategory *)category {
    [self selectItem:NO];
    _model = category;
    _descriptionText.text = SF(@"You can add '%@' to your list anyway.  You can also search for products named '%@' by tapping the Search button on the keyboard.", category.name, category.name);
    [_addToShoppingList addTarget:self action:@selector(addToShoppingList:) forControlEvents:UIControlEventTouchUpInside];
    self.selectionStyle = UITableViewCellSelectionStyleNone;
}

- (void)setFavorite:(BOOL)favorite {
    _isMarkedFavorite = favorite;
    [_markFavorite setBackgroundImage:(favorite) ?[UIImage imageNamed:@"icon-heart-selected"] :[UIImage imageNamed:@"icon-heart"] forState:UIControlStateNormal];
}

- (void)setQuantity:(NSNumber *)quantity {
    _quantity = quantity;
    _quantityField.text = SF(@"%@", _quantity);
    _quantityStepper.value = [_quantity doubleValue];
    _quantityStepper.minimumValue = 1;
}

- (void)selectItem:(BOOL)selected {
    _selected = selected;

    if (_circleImage)[_circleImage removeFromSuperview];

    _circleImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    _circleImage.image = selected ?[UIImage imageNamed:@"checkbox-checked"] :[UIImage imageNamed:@"addbox"];

    [_quantityView addSubview:_circleImage];
}

- (IBAction)addToShoppingList:(id)sender {
    _selected = YES;
    [self selectItem:_selected];
    [_delegate addToShoppingList:_model];
    [self showToast];
}

- (void)showToast {
    [_delegate showToastWithImageView:_resultImage andMessage:@"Added to Shopping List..."];
}

- (void)prepareForReuse {
    _selected = NO;
    _maxSavings.hidden = YES;
    _maxSavings.text = nil;
    _descriptionText.text = nil;
    _storeLogo.image = nil;
    [[_containerView subviews]enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    [_containerView removeFromSuperview];
    [_addToShoppingList removeTarget:self action:@selector(addToShoppingList:) forControlEvents:UIControlEventTouchUpInside];
    _resultImage.image = [UIImage imageNamed:@"noproductimage"];
    [_circleImage removeFromSuperview];
    _model = nil;
}

- (IBAction)quantityStepper:(id)sender {
    _quantity = [NSNumber numberWithDouble:_quantityStepper.value];
    _quantityField.text = SF(@"%@", _quantity);
    [_delegate quantityChanged:_quantity];
}

- (IBAction)markFavorite:(id)sender {
    [self setFavorite:!_isMarkedFavorite];
    [_delegate markAsFavorite:_model isFavorite:_isMarkedFavorite];
}

@end
