//
//  IKODealsProductCell.m
//  uGrocery
//
//  Created by Duane Schleen on 9/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODealsProductCell.h"
#import "IKOProduct.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "Haneke.h"
#import "IKODataStorageManager+uGrocery.h"
#import "NSArray+Additions.h"

@interface IKODealsProductCell ()

@property (nonatomic) BOOL cellOnSale;
@property (nonatomic) BOOL cellHasCoupons;
@property (nonatomic) UIView *containerView;

@end
@implementation IKODealsProductCell {
    IKOProduct *_product;
    UIImageView *_circleImage;
    BOOL _selected;
    NSDictionary *_productNode;
    NSArray *_stores;
    UIView *_storeLogos;
}

- (void)configureWith:(IKOProduct *)product andProductNode:(NSDictionary *)productNode {
    _product = product;
    _productNode = productNode;

    [self selectItem:NO];

    if (product.imageUri && [product.hasImage integerValue] == 1)
        [_productImage hnk_setImageFromURL:[NSURL URLWithString:SF(kProductImageUrl, product.imageUri, product.productId)] placeholder:[UIImage imageNamed:@"noproductimage"]];
    _descriptionLabel.text = product.productDescription;
    _discount.text = SF(@"SAVE UP TO %@%%", productNode[@"MaxSavings"]);

    NSMutableArray __block *tempStores = [NSMutableArray new];

    [_productNode enumerateKeysAndObjectsUsingBlock:^(id key, NSDictionary *obj, BOOL *stop) {
        if ([key integerValue]) {
            BOOL onSale = [obj[@"IsOnSale"] boolValue];
            BOOL hasCoupons = [obj[@"HasCoupons"] boolValue];

            if (onSale | hasCoupons)
                [tempStores addObject:key];

            if (onSale)
                _cellOnSale = YES;
            if (hasCoupons)
                _cellHasCoupons = YES;
        }
    }];

    [self buildStoreImagesFor:tempStores];

    _stores = [tempStores sortedArray];
    [self layoutDealIcons];
}

- (void)buildStoreImagesFor:(NSArray *)stores {
    static CGFloat heightContstraint = 12;

    CGFloat __block totalWidth = 0;
    NSMutableArray __block *storeImageViews = [NSMutableArray new];
    IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];

    [stores enumerateObjectsUsingBlock:^(NSString *storeId, NSUInteger idx, BOOL *stop) {
        UIImage *storeImage = [storeDataStore storeImageFor:[NSNumber numberWithInteger:[storeId integerValue]]];
        UIImageView *imageView = [[UIImageView alloc]initWithImage:storeImage];
        imageView.contentMode = UIViewContentModeScaleAspectFit;
        CGFloat factor = heightContstraint / storeImage.size.height;
        CGFloat newWidth = storeImage.size.width * factor;
        imageView.frame = CGRectMake(totalWidth, 0, newWidth, heightContstraint);
        totalWidth = totalWidth + newWidth + 5;
        [storeImageViews addObject:imageView];
    }];

    _storeLogos = [[UIView alloc]initWithFrame:CGRectMake(101, 80, totalWidth, heightContstraint)];
    [storeImageViews enumerateObjectsUsingBlock:^(UIImageView *obj, NSUInteger idx, BOOL *stop) {
        [_storeLogos addSubview:obj];
    }];

    [self addSubview:_storeLogos];
}

- (void)layoutDealIcons {
    int factor = (_cellHasCoupons & _cellOnSale) ? 2 : 1;
    _containerView = [[UIView alloc]initWithFrame:CGRectMake(210, _discount.frame.origin.y + 1, (factor * 18), 18)];
    if (_cellOnSale) {
        UIImageView *saleImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-sale"]];
        saleImage.frame = CGRectMake(0, 0, 18, 18);
        [_containerView addSubview:saleImage];
    }
    if (_cellHasCoupons) {
        UIImageView *couponImage = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"icon-coupon"]];
        couponImage.frame = CGRectMake(18* (factor-1), 0, 18, 18);
        [_containerView addSubview:couponImage];
    }
    [self addSubview:_containerView];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];
}

- (void)selectItem:(BOOL)selected {
    _selected = selected;

    if (_circleImage)[_circleImage removeFromSuperview];

    _circleImage = [[UIImageView alloc]initWithFrame:CGRectMake(0, 0, 30, 30)];
    _circleImage.image = selected ?[UIImage imageNamed:@"checkbox-checked"] :[UIImage imageNamed:@"addbox"];

    [_quantityView addSubview:_circleImage];
}

- (void)prepareForReuse {
    [[_containerView subviews]enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
    [_containerView removeFromSuperview];
    _product = nil;
    _cellHasCoupons = _cellOnSale = _selected = NO;
    _productImage.image = [UIImage imageNamed:@"noproductimage"];
    _descriptionLabel.text = nil;
    [_storeLogos removeFromSuperview];
    _storeLogos = nil;
    [_quantityView.subviews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];
}

- (IBAction)selectedTapped:(id)sender {
    _selected = !_selected;
    [self selectItem:_selected];
    if (_selected)
        [_selectionDelegate annimateAddToShoppingList:_productImage];
    [_selectionDelegate selectProduct:_product selected:_selected];
}

@end
