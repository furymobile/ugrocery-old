//
//  IKOCouponCell.h
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOCouponCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *discount;
@property (weak, nonatomic) IBOutlet UILabel *source;
@property (weak, nonatomic) IBOutlet UILabel *expiration;
@property (weak, nonatomic) IBOutlet UILabel *minimumQuantity;
@property (weak, nonatomic) IBOutlet UILabel *couponDescription;

@end
