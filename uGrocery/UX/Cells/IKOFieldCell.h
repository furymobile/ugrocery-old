//
//  IKOFieldCell.h
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IKOFieldCellDelegate <NSObject>

@optional

- (void)textFieldDidClear:(UITextField *)textField;

@end

@interface IKOFieldCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *fieldLabel;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UIButton *clear;
@property (nonatomic) NSString *key;

@property (nonatomic) id<IKOFieldCellDelegate> delegate;

- (void)configureWithLabel:(NSString *)label andValue:(NSString *)value andKey:(NSString *)key;
- (void)showClearButton;

@end
