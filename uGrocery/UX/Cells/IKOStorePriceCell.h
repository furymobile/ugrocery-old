//
//  IKOStorePriceCell.h
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOStorePriceCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *storeName;
@property (weak, nonatomic) IBOutlet UILabel *price;
@property (weak, nonatomic) IBOutlet UIImageView *storeLogo;
@property (weak, nonatomic) IBOutlet UIImageView *disclosure;

@property (nonatomic, readonly) BOOL onSale;
@property (nonatomic, readonly) BOOL hasCoupons;

- (void)setImages:(BOOL)onSale hasCoupons:(BOOL)hasCoupons;

@end
