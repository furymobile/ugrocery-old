//
//  IKOPriceUpdateCell.m
//  uGrocery
//
//  Created by Duane Schleen on 2/10/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKOPriceUpdateCell.h"
#import "IKOStore.h"
#import "IKOAuthenticationManager.h"
#import "UIColor+uGrocery.h"

@interface IKOPriceUpdateCell () <UIPickerViewDelegate, UIPickerViewDataSource>

@property (nonatomic) BOOL isExpanded;
@property (nonatomic) UIDatePicker *datePicker;
@property (nonatomic) UIPickerView *storePicker;
@property (nonatomic) NSArray *stores;
@property (nonatomic) NSDate *selectedSaleEndDate;

@end

@implementation IKOPriceUpdateCell

- (void)configureWith:(NSString *)price
  andStoreName:(NSString *)storeName
  andSalePrice:(NSString *)salePrice
  andSaleEndDate:(NSNumber *)saleEndDate
  andSaleDescription:(NSString *)saleDescription
  andStores:(NSArray *)stores;
{
    self.updatedPrice.delegate = self;
    self.salePrice.delegate = self;
    self.saleEndDate.delegate = self;
    self.saleDescription.delegate = self;

    self.updatedPrice.text = price;
    self.updatedStore.text = storeName;

    self.stores = stores;

    if ([saleEndDate integerValue] > 0) {
        self.selectedSaleEndDate = [NSDate dateWithTimeIntervalSince1970:[saleEndDate integerValue]];
    }

    _datePicker = [UIDatePicker new];
    [_datePicker setDate:self.selectedSaleEndDate ? :[NSDate date]];
    _datePicker.datePickerMode = UIDatePickerModeDate;
    [_datePicker addTarget:self action:@selector(datePickerValueChanged:) forControlEvents:UIControlEventValueChanged];
    [self.saleEndDate setInputView:_datePicker];

    if ([saleEndDate integerValue] > 0)
        [self datePickerValueChanged:nil];

    _storePicker = [UIPickerView new];
    _storePicker.dataSource = self;
    _storePicker.delegate = self;
    [_updatedStore setInputView:_storePicker];

    //self.saleEndDate.text = saleEndDate;

    self.saleDescription.text = saleDescription;
    self.salePrice.text = salePrice;
    
    _ppfSafeView.hidden =  NO;//![[IKOAuthenticationManager sharedInstance] isAdministrator];
    [_ppfSafe setOnTintColor:[UIColor UGGreen]];}

- (IBAction)clearUpdatedPrice:(id)sender {
    _updatedPrice.text = nil;
    [_priceUpdateDelegate textFieldDidClear:_updatedPrice];
}

- (IBAction)clearSalePrice:(id)sender {
    _salePrice.text = nil;
    [_priceUpdateDelegate textFieldDidClear:_salePrice];
}

- (IBAction)clearSaleEndDate:(id)sender {
    _saleEndDate.text = nil;
    _selectedSaleEndDate = nil;
    _datePicker.date = [NSDate date];
    [_priceUpdateDelegate textFieldDidClear:_saleEndDate];
}

- (IBAction)clearSaleDescription:(id)sender {
    _saleDescription.text = nil;
    [_priceUpdateDelegate textFieldDidClear:_saleDescription];
}

- (IBAction)expancCellButton:(id)sender {
    [_priceUpdateDelegate toggleExpansion];
}

- (IBAction)priceUpdateButton:(id)sender {
    [_priceUpdateDelegate updateWith:_updatedPrice.text andStoreName:_updatedStore.text andSalePrice:_salePrice.text andSaleEndDate:[NSNumber numberWithInt:[_selectedSaleEndDate timeIntervalSince1970]] andSaleDescription:_saleDescription.text andPpfSafe:_ppfSafe.on];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField != _salePrice && textField != _updatedPrice)
        return YES;

    if ([string isEqualToString:@"."])
        return NO;

    if (string.length == 0)
        return YES;

    NSInteger MAX_DIGITS = 11; // $999,999,999.99

    NSNumberFormatter *numberFormatter = [NSNumberFormatter new];
    [numberFormatter setNumberStyle:NSNumberFormatterCurrencyStyle];
    [numberFormatter setMaximumFractionDigits:2];
    [numberFormatter setMinimumFractionDigits:2];

    NSString *stringMaybeChanged = [NSString stringWithString:string];
    if (stringMaybeChanged.length > 1) {
        NSMutableString *stringPasted = [NSMutableString stringWithString:stringMaybeChanged];

        [stringPasted replaceOccurrencesOfString:numberFormatter.currencySymbol
        withString:@""
        options:NSLiteralSearch
        range:NSMakeRange(0, [stringPasted length])];

        [stringPasted replaceOccurrencesOfString:numberFormatter.groupingSeparator
        withString:@""
        options:NSLiteralSearch
        range:NSMakeRange(0, [stringPasted length])];

        NSDecimalNumber *numberPasted = [NSDecimalNumber decimalNumberWithString:stringPasted];
        stringMaybeChanged = [numberFormatter stringFromNumber:numberPasted];
    }

    UITextRange *selectedRange = [textField selectedTextRange];
    UITextPosition *start = textField.beginningOfDocument;
    NSInteger cursorOffset = [textField offsetFromPosition:start toPosition:selectedRange.start];
    NSMutableString *textFieldTextStr = [NSMutableString stringWithString:textField.text];
    NSUInteger textFieldTextStrLength = textFieldTextStr.length;

    [textFieldTextStr replaceCharactersInRange:range withString:stringMaybeChanged];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.currencySymbol
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.groupingSeparator
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    [textFieldTextStr replaceOccurrencesOfString:numberFormatter.decimalSeparator
    withString:@""
    options:NSLiteralSearch
    range:NSMakeRange(0, [textFieldTextStr length])];

    if (textFieldTextStr.length <= MAX_DIGITS) {
        NSDecimalNumber *textFieldTextNum = [NSDecimalNumber decimalNumberWithString:textFieldTextStr];
        NSDecimalNumber *divideByNum = [[[NSDecimalNumber alloc]initWithInt:10]decimalNumberByRaisingToPower:numberFormatter.maximumFractionDigits];
        NSDecimalNumber *textFieldTextNewNum = [textFieldTextNum decimalNumberByDividingBy:divideByNum];
        NSString *textFieldTextNewStr = [numberFormatter stringFromNumber:textFieldTextNewNum];

        textField.text = textFieldTextNewStr;

        if (textField == self.updatedPrice) {
            self.updatedPrice.text = textField.text;
        } else if (textField == self.salePrice) {
            self.salePrice.text = textField.text;
        }

        if (cursorOffset != textFieldTextStrLength) {
            NSInteger lengthDelta = textFieldTextNewStr.length - textFieldTextStrLength;
            NSInteger newCursorOffset = MAX(0, MIN(textFieldTextNewStr.length, cursorOffset + lengthDelta));
            UITextPosition *newPosition = [textField positionFromPosition:textField.beginningOfDocument offset:newCursorOffset];
            UITextRange *newRange = [textField textRangeFromPosition:newPosition toPosition:newPosition];
            [textField setSelectedTextRange:newRange];
        }
    }

    return NO;
}

- (void)datePickerValueChanged:(id)sender {
    NSDateFormatter *df = [NSDateFormatter new];
    [df setDateFormat:@"MM/dd/YYYY"];
    [self.saleEndDate setText:[df stringFromDate:_datePicker.date]];
    _selectedSaleEndDate = _datePicker.date;
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [_priceUpdateDelegate textFieldDidBeginEditing:textField];
}

- (void)prepareForReuse {
    _saleDescription.text = nil;
    _saleEndDate.text = nil;
    _salePrice.text = nil;
    _updatedPrice.text = nil;
    _updatedStore.text = nil;
}

#pragma mark - Store Picker

- (NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

- (NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return _stores.count;
}

- (NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    IKOStore *store = _stores[row];
    return store.name;
}

- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    IKOStore *store = _stores[row];
    _updatedStore.text = store.name;
    [_priceUpdateDelegate selectedStore:store];
}

@end
