//
//  IKOMainNavigationCell.h
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOMainNavigationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *scanButton;
@property (weak, nonatomic) IBOutlet UIButton *searchButton;
@property (weak, nonatomic) IBOutlet UIButton *browseButton;

@end
