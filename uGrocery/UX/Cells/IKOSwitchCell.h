//
//  IKOSwitchCell.h
//  uGrocery
//
//  Created by Duane Schleen on 11/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOSwitchCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *cellLabel;
@property (weak, nonatomic) IBOutlet UISwitch *cellSwitch;

@end
