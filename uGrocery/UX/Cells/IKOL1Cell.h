//
//  IKOL1Cell.h
//  uGrocery
//
//  Created by Duane Schleen on 2/20/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOL1Cell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *categoryShadow;
@property (weak, nonatomic) IBOutlet UILabel *categoryName;
@property (weak, nonatomic) IBOutlet UIImageView *categoryImage;

@end
