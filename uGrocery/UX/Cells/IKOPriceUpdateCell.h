//
//  IKOPriceUpdateCell.h
//  uGrocery
//
//  Created by Duane Schleen on 2/10/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKOPriceUpdate, IKOStore;

@protocol IKOPriceUpdateCellDelegate <NSObject, UITextFieldDelegate>

@required

- (void)toggleExpansion;
- (void)updateWith:(NSString *)price
        andStoreName:(NSString *)storeName
        andSalePrice:(NSString *)salePrice
        andSaleEndDate:(NSNumber *)saleEndDate
        andSaleDescription:(NSString *)saleDescription
        andPpfSafe:(BOOL)ppfSafe;
- (void)textFieldDidClear:(UITextField *)textField;
- (void)textFieldDidBeginEditing:(UITextField *)textField;
- (void)selectedStore:(IKOStore *)store;

@end

@interface IKOPriceUpdateCell : UITableViewCell <UITextFieldDelegate>

@property (nonatomic) id<IKOPriceUpdateCellDelegate> priceUpdateDelegate;

@property (weak, nonatomic) IBOutlet UIButton *expandCellButton;

@property (weak, nonatomic) IBOutlet UITextField *saleDescription;
@property (weak, nonatomic) IBOutlet UITextField *saleEndDate;
@property (weak, nonatomic) IBOutlet UITextField *salePrice;
@property (weak, nonatomic) IBOutlet UITextField *updatedPrice;
@property (weak, nonatomic) IBOutlet UITextField *updatedStore;
@property (weak, nonatomic) IBOutlet UISwitch *ppfSafe;
@property (weak, nonatomic) IBOutlet UIView *ppfSafeView;

- (void)configureWith:(NSString *)price
  andStoreName:(NSString *)storeName
  andSalePrice:(NSString *)salePrice
  andSaleEndDate:(NSNumber *)saleEndDate
  andSaleDescription:(NSString *)saleDescription
  andStores:(NSArray *)stores;

- (IBAction)clearUpdatedPrice:(id)sender;
- (IBAction)clearSalePrice:(id)sender;
- (IBAction)clearSaleEndDate:(id)sender;
- (IBAction)clearSaleDescription:(id)sender;

- (IBAction)expancCellButton:(id)sender;
- (IBAction)priceUpdateButton:(id)sender;

@end
