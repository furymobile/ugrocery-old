//
//  IKOMyProductCell.h
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SWRevealTableViewCell.h"

@class IKOFavoriteItem;
@class IKOProduct;

@protocol IKOMyProductSelectionDelegate <NSObject>

@required

- (void)selectMyProduct:(IKOFavoriteItem *)myProduct selected:(BOOL)selected;
- (void)annimateAddToShoppingList:(UIImageView *)imageView;

@end

@interface IKOMyProductCell : SWRevealTableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *markFavorite;

@property (nonatomic) id<IKOMyProductSelectionDelegate> selectionDelegate;

@property (weak, nonatomic) IBOutlet UIView *quantityView;
@property (weak, nonatomic) IBOutlet UIImageView *productImage;
@property (weak, nonatomic) IBOutlet UILabel *descriptionLabel;
@property (weak, nonatomic) IBOutlet UILabel *measure;
@property (weak, nonatomic) IBOutlet UIButton *selectedButton;

@property (weak, nonatomic) IBOutlet UILabel *maxSavings;
@property (weak, nonatomic) IBOutlet UIImageView *storeLogo;

- (void)configureWith:(IKOFavoriteItem *)item;
- (void)configurewith:(IKOProduct *)product;

- (IBAction)selectedTapped:(id)sender;
- (void)selectItem:(BOOL)selected;

@end
