//
//  PushNoAnimationSegue.m
//  Copyright (c) 2014 Fury Mobile. All rights reserved.
//
//  License:
//
//  Apache License
//  Version 2.0, January 2004
//  http://www.apache.org/licenses/
//

#import "PushNoAnimationSegue.h"

@implementation PushNoAnimationSegue

- (void)perform {
    [[[self sourceViewController]navigationController]pushViewController:[self destinationViewController] animated:NO];
}

@end
