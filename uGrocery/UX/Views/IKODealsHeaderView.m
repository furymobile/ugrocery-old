//
//  IKODealsSHeaderView.m
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODealsHeaderView.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
//#import "UIImage+Sizing.h"
#import "UIImage+Rotation.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UIScreen+Additions.h"
#import "IKOUtilities.h"

const CGFloat minDealsHeaderHeight = 50;

@implementation IKODealsHeaderView {
    NSArray *_storeIds;
    CGRect _smallSize;
    UIView *_backgroundCanvas;
    UIView *_findLowestBillView;
    NSMutableArray *_storeViews;
    NSString *_winnerStoreId;
    UIButton *_flbButton;
    UIButton *_expansionOverlayButton;
    UIView *_line;
    CGRect _originalExpansionFrame;
    NSMutableArray *_selectionButtons;
    UIImageView *_blurImageView;
    UIImageView *_mainCarat;
}

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, minDealsHeaderHeight)];
    if (self) {
        self.clipsToBounds = YES;
        _smallSize = CGRectMake(0, 0, [UIScreen currentSize].width, minDealsHeaderHeight);
        _isExpanded = NO;
        _storeIds = @[@"2",@"4",@"5",@"7"];
        _storeViews = [NSMutableArray new];
        _selectionButtons = [NSMutableArray new];
        [self buildInitialUI];
        [self buildFullUI];
    }
    return self;
}

- (void)toggleSelectionButtonVisibility:(BOOL)hidden {
    [_selectionButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        obj.hidden = !hidden;
    }];
}

- (void)buildInitialUI {
    self.backgroundColor = [UIColor whiteColor];
    _backgroundCanvas = [[UIView alloc]initWithFrame:_smallSize];
    _backgroundCanvas.backgroundColor = [UIColor clearColor];
    _findLowestBillView = [[UIView alloc]initWithFrame:_smallSize];

    _expansionOverlayButton = [[UIButton alloc]initWithFrame:CGRectMake(15, 0, self.frame.size.width - 30, minDealsHeaderHeight)];
    _expansionOverlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_expansionOverlayButton setImage:[UIImage imageNamed:@"caret-down-orange"] forState:UIControlStateNormal];
    [_expansionOverlayButton addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
    _originalExpansionFrame = _expansionOverlayButton.frame;

    UILabel *label = [[UILabel alloc]initWithFrame:_expansionOverlayButton.frame];
    label.text = @"ALL STORES";
    label.font = [UIFont IKOMediumFontOfSize:17];
    label.textColor = [UIColor IKOOrangeColor];
    label.textAlignment = NSTextAlignmentCenter;

    [_findLowestBillView addSubview:label];
    [_backgroundCanvas addSubview:_findLowestBillView];

    _line = [[UIView alloc]initWithFrame:CGRectMake(0, 49, [UIScreen currentSize].width, 1)];
    _line.backgroundColor = [UIColor IKODarkGrayColor];

    [self addSubview:_line];
    [self addSubview:_backgroundCanvas];
    [self addSubview:_expansionOverlayButton];
}

- (void)buildFullUI {
    self.frame = _smallSize;

    [_storeViews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];

    int totalHeight = minDealsHeaderHeight + ((int)_storeIds.count * minDealsHeaderHeight);
    _backgroundCanvas.frame = CGRectMake(0, 0, [UIScreen currentSize].width, totalHeight);
    int __block initialYOffset = minDealsHeaderHeight;

    [_storeIds enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *storeView = [self buildViewForStoreId:obj withYOffset:initialYOffset];
        [_storeViews addObject:storeView];
        [_backgroundCanvas addSubview:storeView];
        initialYOffset += minDealsHeaderHeight;
    }];
}

- (UIView *)buildViewForStoreId:(NSString *)storeId withYOffset:(int)offset {
    CGRect position = CGRectMake(0, offset, [UIScreen currentSize].width, minDealsHeaderHeight);
    UIView *storeView = [[UIView alloc]initWithFrame:position];
    storeView.backgroundColor = [UIColor clearColor];

    IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];
    UIImage *storeImage = [storeDataStore largerStoreImageFor:[NSNumber numberWithInteger:[storeId integerValue]]];

    UIImageView *imageView = [[UIImageView alloc]initWithImage:storeImage];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    imageView.frame = CGRectMake(9, 10, 30, 30);

    UILabel *storeName = [[UILabel alloc]init];
    storeName.text = [storeDataStore storeNameFor:[NSNumber numberWithInteger:[storeId integerValue]]];
    storeName.font = [UIFont IKOLightFontOfSize:17];
    storeName.frame = CGRectMake(45, 0, 200, 50);
    storeName.textColor = [UIColor IKODarkGrayColor];

    UIView *cellLine = [[UIView alloc]initWithFrame:CGRectMake(15, storeView.frame.size.height - 1, [UIScreen currentSize].width, 1)];
    cellLine.backgroundColor = [UIColor IKODividerColor];

    UIButton *selectionButton = [[UIButton alloc]initWithFrame:CGRectMake(9, 0, [UIScreen currentSize].width - 20, minDealsHeaderHeight)];
    selectionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [selectionButton setImage:(_isExpanded) ?[UIImage imageNamed:@"caret-down-orange"] :[[UIImage imageNamed:@"caret-down"]imageRotatedByDegrees:-90] forState:UIControlStateNormal];
    [selectionButton addTarget:self action:@selector(selectStore:) forControlEvents:UIControlEventTouchUpInside];
    selectionButton.tag = [storeId intValue];
    [_selectionButtons addObject:selectionButton];

    [storeView addSubview:cellLine];
    [storeView addSubview:imageView];
    [storeView addSubview:storeName];
    [storeView insertSubview:selectionButton atIndex:30];

    return storeView;
}

- (void)setFrameToStoreId:(NSString *)storeId {
    int __block offset = 0;
    [_blurImageView removeFromSuperview];

    if (![storeId isEqualToString:@"0"]) {
        [_storeIds enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
            offset -= minDealsHeaderHeight;
            if ([obj isEqualToString:storeId])
                *stop = YES;
        }];
    }
    _backgroundCanvas.frame = CGRectMake(0, offset, [UIScreen currentSize].width, minDealsHeaderHeight);
    self.frame = CGRectMake(0, 0, [UIScreen currentSize].width, minDealsHeaderHeight);
    [self drawLineAt:minDealsHeaderHeight-1];
    [_expansionOverlayButton removeTarget:self action:@selector(selectStore:) forControlEvents:UIControlEventTouchUpInside];
    [_expansionOverlayButton addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
    [_expansionOverlayButton setImage:[UIImage imageNamed:@"caret-down-orange"] forState:UIControlStateNormal];
    [self toggleSelectionButtonVisibility:NO];
}

- (void)drawLineAt:(CGFloat)y {
    [_line removeFromSuperview];
    _line = [[UIView alloc]initWithFrame:CGRectMake(0, y, [UIScreen currentSize].width, 1)];
    _line.backgroundColor = [UIColor IKODarkGrayColor];
    [self addSubview:_line];
}

- (void)expandView {
    [self expand:nil];
}

#pragma mark - Actions

- (IBAction)selectStore:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    NSString *storeId = SF(@"%ld",(long)selectedButton.tag);
    [self setFrameToStoreId:storeId];
    [_delegate selectedStoreId:storeId];
}

- (IBAction)expand:(id)sender {
    [_line removeFromSuperview];
    [_expansionOverlayButton setImage:[[UIImage imageNamed:@"caret-down"]imageRotatedByDegrees:-90] forState:UIControlStateNormal];
    [_expansionOverlayButton removeTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];
    [_expansionOverlayButton addTarget:self action:@selector(selectStore:) forControlEvents:UIControlEventTouchUpInside];
    [self toggleSelectionButtonVisibility:YES];
    _backgroundCanvas.frame = CGRectMake(0, 0, [UIScreen currentSize].width, minDealsHeaderHeight + (minDealsHeaderHeight * _storeIds.count));
    [UIView animateWithDuration:0.3f animations:^{
        self.frame = _backgroundCanvas.frame;
    } completion:^(BOOL finished) {
        [self drawLineAt:minDealsHeaderHeight + (minDealsHeaderHeight * _storeIds.count) - 1];
    }];
    if (_overlaysView) {
        _blurImageView = [[UIImageView alloc]initWithImage:[IKOUtilities blurView:_overlaysView]];
        [_overlaysView.superview insertSubview:_blurImageView aboveSubview:_overlaysView];
    }
}

@end
