//
//  IKOCloseHeaderView.h
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOStoreHeaderView : UIView

@property (nonatomic) UIButton *closeButton;

- (id)initWithFrame:(CGRect)frame andImage:(UIImage *)image andStoreName:(NSString *)storeName;

@end
