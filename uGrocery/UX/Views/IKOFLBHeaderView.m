//
//  IKOFLBSHeaderView.m
//  uGrocery
//
//  Created by Duane Schleen on 8/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOFLBHeaderView.h"
#import "IKOFLBHelper.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIImage+Rotation.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UIScreen+Additions.h"
#import "IKOUtilities.h"
#import "UIView+Animation.h"
#import "UIAlertView+Blocks.h"
#import "IKOAuthenticationManager.h"
#import <MessageUI/MessageUI.h>

const CGFloat maxHeight = 50;
const CGFloat footerHeight = 35;

@interface IKOFLBHeaderView() <MFMailComposeViewControllerDelegate>

@property (nonatomic) CGRect defaultRect;
@property (nonatomic) UIImageView *collapseImage;
@property (nonatomic) UIView *expandedHeaderBG;
@property (nonatomic) UIButton *shareButton;
@property (nonatomic) UIButton *shareButton2;

@end

@implementation IKOFLBHeaderView {
    IKOFLBHelper *_flbHelper;
    UIView *_backgroundCanvas;
    UIView *_findLowestBillView;
    NSMutableArray *_storeSummaryViews;
    NSString *_winnerStoreId;
    UIButton *_flbButton;
    UIButton *_expansionOverlayButton;
    UIButton *_collapseOverlayButton;
    UIView *_line;
    NSMutableArray *_selectionButtons;
    UIImageView *_blurImageView;
    UIView *_waitIndicator;
    UIImageView *_spinnerView;
    UILabel *_saveLabel;
}

- (id)init {
    self = [super initWithFrame:CGRectMake(8, 8, [UIScreen currentSize].width-16, maxHeight)];
    if (self) {
        self.clipsToBounds = YES;
        self.backgroundColor = [UIColor UGWhite];
        _defaultRect = CGRectMake(8, 8, [UIScreen currentSize].width-16, 50);
        _isExpanded = NO;
        _storeSummaryViews = [NSMutableArray new];
        _selectionButtons = [NSMutableArray new];
        [self buildInitialUI];
    }
    return self;
}

- (void)configureWithFLBHelper:(IKOFLBHelper *)helper {
    _waitIndicator.hidden = YES;
    _flbHelper = helper;
    [self buildFullUI];
}

- (NSMutableAttributedString *)buildAttributedTitle {
    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc]initWithString:
                                           @"find lowest bill"];
    
    UIFont *myStringFont1 = [UIFont IKOMediumFontOfSize:20];
    UIFont *myStringFont2 = [UIFont IKOHeavyFontOfSize:20];
    

    NSMutableParagraphStyle *myStringParaStyle1 = [[NSMutableParagraphStyle alloc]init];
    myStringParaStyle1.alignment = NSTextAlignmentCenter;
    
    [myString addAttribute:NSForegroundColorAttributeName value:[UIColor UGWhite] range:NSMakeRange(0,myString.length)];
    [myString addAttribute:NSParagraphStyleAttributeName value:myStringParaStyle1 range:NSMakeRange(0,5)];
    [myString addAttribute:NSFontAttributeName value:myStringFont1 range:NSMakeRange(0,5)];
    [myString addAttribute:NSParagraphStyleAttributeName value:myStringParaStyle1 range:NSMakeRange(5,7)];
    [myString addAttribute:NSFontAttributeName value:myStringFont2 range:NSMakeRange(5,7)];
    [myString addAttribute:NSParagraphStyleAttributeName value:myStringParaStyle1 range:NSMakeRange(12,4)];
    [myString addAttribute:NSFontAttributeName value:myStringFont1 range:NSMakeRange(12,4)];
    
    return myString;
}

- (void)resetHeaderView {
    [_saveLabel removeFromSuperview];
    _flbButton.hidden = NO;
    [self.subviews enumerateObjectsUsingBlock:^(UIView *view, NSUInteger idx, BOOL *stop) {
        [view removeFromSuperview];
    }];
    self.frame = _defaultRect;
    [_blurImageView removeFromSuperview];
    [self buildInitialUI];
}

- (void)toggleSelectionButtonVisibility:(BOOL)hidden {
    [_selectionButtons enumerateObjectsUsingBlock:^(UIButton *obj, NSUInteger idx, BOOL *stop) {
        obj.hidden = !hidden;
    }];
    _saveLabel.hidden = !hidden;
}

- (void)buildInitialUI {
    _backgroundCanvas = [[UIView alloc]initWithFrame:_defaultRect];
    _backgroundCanvas.backgroundColor = [UIColor whiteColor];
    
    _findLowestBillView = [[UIView alloc]initWithFrame:CGRectMake(-8, -8, _defaultRect.size.width, _defaultRect.size.height)];
    _findLowestBillView.backgroundColor = [UIColor clearColor];
    
    _flbButton = [UIButton buttonWithType:UIButtonTypeCustom];
    _flbButton.backgroundColor = [UIColor UGRed];
    _flbButton.frame = CGRectMake(0, 0, _defaultRect.size.width - 50, _defaultRect.size.height);

    if ([[IKOAuthenticationManager sharedInstance]isPremium]) {
        [_flbButton setAttributedTitle:[self buildAttributedTitle] forState:UIControlStateNormal];
        [_flbButton addTarget:self action:@selector(findLowestBillAction:) forControlEvents:UIControlEventTouchUpInside];
    } else {
        [_flbButton setTitle:@"Find Lowest Bill with Premium" forState:UIControlStateNormal];
        [_flbButton addTarget:self action:@selector(launchStore:) forControlEvents:UIControlEventTouchUpInside];
    }
    
    _shareButton = [[UIButton alloc] initWithFrame:CGRectMake(_defaultRect.size.width - 50, 0, 50, 50)];
    _shareButton.backgroundColor = [UIColor UGLtGrey];
    [_shareButton setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    [_shareButton addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    
    _expansionOverlayButton = [[UIButton alloc]initWithFrame:CGRectMake(9, 0, [UIScreen currentSize].width - 84 , maxHeight)];
    _expansionOverlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    
     if ([[IKOAuthenticationManager sharedInstance]isPremium])
        [self drawCollapseIndicator:[[IKOAuthenticationManager sharedInstance]isPremium]];

    [_expansionOverlayButton addTarget:self action:@selector(expand:) forControlEvents:UIControlEventTouchUpInside];

    [_findLowestBillView addSubview:_expansionOverlayButton];
    [_findLowestBillView addSubview:_flbButton];
    [_findLowestBillView addSubview:_shareButton];
    [_backgroundCanvas addSubview:_findLowestBillView];

    [self buildWaitIndicator];

    [self addSubview:_backgroundCanvas];
    [self addSubview:_waitIndicator];
}

- (void)buildWaitIndicator {
    _waitIndicator = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _defaultRect.size.width, maxHeight)];
    _waitIndicator.backgroundColor = [UIColor UGBlue];

    UIView *sub = [[UIView alloc]initWithFrame:CGRectMake(([UIScreen currentSize].width/2)-130, 0, 260, 50)];
    _spinnerView = [[UIImageView alloc]initWithImage:[UIImage imageNamed:@"spinner-white"]];
    _spinnerView.frame = CGRectMake(15, 10, 30, 30);

    UILabel *waitText = [[UILabel alloc]init];
    waitText.text = @"Fetching the lowest bill...";
    waitText.font = [UIFont IKOHeavyFontOfSize:18];
    waitText.frame = CGRectMake(60, 0, 200, 50);
    waitText.textColor = [UIColor UGWhite];

    [sub addSubview:waitText];
    [sub addSubview:_spinnerView];

    [_waitIndicator addSubview:sub];
    _waitIndicator.hidden = YES;
}

- (void)buildFullUI {
    self.frame = _defaultRect;

    [_storeSummaryViews enumerateObjectsUsingBlock:^(UIView *obj, NSUInteger idx, BOOL *stop) {
        [obj removeFromSuperview];
    }];

    if (!_flbHelper)
        return;

    _winnerStoreId = [_flbHelper summaryWinner];

    int __block initialYOffset = maxHeight;

    [[_flbHelper summaryStores] enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        UIView *storeView = [self buildViewForStoreId:obj withYOffset:initialYOffset];
        [_storeSummaryViews addObject:storeView];
        [_backgroundCanvas addSubview:storeView];
        initialYOffset += maxHeight;
    }];

    [self buildFooterWithYOffset:initialYOffset];

    _saveLabel = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, _flbButton.frame.size.width, _flbButton.frame.size.height)];
    _saveLabel.attributedText = [self buildSavingsString];
    
    _expandedHeaderBG = [[UIView alloc] initWithFrame:CGRectMake(0, 0, _flbButton.frame.size.width, _flbButton.frame.size.height)];
    _expandedHeaderBG.backgroundColor = [UIColor UGGreen];

    _collapseOverlayButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width - 50, maxHeight)];
    _collapseOverlayButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [self drawCollapseIndicator:NO];
    [_collapseOverlayButton addTarget:self action:@selector(collapse:) forControlEvents:UIControlEventTouchUpInside];

    _shareButton2 = [[UIButton alloc] initWithFrame:CGRectMake(_defaultRect.size.width - 50, 0, 50, 50)];
    _shareButton2.backgroundColor = [UIColor UGLtGrey];
    [_shareButton2 setImage:[UIImage imageNamed:@"share"] forState:UIControlStateNormal];
    [_shareButton2 addTarget:self action:@selector(share:) forControlEvents:UIControlEventTouchUpInside];
    
    [_backgroundCanvas addSubview:_expandedHeaderBG];
    [_backgroundCanvas addSubview:_shareButton2];
    [_backgroundCanvas addSubview:_saveLabel];
    [_backgroundCanvas addSubview:_collapseOverlayButton];
    _flbButton.hidden = YES;
}

- (void)drawCollapseIndicator:(BOOL)isDown {
    [_collapseImage removeFromSuperview];
    _collapseImage = [[UIImageView alloc] initWithFrame:CGRectMake(_defaultRect.size.width - 87, 14,25,25)];
    _collapseImage.image = (isDown) ?[UIImage imageNamed:@"triangle-down"] :[UIImage imageNamed:@"triangle-up"];
    [_flbButton addSubview:_collapseImage];
}

- (NSMutableAttributedString *)buildSavingsString {
    NSString *savings = [_flbHelper savingsDelta];
    NSString *fullLabel = SF(@"Save up to %@ on this list!", savings);
    NSMutableAttributedString *myString = [[NSMutableAttributedString alloc]initWithString:
      fullLabel];

    UIFont *myStringFont1 = [UIFont IKOMediumFontOfSize:18];
    UIFont *myStringFont2 = [UIFont IKOHeavyFontOfSize:18];

    NSMutableParagraphStyle *myStringParaStyle1 = [[NSMutableParagraphStyle alloc]init];
    myStringParaStyle1.alignment = 1;

    [myString addAttribute:NSForegroundColorAttributeName value:[UIColor UGWhite] range:NSMakeRange(0,fullLabel.length)];

    [myString addAttribute:NSParagraphStyleAttributeName value:myStringParaStyle1 range:NSMakeRange(0,fullLabel.length)];
    [myString addAttribute:NSFontAttributeName value:myStringFont1 range:NSMakeRange(0,fullLabel.length)];

    [myString addAttribute:NSParagraphStyleAttributeName value:myStringParaStyle1 range:NSMakeRange(11,savings.length)];
    [myString addAttribute:NSFontAttributeName value:myStringFont2 range:NSMakeRange(11,savings.length)];

    return myString;
}

- (void)buildFooterWithYOffset:(int)offset {
    UIView *footer = [[UIView alloc]initWithFrame:CGRectMake(0, offset, _defaultRect.size.width, footerHeight)];
    footer.backgroundColor = [UIColor UGBlue];

    UILabel *totalsText = [[UILabel alloc]initWithFrame:CGRectMake(0, 0, _defaultRect.size.width, footerHeight)];
    totalsText.text = SF(@"Totals are based on %lu comparable products", (unsigned long)[_flbHelper countOfProducts]);
    totalsText.font = [UIFont IKOMediumFontOfSize:14];
    totalsText.textAlignment = NSTextAlignmentCenter;
    totalsText.textColor = [UIColor whiteColor];

    [footer addSubview:totalsText];
    [_backgroundCanvas addSubview:footer];
}

- (UIView *)buildViewForStoreId:(NSString *)storeId withYOffset:(int)offset {
    CGRect position = CGRectMake(0, 0 + offset, [UIScreen currentSize].width-16, maxHeight);
    UIView *storeView = [[UIView alloc]initWithFrame:position];
    storeView.backgroundColor = [UIColor clearColor];

    IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];
    UIImage *storeImage = [storeDataStore largerStoreImageFor:[NSNumber numberWithInteger:[storeId integerValue]]];
    NSString *summaryPrice = [_flbHelper summaryPriceForStore:storeId];

    UIImageView *imageView = [[UIImageView alloc]initWithImage:storeImage];
    [imageView setContentMode:UIViewContentModeScaleAspectFit];
    imageView.frame = CGRectMake(9, 10, 30, 30);

    UILabel *storeName = [[UILabel alloc]init];
    storeName.text = [storeDataStore storeNameFor:[NSNumber numberWithInteger:[storeId integerValue]]];
    storeName.font = [UIFont IKOMediumFontOfSize:17];
    storeName.frame = CGRectMake(45, 0, 200, 50);
    storeName.textColor = [UIColor UGDkGrey];

    UILabel *summaryLabel = [[UILabel alloc]initWithFrame:CGRectMake(_defaultRect.size.width - 140, 14, 104, 21)];
    summaryLabel.textAlignment = NSTextAlignmentRight;
    summaryLabel.text = summaryPrice;
    summaryLabel.font = [UIFont IKOHeavyFontOfSize:22];

    UIView *cellLine = [[UIView alloc]initWithFrame:CGRectMake(15, storeView.frame.size.height - 1, [UIScreen currentSize].width, 1)];
    cellLine.backgroundColor = [UIColor IKODividerColor];

    UIButton *selectionButton = [[UIButton alloc]initWithFrame:CGRectMake(0, 0, _defaultRect.size.width - 20, maxHeight)];
    selectionButton.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [selectionButton setImage:(_isExpanded) ?[UIImage imageNamed:@"caret-down"] :[[UIImage imageNamed:@"caret-down"]imageRotatedByDegrees:-90] forState:UIControlStateNormal];
    [selectionButton addTarget:self action:@selector(selectStore:) forControlEvents:UIControlEventTouchUpInside];
    selectionButton.tag = [storeId intValue];

    [_selectionButtons addObject:selectionButton];

    if ([storeId isEqualToString:SF(@"%@", _winnerStoreId)]) {
        summaryLabel.textColor = [UIColor UGGreen];
    } else {
        summaryLabel.textColor = [UIColor UGDkGrey];
    }

    [storeView addSubview:cellLine];
    [storeView addSubview:imageView];
    [storeView addSubview:storeName];
    [storeView addSubview:summaryLabel];
    [storeView insertSubview:selectionButton atIndex:30];

    return storeView;
}

- (void)setFrameToStoreId:(NSString *)storeId {
    if (!_flbHelper)
        return;

    _expandedHeaderBG.hidden = YES;
    _expansionOverlayButton.hidden = NO;
    [self drawCollapseIndicator:YES];

    _collapseOverlayButton.hidden = YES;
    [_blurImageView removeFromSuperview];

    int __block offset = 0;
    [[_flbHelper summaryStores]enumerateObjectsUsingBlock:^(NSString *obj, NSUInteger idx, BOOL *stop) {
        offset -= maxHeight;
        if ([obj isEqualToString:storeId])
            *stop = YES;
    }];

    _backgroundCanvas.frame = CGRectMake(8, offset, [UIScreen currentSize].width-16, maxHeight);
    [self insertSubview:_expansionOverlayButton atIndex:30];
    self.frame = _defaultRect;
    [self toggleSelectionButtonVisibility:NO];
}

- (void)drawLineAt:(CGFloat)y {
    [_line removeFromSuperview];
    _line = [[UIView alloc]initWithFrame:CGRectMake(0, y, _defaultRect.size.width, 1)];
    _line.backgroundColor = [UIColor IKODarkGrayColor];
    [self addSubview:_line];
}

- (void)expandView {
    [self expand:nil];
}

- (void)collapse {
    [self collapse:nil];
}

#pragma mark - Actions

- (IBAction)selectStore:(id)sender {
    UIButton *selectedButton = (UIButton *)sender;
    NSString *storeId = SF(@"%ld",(long)selectedButton.tag);
    [self setFrameToStoreId:storeId];
    [_delegate selectedStoreId:storeId];
}

- (IBAction)collapse:(id)sender {
    _expandedHeaderBG.hidden = YES;
    _expansionOverlayButton.hidden = NO;
    [self drawCollapseIndicator:YES];
    _collapseOverlayButton.hidden = YES;
    [_blurImageView removeFromSuperview];
    _shareButton2.hidden = YES;
    _shareButton.hidden = NO;
    _flbButton.hidden = NO;
    _backgroundCanvas.frame = _defaultRect;
    [self insertSubview:_expansionOverlayButton atIndex:30];
    self.frame = _defaultRect;
    [self toggleSelectionButtonVisibility:NO];
    [_delegate selectedStoreId:nil];
}

- (IBAction)expand:(id)sender {
    _expandedHeaderBG.hidden = NO;
    _expansionOverlayButton.hidden = YES;
    _collapseOverlayButton.hidden = NO;
    _flbButton.hidden = YES;
    _shareButton2.hidden = NO;
    _shareButton.hidden = YES;
    _isExpanded = !_isExpanded;
    if (_isExpanded) {
        [_expansionOverlayButton removeFromSuperview];
    } else {
        [self insertSubview:_expansionOverlayButton atIndex:30];
    }
    [self toggleSelectionButtonVisibility:YES];
    CGRect newFrame = CGRectMake(8, 8, _defaultRect.size.width, maxHeight + (maxHeight * [_flbHelper summaryStores].count)+ footerHeight);
    _backgroundCanvas.frame = CGRectMake(0, 0, _defaultRect.size.width, maxHeight + (maxHeight * [_flbHelper summaryStores].count)+ footerHeight);
    [UIView animateWithDuration:0.3f animations:^{
        self.frame = newFrame;
    } completion:^(BOOL finished) {
        //[self drawLineAt:footerHeight + maxHeight + (maxHeight * [_flbHelper summaryStores].count) - 1];
    }];
    if (_overlaysView) {
        _blurImageView = [[UIImageView alloc]initWithImage:[IKOUtilities blurView:_overlaysView]];
        [_overlaysView.superview insertSubview:_blurImageView aboveSubview:_overlaysView];
    }
}

- (IBAction)launchStore:(id)sender {
    [_delegate launchStore];
}

- (IBAction)findLowestBillAction:(id)sender {
    if ([IKOUtilities notConnectedAlert])
        return;
    _waitIndicator.hidden = NO;
    [_spinnerView spinWithDuration:15 rotations:1 repeat:10000];
    [_delegate findLowestBill];
}

- (IBAction)share:(id)sender {
    if ([MFMailComposeViewController canSendMail])
    {
        MFMailComposeViewController *mail = [[MFMailComposeViewController alloc] init];
        mail.mailComposeDelegate = self;
        [mail setSubject:@"uGrocery Shopping List"];
        [mail setMessageBody:[self buildList] isHTML:YES];
        
        [_currentViewController presentViewController:mail animated:YES completion:NULL];
    }
    else
    {
        [UIAlertView showWithTitle:@"Error"
                           message:@"Your device isn't configured to use email."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
    }
}

-(void)mailComposeController:(MFMailComposeViewController *)controller didFinishWithResult:(MFMailComposeResult)result error:(NSError *)error {
    
    NSString *message;
    
    switch (result) {
        case MFMailComposeResultSent:
            message = @"Your Shopping List has been successfully shared.";
            break;
        case MFMailComposeResultSaved:
            message = @"Saved a draft of your Shopping List email.";
            break;
        case MFMailComposeResultCancelled:
            NSLog(@"Cancelled sending this email.");
            [_currentViewController dismissViewControllerAnimated:YES completion:NULL];
            return;
            break;
        case MFMailComposeResultFailed:
            message = @"Share failed: An error occurred when trying to share your Shopping List.";
            break;
        default:
            message = @"An error occurred when trying to share your Shopping List";
            break;
    }
    
    [UIAlertView showWithTitle:@"Share Shopping List"
                       message:message
             cancelButtonTitle:@"OK"
             otherButtonTitles:nil
                      tapBlock:nil];
    
    [_currentViewController dismissViewControllerAnimated:YES completion:NULL];
}

-(NSString *)buildList {
    IKOShoppingListDataStore *shoppingListStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOShoppingListItem class]];
    
    NSMutableString *output = [NSMutableString new];
    [output appendString:@"<image width=200px src='https://ugrocery.com/wp-content/uploads/uGrocery-black-and-blue.png'/><br><br><table border=0 bgcolor='#c0c0c0'>"];
    NSArray *list = [shoppingListStore allObjects];
    for (IKOShoppingListItem *item in list) {
        [output appendString:SF(@"<tr bgcolor='#ffffff'><td>%@</td><td>%@</td></tr>", item.quantity, item.listItemDescription)];
    }
    [output appendString:@"</tr></table><br><br><font size=3>Powered by <a href='http://www.ugrocery.com'>uGrocery</a></font>"];
    return output;
}

@end
