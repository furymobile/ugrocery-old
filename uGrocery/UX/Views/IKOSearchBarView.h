//
//  IKOSearchBarView.h
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IKOSearchBarViewDelegate <NSObject>

@required
- (void)scanBarcode;
- (void)triggerProductSearchFor:(NSString *)term;
@end

@interface IKOSearchBarView : UIView

@property (nonatomic) id<IKOSearchBarViewDelegate> delegate;

@property (nonatomic) UISearchBar *searchBar;
@property (nonatomic) UIButton *triggerProductSearch;
@property (nonatomic) UIButton *scanBarcode;

- (IBAction)scanBarCode:(id)sender;

@end
