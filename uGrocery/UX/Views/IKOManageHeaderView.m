//
//  IKOManageHeaderView.m
//  uGrocery
//
//  Created by Duane Schleen on 9/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOManageHeaderView.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"

@implementation IKOManageHeaderView {
    UIButton *_deleteList;
    UIButton *_uncrossList;
    BOOL _crossed;
}

- (id)init {
    self = [super initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 50)];
    if (self) {
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];

    _deleteList = [UIButton buttonWithType:UIButtonTypeSystem];
    _deleteList.frame = CGRectMake(15, 10, 65, 30);
    _deleteList.titleLabel.font = [UIFont IKOMediumFontOfSize:14];
    _deleteList.tintColor = [UIColor UGBlue];
    [_deleteList setTitle:@"Delete List" forState:UIControlStateNormal];
    _deleteList.contentHorizontalAlignment = UIControlContentHorizontalAlignmentLeft;
    [_deleteList addTarget:self action:@selector(deleteList:) forControlEvents:UIControlEventTouchUpInside];

    _uncrossList = [UIButton buttonWithType:UIButtonTypeSystem];
    _uncrossList.frame = CGRectMake([UIScreen currentSize].width - 92, 10, 77, 30);
    _uncrossList.titleLabel.font = [UIFont IKOMediumFontOfSize:14];
    _uncrossList.tintColor = [UIColor UGBlue];
    [_uncrossList setTitle:@"Cross List" forState:UIControlStateNormal];
    _uncrossList.contentHorizontalAlignment = UIControlContentHorizontalAlignmentRight;
    [_uncrossList addTarget:self action:@selector(uncrossList:) forControlEvents:UIControlEventTouchUpInside];

    UIView *line = [[UIView alloc]initWithFrame:CGRectMake(0, 49, [UIScreen currentSize].width, 1)];
    line.backgroundColor = [UIColor IKODarkGrayColor];

    [self addSubview:_deleteList];
    [self addSubview:_uncrossList];
    [self addSubview:line];
}

- (void)updateButtons:(BOOL)isCrossed {
    _crossed = isCrossed;
    if (isCrossed) {
        [_uncrossList setTitle:@"Uncross List" forState:UIControlStateNormal];
    } else {
        [_uncrossList setTitle:@"Cross List" forState:UIControlStateNormal];
    }
}

- (IBAction)deleteList:(id)sender {
    [_headerDelegate deleteList];
}

- (IBAction)uncrossList:(id)sender {
    _crossed = !_crossed;
    [_headerDelegate uncrossList:_crossed];
}

@end