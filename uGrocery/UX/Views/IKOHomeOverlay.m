//
//  IKOHomeOverlay.m
//  uGrocery
//
//  Created by Duane Schleen on 8/4/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOHomeOverlay.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"

@implementation IKOHomeOverlay

- (void)setIsAuthenticated:(BOOL)authenticated {
    _signIn.hidden = _signUp.hidden = authenticated;
}

@end
