//
//  IKOManageHeaderView.h
//  uGrocery
//
//  Created by Duane Schleen on 9/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol IKOManageHeaderDelegate <NSObject>

@required

- (void)deleteList;
- (void)uncrossList:(BOOL)crossedOff;

@end


@interface IKOManageHeaderView : UIView

@property (nonatomic) id<IKOManageHeaderDelegate> headerDelegate;

- (void)updateButtons:(BOOL)crossedOff;

@end