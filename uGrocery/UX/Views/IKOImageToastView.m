//
//  IKOImageToastView.m
//  uGrocery
//
//  Created by Duane Schleen on 8/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOImageToastView.h"
#import "UIFont+uGrocery.h"
#import "UIColor+uGrocery.h"

@implementation IKOImageToastView {
    UIImage *_image;
    NSString *_message;
}

- (id)initWithFrame:(CGRect)frame
  andImage:(UIImage *)image
  andMessage:(NSString *)message {
    self = [super initWithFrame:frame];
    if (self) {
        _image = image;
        _message = message;
        [self buildUI];
    }
    return self;
}

- (void)buildUI {
    self.backgroundColor = [UIColor whiteColor];

    self.layer.cornerRadius = 5;
    self.layer.masksToBounds = YES;

    self.layer.borderColor = [UIColor UGGreen].CGColor;
    self.layer.borderWidth = 1.0f;

//    UIView* mask = [[UIView alloc] initWithFrame:self.frame];
//    mask.backgroundColor = [UIColor grayColor];
//    //mask.alpha = 0.8f;
//
    UIImageView *imageView = [[UIImageView alloc]initWithFrame:CGRectMake(5, 5, 38, 38)];
    imageView.image = _image;

    UILabel *label = [[UILabel alloc]initWithFrame:CGRectMake((_image) ? 50 : 5, 5, 200, 38)];
    label.font = [UIFont IKOMediumFontOfSize:13];
    label.textColor = [UIColor UGGreen];
    if (!_image)
        label.textAlignment = NSTextAlignmentCenter;
    label.text = _message;

//    [self addSubview:mask];
    [self addSubview:imageView];
    [self addSubview:label];
}

@end
