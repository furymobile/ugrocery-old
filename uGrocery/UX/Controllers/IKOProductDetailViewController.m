//
//  IKOProductDetailViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/28/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

typedef NS_ENUM (NSInteger, IKOProductDetailSections) {
    ProductDetailSectionProduct = 0,
    ProductDetailSectionStores,
    ProductDetailSectionDisclaimer,
    NUM_PRODUCT_DETAIL_TABLE_SECTIONS
};

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOFLBHelper.h"
#import "IKOFLBRequestPayload.h"
#import "IKOFieldCell.h"
#import "IKOObjectSelectorVC.h"
#import "IKOPriceUpdate.h"
#import "IKOProductDetailViewController.h"
#import "IKOSDK.h"
#import "IKOSearchResultCell.h"
#import "IKOSpinnerViewCell.h"
#import "IKOStorePriceCell.h"
#import "IKOUtilities.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIImage+Rotation.h"
#import "IKOTabBarController.h"
#import "UIViewController+Utilities.h"

@interface IKOProductDetailViewController ()

@property (nonatomic) BOOL flbDataLoaded;

@end

@implementation IKOProductDetailViewController {
    IKOFLBHelper *_flbHelper;
    NSArray *_stores;
    IKODealViewController *_dealViewController;
    BOOL _hasSubstitutes;
    BOOL _priceUpdateExpanded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    _stores = [NSArray new];
    [self setupFavoriteButton];

    if ([self.product.hasPrices integerValue] == 1)
        [self findLowestBill];
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    _priceUpdateExpanded = [[self.settingsDataStore objectForKey:kLastPriceCaptureExpanded]boolValue];
    NSDictionary *params = @{@"Product ID": self.product.productId ? : @"", @"Product Description": self.product.productDescription ? : @"", @"UPC": self.product.upc ? : @"", @"UPC-E": self.product.upce ? : @""};
    ANALYTIC_PARAMS(@"Opened Product Details", params);
}

- (void)viewDidAppear:(BOOL)animated {
    [IKOAnalytics logApptentiveEvent:@"Product_Details" fromViewController:self];
}

- (void)findLowestBill {
    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc]initWithProducts:@[self.product]];
    NSUInteger numberOfViewControllersOnStack = [self.navigationController.viewControllers count];
    UIViewController *parentViewController = self.navigationController.viewControllers[numberOfViewControllersOnStack - 2];
    Class parentVCClass = [parentViewController class];
    payload.flbPath =  NSStringFromClass(parentVCClass);
    NSNumber *settingValue = [self.settingsDataStore objectForKey:kShowCoupons];
    payload.withCoupons = [NSNumber numberWithBool:[settingValue boolValue]];

    if (self.lastDetectedServerResponse) {
        _flbDataLoaded = YES;
        _flbHelper = [[IKOFLBHelper alloc]initWithServerResponse:self.lastDetectedServerResponse];
        _stores = [_flbHelper storesFor:self.product.productId];
        _hasSubstitutes = [_flbHelper hasSubstitutes];
        [self.tableView reloadData];
    } else {
        [[IKOSDK new] findLowestBillsDetailWithPayload:[payload JSONRepresentation] storeIds:@[@2,@4,@5,@7] user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *responseParams) {
    #if TARGET_IPHONE_SIMULATOR
            DDLogVerbose(@"Response: %@", responseParams);
    #endif
            _flbDataLoaded = YES;
            _flbHelper = [[IKOFLBHelper alloc]initWithServerResponse:responseParams];
            _stores = [_flbHelper storesFor:self.product.productId];
            
            self.shoppingListItem.listItemDescription = self.product.productDescription;
            IKOShoppingListItem *tempItem = self.shoppingListItem;
            [self.shoppingListDataStore saveShoppingListItem:&tempItem];
            
            NSDictionary *productNode = [_flbHelper productNodeForProductId:self.product.productId];
            self.product = [IKOProduct objectFromJSON:productNode];
            [self.productDataStore saveProduct:self.product];
            
            _hasSubstitutes = [_flbHelper hasSubstitutes];
            [self.tableView reloadData];
        }];
    }
}

- (void)setupFavoriteButton {
    self.navigationItem.rightBarButtonItem = nil;
    UIImage *image = [[UIImage imageNamed:([self.myProductItem.isFavorite boolValue]) ? @"icon-heart-selected" : @"icon-heart"]imageWithRenderingMode:UIImageRenderingModeAlwaysOriginal];
    UIBarButtonItem *button = [[UIBarButtonItem alloc]initWithImage:image style:UIBarButtonItemStylePlain target:self action:@selector(toggleFavorite)];
    self.navigationItem.rightBarButtonItem = button;
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([self.product.hasPrices integerValue] == 1)
        return NUM_PRODUCT_DETAIL_TABLE_SECTIONS;
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == ProductDetailSectionStores) {
        return _stores.count + 3;
    } else if (section == ProductDetailSectionDisclaimer) {
        return (_hasSubstitutes) ? 2 : 1;
    }
    if ([self showPriceCapture]) {
        return 2;
    }
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case ProductDetailSectionProduct: {
                if (indexPath.row == 0) {
                    return (self.shoppingListItem) ? 171 : 143;
                }
                return (_priceUpdateExpanded) ? 279 : 44;
                break;
            }
        case ProductDetailSectionDisclaimer:
            return (indexPath.row == 0) ? 69 : 52;
        default: {
                if (indexPath.row == 0) {
                    return 44;
                } else if (indexPath.row == 1) {
                    if (_flbDataLoaded) {
                        return 0;
                    }
                    return 176;
                } else if (indexPath.row == 2) {
                    if (![IKONetworkManager connectedToNetwork]) {
                        return 77;
                    }
                    return 0;
                }

                return 44;
            }
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case ProductDetailSectionProduct: {
                if (indexPath.row == 0) {//product cell
                    IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
                    [cell configureWithProduct:self.product];
                    cell.descriptionText.attributedText = [IKOUtilities attributedLabelFor:self.product.productDescription andMeasure:self.product.measureText titleSize:16];
                    cell.delegate = self;
                    if (self.myProductItem)
                        [cell setFavorite:[self.myProductItem.isFavorite boolValue]];
                    cell.accessoryType = UITableViewCellAccessoryNone;
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    if (self.shoppingListItem) {
                        cell.quantityView.hidden = NO;
                        cell.addToShoppingList.hidden = YES;
                        [cell setQuantity:self.shoppingListItem.quantity];
                    }
                    return cell;
                } else {
                    IKOPriceUpdateCell *cell = (IKOPriceUpdateCell *)[self.tableView dequeueReusableCellWithIdentifier:@"PriceUpdateCell" forIndexPath:indexPath];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.priceUpdateDelegate = self;
                    [cell configureWith:self.price andStoreName:self.store.name ? : @"" andSalePrice:self.salePrice andSaleEndDate:self.saleEndDate andSaleDescription:self.saleDescription andStores:[self.storeDataStore allObjects]];
                    [self colorText:cell.updatedPrice forObject:self.price];
                    [self colorText:cell.salePrice forObject:self.salePrice];
                    [self colorText:cell.updatedStore forObject:self.store];
                    [self colorText:cell.saleDescription forObject:self.saleDescription];
                    [self colorText:cell.saleEndDate forObject:self.saleEndDate];

                    [cell.expandCellButton setImage:(_priceUpdateExpanded) ?[[UIImage imageNamed:@"caret-down-white"]imageRotatedByDegrees:180] :[UIImage imageNamed:@"caret-down-white"] forState:UIControlStateNormal];

                    return cell;
                }
                break;
            }
        case ProductDetailSectionStores: {
                if (indexPath.row == 0) {
                    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StoreHeaderCell" forIndexPath:indexPath];
                    return cell;
                } else if (indexPath.row == 1) {
                    IKOSpinnerViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"SpinnerCell" forIndexPath:indexPath];
                    [cell runSpinner];
                    return cell;
                } else if (indexPath.row == 2) {
                    UITableViewCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"NoConnectionCell" forIndexPath:indexPath];
                    return cell;
                } else {
                    IKOStorePriceCell *cell = [self.tableView dequeueReusableCellWithIdentifier:@"StorePriceCell" forIndexPath:indexPath];
                    NSString *storeId = _stores[indexPath.row - 3];
                    NSString *store = [self.storeDataStore storeNameFor:[NSNumber numberWithInteger:[storeId integerValue]]];
                    NSString *price = [_flbHelper priceForProductId:self.product.productId inStore:storeId];
                    BOOL winner = [_flbHelper isWinnerForProductId:self.product.productId inStore:storeId];
                    NSDictionary *saleData = [_flbHelper saleDataForProductId:self.product.productId atStoreId:storeId];
                    NSArray *couponData = [_flbHelper couponsDataForProductId:self.product.productId atStoreId:storeId];

                    cell.selectionStyle = ((saleData != nil) | (couponData != nil))
                      ? UITableViewCellSelectionStyleGray
                      : UITableViewCellSelectionStyleNone;

                    cell.disclosure.hidden = !((saleData != nil) | (couponData != nil));

                    cell.userInteractionEnabled = ((saleData != nil) | (couponData != nil));

                    cell.storeName.text = store;
                    cell.storeLogo.image = [self.storeDataStore largerStoreImageFor:[NSNumber numberWithInteger:[storeId integerValue]]];
                    cell.price.text = price;

                    [cell setImages:saleData != nil hasCoupons:couponData != nil];
                    if (winner) {
                        cell.price.textColor = [UIColor UGGreen];
                        cell.price.font = [UIFont IKOHeavyFontOfSize:20];
                    }

                    return cell;
                }
                break;
            }
        case ProductDetailSectionDisclaimer: {
                return (indexPath.row == 0)
                       ?[self.tableView dequeueReusableCellWithIdentifier:@"DisclaimerCell" forIndexPath:indexPath]
                       :[self.tableView dequeueReusableCellWithIdentifier:@"DisclaimerCell2" forIndexPath:indexPath];
            }
    }
    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (![IKONetworkManager connectedToNetwork])
        return;

    if (indexPath.section == ProductDetailSectionStores) {
        [self showDealForProduct:self.product atStore:_stores[indexPath.row-3]];
    }
}

- (void)toggleFavorite {
    if (![[IKOAuthenticationManager sharedInstance]isPremium]) {
        [UIAlertView showWithTitle:@"Go Premium"
        message:@"Get uGrocery Premium now to save your favorite products."
        cancelButtonTitle:@"Not Now"
        otherButtonTitles:@[@"Go Premium"]
        tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
            if (buttonIndex == 1) {
                [(IKOTabBarController *)self.navigationController.tabBarController launchStore];
            }
        }];
        return;
    }
    [self markAsFavorite:nil isFavorite:![self.myProductItem.isFavorite boolValue]];
    [self setupFavoriteButton];
}

- (void)markAsFavorite:(id)object isFavorite:(BOOL)favorite {
    NSDictionary *params = @{@"Product ID": self.product.productId ? : @"", @"Product Description": self.product.productDescription ? : @"", @"UPC": self.product.upc ? : @"", @"UPC-E": self.product.upce ? : @""};
    if (favorite) {
        ANALYTIC_PARAMS(@"Favorited Product", params);
    } else {
        ANALYTIC_PARAMS(@"Unfavorited Product", params);
    }

    if (!self.myProductItem)
        self.myProductItem = [[IKOFavoriteItem alloc]initWithProduct:self.product];

    self.myProductItem.isFavorite = [NSNumber numberWithBool:favorite];
    IKOFavoriteItem *item = self.myProductItem;
    [self.myProductsDataStore saveMyProductItem:&item];
}

#pragma mark - Actions

- (void)showDealForProduct:(IKOProduct *)product atStore:(NSString *)storeId {
    _dealViewController = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoDealViewController"];
    _dealViewController.storeId = storeId;
    _dealViewController.product = product;
    _dealViewController.saleData = [_flbHelper saleDataForProductId:product.productId atStoreId:storeId];
    _dealViewController.couponData = [_flbHelper couponsDataForProductId:product.productId atStoreId:storeId];
    _dealViewController.maxSavings = [_flbHelper maxSavingsFor:product.productId atStoreId:storeId];

    if ((_dealViewController.saleData != nil) | (_dealViewController.couponData != nil)) {
        [self.navigationController pushViewController:_dealViewController animated:YES];
        [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
    }
}

- (void)update {
    if (SF(@"%@",self.price).length > 0 && self.store == nil) {
        [UIAlertView showWithTitle:@"Store Required"
                           message:@"A store is required when you enter a price."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
    } else if (SF(@"%@",self.salePrice).length > 0 && [self.saleEndDate intValue] == 0 ) {
        [UIAlertView showWithTitle:@"End Date Required"
                           message:@"An end date is required when you enter a sale price."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
    } else {
        [self updatePrice];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updatePrice {
    IKOPriceUpdate *submission = [[IKOPriceUpdate alloc]initWithProduct:self.product andUser:[IKOAuthenticationManager currentUser]];
    [super sendProductUpdate:submission];
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    switch (textField.tag) {
        case 1:
            return [super currencyTextField:textField shouldChangeCharactersInRange:range replacementString:string];
            break;
        default:
            break;
    }
    return YES;
}

#pragma mark - IKOPriceUpdateCellDelegate

- (void)toggleExpansion {
    _priceUpdateExpanded = !_priceUpdateExpanded;
    [self.settingsDataStore setObject:(_priceUpdateExpanded) ? @"1" : @"0" forKey:kLastPriceCaptureExpanded];
    [self.tableView reloadData];
}

- (void)updateWith:(NSString *)price andStoreName:(NSString *)storeName andSalePrice:(NSString *)salePrice andSaleEndDate:(NSNumber *)saleEndDate andSaleDescription:(NSString *)saleDescription andPpfSafe:(BOOL)ppfSafe{
    self.price = price;
    self.salePrice = salePrice;
    self.saleDescription = saleDescription;
    self.saleEndDate = saleEndDate;
    self.ppfSafe = ppfSafe;
    [self update];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.hasEdited addObject:[NSNumber numberWithInteger:textField.tag]];
    [self colorText:textField forObject:self.price];
}

- (void)textFieldDidClear:(UITextField *)textField {
    if (textField.tag == 1) {
        self.price = nil;
    } else if (textField.tag == 3) {
        self.salePrice = nil;
    } else if (textField.tag == 4) {
        self.saleEndDate = nil;
    } else if (textField.tag == 5) {
        self.saleDescription = nil;
    }
}

- (void)selectedStore:(IKOStore *)store {
    self.store = store;
}

@end
