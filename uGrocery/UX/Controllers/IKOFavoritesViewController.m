//
//  IKOFavoritesViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/13/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOMyProductCell.h"
#import "IKOFavoritesViewController.h"
#import "IKOProductDetailViewController.h"
#import "IKOUtilities.h"
#import "IKOZeroStateOverlay.h"
#import "IKOZeroStateVC.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"
#import "UIView+CenteredSectionTitle.h"
#import "IKOTabBarController.h"
#import "UIViewController+Utilities.h"
#import "IKOSDK.h"
#import "IKOFLBHelper.h"

NSString *kFavoritesCategory = @"99997";
NSString *kHeaderCategory = @"99996";

@interface IKOFavoritesViewController ()

@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOFavoritesDataStore *myProductsDataStore;
@property (nonatomic) NSMutableDictionary *favoriteItems;
@property (nonatomic) NSMutableDictionary *products;

@end

@implementation IKOFavoritesViewController {
    IKOProductDataStore *_productDataStore;
    IKOProductCategoryDataStore *_categoryDataStore;
    IKOZeroStateOverlay *_zeroStateOverlay;
    NSMutableDictionary *_items;
    NSArray *_l1ids;
    NSMutableArray *_l1s;
    NSIndexPath *_revealingCellIndexPath;
    UIView *_zeroStateView;
    UIImageView *_blurImageView;
    UIView *_storeMessage;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];
    
    _myProductsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _productDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _categoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];

    _products = [NSMutableDictionary new];
    
    IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
    [vc loadView];
    vc.zeroStateTitle.text = @"Save some Favorites!";
    vc.zeroStateMessage.text = @"When you add a product to your Shopping List or mark one as a Favorite, it will appear here";
    vc.screenImage.image = [UIImage imageNamed:@"icon-myproducts"];
    _zeroStateView = vc.view;
    _zeroStateView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
    self.tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, self.tableView.frame.size.width, 15)];
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened My Products");
    [super viewWillAppear:animated];
    [_blurImageView removeFromSuperview];
    [_storeMessage removeFromSuperview];
    if ([[IKOAuthenticationManager sharedInstance]isPremium]) {
        [self showZeroStateIfNecessary];
    }

    [self fetch];
}

- (void)viewDidAppear:(BOOL)animated {
    if (![[IKOAuthenticationManager sharedInstance]isPremium]) {
        [self performSelector:@selector(showNonPremium) withObject:nil afterDelay:0.2];
    }
    [IKOAnalytics logApptentiveEvent:@"Favorites" fromViewController:self];
}

- (void)showNonPremium {
    self.tableView.userInteractionEnabled = false;
    _blurImageView = [[UIImageView alloc]initWithImage:[IKOUtilities blurView:self.tableView.superview]];
    _blurImageView.alpha = 0;
    [self.tableView.superview insertSubview:_blurImageView atIndex:999];
    [UIView animateWithDuration:1.5 animations:^{
        _blurImageView.alpha = 1;
    } completion:^(BOOL finished) {
        UIViewController *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoPremiumOverlay"];
        [vc loadView];
        UIButton *storeButton = (UIButton *)[vc.view viewWithTag:666];
        [storeButton addTarget:self action:@selector(launchStore) forControlEvents:UIControlEventTouchUpInside];
        _storeMessage = vc.view;
        [self.view.superview insertSubview:_storeMessage aboveSubview:_blurImageView];
    }];
}

- (void)viewDidDisappear:(BOOL)animated {
    for ( UITableViewCell *cell in [self.tableView visibleCells] ) {
        if ([cell isKindOfClass:[SWRevealTableViewCell class]])
            [(SWRevealTableViewCell*)cell setRevealPosition:SWCellRevealPositionCenter animated:YES];
    }
}

- (void)showZeroStateIfNecessary {
    if ([_myProductsDataStore allObjects].count == 0) {
        _zeroStateView.alpha = 0;
        [self.view insertSubview:_zeroStateView atIndex:0];
        [UIView animateWithDuration:0.2 animations:^{
            _zeroStateView.alpha = 1;
        }];
        self.tableView.userInteractionEnabled = NO;
    } else {
        [_zeroStateView removeFromSuperview];
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void)launchStore {
    [((IKOTabBarController *)self.tabBarController)launchStore];
}

- (void)fetch {
    _items =[NSMutableDictionary new];
    _l1s = [NSMutableArray new];
    NSArray *myProductItems = [_myProductsDataStore allObjects];
    _l1ids = [myProductItems valueForKeyPath:@"@distinctUnionOfObjects.l1Id"];

    IKOProductCategory *favorites = [IKOProductCategory new];
    favorites.name = @"Favorites";
    favorites.productCategoryId = [NSNumber numberWithInteger:[kFavoritesCategory integerValue]];
    [_l1s addObject:favorites];
    
    IKOProductCategory *header = [IKOProductCategory new];
    header.name = @"";
    header.productCategoryId = [NSNumber numberWithInteger:[kHeaderCategory integerValue]];
    [_l1s addObject:header];
    

    [_l1ids enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_l1s addObject:[_categoryDataStore getProductCategory:obj]];
    }];

    _favoriteItems = [NSMutableDictionary new];
    for (IKOProductCategory *category in _l1s) {
        NSMutableArray *itemsForCategory = [NSMutableArray new];
        [myProductItems enumerateObjectsUsingBlock:^(IKOFavoriteItem *obj, NSUInteger idx, BOOL *stop) {
            if ([obj.l1Id isEqualToNumber:category.productCategoryId])
                [itemsForCategory addObject:obj];

            if ([obj.isFavorite integerValue] == 1)
                [_favoriteItems setObject:obj forKey:obj.productId];
        }];
        [_items setObject:itemsForCategory forKey:SF(@"%@", category.productCategoryId)];
    }

    [_items setObject:[_favoriteItems allValues] forKey:kFavoritesCategory];
    [_items setObject:[NSArray new] forKey:kHeaderCategory];

    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc]initWithMyProducts:myProductItems];
    [[IKOSDK new]findLowestBillWithPayload:[payload JSONRepresentation] storeIds:[IKOStoreDataStore allStoreIds] user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *response) {
#if TARGET_IPHONE_SIMULATOR
        DDLogVerbose(@"Response: %@", response);
#endif
        IKOProduct __block *p = nil;
        NSDictionary *products = response.payload[@"Products"];
        NSArray *productSortDict = [response.payload[@"ProductSort"] componentsSeparatedByString:@","];
        
        [productSortDict enumerateObjectsUsingBlock:^(NSString*  _Nonnull productId, NSUInteger idx, BOOL * _Nonnull stop) {
            NSDictionary *pp = products[productId][@"Product"];
            if (pp) {
                p = [IKOProduct objectFromJSON:pp];
                [self maxSavingsForProduct:p inStores:products[productId]];
            }
        
            self.products[p.productId] = p;
        }];

        [self.tableView reloadData];
    }
    andMethod:@"list"];
    
    [self.tableView reloadData];
    if ([[IKOAuthenticationManager sharedInstance]isPremium])
        [self showZeroStateIfNecessary];
}

- (void)maxSavingsForProduct:(IKOProduct *)product inStores:(NSDictionary*)stores {
    [stores enumerateKeysAndObjectsUsingBlock:^(id  _Nonnull key, NSDictionary*  _Nonnull obj, BOOL * _Nonnull stop) {
        if (![key isEqualToString:@"Product"]) {
            if (( obj[@"IsWinner"] && [obj[@"IsWinner"] boolValue] && [obj[@"MaxSavings"] isEqualToNumber:@0] ) || [obj[@"SaleDetails"] isKindOfClass:[NSDictionary class]]) {
                product.maxSavings = obj[@"MaxSavings"];
                product.hasCoupons = obj[@"HasCoupons"];
                product.winningStore = [NSNumber numberWithInteger:[key integerValue]];
                product.isOnSale = obj[@"IsOnSale"];
                *stop = YES;
            }
        }
    }];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSString *productCategoryId = SF(@"%@", s.productCategoryId);
    if ([productCategoryId isEqualToString:kHeaderCategory]) {
        return 40;
    }
    if (_products) {
        NSArray *productSubset = _items[SF(@"%@", s.productCategoryId)];
        IKOFavoriteItem *item = productSubset[indexPath.row];
        IKOProduct *temp = _products[item.productId];
        if (temp) {
            if ([temp.maxSavings intValue] > 0) {
                return 120;
            }
            return 90;
        }
    }
    return 90;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return [_items allKeys].count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    NSString *productCategoryId = SF(@"%@", s.productCategoryId);
    if ([productCategoryId isEqualToString:kHeaderCategory] && _l1s.count > 2) {
        return 1;
    }

    return ((NSArray *)_items[SF(@"%@", s.productCategoryId)]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    NSString *productCategoryId = SF(@"%@", s.productCategoryId);
    if ([productCategoryId isEqualToString:kFavoritesCategory]) {
        if (((NSArray *)_items[SF(@"%@", s.productCategoryId)]).count == 0) {
            return 0;
        }
    } else if ([productCategoryId isEqualToString:kHeaderCategory]) {
        return 0;
    }
    
    return 28;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    NSString *productCategoryId = SF(@"%@", s.productCategoryId);
    if ([productCategoryId isEqualToString:kFavoritesCategory]) {
        return [UIView favoriteSectionHeader:@"Favorites" target:self selector:@selector(addAllFavorites)];
    } else  if ([productCategoryId isEqualToString:kHeaderCategory]) {
        return nil;
    }
    return [UIView viewWithSectionHeader:s.name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productSubset = _items[SF(@"%@", s.productCategoryId)];
    
    NSString *productCategoryId = SF(@"%@", s.productCategoryId);
    if ([productCategoryId isEqualToString:kHeaderCategory]) {
        return [self.tableView dequeueReusableCellWithIdentifier:@"ShoppingListHistoryHeader" forIndexPath:indexPath];
    }

    IKOMyProductCell *cell = (IKOMyProductCell *)[self.tableView dequeueReusableCellWithIdentifier:@"MyProductCell" forIndexPath:indexPath];
    IKOFavoriteItem *item = productSubset[indexPath.row];
    [cell configureWith:item];
    [cell selectItem:[_shoppingListDataStore isOnShoppingList:item.productId]];
    cell.dataSource = self;
    cell.delegate = self;
    cell.selectionDelegate = self;
 
    IKOProduct *temp = _products[item.productId];
    if (temp) {
        [cell configurewith:temp];
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showProductDetails" sender:self];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductDetailViewController *vc = [segue destinationViewController];
        IKOProductCategory *s = _l1s[[self.tableView indexPathForSelectedRow].section];
        NSArray *productSubset = _items[SF(@"%@", s.productCategoryId)];
        IKOFavoriteItem *productItem = productSubset[[self.tableView indexPathForSelectedRow].row];
        vc.product = [_productDataStore getProduct:productItem.productId];
    }
}

#pragma mark - SWRevealTableViewCell delegate

- (void)revealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell willMoveToPosition:(SWCellRevealPosition)position {
    if ( position == SWCellRevealPositionCenter )
        return;
    
    for ( SWRevealTableViewCell *cell in [self.tableView visibleCells] ) {
        if ( cell == revealTableViewCell )
            continue;
        if ([cell respondsToSelector:@selector(setRevealPosition:animated:)])
            [cell setRevealPosition:SWCellRevealPositionCenter animated:YES];
    }
}

- (void)revealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell didMoveToPosition:(SWCellRevealPosition)position {
}

#pragma mark - SWRevealTableViewCell data source

- (NSArray *)leftButtonItemsInRevealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell {
    return nil;
}

- (NSArray *)rightButtonItemsInRevealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell {
    SWCellButtonItem *item1 = [SWCellButtonItem itemWithTitle:@"Remove" handler:^(SWCellButtonItem *item, SWRevealTableViewCell *cell)
    {
        _revealingCellIndexPath = [self.tableView indexPathForCell:cell];
        [self deleteItemAtIndexPath:_revealingCellIndexPath];
        return YES;
    }];

    item1.backgroundColor = [UIColor redColor];
    item1.tintColor = [UIColor whiteColor];
    item1.width = 75;
    revealTableViewCell.performsActionOnRightOverdraw = YES;

    return @[item1];
}

- (void)deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productSubset = _items[SF(@"%@", s.productCategoryId)];
    IKOFavoriteItem *myProductItem = productSubset[indexPath.row];
    BOOL removed = [_myProductsDataStore removeMyProductItem:myProductItem];
    if (removed)
        [self fetch];
}

- (void)addAllFavorites {
    [UIAlertView showWithTitle:@"Add Favorites?"
    message:@"Add favorites to your Shopping List?"
    cancelButtonTitle:@"Cancel"
    otherButtonTitles:@[@"OK"]
    tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            ANALYTIC(@"Added all Favorites to Shopping List");
            NSArray *productSubset = _items[kFavoritesCategory];
            [productSubset enumerateObjectsUsingBlock:^(IKOFavoriteItem *item, NSUInteger idx, BOOL *stop) {
                IKOShoppingListItem *shoppingListItem = [[IKOShoppingListItem alloc]initWithMyProductItem:item];
                [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
            }];
            [self fetch];
        }
    }];
}

#pragma mark - IKOMyProductSelectionDelegate

- (void)selectMyProduct:(IKOFavoriteItem *)myProduct selected:(BOOL)selected {
    if (selected) {
        [IKOAnalytics logApptentiveEvent:@"Added_To_Shopping_List_From_Favorites" fromViewController:self];
        IKOShoppingListItem *shoppingListItem = [[IKOShoppingListItem alloc]initWithMyProductItem:myProduct];
        [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
        NSDictionary *params = @{@"Product ID": myProduct.productId ? : @"", @"Product Description": myProduct.productDescription ? : @""};
        ANALYTIC_PARAMS(@"Added Favorite to Shopping List", params);
    } else {
        [_shoppingListDataStore removeProductFromShoppingList:[_productDataStore getProduct:myProduct.productId]];
        NSDictionary *params = @{@"Product ID": myProduct.productId ? : @"", @"Product Description": myProduct.productDescription ? : @""};
        ANALYTIC_PARAMS(@"Removed Favorite from Shopping List", params);
    }
}

- (void)annimateAddToShoppingList:(UIImageView *)imageView {
    CGRect toRect = [IKOUtilities frameForTabInTabBar:self.tabBarController.tabBar withIndex:3 inView:self.view];
    CGPoint center = CGPointMake( CGRectGetMidX(toRect), CGRectGetMidY(toRect));
    [IKOUtilities animateAddToCartInView:self.view imageviewToAnimate:imageView animateToPoint:center];
}

@end
