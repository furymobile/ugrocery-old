//
//  IKOObjectSelectorViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 10/31/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOObjectSelectorVC : UITableViewController  <UISearchBarDelegate>

@property (nonatomic, copy) void (^completionBlock)(id object);
@property (nonatomic) Class klass;

@end
