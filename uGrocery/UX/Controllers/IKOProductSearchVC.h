//
//  IKOProductSearchViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOSearchBarView.h"
#import "IKOBarcodeScanner.h"
#import "IKOAddToShoppingListProtocol.h"

@class IKOSearchItem;
@class IKOProductCategory;

@interface IKOProductSearchVC : UIViewController <UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, IKOSearchBarViewDelegate, IKOBarcodeScannerDelegate, IKOAddToShoppingListProtocol>

@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITableView *recentsTableView;

@property (strong, nonatomic) IKOPagingInfo *pagingInfo;
@property (nonatomic) IKOSearchItem *selectedSearchItem;
@property (nonatomic) NSMutableArray *productSearchResults;
@property (nonatomic) IKOProductCategory *adhocCategory;
@property (nonatomic) BOOL shouldRemoveInfiniteScroll;

- (void)triggerSearchFromShoppingList;
- (void)setViewingProducts:(BOOL)viewingProducts;

@end
