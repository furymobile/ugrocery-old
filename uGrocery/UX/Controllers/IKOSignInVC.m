//
//  IKOSignInViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOSignInVC.h"
#import "IKOAuthenticationManager.h"
#import "UIViewController+Utilities.h"
#import "IKORegex.h"
#import "UIAlertView+Blocks.h"
#import "IKOSDK.h"
#import "IKOServerResponse.h"
#import "IKOError.h"
#import "UIColor+uGrocery.h"

@implementation IKOSignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    _emailAddress.delegate = self;
    _password.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (void)viewDidAppear:(BOOL)animated {
    [self performSelector:@selector(becomeFirstResponder) withObject:_emailAddress afterDelay:1];
}

#pragma mark - Validation

- (BOOL)isValid {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:kValidateEmailRegex options:NSRegularExpressionCaseInsensitive error:nil];
    BOOL validEmail = [regex numberOfMatchesInString:_emailAddress.text options:NSMatchingReportProgress range:NSMakeRange(0, [_emailAddress.text length])] > 0;
    if (!validEmail) {
        [UIAlertView showWithTitle:@"Invalid Email"
        message:@"Please enter a valid Email Address."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }

    if (_password.text.length == 0) {
        [UIAlertView showWithTitle:@"Password"
        message:@"Password is a required field."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }
    return YES;
}

- (IBAction)signIn:(id)sender {
    if ([self isValid]) {
        [[IKOSDK new]loginUser:_emailAddress.text
        password:_password.text
        callback:^(IKOError *error, IKOServerResponse *response) {
            if (response.success) {
                ANALYTIC(@"Successfully Signed In");
                [[IKOAuthenticationManager sharedInstance]authenticatedUser:response];
                if (_completionBlock)
                    _completionBlock();
            } else {
                ANALYTIC_ERROR(@"Unable to Sign In", error.error);
                DDLogError(@"Unable to sign in: %@", error.errorDescription);
                [UIAlertView showWithTitle:@"Unable to Sign In"
                message:SF(@"We were unable to sign in with the credentials supplied. %@", error.responseErrorDescription ? error.responseErrorDescription : @"")
                cancelButtonTitle:@"OK"
                otherButtonTitles:nil
                tapBlock:nil];
            }
        }];
    }
}

- (IBAction)cancel:(id)sender {
    if (_completionBlock)
        _completionBlock();
}

- (IBAction)forgotPassword:(id)sender {
    [self performSegueWithIdentifier:@"showForgotPassword" sender:self];
}

#pragma mark =- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _emailAddress) {
        [_password becomeFirstResponder];
    } else if (textField == _password) {
        [self signIn:self];
    }
    return YES;
}

@end
