//
//  IKOShoppingListViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOFLBHeaderView.h"
#import "IKOFLBHelper.h"
#import "IKOFLBRequestPayload.h"
#import "IKOProductDetailViewController.h"
#import "IKOProductSearchVC.h"
#import "IKOSDK.h"
#import "IKOShoppingListViewController.h"
#import "IKOUtilities.h"
#import "IKOZeroStateVC.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"
#import "UIView+CenteredSectionTitle.h"
#import "UIViewController+Utilities.h"
#import "IKOTabBarController.h"

NSUInteger const kCrossedOffCategory = 99999;
NSUInteger const kOtherCategory = 99998;

@interface IKOShoppingListViewController ()

@property (nonatomic) CGRect defaultTableRect;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;

@end

@implementation IKOShoppingListViewController {
    BOOL _crossed;
    BOOL _showingManageMenu;
    IKOFLBHeaderView *_flbHeaderView;
    IKOFLBHelper *_flbHelper;
    IKOManageHeaderView *_manageHeaderView;
    IKOProductCategory *_crossedOffCategory;
    IKOProductCategory *_otherCategory;
    IKOProductCategoryDataStore *_categoryDataStore;
    IKOProductDataStore *_productDataStore;
    NSIndexPath *_revealingCellIndexPath;
    NSMutableArray *_l1ids;
    NSMutableArray *_l1s;
    NSMutableDictionary *_items;
    NSString *_currentStore;
    UIBarButtonItem *_crossList;
    UIBarButtonItem *_manageButton;
    UIView *_footerView;
    UIView *_zeroStateView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];

    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _productDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _categoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];

    _otherCategory = [[IKOProductCategory alloc]init];
    _otherCategory.productCategoryId = [NSNumber numberWithInteger:kOtherCategory];
    _otherCategory.name = @"Other Items";

    _crossedOffCategory = [[IKOProductCategory alloc]init];
    _crossedOffCategory.productCategoryId = [NSNumber numberWithInteger:kCrossedOffCategory];
    _crossedOffCategory.name = @"Crossed Off";

    IKOZeroStateVC *_vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
    _zeroStateView = _vc.view;
    _zeroStateView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [UIScreen currentSize].height-113);

    _manageButton = [[UIBarButtonItem alloc]initWithTitle:@"manage" style:UIBarButtonItemStylePlain target:self action:@selector(manage:)];

    _footerView = self.tableView.tableFooterView;
    self.tableView.tableFooterView = nil;

    _flbHeaderView = [[IKOFLBHeaderView alloc]init];
    _flbHeaderView.delegate = self;
    _flbHeaderView.overlaysView = _tableView;
    _flbHeaderView.currentViewController = self;

    _manageHeaderView = [[IKOManageHeaderView alloc]init];
    _manageHeaderView.headerDelegate = self;
    _manageHeaderView.hidden = YES;
    [self.view insertSubview:_manageHeaderView aboveSubview:_tableView];

    _defaultTableRect = _tableView.frame;
    [self displayHeaderIfNeeded];
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Shopping List");
    [super viewWillAppear:animated];
    [_flbHeaderView resetHeaderView];
    [self fetch];
}

- (void)viewDidAppear:(BOOL)animated {
    [IKOAnalytics logApptentiveEvent:@"Shopping_List" fromViewController:self];
}

- (void)viewWillDisappear:(BOOL)animated {
    _flbHelper = nil;
    [_flbHeaderView resetHeaderView];
    [_manageButton setTitle:@"manage"];
    _manageHeaderView.hidden = YES;
    _showingManageMenu = NO;
    _flbHeaderView.hidden = NO;
}

- (void)viewDidDisappear:(BOOL)animated {
    for ( SWRevealTableViewCell *cell in [self.tableView visibleCells] )
        [cell setRevealPosition:SWCellRevealPositionCenter animated:YES];
}

- (void)showZeroStateIfNecessary {
    if ([_shoppingListDataStore allObjects].count == 0) {
        _zeroStateView.alpha = 0;
        [self.view insertSubview:_zeroStateView atIndex:999];
        [UIView animateWithDuration:0.2 animations:^{
            _zeroStateView.alpha = 1;
        }];

        self.tableView.userInteractionEnabled = NO;

        self.navigationItem.leftBarButtonItem = nil;
    } else {
        [_zeroStateView removeFromSuperview];
        self.tableView.userInteractionEnabled = YES;
        self.navigationItem.leftBarButtonItem = _manageButton;
    }
}

- (void)displayHeaderIfNeeded {
    if ([_shoppingListDataStore countofUncrossedItems] > 0 && [_shoppingListDataStore hasComparables]) {
        [self.view insertSubview:_flbHeaderView aboveSubview:_tableView];
        _tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 60)];
    } else {
        [_flbHeaderView removeFromSuperview];
        _tableView.tableHeaderView = nil;
    }
}

- (void)fetch {
    _items =[NSMutableDictionary new];
    _l1s = [NSMutableArray new];

    NSArray *shoppingListItems = [_shoppingListDataStore allObjects];

    _l1ids = [NSMutableArray arrayWithArray:[shoppingListItems valueForKeyPath:@"@distinctUnionOfObjects.l1Id"]];

    [_l1ids enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        [_l1s addObject:[_categoryDataStore getProductCategory:obj]];
    }];

    for (IKOProductCategory *category in _l1s) {
        NSMutableArray *itemsForCategory = [NSMutableArray new];
        [shoppingListItems enumerateObjectsUsingBlock:^(IKOShoppingListItem *obj, NSUInteger idx, BOOL *stop) {
            if ([obj.crossedOff isEqualToNumber:@0] && [obj.l1Id isEqualToNumber:category.productCategoryId])
                [itemsForCategory addObject:obj];
        }];
        [_items setObject:itemsForCategory forKey:category.productCategoryId];
    }

    NSMutableArray *otherItems = [NSMutableArray new];
    NSMutableArray *crossedOffItems = [NSMutableArray new];
    [shoppingListItems enumerateObjectsUsingBlock:^(IKOShoppingListItem *obj, NSUInteger idx, BOOL *stop) {
        if ([obj.crossedOff isEqualToNumber:@0] && (!obj.categoryId || [obj.categoryId isEqualToNumber:@0])) {
            obj.categoryId = _otherCategory.productCategoryId;
            [otherItems addObject:obj];
        }else if ([obj.crossedOff isEqualToNumber:@1]) {
            [crossedOffItems addObject:obj];
        }
    }];
    [_items setObject:otherItems forKey:_otherCategory.productCategoryId];
    [_items setObject:crossedOffItems forKey:_crossedOffCategory.productCategoryId];

    [_l1s addObject:_otherCategory];
    [_l1ids addObject:_otherCategory.productCategoryId];

    [_l1s addObject:_crossedOffCategory];
    [_l1ids addObject:_crossedOffCategory.productCategoryId];

    self.tableView.tableFooterView = nil;

    [self.tableView reloadData];

    [self displayHeaderIfNeeded];
    [self showZeroStateIfNecessary];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 90;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _l1s.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    return ((NSArray *)_items[s.productCategoryId]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    NSArray *productSubset = _items[s.productCategoryId];
    if (productSubset.count > 0)
        return 28;
    return 0.001;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    NSArray *productSubset = _items[s.productCategoryId];
    return (productSubset.count > 0) ?[UIView viewWithSectionHeader:s.name] : nil;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productSubset = _items[s.productCategoryId];
    IKOShoppingListItem *item = productSubset[indexPath.row];
    IKOShoppingListCell *cell = (IKOShoppingListCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ShoppingListCell" forIndexPath:indexPath];
    [cell configureWith:item];
    if ([item.crossedOff isEqual:@1] || !_currentStore) {
        [cell configureWithPrice:nil];
    } else {
        [cell configureWithPrice:[_flbHelper priceForProductId:item.productId inStore:_currentStore]];
    }
    cell.dataSource = self;
    cell.delegate = self;
    cell.crossOffDelegate = self;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productSubset = _items[s.productCategoryId];
    IKOShoppingListItem *item = productSubset[indexPath.row];

    if ([item.crossedOff integerValue] == 1)
        return;

    if ([item.isAdhoc integerValue] == 1)
        return;

    if (![item.productId isEqualToNumber:@0]) {
        [self performSegueWithIdentifier:@"showProductDetails" sender:self];
    } else {
        IKOProductSearchVC *productSearchVC = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductSearchViewController"];
        IKOProductCategory *s = _l1s[[self.tableView indexPathForSelectedRow].section];
        NSArray *productSubset = _items[s.productCategoryId];
        IKOShoppingListItem *shoppingListItem = productSubset[[self.tableView indexPathForSelectedRow].row];

        [productSearchVC setViewingProducts:YES];
        productSearchVC.selectedSearchItem = [[IKOSearchItem alloc]initWithShoppingListItem:shoppingListItem];
        [productSearchVC triggerSearchFromShoppingList];
        [self.navigationController pushViewController:productSearchVC animated:YES];
    }
}

#pragma mark - SWRevealTableViewCell delegate

- (void)revealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell willMoveToPosition:(SWCellRevealPosition)position {
    if ( position == SWCellRevealPositionCenter )
        return;

    for ( SWRevealTableViewCell *cell in [self.tableView visibleCells] ) {
        if ( cell == revealTableViewCell )
            continue;
        [cell setRevealPosition:SWCellRevealPositionCenter animated:YES];
    }
}

- (void)revealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell didMoveToPosition:(SWCellRevealPosition)position {
}

#pragma mark - SWRevealTableViewCell data source

- (NSArray *)leftButtonItemsInRevealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell {
    return nil;
}

- (NSArray *)rightButtonItemsInRevealTableViewCell:(SWRevealTableViewCell *)revealTableViewCell {
    SWCellButtonItem *item1 = [SWCellButtonItem itemWithTitle:@"Remove" handler:^(SWCellButtonItem *item, SWRevealTableViewCell *cell)
    {
        _revealingCellIndexPath = [self.tableView indexPathForCell:cell];
        [self deleteItemAtIndexPath:_revealingCellIndexPath];
        return YES;
    }];

    item1.backgroundColor = [UIColor redColor];
    item1.tintColor = [UIColor whiteColor];
    item1.width = 75;

    return @[item1];
}

- (void)deleteItemAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productSubset = _items[s.productCategoryId];
    IKOShoppingListItem *shoppingListItem = productSubset[indexPath.row];
    BOOL removed = [_shoppingListDataStore removeShoppingListItem:shoppingListItem];
    if (removed) {
        [self fetch];
        [self resetScreen];
    }
}

- (void)resetScreen {
    _flbHelper = nil;
    [_flbHeaderView resetHeaderView];
}

#pragma mark - IKOCrossOffShoppingListItemProtocol

- (void)crossOff:(IKOShoppingListItem *)item crossed:(BOOL)isCrossed sender:(id)sender {
    NSIndexPath *indexPath = [self.tableView indexPathForCell:sender];
    [_items enumerateKeysAndObjectsUsingBlock:^(id key, NSMutableArray *arr, BOOL *stop) {
        if ([arr containsObject:item]) {
            [arr removeObject:item];
        }
    }];
    [self.tableView beginUpdates];
    [self.tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationFade];
    [self.tableView endUpdates];

    [_shoppingListDataStore saveShoppingListItem:&item];
    [self performSelector:@selector(fetch) withObject:self afterDelay:0.6];

    if ([_shoppingListDataStore countofUncrossedItems] == 0) {
        [self displayHeaderIfNeeded];
        _flbHelper = nil;
        [_flbHeaderView resetHeaderView];
    }
}

#pragma mark - IKOFLBHeaderDelegate

- (void)launchStore {
    [((IKOTabBarController *)self.tabBarController)launchStore];
}

- (void)selectedStoreId:(NSString *)storeId {
    if (storeId) ANALYTIC_PARAMS(@"Changed Store", @{@"StoreID": storeId});
    _currentStore = storeId;
    [self.tableView reloadData];
}

- (void)findLowestBill {
    ANALYTIC(@"Triggered Find Lowest Bill");
    [IKOAnalytics logApptentiveEvent:@"Trigger_Find_Lowest_Bill_From_Shopping_List" fromViewController:self];
    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc]initWithShoppingListItems:[_shoppingListDataStore allObjects]];
    [[IKOSDK new]findLowestBillWithPayload:[payload JSONRepresentation] storeIds:@[@2,@4,@5,@7] user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *responseParams) {
#if TARGET_IPHONE_SIMULATOR
        DDLogVerbose(@"Response: %@", responseParams);
#endif
        _flbHelper = [[IKOFLBHelper alloc]initWithServerResponse:responseParams];
        [_flbHeaderView configureWithFLBHelper:_flbHelper];
        //_currentStore = [_flbHelper summaryWinner];
        [_flbHeaderView expandView];

        self.tableView.tableFooterView = _footerView;
        [self.tableView reloadData];
    }
    andMethod:@"list"];
}

#pragma mark - Actions

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    if (_flbHelper && !_currentStore) {
        [_flbHeaderView collapse];
    } else if (_flbHelper && _currentStore) {
        [_flbHeaderView setFrameToStoreId:_currentStore];
    }
}

- (IBAction)manage:(id)sender {
    ANALYTIC(@"Tapped Manage");
    _showingManageMenu = !_showingManageMenu;
    [_flbHeaderView setFrameToStoreId:_currentStore];
    [_manageHeaderView updateButtons:!([_shoppingListDataStore countofUncrossedItems] > 0)];
    if (_showingManageMenu) {
        [_manageButton setTitle:@"Done"];
        _manageHeaderView.hidden = NO;
        _flbHeaderView.hidden = YES;
    } else {
        [_manageButton setTitle:@"Manage"];
        _manageHeaderView.hidden = YES;
        _flbHeaderView.hidden = NO;
    }
}

- (IBAction)search:(id)sender {
    ANALYTIC(@"Tapped Search");
    [self performSegueWithIdentifier:@"showSearch" sender:self];
}

- (void)uncrossList:(BOOL)crossedOff {
    BOOL eventualCross = crossedOff;
    WEAK_SELF(weakSelf);
    [UIAlertView showWithTitle:SF(@"%@ List?", (eventualCross) ? @"Cross" : @"Uncross")
    message:SF(@"Are you sure you want to %@ this Shopping List?", (eventualCross) ? @"cross" : @"uncross")
    cancelButtonTitle:@"Cancel"
    otherButtonTitles:@[@"OK"]
    tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            ANALYTIC(@"Crossed off Entire List");
            [self manage:nil];
            [weakSelf.shoppingListDataStore crossOffList:eventualCross];
            _flbHelper = nil;
            [_flbHeaderView resetHeaderView];
            [self showZeroStateIfNecessary];
            [self displayHeaderIfNeeded];
            [self fetch];
        }
    }];
}

- (void)deleteList {
    WEAK_SELF(weakSelf);
    [UIAlertView showWithTitle:@"Delete List?"
    message:@"Are you sure you want to delete this Shopping List?"
    cancelButtonTitle:@"Cancel"
    otherButtonTitles:@[@"OK"]
    tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
        if (buttonIndex == 1) {
            ANALYTIC(@"Deleted Entire List");
            [weakSelf.shoppingListDataStore deleteList];
            _flbHeaderView = [[IKOFLBHeaderView alloc]init];
            _flbHeaderView.delegate = self;
            _flbHelper = nil;
            [self showZeroStateIfNecessary];
            [self displayHeaderIfNeeded];
            [self manage:nil];
            [self fetch];
        }
    }];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductDetailViewController *vc = [segue destinationViewController];
        IKOProductCategory *s = _l1s[[self.tableView indexPathForSelectedRow].section];
        NSArray *productSubset = _items[s.productCategoryId];
        IKOShoppingListItem *shoppingListItem = productSubset[[self.tableView indexPathForSelectedRow].row];
        vc.product = [_productDataStore getProduct:shoppingListItem.productId];
    }
}

@end
