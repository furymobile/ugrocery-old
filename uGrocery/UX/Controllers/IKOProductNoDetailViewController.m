//
//  IKOProductNoDetailViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 10/29/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

typedef NS_ENUM (NSInteger, IKOProductNoDetailRows) {
    ProductDetailRowPriceCapture = 0,
    ProductDetailRowUPCCode,
    ProductDetailRowDescription,
    ProductDetailRowCategory,
    ProductDetailRowBrand,
    ProductDetailRowSize,
    NUM_PRODUCT_DETAIL_ROWS
};

#import "IKOProductNoDetailViewController.h"
#import "IKOAuthenticationManager.h"
#import "IKOProduct.h"
#import "IKOBrand.h"
#import "IKOProductCategory.h"
#import "UIAlertView+Blocks.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOPriceUpdate.h"
#import "IKOObjectSelectorVC.h"
#import "UIImage+Rotation.h"

@interface IKOProductNoDetailViewController ()

@property (nonatomic) IKOProductNoDetailRows currentRow;

@end

@implementation IKOProductNoDetailViewController {
    NSArray *_stores;
    BOOL _priceUpdateExpanded;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildUI];
    _stores = [self.storeDataStore allObjects];
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    if (![self.settingsDataStore hasSettingForKey:kShowAdvancedPriceCapture])
        [self updatePrice];
}

- (void)buildUI {
    self.title = @"Product Details";
    if ([self showPriceCapture]) {
//        UIBarButtonItem *updateButton = [[UIBarButtonItem alloc] initWithTitle:@"Update" style:UIBarButtonItemStylePlain target:self action:@selector(update)];
//        self.navigationItem.rightBarButtonItem = updateButton;
        self.tableView.tableHeaderView = nil;
        self.tableView.tableFooterView = nil;
    }
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([self showPriceCapture]) {
        return NUM_PRODUCT_DETAIL_ROWS;
    }
    return 3;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row < 2 && ![self showPriceCapture]) {
        return 0;
    } else if (indexPath.row == 0) {
        return [[IKOAuthenticationManager sharedInstance] isAdministrator] ? 279 : 222;
    }

    return 60;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == ProductDetailRowPriceCapture) {
        IKOPriceUpdateCell *pcell = (IKOPriceUpdateCell *)[self.tableView dequeueReusableCellWithIdentifier:@"PriceUpdateCell" forIndexPath:indexPath];
        pcell.selectionStyle = UITableViewCellSelectionStyleNone;
        pcell.priceUpdateDelegate = self;
        [pcell configureWith:self.price andStoreName:self.store.name ? : @"" andSalePrice:self.salePrice andSaleEndDate:self.saleEndDate andSaleDescription:self.saleDescription andStores:[self.storeDataStore allObjects]];
        [self colorText:pcell.updatedPrice forObject:self.price];
        [self colorText:pcell.salePrice forObject:self.salePrice];
        [self colorText:pcell.updatedStore forObject:self.store];
        [self colorText:pcell.saleDescription forObject:self.saleDescription];
        [self colorText:pcell.saleEndDate forObject:self.saleEndDate];

        return pcell;
    }

    IKOFieldCell *cell = (IKOFieldCell *)[self.tableView dequeueReusableCellWithIdentifier:@"FieldCell" forIndexPath:indexPath];
    cell.textField.keyboardType = UIKeyboardTypeDefault;
    switch (indexPath.row) {
        case ProductDetailRowUPCCode:
            [cell configureWithLabel:@"UPC Code" andValue:self.product.upc andKey:@"upc"];
            cell.userInteractionEnabled = NO;
            break;
        case ProductDetailRowDescription:
            [cell configureWithLabel:@"Description" andValue:self.product.productDescription andKey:@"productDescription"];
            if (![self showPriceCapture]) {
                cell.textField.returnKeyType = UIReturnKeyDone;
                [cell.textField becomeFirstResponder];
            }
            break;
        case ProductDetailRowCategory:
            [cell configureWithLabel:@"L3 Category" andValue:self.category.name ? : @"" andKey:@"categoryId"];
            cell.textField.userInteractionEnabled = NO;
            cell.textField.tag = ProductDetailRowCategory + 100;
            cell.delegate = self;
            [self colorText:cell.textField forObject:self.category];
            [cell showClearButton];
            break;
        case ProductDetailRowBrand:
            [cell configureWithLabel:@"Brand" andValue:self.brand.name ? : @"" andKey:@"brandId"];
            cell.textField.userInteractionEnabled = NO;
            cell.textField.tag = ProductDetailRowBrand + 100;
            cell.delegate = self;
            [self colorText:cell.textField forObject:self.brand];
            [cell showClearButton];
            break;
        case ProductDetailRowSize:
            [cell configureWithLabel:@"Size" andValue:self.size andKey:@"size"];
            break;
        default:
            break;
    }
    if (!cell.textField.tag)
        cell.textField.tag = indexPath.row;
    cell.textField.delegate = self;
    cell.selectionStyle = UITableViewCellSelectionStyleNone;
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOFieldCell *cell = (IKOFieldCell *)[self.tableView cellForRowAtIndexPath:indexPath];
    switch (indexPath.row) {
        case ProductDetailRowCategory:
        case ProductDetailRowBrand:
            [self.hasEdited addObject:[NSNumber numberWithInteger:cell.textField.tag]];
            [self colorText:cell.textField forObject:[NSString new]];
            [self.currentField resignFirstResponder];
            self.currentField = cell.textField;
            [self performSegueWithIdentifier:@"selectObject" sender:self];
            break;
        default:
            if (indexPath.row != 0)
                self.currentField = cell.textField;
            break;
    }
}

- (BOOL)isValid {
    if (self.product.productDescription.length == 0) {
        [UIAlertView showWithTitle:@"Description Required"
        message:@"A description is required in order to add this item to your Shopping List"
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }
    return YES;
}

- (IBAction)addToShoppingList:(id)sender {
    if ([self isValid]) {
        [self.currentField resignFirstResponder];
        [super addToShoppingList:self.product];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark UITextFieldDelegate

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString *proposedNewString = [[textField text]stringByReplacingCharactersInRange:range withString:string];
    switch (textField.tag) {
        case ProductDetailRowDescription:
            self.product.productDescription = proposedNewString;
            break;
        case ProductDetailRowSize:
            self.size = proposedNewString;
            break;
        default:
            break;
    }
    return YES;
}

- (void)update {
    if (self.price != nil && self.store == nil) {
        [UIAlertView showWithTitle:@"Store Required"
        message:@"A store is required when you enter a price."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
    } else {
        [self updatePrice];
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (void)updatePrice {
    IKOPriceUpdate *submission = [[IKOPriceUpdate alloc]initWithProduct:self.product andUser:[IKOAuthenticationManager currentUser]];

    submission.brandId = self.brand.brandId;
    submission.l3Id = self.category.productCategoryId;

    [super sendProductUpdate:submission];
}

#pragma mark - IKOFieldCellDelegate

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"selectObject"]) {
        IKOObjectSelectorVC *vc = [segue destinationViewController];
        if (self.currentField.tag == ProductDetailRowCategory + 100) {
            vc.klass = [IKOProductCategory class];
            vc.completionBlock = ^(IKOProductCategory *category) {
                self.category = category;
                self.currentField.text = category.name;
                [self.navigationController popViewControllerAnimated:YES];
            };
        } else if (self.currentField.tag == ProductDetailRowBrand + 100) {
            vc.klass = [IKOBrand class];
            vc.completionBlock = ^(IKOBrand *brand) {
                self.brand = brand;
                self.currentField.text = brand.name;
                [self.navigationController popViewControllerAnimated:YES];
            };
        }
    }
}

#pragma mark - IKOPriceUpdateCellDelegate

- (void)toggleExpansion {
}

- (void)updateWith:(NSString *)price andStoreName:(NSString *)storeName andSalePrice:(NSString *)salePrice andSaleEndDate:(NSNumber *)saleEndDate andSaleDescription:(NSString *)saleDescription andPpfSafe:(BOOL)ppfSafe {
    self.price = price;
    self.salePrice = salePrice;
    self.saleDescription = saleDescription;
    self.saleEndDate = saleEndDate;
    self.ppfSafe = ppfSafe;
    [self update];
}

- (void)textFieldDidBeginEditing:(UITextField *)textField {
    [self.hasEdited addObject:[NSNumber numberWithInteger:textField.tag]];
    [self colorText:textField forObject:self.price];
    self.currentField = textField;
}

- (void)textFieldDidClear:(UITextField *)textField {
    if (textField.tag == 1) {
        self.price = nil;
    } else if (textField.tag == 3) {
        self.salePrice = nil;
    } else if (textField.tag == 4) {
        self.saleEndDate = nil;
    } else if (textField.tag == 5) {
        self.saleDescription = nil;
    } else if (textField.tag == ProductDetailRowBrand + 100) {
        self.brand = nil;
    } else if (textField.tag == ProductDetailRowCategory + 100) {
        self.category = nil;
    }
}

- (void)selectedStore:(IKOStore *)store {
    self.store = store;
}

@end
