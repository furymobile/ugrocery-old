//
//  IKOLaunchViewController.h
//  uGrocery
//
//  Created by A750954 on 3/3/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOSizedBackgroundVC.h"

@interface IKOLaunchVC : IKOSizedBackgroundVC

@property (weak, nonatomic) IBOutlet UIImageView *spinner;

@end
