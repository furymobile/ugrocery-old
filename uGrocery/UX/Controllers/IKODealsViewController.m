//
//  IKODealsViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 9/1/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKODealsHeaderView.h"
#import "IKODealsProductCell.h"
#import "IKODealsViewController.h"
#import "IKOFLBHelper.h"
#import "IKOFLBRequestPayload.h"
#import "IKOProductDetailViewController.h"
#import "IKOSDK.h"
#import "IKOUtilities.h"
#import "IKOZeroStateVC.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"
#import "UIView+Animation.h"
#import "UIView+CenteredSectionTitle.h"
#import "UIViewController+Utilities.h"

@interface IKODealsViewController ()

@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOFavoritesDataStore *myProductsDataStore;
@property (nonatomic) IKOProductCategoryDataStore *categoryDataStore;
@property (nonatomic) IKOProductDataStore *productDataStore;
@property (nonatomic) IKOSettingsDataStore *settingDataStore;
@property (nonatomic) NSString *selectedStoreId;

@end

@implementation IKODealsViewController {
    IKOFLBHelper *_flbHelper;
    IKODealsHeaderView *_dealsHeaderView;
    NSMutableArray *_l1ids;
    NSMutableArray *_l1s;
    UIView *_zeroStateView;
    NSMutableDictionary *_products;
    NSMutableSet *_productsCache;
    UIView *_waitView;
    UIBarButtonItem *_checkButton;
    UIBarButtonItem *_addButton;
    NSDictionary *_activeAttributes;
    NSDictionary *_inactiveAttributes;
    UIBarButtonItem *_toggleDealsButton;
    BOOL _hasMyProducts;
    IKOProductCategory *_myDealsCategory;
    NSMutableDictionary *_myDealsProductCategoryMapping;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];

    _myProductsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _categoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    _productDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _settingDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];

    _products = [NSMutableDictionary new];
    _productsCache = [NSMutableSet new];

    _myDealsCategory = [IKOProductCategory new];
    _myDealsCategory.productCategoryId = @99992;
    _myDealsCategory.name = @"My Deals";
    _myDealsProductCategoryMapping = [NSMutableDictionary new];

    _selectedStoreId = [_settingDataStore hasSettingForKey:kLastDealsStore] ?[_settingDataStore objectForKey:kLastDealsStore] : @"0";

    _dealsHeaderView = [[IKODealsHeaderView alloc]init];
    _dealsHeaderView.delegate = self;
    _dealsHeaderView.overlaysView = _tableView;

    [_dealsHeaderView setFrameToStoreId:_selectedStoreId];

    [self findDeals:YES];
}

- (void)buildZeroState {
    IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
    [vc loadView];
    if ([IKONetworkManager connectedToNetwork]) {
        vc.zeroStateTitle.text = @"No available Deals";
        vc.zeroStateMessage.text = @"There are no available Deals to display at this time.  Try again shortly to find the latest deals.";
    } else {
        vc.zeroStateTitle.text = @"No Connection";
        vc.zeroStateMessage.text = @"Oops, currently you aren’t connected, please try again with an Internet connection.";
    }
    vc.screenImage.image = [UIImage imageNamed:@"icon-deals"];
    _zeroStateView = vc.view;
    _zeroStateView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
}

- (void)viewDidAppear:(BOOL)animated {
    [IKOAnalytics logApptentiveEvent:@"Show_Deals" fromViewController:self];
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Deals Screen");
    [super viewWillAppear:animated];
    NSIndexPath *selectedPath = [self.tableView indexPathForSelectedRow];

    if (selectedPath) {
        IKOProductCategory *s = _l1s[[self.tableView indexPathForSelectedRow].section];
        NSArray *productForCategory = _products[[s.productCategoryId stringValue]];
        IKOProduct *product = productForCategory[selectedPath.row];
        IKODealsProductCell *cell = (IKODealsProductCell *)[self.tableView cellForRowAtIndexPath:selectedPath];
        if (product.productId && cell)
            [cell selectItem:[_shoppingListDataStore isOnShoppingList:product.productId]];
    }
    [self.tableView deselectRowAtIndexPath:selectedPath animated:YES];
}

- (void)displayHeaderIfNeeded {
    if (_flbHelper) {
        [self.view insertSubview:_dealsHeaderView aboveSubview:_tableView];
        _tableView.tableHeaderView = [[UIView alloc]initWithFrame:CGRectMake(0, 0, _tableView.frame.size.width, 60)];
    } else {
        [_dealsHeaderView removeFromSuperview];
        _tableView.tableHeaderView = nil;
    }
}

- (void)displayWaitIndicator:(BOOL)show {
    [_zeroStateView removeFromSuperview];
    self.tableView.userInteractionEnabled = YES;
    if (show) {
        IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
        [vc loadView];
        vc.zeroStateTitle.text = @"Fetching the Hottest Deals!";
        vc.zeroStateMessage.text = @"We're fetching the hottest deals... Just a few more moments!";
        vc.screenImage.image = [UIImage imageNamed:@"spinner"];
        [vc.screenImage spinWithDuration:15 rotations:1 repeat:10000];
        _waitView = vc.view;
        _waitView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
        _waitView.alpha = 0;
        [self.view insertSubview:_waitView atIndex:999];
        [UIView animateWithDuration:0.2 animations:^{
            _waitView.alpha = 1;
        }];
        self.tableView.userInteractionEnabled = NO;
    } else {
        [_waitView removeFromSuperview];
        _waitView = nil;
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void)showZeroStateIfNecessary {
    [self buildZeroState];
    if ([_products allKeys].count == 0) {
        _selectedStoreId = @"0";
        [_dealsHeaderView setFrameToStoreId:_selectedStoreId];
        _zeroStateView.alpha = 0;
        [self.view insertSubview:_zeroStateView atIndex:998];
        [UIView animateWithDuration:0.2 animations:^{
            _zeroStateView.alpha = 1;
        }];
        _dealsHeaderView.hidden = YES;
        self.tableView.userInteractionEnabled = NO;
    } else {
        [_zeroStateView removeFromSuperview];
        _dealsHeaderView.hidden = NO;
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void)findDeals:(BOOL)honorCache {
    _flbHelper = nil;
    _l1ids = nil;
    _l1s = nil;
    _products = nil;
    _productsCache = nil;
    [self.tableView reloadData];
    [self displayWaitIndicator:YES];
    [self displayHeaderIfNeeded];
    IKOFLBRequestPayload *payload = [IKOFLBRequestPayload new];
    NSNumber *settingValue = [_settingDataStore objectForKey:kShowCoupons];
    payload.withCoupons = [NSNumber numberWithBool:[settingValue boolValue]];

    BOOL useCache = NO;
    BOOL hasCache = NO;
    NSString *lastUpdatedCacheKey;
    NSString *dealsCacheKey;

    if (_selectedStoreId == 0) {
        lastUpdatedCacheKey = kLastDealsUpdatedOn;
        hasCache = [_settingDataStore hasSettingForKey:lastUpdatedCacheKey];
        dealsCacheKey = kLastDeals;
    } else {
        lastUpdatedCacheKey = SF(@"%@.Store.%@", kLastDealsUpdatedOn, _selectedStoreId);
        hasCache = [_settingDataStore hasSettingForKey:lastUpdatedCacheKey];
        dealsCacheKey = SF(@"%@.Store.%@", kLastDeals, _selectedStoreId);
    }
    if (honorCache && hasCache) {
        NSDate *lastUpdated = [_settingDataStore objectForKey:lastUpdatedCacheKey];
        NSDate *updateAfter = [lastUpdated dateByAddingTimeInterval:86400]; //24 hours
        if ([updateAfter compare:[NSDate date]] == NSOrderedDescending) {
            useCache = YES;
        }
    }

    if (honorCache && useCache) {
        _flbHelper = [[IKOFLBHelper alloc]initWithServerResponse:[_settingDataStore objectForKey:dealsCacheKey]];
        if ([_flbHelper.flbData allKeys].count == 0) {
            //clear out everything
        } else {
            [self fetch];
            return;
        }
    }

    NSArray *storesToRetrieve;
    if ([_selectedStoreId isEqualToString:@"0"]) {
        storesToRetrieve = @[@2,@4,@5,@7];
    } else {
        NSNumberFormatter *f = [[NSNumberFormatter alloc]init];
        [f setNumberStyle:NSNumberFormatterDecimalStyle];
        NSNumber *n = [f numberFromString:_selectedStoreId];
        storesToRetrieve = @[n];
    }
    [[IKOSDK new]findDealsTopWithPayload:[payload JSONRepresentation]
    storeIds:storesToRetrieve
    user:[IKOAuthenticationManager currentUser]
    callback:^(IKOError *error, IKOServerResponse *response) {
        [_settingDataStore setObject:response forKey:dealsCacheKey];
        [_settingDataStore setObject:[NSDate date] forKey:lastUpdatedCacheKey];
        _flbHelper = [[IKOFLBHelper alloc]initWithServerResponse:response];
        [self fetch];
    }];
}

- (void)fetch {
    _l1ids = [NSMutableArray arrayWithArray:[_flbHelper categoriesForDeals]];
    _products = [NSMutableDictionary new];
    _productsCache = [NSMutableSet new];
    _l1s = [NSMutableArray new];

    NSMutableArray *myDealProducts = [NSMutableArray new];

    [_l1ids enumerateObjectsUsingBlock:^(id l1id, NSUInteger idx, BOOL *stop) {
        IKOProductCategory *pc = [_categoryDataStore getProductCategory:l1id];
        if(pc)
            [_l1s addObject:pc];

        NSArray *products = [_flbHelper productsForDealsCategory:l1id];
        NSMutableArray *tempProducts = [NSMutableArray new];
        [products enumerateObjectsUsingBlock:^(IKOProduct *tempProduct, NSUInteger idx, BOOL *stop) {
            if(tempProduct) {
                [_productDataStore saveProduct:tempProduct];
                if ([_myProductsDataStore getMyProductItemForProductId:tempProduct.productId] != nil) {
                    [myDealProducts addObject:tempProduct];
                    [_myDealsProductCategoryMapping setObject:pc forKey:tempProduct.productId];
                }

                [tempProducts addObject:tempProduct];
                [_productsCache addObject:tempProduct];
            }
        }];
        _products[l1id] = tempProducts;
    }];

    if ([[IKOAuthenticationManager sharedInstance]isPremium]) {
        if (myDealProducts.count > 0) {
            _products[SF(@"%@", _myDealsCategory.productCategoryId)] = myDealProducts;
            [_l1ids insertObject:_myDealsCategory.productCategoryId atIndex:0];
            [_l1s insertObject:_myDealsCategory atIndex:0];
        }
    }

    [self displayWaitIndicator:NO];
    [self showZeroStateIfNecessary];
    [self displayHeaderIfNeeded];
    [self.tableView reloadData];
}

#pragma mark - Table view data source

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 105;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _l1ids.count;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    return ((NSArray *)_products[SF(@"%@",s.productCategoryId)]).count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    IKOProductCategory *s = _l1s[section];
    return [UIView viewWithSectionHeader:s.name];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOProductCategory *s = _l1s[indexPath.section];
    NSArray *productForCategory = _products[SF(@"%@", s.productCategoryId)];
    IKOProduct *product = productForCategory[indexPath.row];

    if ([s.productCategoryId isEqual:@99992])
        s = _myDealsProductCategoryMapping[product.productId];

    IKODealsProductCell *cell = (IKODealsProductCell *)[self.tableView dequeueReusableCellWithIdentifier:@"DealsProductCell" forIndexPath:indexPath];
    [cell configureWith:product andProductNode:[_flbHelper productNodeForCategoryId:s.productCategoryId andProductId:product.productId]];
    [cell selectItem:[_shoppingListDataStore isOnShoppingList:product.productId]];
    cell.selectionDelegate = self;

    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:@"showProductDetails" sender:self];
}

#pragma mark - IKODealsProductSelectionDelegate

- (void)selectProduct:(IKOProduct *)product selected:(BOOL)selected {
    if (selected) {
        IKOShoppingListItem *shoppingListItem = [[IKOShoppingListItem alloc]initWithProduct:product];
        BOOL added = [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
        if (added) {
            IKOFavoriteItem *myProductItem = [[IKOFavoriteItem alloc]initWithProduct:product];
            myProductItem.l1Id = shoppingListItem.l1Id;
            [_myProductsDataStore saveMyProductItem:&myProductItem];
            NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ? : @"", @"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
            ANALYTIC_PARAMS(@"Added Deal to Shopping List", params);
        }
    } else {
        [_shoppingListDataStore removeProductFromShoppingList:product];
    }
}

- (void)annimateAddToShoppingList:(UIImageView *)imageView {
    CGRect toRect = [IKOUtilities frameForTabInTabBar:self.tabBarController.tabBar withIndex:3 inView:self.view];
    CGPoint center = CGPointMake( CGRectGetMidX(toRect), CGRectGetMidY(toRect));
    [IKOUtilities animateAddToCartInView:self.view imageviewToAnimate:imageView animateToPoint:center];
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductCategory *s = _l1s[[self.tableView indexPathForSelectedRow].section];
        NSArray *productForCategory = _products[[s.productCategoryId stringValue]];
        IKOProduct *product = productForCategory[[self.tableView indexPathForSelectedRow].row];

        if (!product) {
            //need to get it, add it and set it
        }
        IKOProductDetailViewController *vc = [segue destinationViewController];
        vc.product = product;
    }
}

- (IBAction)refresh:(id)sender {
    [_settingDataStore removeObjectForKey:kLastDealsUpdatedOn];
    [_settingDataStore removeObjectForKey:kLastDeals];
    
    [self findDeals:NO];
}

#pragma mark - IKODealsHeaderDelegate

- (void)scrollViewWillBeginDragging:(UIScrollView *)scrollView {
    [_dealsHeaderView setFrameToStoreId:_selectedStoreId];
}

- (void)selectedStoreId:(NSString *)storeId {
    if (storeId) ANALYTIC_PARAMS(@"Changed Store", @{@"StoreID": storeId});
    _selectedStoreId = storeId;
    if ([storeId integerValue] > 0) {
        [_settingDataStore setObject:storeId forKey:kLastDealsStore];
    } else {
        [_settingDataStore removeObjectForKey:kLastDealsStore];
    }
    [self findDeals:YES];
}

@end
