//
//  IKOLandingViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 11/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOLandingVC.h"
#import "IKOAuthenticationManager.h"
#import "IKOSignInVC.h"
#import "IKORegisterUserVC.h"
#import "UIViewController+Utilities.h"
#import "UIAlertView+Blocks.h"
#import "IKOUtilities.h"

@implementation IKOLandingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
        [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
    [self setupViewController];
}

- (void)viewWillAppear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

- (IBAction)signin:(id)sender {
    ANALYTIC(@"Tapped Sign In");
    if ([self checkForInternet])
        [self performSegueWithIdentifier:@"signin" sender:self];
}

- (IBAction)signup:(id)sender {
    ANALYTIC(@"Tapped Sign Up");
    if ([self checkForInternet])
        [self performSegueWithIdentifier:@"signup" sender:self];
}

- (BOOL)checkForInternet {
    if ([IKOUtilities notConnectedAlert])
        return NO;
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"signin"]) {
        IKOSignInVC *vc = [segue destinationViewController];
        vc.completionBlock = ^{
            [self.navigationController popViewControllerAnimated:NO];
            if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
                [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
        };
    } else if ([[segue identifier]isEqualToString:@"signup"]) {
        IKORegisterUserVC *vc = [segue destinationViewController];
        vc.completionBlock = ^{
            [self.navigationController popViewControllerAnimated:NO];
            if ([[IKOAuthenticationManager sharedInstance]isAuthenticated])
                [self performSegueWithIdentifier:@"userAuthenticated" sender:self];
        };
    }
}

@end
