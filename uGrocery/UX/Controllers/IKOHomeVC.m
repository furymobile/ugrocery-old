//
//  IKONewHomeViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 2/20/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

typedef NS_ENUM (NSInteger, IKOHomeRows) {
    IKOHomeNavigationRow = 0,
    IKOHomeStoreLogoRow = 1,
    IKOHomeViewTopDealsRow = 2,
    IKOHomeViewFavoritesRow = 3,
    IKOHomeCategoryHeaderRow = 4
};

#import "IKOHomeVC.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UIViewController+Utilities.h"
#import "UIAlertView+Blocks.h"
#import "IKOAuthenticationManager.h"
#import "IKOSDK.h"
#import "IKOLocationManager.h"
#import "IKOTabBarController.h"
#import "IKOL1Cell.h"
#import "UIScreen+Additions.h"
#import "IKOProductListVC.h"
#import "IKOMainNavigationCell.h"
#import "IKOProductDetailViewController.h"
#import "IKOUtilities.h"

@interface IKOHomeVC ()

@property (nonatomic) BOOL showRenewMessage;
@property (nonatomic) NSArray* L1s;
@property (nonatomic) IKOProductCategoryDataStore *productCategoryDataStore;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOBarcodeScanner *barcodeScanner;

@end

@implementation IKOHomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tabBarController.navigationController setNavigationBarHidden:YES animated:NO];
    self.navigationItem.titleView = [[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logo-ugrocery"] ];
    
    _barcodeScanner = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoBarcodeScanner"];
    _barcodeScanner.delegate = self;
    
    [self loadData];
    if ([[IKOAuthenticationManager sharedInstance] daysRemainingOnSubscription] > 0)
        _showRenewMessage = [[IKOAuthenticationManager sharedInstance] daysRemainingOnSubscription] < 3;
    
    [self checkForUpgrade];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    __weak __typeof__(self) weakSelf = self;
    [[IKOLocationManager sharedInstance]startUpdatingLocation];
    [[NSNotificationCenter defaultCenter]postNotificationName:IKONotificationShoppingListChanged object:_shoppingListDataStore];
    if (_showRenewMessage){
        
        [UIAlertView showWithTitle:@"Please Renew"
                           message:SF(@"Your subscription to uGrocery will expire in %d days! Renew now to maintain Premium access", [[IKOAuthenticationManager sharedInstance] daysRemainingOnSubscription])
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:@[@"Renew"]
                          tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                              weakSelf.showRenewMessage = false;
                              if (buttonIndex == 1) {
                                  [(IKOTabBarController *)weakSelf.navigationController.tabBarController launchStore];
                              }
                          }];
    }
    
    if ([IKOAuthenticationManager sharedInstance].showFirstLaunchDialog)
        [self showFirstLaunchDialog];
    
    [IKOAnalytics logApptentiveEvent:@"App_Launch" fromViewController:self];
}
- (void)loadData {
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _productCategoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    
    _L1s = [[_productCategoryDataStore categoriesForLevel:@1] sortedArrayUsingComparator:^NSComparisonResult(IKOProductCategory *obj1, IKOProductCategory *obj2) {
        if ([obj1.sort integerValue] > [obj2.sort integerValue]) {
            return (NSComparisonResult)NSOrderedDescending;
        }
        
        if ([obj1.sort integerValue] < [obj2.sort integerValue]) {
            return (NSComparisonResult)NSOrderedAscending;
        }
        return (NSComparisonResult)NSOrderedSame;
    }];
}

- (void)launchStore {
    [((IKOTabBarController *)self.tabBarController)launchStore];
}

- (void)showFirstLaunchDialog {
    [UIAlertView showWithTitle:@"Welcome to uGrocery"
                       message:@"We hope you enjoy uGrocery Premium free for 7 days. Make your list & tap 'Find Lowest Bill' to always know the best price on your groceries."
             cancelButtonTitle:@"Start Saving!"
             otherButtonTitles:nil
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          [IKOAuthenticationManager sharedInstance].showFirstLaunchDialog = NO;
                      }];
}

- (void)checkForUpgrade {
    [[IKOSDK new]getApiVersion:^(IKOError *error, IKOServerResponse *response) {
        if (!error) {
            if ([SF(@"%@",response.payload[@"BcBreak"])isEqualToString:@"1"]) {
                [UIAlertView showWithTitle:@"Update Required"
                                   message:@"An update is available for uGrocery.  Please update your application in the AppStore."
                         cancelButtonTitle:@"Cancel"
                         otherButtonTitles:@[@"OK"]
                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                      if (buttonIndex == 1) {
                                          [[UIApplication sharedApplication]openURL:[NSURL URLWithString:@"itms-apps://"]];
                                      }
                                  }];
            }
        }
    }];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0)
        return 5;
    return _L1s.count;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case IKOHomeNavigationRow:
                return [UIScreen currentSize].width / 3;
                break;
            case IKOHomeViewFavoritesRow:
            case IKOHomeViewTopDealsRow:
                return 61;
                break;
            case IKOHomeStoreLogoRow:
                return 67;
                break;
            case IKOHomeCategoryHeaderRow:
                return 41;
                break;
                
        }
    }
    return 79;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell;
    if (indexPath.section == 0) {
        switch (indexPath.row) {
            case IKOHomeNavigationRow: {
                IKOMainNavigationCell *navCell = (IKOMainNavigationCell *)[tableView dequeueReusableCellWithIdentifier:@"navigationRow" forIndexPath:indexPath];
                [navCell.searchButton addTarget:self action:@selector(triggerSearch:) forControlEvents:UIControlEventTouchUpInside];
                [navCell.scanButton addTarget:self action:@selector(triggerScan:) forControlEvents:UIControlEventTouchUpInside];
                [navCell.browseButton addTarget:self action:@selector(triggerBrowse:) forControlEvents:UIControlEventTouchUpInside];
                return navCell;
                break;
            }
            case IKOHomeStoreLogoRow:
                cell = [tableView dequeueReusableCellWithIdentifier:@"logosRow" forIndexPath:indexPath];
                return cell;
                break;
            case IKOHomeViewFavoritesRow:
                return [tableView dequeueReusableCellWithIdentifier:@"favoritesRow" forIndexPath:indexPath];
                break;
            case IKOHomeViewTopDealsRow:
                return [tableView dequeueReusableCellWithIdentifier:@"dealsRow" forIndexPath:indexPath];
                break;
            case IKOHomeCategoryHeaderRow:
                return [tableView dequeueReusableCellWithIdentifier:@"categoryHeaderRow" forIndexPath:indexPath];
                break;
        }
    } else {
        IKOL1Cell *l1Cell  = [tableView dequeueReusableCellWithIdentifier:@"l1Row" forIndexPath:indexPath];
        IKOProductCategory *category = _L1s[indexPath.row];
        l1Cell.categoryImage.image = [UIImage imageNamed:SF(@"%@",category.productCategoryId)];
        l1Cell.categoryName.text = l1Cell.categoryShadow.text = category.name;
        return l1Cell;
    }
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0 && indexPath.row == IKOHomeViewTopDealsRow) {
        [self performSegueWithIdentifier:@"showDeals" sender:self];
    }
}

-(IBAction)triggerSearch:(id)sender {
    [self performSegueWithIdentifier:@"showSearch" sender:self];
}

-(IBAction)triggerScan:(id)sender {

        if (![IKOAuthenticationManager sharedInstance].isAdministrator & [IKOUtilities notConnectedAlert])
            return;
        if ([IKOUtilities hasCameraPermission]) {
            ANALYTIC(@"Tapped Scan Barcode");
            [self.navigationController pushViewController:_barcodeScanner animated:NO];
        } else {
            [UIAlertView showWithTitle:@"Cannot Access Camera"
                               message:@"Check your device settings and ensure you have allowed uGrocery to access your camera."
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:nil];
        }

}

-(IBAction)triggerBrowse:(id)sender {
    [self.tableView scrollToRowAtIndexPath:[NSIndexPath indexPathForItem:IKOHomeCategoryHeaderRow inSection:0] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProducts"]) {
        IKOProductListVC *vc = [segue destinationViewController];
        vc.selectedCategory = _L1s[[self.tableView indexPathForSelectedRow].row];
    } else if ([[segue identifier]isEqualToString:@"showDeals"]) {
        IKOProductListVC *vc = [segue destinationViewController];
        vc.selectedCategory = nil;
    }
}

#pragma mark - IKOBarcodeScannerDelegate

- (void)closeScanner {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)showProductDetailsFor:(IKOProduct *)product {
    IKOProductDetailViewController *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductDetails"];
    vc.product = product;
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}


@end
