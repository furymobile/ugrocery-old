//
//  IKOProductCategoryListVC.m
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import "IKOProductCategoryListVC.h"
#import "UIViewController+Utilities.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOProductListVC.h"
#import "UIColor+uGrocery.h"

@interface IKOProductCategoryListVC ()

@property (nonatomic) IKOProductCategoryDataStore *productCategoryDataStore;
@property (nonatomic) NSArray *categories;

@end

@implementation IKOProductCategoryListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self loadData];
    
    self.title = _selectedCategory.name;
    self.tableView.backgroundColor = [UIColor UGReallyLtGrey];
    self.edgesForExtendedLayout = UIRectEdgeNone;
}

- (void)loadData {
    _productCategoryDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOProductCategory class]];
    _categories = [_productCategoryDataStore childrentForCategory:_selectedCategory.productCategoryId];
    _categories = [_categories filteredArrayUsingPredicate:[NSPredicate predicateWithBlock:^BOOL(IKOProductCategory *object, NSDictionary *bindings) {
        return ![object.name hasPrefix:@"placeholde"];
    }]];
}

- (void)viewDidDisappear:(BOOL)animated {
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _categories.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 60;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewCell *cell = (UITableViewCell *)[self.tableView dequeueReusableCellWithIdentifier:@"l3Cell" forIndexPath:indexPath];
    IKOProductCategory *category = _categories[indexPath.row];
    UILabel *label = [(UILabel*) cell viewWithTag:666];
    label.text = category.name.lowercaseString;
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProducts"]) {
        IKOProductListVC *vc = [segue destinationViewController];
        vc.selectedCategory = _categories[[self.tableView indexPathForSelectedRow].row];
    }
}

@end
