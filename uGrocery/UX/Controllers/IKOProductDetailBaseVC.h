//
//  IKOProductDetailBaseViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 10/30/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "IKOAddToShoppingListProtocol.h"

@class IKOBrand;
@class IKOFavoriteItem;
@class IKOFavoritesDataStore;
@class IKOPriceUpdate;
@class IKOPriceUpdateDataStore;
@class IKOProduct;
@class IKOProductCategory;
@class IKOSettingsDataStore;
@class IKOShoppingListDataStore;
@class IKOShoppingListItem;
@class IKOStore;
@class IKOStoreDataStore;
@class IKOServerResponse;
@class IKOProductDataStore;

@interface IKOProductDetailBaseVC : UITableViewController <IKOAddToShoppingListProtocol>

@property (nonatomic) IKOFavoriteItem *myProductItem;
@property (nonatomic) IKOFavoritesDataStore *myProductsDataStore;
@property (nonatomic) IKOPriceUpdateDataStore *priceUpdateDataStore;
@property (nonatomic) IKOProduct *product;
@property (nonatomic) IKOSettingsDataStore *settingsDataStore;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOShoppingListItem *shoppingListItem;
@property (nonatomic) IKOStoreDataStore *storeDataStore;
@property (nonatomic) IKOProductDataStore *productDataStore;

@property (nonatomic) UITextField *currentField;
@property (nonatomic) NSString *price;
@property (nonatomic) NSString *salePrice;
@property (nonatomic) NSString *size;
@property (nonatomic) NSString *saleDescription;
@property (nonatomic) NSNumber *saleEndDate;
@property (nonatomic) BOOL ppfSafe;

@property (nonatomic) IKOStore *store;
@property (nonatomic) IKOBrand *brand;
@property (nonatomic) IKOProductCategory *category;
@property (nonatomic) IKOServerResponse *lastDetectedServerResponse;

@property (nonatomic) NSMutableSet *hasEdited;

- (void)sendProductUpdate:(IKOPriceUpdate *)submission;
- (void)showToastWithImageView:(UIImageView *)image andMessage:(NSString *)message;
- (void)addToShoppingList:(id)object;
- (void)initDefaults;
- (void)saveDefaults;
- (BOOL)currencyTextField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string;
- (void)colorText:(UITextField *)textField forObject:(id)obj;
- (BOOL)showPriceCapture;

@end
