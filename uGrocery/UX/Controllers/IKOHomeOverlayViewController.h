//
//  IKOHomeOverlayViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 9/19/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IKOHomeOverlay;

@interface IKOHomeOverlayViewController : UIViewController

@property (weak, nonatomic) IBOutlet IKOHomeOverlay *homeOverlay;

@end
