//
//  IKODealViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 9/2/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKODealViewController.h"
#import "IKOStoreHeaderView.h"
#import "IKOSearchResultCell.h"
#import "IKOSaleCell.h"
#import "IKOCouponCell.h"
#import "UIView+CenteredSectionTitle.h"
#import "IKOProduct.h"
#import "UIScreen+Additions.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOUtilities.h"

@interface IKODealViewController ()

@property (nonatomic) IKOSettingsDataStore *settingsDataStore;

@end

@implementation IKODealViewController {
    IKOStoreHeaderView *_closeHeaderView;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    self.title = @"deal details";

    self.tableView.backgroundColor = [UIColor whiteColor];
    IKOStoreDataStore *storeDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOStore class]];
    _closeHeaderView = [[IKOStoreHeaderView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 44) andImage:[storeDataStore largerStoreImageFor:[NSNumber numberWithInteger:[_storeId integerValue]]] andStoreName:[storeDataStore storeNameFor:[NSNumber numberWithInteger:[_storeId integerValue]]]];

    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    self.tableView.tableHeaderView = _closeHeaderView;
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened Deal Detail Screen");
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    NSNumber *settingValue = [self.settingsDataStore objectForKey:kShowCoupons];
    return [settingValue boolValue] ? 3 : 2;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0:
            return 110;
            break;
        case 1:
            return 158;
        default:
            return 138;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return ([_saleData allKeys]) ? 28 : 0;
            break;
        case 2:
            return (_couponData) ? 28 : 0;
            break;
        default:
            return 0;
            break;
    }
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return [UIView viewWithSectionHeader:@"Sale Details"];
            break;
        case 2:
            return [UIView viewWithSectionHeader:@"Save More With Coupons"];
            break;
        default:
            return nil;
            break;
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case 1:
            return ([_saleData allKeys]) ? 1 : 0;
            break;
        case 2:
            return (_couponData) ? _couponData.count : 0;
            break;
        default:
            return 1;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case 0: {
                IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
                [cell configureWithProduct:_product];
                cell.descriptionText.attributedText = [IKOUtilities attributedLabelFor:self.product.productDescription andMeasure:self.product.measureText titleSize:16];
                cell.discountText.text = SF(@"SAVE UP TO %@%%", _maxSavings);
                return cell;
                break;
            }
        case 1: {
                IKOSaleCell *cell = (IKOSaleCell *)[self.tableView dequeueReusableCellWithIdentifier:@"SaleCell" forIndexPath:indexPath];
                cell.regularPrice.text = SF(@"Regular Price: %@", [NSString stringWithFormat:@"$%.2lf", [(NSString *)_saleData[@"BasePrice"] doubleValue]]);
                cell.salesPrice.text = SF(@"Sale Price: %@", [NSString stringWithFormat:@"$%.2lf", [(NSString *)_saleData[@"SalePrice"] doubleValue]]);
                double percentage = [(NSString *)_saleData[@"BasePrice"] doubleValue] - [(NSString *)_saleData[@"SalePrice"] doubleValue];
                cell.discount.text = SF(@"SALE PRICE: %@ - SAVE %@",  [NSString stringWithFormat:@"$%.2lf", [(NSString *)_saleData[@"SalePrice"] doubleValue]], [NSString stringWithFormat:@"$%.2lf", percentage]);

                NSDateFormatter *dateFormatter = [[NSDateFormatter alloc]init];
                [dateFormatter setDateFormat:@"yyyy-MM-dd"];
                NSDate *date  = [dateFormatter dateFromString:_saleData[@"Expires"]];
                [dateFormatter setDateFormat:@"MM/dd/yyyy"];
                NSString *newDate = [dateFormatter stringFromDate:date];

                cell.expiration.text = SF(@"On sale now until %@", newDate);
                cell.minimumQuantity.text = SF(@"Minimum Quantity: %@", _saleData[@"MinQty"]);
                cell.saleDescription.text = SF(@"%@ %@", _saleData[@"AdditionalDesc"], _saleData[@"DetailDesc"]);
                return cell;
                break;
            }
        case 2: {
                NSDictionary *coupon = _couponData[indexPath.row];
                IKOCouponCell *cell = (IKOCouponCell *)[self.tableView dequeueReusableCellWithIdentifier:@"CouponCell" forIndexPath:indexPath];
                cell.discount.text = SF(@"SAVE %@", [NSString stringWithFormat:@"$%.2lf", [(NSString *)coupon[@"DiscountCents"] doubleValue]]);
                cell.source.text = SF(@"Available from %@", coupon[@"Source"]);
                cell.expiration.text = SF(@"Expires on %@", coupon[@"Expires"]);
                cell.minimumQuantity.text = SF(@"Minimum Quantity: %@", coupon[@"MinQty"]);
                cell.couponDescription.text = coupon[@"Description"];
                return cell;
                break;
            }
        default:
            break;
    }

    return [UITableViewCell new];
}

@end
