//
//  IKOProductListVC.m
//  uGrocery
//
//  Created by Duane Schleen on 2/21/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import "IKOProductListVC.h"
#import "IKODataStorageManager+uGrocery.h"
#import "UIViewController+Utilities.h"
#import "IKOPagingInfo.h"
#import "CustomInfiniteIndicator.h"
#import "UIScrollView+InfiniteScroll.h"
#import "IKOServerResponse.h"
#import "IKOSearchResultCell.h"
#import "IKOUtilities.h"
#import "IKOProductDetailViewController.h"
#import "HTHorizontalSelectionList.h"
#import "UIFont+uGrocery.h"
#import "UIColor+uGrocery.h"
#import "IKOProductCategoryListVC.h"
#import "UIScreen+Additions.h"
#import "IKOZeroStateVC.h"
#import "UIView+Animation.h"

@interface IKOProductListVC () <HTHorizontalSelectionListDataSource, HTHorizontalSelectionListDelegate>

@property (nonatomic) HTHorizontalSelectionList *selectionList;

@property (nonatomic) IKOProductDataStore *productsDataStore;
@property (nonatomic) IKOProductCategoryDataStore *productCategoryDataStore;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOFavoritesDataStore *favoritesDataStore;

@property (nonatomic) UIView *waitView;

@property (strong, nonatomic) IKOPagingInfo *pagingInfo;
@property (nonatomic) NSMutableArray *productSearchResults;
@property (nonatomic) NSArray *L2s;
@property (nonatomic) IKOProductCategory *selectedL2;
@property (nonatomic) BOOL shouldRemoveInfiniteScroll;
@property (nonatomic) UIView *zeroStateView;


@end

@implementation IKOProductListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self loadData];
    [self initializeHorizontalScroller];
    
    self.title = self.selectedCategory.name.lowercaseString;
    self.tableView.backgroundColor = [UIColor UGReallyLtGrey];
    self.edgesForExtendedLayout = UIRectEdgeNone;
    [self.tableView setContentInset:UIEdgeInsetsMake(20,0,0,0)];
    
    if (!_pagingInfo)
        _pagingInfo = [[IKOPagingInfo alloc] initWithPage:@1 andLimit:FETCH_PAGE_SIZE];
    
    if (!_productSearchResults)
        _productSearchResults = [NSMutableArray new];
    
    if (!_selectedCategory) {
        self.title = @"top deals";
        [self.tableView setContentInset:UIEdgeInsetsMake(0,0,0,0)];
    }
    
    if (!_shouldRemoveInfiniteScroll) {
        WEAK_SELF(weakSelf);
        
        // Create custom indicator
        CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        self.tableView.infiniteScrollIndicatorView = indicator;
        // self.tableView.infiniteScrollIndicatorMargin = 20;
        
        [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
            [weakSelf fetchNextPage];
        }];
    }
    [self displayWaitIndicator:YES];
}

-(void)viewWillAppear:(BOOL)animated {
    _scrollerView.hidden = ![SF(@"%@", _selectedCategory.level) isEqualToString:@"1"];
}

- (void)viewDidAppear:(BOOL)animated
{
	[super viewDidAppear:animated];
	[self bounceSelectionList];
}

- (void)displayWaitIndicator:(BOOL)show {
    [_zeroStateView removeFromSuperview];
    self.tableView.userInteractionEnabled = YES;
    if (show) {
        IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
        [vc loadView];
        vc.zeroStateTitle.text = @"Fetching the Hottest Deals!";
        vc.zeroStateMessage.text = @"We're fetching the hottest deals... Just a few more moments!";
        vc.screenImage.image = [UIImage imageNamed:@"spinner"];
        [vc.screenImage spinWithDuration:15 rotations:1 repeat:10000];
        _waitView = vc.view;
        _waitView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
        _waitView.alpha = 0;
        [self.view insertSubview:_waitView atIndex:999];
        [UIView animateWithDuration:0.2 animations:^{
            _waitView.alpha = 1;
        }];
        self.tableView.userInteractionEnabled = NO;
    } else {
        [_waitView removeFromSuperview];
        _waitView = nil;
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void)initializeHorizontalScroller {
    _selectionList = [[HTHorizontalSelectionList alloc] initWithFrame:CGRectMake(0, 0, _scrollerView.frame.size.width, 50)];
    _selectionList.delegate = self;
    _selectionList.dataSource = self;
    _selectionList.selectionIndicatorStyle = HTHorizontalSelectionIndicatorStyleNone;
    _selectionList.backgroundColor = [UIColor UGBlue];
	_scrollerView.backgroundColor = [UIColor UGBlue];
    [_scrollerView addSubview:_selectionList];
}

- (void)loadData {
    _productsDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOProduct class]];
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _favoritesDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _productCategoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    _L2s = [_productCategoryDataStore childrentForCategory:_selectedCategory.productCategoryId];
    [self fetchNextPage];
}

- (void)fetchNextPage {
        [_pagingInfo nextPage];
        if ((_pagingInfo.totalPages == nil) | [_pagingInfo hasMorePages]) {
            if (!_selectedCategory) {
                [_productsDataStore allProductsWithPagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                    [self updatePostFetch:products andResponse:response];
                    [self displayWaitIndicator:NO];
                }];
            } else if ([SF(@"%@", _selectedCategory.level) isEqualToString:@"1"]) {
                [_productsDataStore productsForL1:_selectedCategory.productCategoryId pagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                    [self updatePostFetch:products andResponse:response];
                    [self displayWaitIndicator:NO];
                }];
            } else {
                [_productsDataStore productsForCategory:_selectedCategory.productCategoryId pagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                    [self updatePostFetch:products andResponse:response];
                    [self displayWaitIndicator:NO];
                }];
            }
        } else {
            [self.tableView removeInfiniteScroll];
        }
}

- (void)updatePostFetch:(NSArray *)products andResponse:(IKOServerResponse *)response {
    [self.productSearchResults addObjectsFromArray:products];
    _pagingInfo = response.pagingInfo;
    [self.tableView reloadData];
    [self.tableView finishInfiniteScroll];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return _productSearchResults.count;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 119;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
    IKOProduct *product = _productSearchResults[indexPath.row];
    cell.delegate = self;
    [cell configureWithProduct:product];
    [cell selectItem:[_shoppingListDataStore isOnShoppingList:product.productId]];
    return cell;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductDetailViewController *vc = [segue destinationViewController];
        vc.product = _productSearchResults[[self.tableView indexPathForSelectedRow].row];
    } else if ([[segue identifier]isEqualToString:@"showL3s"]) {
        IKOProductCategoryListVC *vc = [segue destinationViewController];
        vc.selectedCategory = _selectedL2;
    }
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:NO];
}

#pragma mark - IKOAddToShoppingListProtocol

- (void)addToShoppingList:(id)object {
    IKOShoppingListItem *shoppingListItem;
    
    BOOL added;
    if ([object isKindOfClass:[IKOProduct class]]) {
        IKOProduct *product = (IKOProduct *)object;
        shoppingListItem = [[IKOShoppingListItem alloc]initWithProduct:product];
        added = [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
        
        if (added) {
            NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ? : @"", @"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
            ANALYTIC_PARAMS(@"Added Product to Shopping List", params);
            
            IKOFavoriteItem *myProductItem = [[IKOFavoriteItem alloc]initWithProduct:object];
            myProductItem.l1Id = shoppingListItem.l1Id;
            [_favoritesDataStore saveMyProductItem:&myProductItem];
        }
    }
}

- (void)showToastWithImageView:(UIImageView *)image andMessage:(NSString *)message {
    
    CGRect toRect = [IKOUtilities frameForTabInTabBar:self.tabBarController.tabBar withIndex:3 inView:self.view];
    CGPoint center = CGPointMake( CGRectGetMidX(toRect), CGRectGetMidY(toRect));
    [IKOUtilities animateAddToCartInView:self.view imageviewToAnimate:image animateToPoint:center];
}

#pragma mark - HTHorizontalSelectionListDataSource Protocol Methods

- (void)bounceSelectionList
{
	__block CGRect frame = _selectionList.frame;
	CGFloat offset = CGRectGetWidth(_selectionList.frame) / 3;
	NSTimeInterval duration = 0.5;
	
	void (^start)() = ^{
		frame.origin.x -= offset;
		_selectionList.frame = frame;
	};
	
	void (^end)(BOOL finished) = ^(BOOL finished){
		frame.origin.x += offset;
		_selectionList.frame = frame;
	};
	
	[UIView animateWithDuration:duration delay:0
						options:UIViewAnimationOptionAutoreverse | UIViewAnimationOptionCurveEaseInOut
					 animations:start completion:end];
}

- (NSInteger)numberOfItemsInSelectionList:(HTHorizontalSelectionList *)selectionList {
	// TODO: fix this hack! Adding cells to fix last item cutoff.
	return _L2s.count + 6;
}

- (NSString *)selectionList:(HTHorizontalSelectionList *)selectionList titleForItemWithIndex:(NSInteger)index
{
	if (index < _L2s.count) {
		IKOProductCategory *category = _L2s[index];
		return category.name.lowercaseString;
	}
	
	return @"   ";
}

#pragma mark - HTHorizontalSelectionListDelegate Protocol Methods

- (void)selectionList:(HTHorizontalSelectionList *)selectionList didSelectButtonWithIndex:(NSInteger)index
{
	if (index < _L2s.count) {
		IKOProductCategory *category = _L2s[index];
		_selectedL2 = category;
		[self performSegueWithIdentifier:@"showL3s" sender:self];
	}
}

@end
