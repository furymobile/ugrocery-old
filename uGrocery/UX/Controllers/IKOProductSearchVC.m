//
//  IKOProductSearchViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/18/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

typedef enum {
    IKOSearchResultItems,
    IKOSearchResultAdhoc
} IKOSearchResultSections;

#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOHomeOverlay.h"
#import "IKOHomeOverlayViewController.h"
#import "IKOImageToastView.h"
#import "IKOProductDetailViewController.h"
#import "IKOProductSearchVC.h"
#import "IKOSearchResultCell.h"
#import "IKOUtilities.h"
#import "IKOZeroStateVC.h"
#import "UIAlertView+Blocks.h"
#import "UIColor+uGrocery.h"
#import "UINavigationBar+Additions.h"
#import "UIScreen+Additions.h"
#import "UIView+Animation.h"
#import "UIView+CenteredSectionTitle.h"
#import "UIView+Toast.h"
#import "UIViewController+Utilities.h"
#import "UserVoice.h"
#import "IKOSearchItem.h"
#import "IKOServerResponse.h"
#import "UIScrollView+InfiniteScroll.h"
#import "CustomInfiniteIndicator.h"
#import "IKOL1Cell.h"

@interface IKOProductSearchVC ()

@property (nonatomic) IKOSearchDataStore *searchDataStore;
@property (nonatomic) IKOProductDataStore *productsDataStore;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOFavoritesDataStore *myProductsDataStore;
@property (nonatomic) IKOBrandDataStore *brandDataStore;
@property (nonatomic) IKOProductCategoryDataStore *productCategoryDataStore;
@property (nonatomic) IKOSettingsDataStore *settingsDataStore;

@property (nonatomic) BOOL shouldShowStoreBrands;
@property (nonatomic) BOOL viewingProducts;
@property (nonatomic) IKOBarcodeScanner *barcodeScanner;
@property (nonatomic) IKOSearchBarView *searchBarView;

@property (nonatomic) IKORecentSearchDataStore *recentSearchDataStore;

@property (nonatomic) BOOL isSearching;
@property (nonatomic) NSArray *searchResults;
@property (nonatomic) NSArray *recentSearchItems;
@property (nonatomic) UIView *waitView;
@property (nonatomic) BOOL doneInitialSearch;

@end


@implementation IKOProductSearchVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];
    [self.recentsTableView setBackgroundColor:[UIColor UGReallyLtGrey]];
    [self.navigationController.navigationBar hideBottomHairline];
    [self.tableView setContentInset:UIEdgeInsetsMake(35,0,0,0)];
    [self.recentsTableView setContentInset:UIEdgeInsetsMake(50,0,0,0)];
    [self initialize];
    [self buildUI];
}

- (void)initialize {
    _adhocCategory = [IKOProductCategory new];

    _searchDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOSearchItem class]];
    _productsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _myProductsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    _recentSearchDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKORecentSearchItem class]];
    _brandDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOBrand class]];
    _productCategoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];

    _recentSearchItems = [_recentSearchDataStore allObjects];
    
    if (!_pagingInfo)
        _pagingInfo = [[IKOPagingInfo alloc] initWithPage:@1 andLimit:FETCH_PAGE_SIZE];
    
    if (!_productSearchResults)
        _productSearchResults = [NSMutableArray new];
    
    if (!_shouldRemoveInfiniteScroll && !_isSearching) {
        WEAK_SELF(weakSelf);
        
        // Create custom indicator
        CustomInfiniteIndicator *indicator = [[CustomInfiniteIndicator alloc] initWithFrame:CGRectMake(0, 0, 24, 24)];
        self.tableView.infiniteScrollIndicatorView = indicator;
       // self.tableView.infiniteScrollIndicatorMargin = 20;
        
        [self.tableView addInfiniteScrollWithHandler:^(UITableView *tableView) {
            [weakSelf fetchNextPage];
        }];
    }

}

- (void)fetchNextPage {
    if (![_pagingInfo hasMorePages]) {
        [self.tableView removeInfiniteScroll];
        return;
    } else {
    
        [_pagingInfo nextPage];
        if (![_pagingInfo hasMorePages])
            [self.tableView removeInfiniteScroll];

        if ([self.selectedSearchItem.brandId integerValue] == 0 && [self.selectedSearchItem.categoryId integerValue] == 0) {
            [_productsDataStore searchProducts:self.selectedSearchItem.itemDescription  pagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                [self updatePostFetch:products andResponse:response];
            }];
        } else if ([self.selectedSearchItem.brandId integerValue] != 0) {
            [_productsDataStore productsForBrand:self.selectedSearchItem.brandId pagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                 [self updatePostFetch:products andResponse:response];
            }];
        } else {
            [_productsDataStore productsForCategory:self.selectedSearchItem.categoryId pagingInfo:_pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
                 [self updatePostFetch:products andResponse:response];
            }];
        }
    }
}

- (void)updatePostFetch:(NSArray *)products andResponse:(IKOServerResponse *)response {
    [self.productSearchResults addObjectsFromArray:products];
    self.pagingInfo = response.pagingInfo;
    [self.tableView reloadData];
    [self.tableView finishInfiniteScroll];
}

- (void)buildUI {
    _searchBarView = [[IKOSearchBarView alloc]initWithFrame:CGRectMake(0, 0, [UIScreen currentSize].width, 44)];
    _searchBarView.delegate = self;
    _searchBarView.searchBar.delegate = self;


    _barcodeScanner = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoBarcodeScanner"];
    _barcodeScanner.delegate = self;

    _recentsTableView.keyboardDismissMode = self.tableView.keyboardDismissMode = UIScrollViewKeyboardDismissModeOnDrag;

    [self.view insertSubview:_searchBarView aboveSubview:_recentsTableView];
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
     
    if ([_settingsDataStore hasSettingForKey:kShowStoreBrands]) {
        NSNumber *settingValue = [_settingsDataStore objectForKey:kShowStoreBrands];
        _shouldShowStoreBrands = [settingValue boolValue];
    }
    if (_viewingProducts) {
        ANALYTIC(@"Product Search Drilling into Product View");
        _searchBarView.hidden = YES;
        [self.tableView setContentInset:UIEdgeInsetsMake(0,0,0,0)];
    } else {
        ANALYTIC(@"Opened Product Search");
        [_searchBarView.searchBar becomeFirstResponder];
    }
    [self.tableView deselectRowAtIndexPath:[self.tableView indexPathForSelectedRow] animated:YES];
}

- (void)viewDidAppear:(BOOL)animated {
    if (_viewingProducts) {
        [IKOAnalytics logApptentiveEvent:@"Product_Search_Results" fromViewController:self];
    } else {
        [IKOAnalytics logApptentiveEvent:@"Top_Level_Search" fromViewController:self];
    }
}

-(void)keyboardWillShow {
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]initWithTitle:@"close" style:UIBarButtonItemStylePlain target:self action:@selector(closeKeyboard)];
    [self.navigationItem setLeftBarButtonItem:closeButton];
}

-(void)keyboardWillHide {
    [self.navigationItem setLeftBarButtonItem:nil];
}

-(void)closeKeyboard {
    [_searchBarView.searchBar resignFirstResponder];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    _recentsTableView.hidden = (_searchBarView.searchBar.text.length > 0) || _viewingProducts || _recentSearchItems.count == 0;

    if (tableView == _recentsTableView)
        return 1;

    if (tableView == _tableView)
        return (_viewingProducts) ? 1 : 2;

    return 0;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == _recentsTableView)
        return _recentSearchItems.count;

    if (tableView == _tableView) {
        if (!_viewingProducts) {
            switch (section) {
                case IKOSearchResultAdhoc:
                    return (_adhocCategory.name.length > 0) ? 1 : 0;
                    break;
                case IKOSearchResultItems:
                    return _searchResults.count;
                    break;
                default:
                    return 0;
                    break;
            }
        } else {
            return _productSearchResults.count + 1;
        }
    }

    return 0;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (tableView == _recentsTableView)
        return 28;

    return 0;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (tableView == _recentsTableView)
        return [UIView viewWithSectionHeader:@"Recent Searches"];
    return nil;
}

- (void)setViewingProducts:(BOOL)viewingProducts {
    _viewingProducts = viewingProducts;
    _searchBarView.hidden = _viewingProducts;
    _recentsTableView.hidden = _viewingProducts;

    [self.tableView setContentInset:(_viewingProducts) ? UIEdgeInsetsMake(0,0,0,0) : UIEdgeInsetsMake(30,0,0,0)];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _recentsTableView)
        return 44;

    if (tableView == _tableView) {
        
        if (!_viewingProducts && indexPath.section == IKOSearchResultAdhoc)
            return 156;

        if (!_viewingProducts && indexPath.section == 1)
            return 44;

        if (_viewingProducts && !_selectedSearchItem && indexPath.row == 0)
            return 0;

        if (_viewingProducts && indexPath.row != 0) {
            IKOProduct *product = _productSearchResults[indexPath.row-1];
            if (!_shouldShowStoreBrands && [product.isStoreBrand integerValue] == 1) {
                return 0;
            }
            return 119;
        }

        if (_searchResults.count > 0) {
            IKOSearchItem *item = _searchResults[indexPath.row];
            if (!_shouldShowStoreBrands && [item.isStoreBrand integerValue] == 1) {
                return 0;
            }
        }

        if (_viewingProducts && indexPath.row == 0 && !self.doneInitialSearch ){
            return 44+167;
        }
        
        return 44;
    }
    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _tableView) {
        if (!_viewingProducts) {
            switch (indexPath.section) {
                case IKOSearchResultAdhoc: {
                        IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"AdhocCell" forIndexPath:indexPath];
                        [cell configurewWithAdhocProduct:_adhocCategory];
                        cell.delegate = self;
                        cell.selectionStyle = UITableViewCellSelectionStyleNone;
                        cell.resultImage.image = [UIImage imageNamed:@"question"];
                        cell.accessoryType = UITableViewCellAccessoryNone;
                        return cell;
                        break;
                    }
                case IKOSearchResultItems: {
                        IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"NoAddCategoryCell" forIndexPath:indexPath];
                        IKOSearchItem *item = _searchResults[indexPath.row];
                        [cell configureWithSearchItem:item andCurrentSearchTerm:_searchBarView.searchBar.text];
                        cell.delegate = self;
                        return cell;
                        break;
                    }
                default:
                    break;
            }
        } else {
            if (indexPath.row == 0) {
                IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"CategoryCell" forIndexPath:indexPath];
                [cell configureWithSearchItem:_selectedSearchItem];
                cell.delegate = self;
                [cell selectItem:NO];
                cell.selectionStyle = UITableViewCellSelectionStyleNone;
                cell.accessoryType = UITableViewCellAccessoryNone;
                
                UIView *spinner = [cell.contentView viewWithTag:666];
                if (spinner)
                    [spinner spinWithDuration:15 rotations:1 repeat:10000];
                return cell;
            } else {
                IKOSearchResultCell *cell = (IKOSearchResultCell *)[self.tableView dequeueReusableCellWithIdentifier:@"ProductCell" forIndexPath:indexPath];
                IKOProduct *product = _productSearchResults[indexPath.row-1];
                cell.delegate = self;
                [cell configureWithProduct:product];
                [cell selectItem:[_shoppingListDataStore isOnShoppingList:product.productId]];
                return cell;
            }
        }
    } else if (tableView == _recentsTableView) {
        IKOSearchResultCell *cell = (IKOSearchResultCell *)[tableView dequeueReusableCellWithIdentifier:@"SearchCell" forIndexPath:indexPath];
        IKORecentSearchItem *item = _recentSearchItems[indexPath.row];
        cell.textLabel.text = item.termDescription;
        cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
        return cell;
    }

    return [UITableViewCell new];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == _recentsTableView) {
        IKORecentSearchItem *item = _recentSearchItems[indexPath.row];
        if (item) {
            [_recentSearchDataStore updateTimestamp:item];
            _recentSearchItems = [_recentSearchDataStore allObjects];
            [_recentsTableView reloadData];
            
            if (([_selectedSearchItem.brandId integerValue] == 0) && ([_selectedSearchItem.categoryId integerValue] == 0)) {
                [self triggerRecentSearch:[[IKOSearchItem alloc] initWithRecentSearchItem:item]];
            } else {
            
                IKOProductSearchVC *productSearchVC = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductSearchViewController"];
                [productSearchVC setViewingProducts:YES];
                productSearchVC.selectedSearchItem = [[IKOSearchItem alloc] initWithRecentSearchItem:item];
                [productSearchVC triggerSearch:YES];
                [self.navigationController pushViewController:productSearchVC animated:YES];
            }
        }
        return;
    }

    if (indexPath.section == IKOSearchResultAdhoc && !_viewingProducts)
        return;

    if (indexPath.section != 0 && indexPath.row == 0)
        return;

    if (!_viewingProducts) {
        IKOProductSearchVC *productSearchVC = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductSearchViewController"];
        [productSearchVC setViewingProducts:YES];
        [_searchBarView.searchBar resignFirstResponder];
        productSearchVC.selectedSearchItem = _searchResults[indexPath.row];
        [_recentSearchDataStore saveRecentSearchItem:[[IKORecentSearchItem alloc]initWithSearchItem:productSearchVC.selectedSearchItem]];
        _recentSearchItems = [_recentSearchDataStore allObjects];
        [_recentsTableView reloadData];
        [productSearchVC triggerSearch:YES];
        [self.navigationController pushViewController:productSearchVC animated:YES];
        return;
    } else if (indexPath.section == 0 && indexPath.row == 0 && _viewingProducts) {
        return;
    }

    [self performSegueWithIdentifier:@"showProductDetails" sender:self];
}

#pragma mark - Triggered Actions

- (void)triggerRecentSearch:(IKOSearchItem *)searchItem {
    NSDictionary *params = @{@"Brand ID": searchItem.brandId ? : @"", @"Description": searchItem.searchName ? : @"", @"Category ID": searchItem.categoryId ? : @""};
    ANALYTIC_PARAMS(@"Tapped Recent Search Item", params);
    IKOProductSearchVC *productSearchVC = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductSearchViewController"];
    [productSearchVC setViewingProducts:YES];
    productSearchVC.title = searchItem.itemDescription.lowercaseString;

    [_searchBarView.searchBar resignFirstResponder];
    productSearchVC.selectedSearchItem = searchItem;
    
    WEAK_SELF(weakSelf);
    if ([searchItem.brandId integerValue] == 0 && [searchItem.categoryId integerValue] == 0) {
        productSearchVC.title= SF(@"\"%@\"", searchItem.itemDescription.lowercaseString);
        productSearchVC.adhocCategory = [IKOProductCategory new];
        productSearchVC.adhocCategory.name = searchItem.itemDescription;
        [_productsDataStore searchProducts:searchItem.itemDescription pagingInfo:weakSelf.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
            productSearchVC.pagingInfo = response.pagingInfo;
            if (![productSearchVC.pagingInfo hasMorePages])
                productSearchVC.shouldRemoveInfiniteScroll = YES;
            productSearchVC.productSearchResults = [NSMutableArray new];
            [productSearchVC.productSearchResults addObjectsFromArray:products];
            productSearchVC.title = SF(@"\"%@\"", searchItem.itemDescription.lowercaseString);
            productSearchVC.doneInitialSearch = YES;
            [self.navigationController pushViewController:productSearchVC animated:YES];
        }];
    } else if ([searchItem.brandId integerValue] != 0) {
        [_productsDataStore productsForBrand:searchItem.brandId pagingInfo:weakSelf.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
            productSearchVC.pagingInfo = response.pagingInfo;
            if (![productSearchVC.pagingInfo hasMorePages])
                productSearchVC.shouldRemoveInfiniteScroll = YES;
            productSearchVC.productSearchResults = [NSMutableArray new];
           [productSearchVC.productSearchResults addObjectsFromArray:products];
            productSearchVC.doneInitialSearch = YES;
            [self.navigationController pushViewController:productSearchVC animated:YES];
        }];
    } else {
        [_productsDataStore productsForCategory:searchItem.categoryId pagingInfo:weakSelf.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
            productSearchVC.pagingInfo = response.pagingInfo;
            if (![productSearchVC.pagingInfo hasMorePages])
                productSearchVC.shouldRemoveInfiniteScroll = YES;
            productSearchVC.productSearchResults = [NSMutableArray new];
            [productSearchVC.productSearchResults addObjectsFromArray:products];
            productSearchVC.doneInitialSearch = YES;
            [self.navigationController pushViewController:productSearchVC animated:YES];
        }];
    }
   
    return;
}

- (void)triggerSearchFromShoppingList {
    [_recentsTableView removeFromSuperview];
    [self triggerSearch:NO];
}

- (void)triggerSearch:(BOOL)showSelectedItem {
    [self initialize];
    
    WEAK_SELF(weakSelf);
    
    if ([_selectedSearchItem.brandId integerValue] != 0) {
        [_productsDataStore productsForBrand:_selectedSearchItem.brandId pagingInfo:self.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
            [weakSelf.productSearchResults addObjectsFromArray:products];
            weakSelf.pagingInfo = response.pagingInfo;
            if (![weakSelf.pagingInfo hasMorePages])
                [weakSelf.tableView removeInfiniteScroll];
            weakSelf.doneInitialSearch = YES;
            [self triggerSearchDisplay:showSelectedItem];
        }];
        
    } else  { //if ([_selectedSearchItem.categoryId integerValue] != 0) {
        [_productsDataStore productsForCategory:_selectedSearchItem.categoryId pagingInfo:self.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
            [weakSelf.productSearchResults addObjectsFromArray:products];
            weakSelf.pagingInfo = response.pagingInfo;
            if (![weakSelf.pagingInfo hasMorePages])
                [weakSelf.tableView removeInfiniteScroll];
            weakSelf.doneInitialSearch = YES;
            [weakSelf triggerSearchDisplay:showSelectedItem];
        }];
    } }

- (void)triggerSearchDisplay:(BOOL)showSelectedItem {
    [self setViewingProducts:YES];
    self.title = _selectedSearchItem.itemDescription.lowercaseString;
    if (!showSelectedItem)
        _selectedSearchItem = nil;
    [_tableView reloadData];
}

#pragma mark - Gestures

- (IBAction)respondToTapGesture:(UITapGestureRecognizer *)recognizer {
    [_searchBarView.searchBar resignFirstResponder];
}

- (IBAction)overlayTapped:(id)sender {
    [_searchBarView.searchBar resignFirstResponder];
}

#pragma mark - Search Bar

- (void)searchBar:(UISearchBar *)searchBar textDidChange:(NSString *)searchText {
    if (searchText.length == 0) {
        [self setViewingProducts:NO];
        _productSearchResults = nil;
        _searchResults = nil;
        _adhocCategory.name = nil;
        [self.tableView reloadData];
        self.navigationItem.leftBarButtonItem = nil;
        return;
    }
    
    _searchResults = [_searchDataStore search:searchText];
    _adhocCategory.name = searchText;
    [self.tableView reloadData];
}

- (BOOL)searchBarShouldBeginEditing:(UISearchBar *)searchBar {
    _isSearching = YES;
    return YES;
}

- (void)searchBarCancelButtonClicked:(UISearchBar *)searchBar {
    _isSearching = NO;
}

- (void)searchBarSearchButtonClicked:(UISearchBar *)searchBar {
    [self searchForTermBegin:searchBar.text];
}

- (void)searchForTermBegin:(NSString *)term {
    
    NSDictionary *params = @{@"Term": term ? : @"" };
    ANALYTIC_PARAMS(@"Performed Term Search", params);

    IKOSearchItem *searchItem = (_selectedSearchItem) ? _selectedSearchItem :[[IKOSearchItem alloc]initWithProductCategory:_adhocCategory];
    
    if (term && term.length < 3) {
        [UIAlertView showWithTitle:@"Product Term Search"
        message:@"Please enter at least 3 characters to search for"
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return;
    } else {
        //lets figure out if this is a brand or category
        IKOBrand *brand = [_brandDataStore brandFromTerm:term];
        if (brand)
            searchItem = [[IKOSearchItem alloc] initWithBrand:brand];
        IKOProductCategory *cat = [_productCategoryDataStore categoryFromTerm:term];
        if (cat)
            searchItem = [[IKOSearchItem alloc] initWithProductCategory:cat];
    }

    IKOProductSearchVC *productSearchVC = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductSearchViewController"];
    [productSearchVC setViewingProducts:YES];
    productSearchVC.title= SF(@"\"%@\"", term.lowercaseString);
    productSearchVC.adhocCategory = [IKOProductCategory new];
    productSearchVC.adhocCategory.name = term;

    [_searchBarView.searchBar resignFirstResponder];
    productSearchVC.selectedSearchItem = searchItem;
    if (!productSearchVC.selectedSearchItem.itemDescription)
        productSearchVC.selectedSearchItem.itemDescription = term;
    productSearchVC.recentsTableView.hidden = YES;
    
    WEAK_SELF(weakSelf);
    
    [_productsDataStore searchProducts:term pagingInfo:self.pagingInfo callback:^(IKOError *error, NSArray *products, IKOServerResponse *response) {
        productSearchVC.productSearchResults = [NSMutableArray new];
        [productSearchVC.productSearchResults addObjectsFromArray:products];
        productSearchVC.pagingInfo = response.pagingInfo;
        if (![productSearchVC.pagingInfo hasMorePages])
            productSearchVC.shouldRemoveInfiniteScroll = YES;
        productSearchVC.doneInitialSearch = YES;
        [weakSelf.recentsTableView reloadData];
        [weakSelf.recentSearchDataStore saveRecentSearchItem:[[IKORecentSearchItem alloc]initWithSearchItem:productSearchVC.selectedSearchItem]];
        weakSelf.recentSearchItems = [_recentSearchDataStore allObjects];
        [self.navigationController pushViewController:productSearchVC animated:YES];
    }];
    
    return;
}

#pragma mark - IKOSearchBarViewDelegate

- (void)scanBarcode {
    if (![IKOAuthenticationManager sharedInstance].isAdministrator & [IKOUtilities notConnectedAlert])
        return;
    if ([IKOUtilities hasCameraPermission]) {
        [self.navigationController pushViewController:_barcodeScanner animated:NO];
    } else {
        [UIAlertView showWithTitle:@"Cannot Access Camera"
                           message:@"Check your device settings and ensure you have allowed uGrocery to access your camera."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
    }
}

- (void)triggerProductSearchFor:(NSString *)term {
    [self searchForTermBegin:term];
}

#pragma mark - IKOBarcodeScannerDelegate
 - (void)closeScanner {
    [self.navigationController popViewControllerAnimated:NO];
}

- (void)showProductDetailsFor:(IKOProduct *)product {
    IKOProductDetailViewController *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoProductDetails"];
    vc.product = product;
    [self.navigationController pushViewController:vc animated:YES];
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - IKOAddToShoppingListProtocol

- (void)addToShoppingList:(id)object {
    IKOShoppingListItem *shoppingListItem;

    BOOL added;
    if ([object isKindOfClass:[IKOProduct class]]) {
        IKOProduct *product = (IKOProduct *)object;
        shoppingListItem = [[IKOShoppingListItem alloc]initWithProduct:product];
        added = [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];

        if (added) {
            NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ? : @"", @"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
            ANALYTIC_PARAMS(@"Added Product to Shopping List", params);

            IKOFavoriteItem *myProductItem = [[IKOFavoriteItem alloc]initWithProduct:object];
            myProductItem.l1Id = shoppingListItem.l1Id;
            [_myProductsDataStore saveMyProductItem:&myProductItem];
        }
    } else if ([object isKindOfClass:[IKOSearchItem class]]) {
        IKOSearchItem *searchItem = (IKOSearchItem *)object;
        shoppingListItem = [[IKOShoppingListItem alloc]initWithSearchItem:searchItem];
        added = [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
        NSDictionary *params = @{@"Brand ID": searchItem.brandId ? : @"", @"Description": searchItem.searchName ? : @"", @"Category ID": searchItem.categoryId ? : @""};
        ANALYTIC_PARAMS(@"Added Item to Shopping List", params);
    } else if ([object isKindOfClass:[IKOProductCategory class]]) {
        IKOProductCategory *category = object;
        shoppingListItem = [[IKOShoppingListItem alloc]initWithProductCategory:object];
        if (!category.productCategoryId)
            shoppingListItem.isAdhoc = @1;
        added = [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
        ANALYTIC(@"Added Category to Shopping List");
    }
}

- (void)showToastWithImageView:(UIImageView *)image andMessage:(NSString *)message {
    [_searchBarView.searchBar resignFirstResponder];

    CGRect toRect = [IKOUtilities frameForTabInTabBar:self.tabBarController.tabBar withIndex:3 inView:self.view];
    CGPoint center = CGPointMake( CGRectGetMidX(toRect), CGRectGetMidY(toRect));
    [IKOUtilities animateAddToCartInView:self.view imageviewToAnimate:image animateToPoint:center];
}

#pragma mark - Navigation

- (IBAction)back:(id)sender {
    if (_viewingProducts) {
        _productSearchResults = nil;
        [self setViewingProducts:NO];
        [self.tableView reloadData];
    } else {
        _searchResults = nil;
        _adhocCategory.name = nil;
        _searchBarView.searchBar.text = nil;
        [_searchBarView.searchBar resignFirstResponder];
        [self.tableView reloadData];
        self.navigationItem.leftBarButtonItem = nil;
        return;
    }
}

- (IBAction)signIn:(id)sender {
    [self performSegueWithIdentifier:@"signIn" sender:self];
}

- (IBAction)signUp:(id)sender {
    [self performSegueWithIdentifier:@"signUp" sender:self];
}

- (IBAction)showSupport:(id)sender {
    [UserVoice presentUserVoiceInterfaceForParentViewController:self];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductDetailViewController *vc = [segue destinationViewController];
        vc.product = _productSearchResults[[self.tableView indexPathForSelectedRow].row-1];
    }
}

@end
