//
//  IKOUserPreferencesViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/7/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKOUserPreferencesViewController.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOAuthenticationManager.h"
#import "UIViewController+Utilities.h"
#import "UIAlertView+Blocks.h"
#import "UIView+CenteredSectionTitle.h"
#import "IKOUserCell.h"
#import "IKOUser.h"
#import "UIColor+uGrocery.h"
#import "IKOSwitchCell.h"

typedef NS_ENUM (NSInteger, UserPreferenceSection) {
    UserInfo = 0,
    UserPreferences,
    NUM_USERPREFERENCE_SECTIONS
};

typedef NS_ENUM (NSInteger, UserInformationRows) {
    UserInfoRow = 0,
    SignInOutRow,
    SignUpRow,
    SignOutRow,
    NUM_USERINFORMATION_ROWS
};

@implementation IKOUserPreferencesViewController {
    IKOSettingsDataStore *_settingsDataStore;
}

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    [self.tableView setBackgroundColor:[UIColor UGReallyLtGrey]];

    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleAuthentication:) name:IKOAuthenticationManagerDidAuthenticate object:nil];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(handleSignOut:) name:IKOAuthenticationManagerDidSignOut object:nil];
}

- (void)viewWillAppear:(BOOL)animated {
    ANALYTIC(@"Opened User Preferences");
    [self.tableView reloadData];
}

#pragma mark - Notifications

- (void)handleAuthentication:(NSNotification *)notification {
    [self.tableView reloadData];
}

- (void)handleSignOut:(NSNotification *)notification {
    [self.tabBarController.navigationController popViewControllerAnimated:NO];
}

#pragma mark - Table view data source

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) {
        return NUM_USERPREFERENCE_SECTIONS;
    }
    return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    switch (section) {
        case UserInfo:
            return ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) ? 2 : 3;
            break;
        case UserPreferences:
            return ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) ? 4 : 0;
        default:
            return 0;
            break;
    }
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    return 28;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    NSString *title;
    switch (section) {
        case UserInfo:
            return nil;
            break;
        case UserPreferences:
            title = @"Preferences";
            break;
        default:
            break;
    }
    return [UIView viewWithSectionHeader:title];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case UserInfo:
            switch (indexPath.row) {
                case UserInfoRow:
                    return ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) ? 129 : 0;
                    break;
                default:
                    return 44;
                    break;
            }
            break;
        default:
            return ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) ? 44 : 0;
            break;
    }
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case UserInfo: {
                switch (indexPath.row) {
                    case UserInfoRow: {
                            IKOUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UserInfoCell" forIndexPath:indexPath];
                            if ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) {
                                cell.userEmail.text = [IKOAuthenticationManager currentUser].loginEmail;
                                cell.userName.text = [@[[IKOAuthenticationManager currentUser].firstName, [IKOAuthenticationManager currentUser].lastName] componentsJoinedByString: @" "];
                            }
                            return cell;
                            break;
                        }
                    case 1: {
                            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionCell" forIndexPath:indexPath];
                            cell.textLabel.text = ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) ? @"Sign Out" : @"Sign In";
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            return cell;
                            break;
                        }
                    case 2: {
                            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionCell" forIndexPath:indexPath];
                            cell.textLabel.text = @"Sign Up";
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            return cell;
                            break;
                        }
                    default:
                        break;
                }
                break;
            }
        case UserPreferences: {
                switch (indexPath.row) {
                    case 0: {
                            IKOSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell" forIndexPath:indexPath];
                            cell.cellLabel.text = @"Show Price Capture";
                            cell.cellSwitch.tag = 0;
                            [cell.cellSwitch setOnTintColor:[UIColor UGGreen]];
                            [cell.cellSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                            if ([_settingsDataStore hasSettingForKey:kShowAdvancedPriceCapture]) {
                                NSNumber *settingValue = [_settingsDataStore objectForKey:kShowAdvancedPriceCapture];
                                [cell.cellSwitch setOn:[settingValue boolValue]];
                            } else {
                                [cell.cellSwitch setOn:NO];
                            }
                            return cell;
                            break;
                        }
                    case 1: {
                            IKOSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell" forIndexPath:indexPath];
                            cell.cellLabel.text = @"Show Store Brands";
                            cell.cellSwitch.tag = 1;
                            [cell.cellSwitch setOnTintColor:[UIColor UGGreen]];
                            [cell.cellSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                            if ([_settingsDataStore hasSettingForKey:kShowStoreBrands]) {
                                NSNumber *settingValue = [_settingsDataStore objectForKey:kShowStoreBrands];
                                [cell.cellSwitch setOn:[settingValue boolValue]];
                            } else {
                                [cell.cellSwitch setOn:YES];
                            }
                            return cell;
                        }
                    case 2: {
                            IKOSwitchCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell" forIndexPath:indexPath];
                            cell.cellLabel.text = @"Show Coupons";
                            cell.cellSwitch.tag = 2;
                            [cell.cellSwitch setOnTintColor:[UIColor UGGreen]];
                            [cell.cellSwitch addTarget:self action:@selector(switchChanged:) forControlEvents:UIControlEventValueChanged];
                            if ([_settingsDataStore hasSettingForKey:kShowCoupons]) {
                                NSNumber *settingValue = [_settingsDataStore objectForKey:kShowCoupons];
                                [cell.cellSwitch setOn:[settingValue boolValue]];
                            } else {
                                [cell.cellSwitch setOn:NO];
                            }
                            return cell;
                        }
                    case 3: {
                            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActionCell" forIndexPath:indexPath];
                            cell.textLabel.text = @"Change Password";
                            cell.accessoryType = UITableViewCellAccessoryDisclosureIndicator;
                            return cell;
                            break;
                        }
                    default:
                        break;
                }

                break;
            }
        default:
            break;
    }

    return [UITableViewCell new];
}

- (IBAction)switchChanged:(id)sender {
    UISwitch *switchControl = (UISwitch *)sender;
    switch (switchControl.tag) {
        case 0: {
                ((UISwitch *)sender).isOn ? ANALYTIC(@"Turned Advanced Price Capture On") : ANALYTIC(@"Turned Advanced Price Capture Off");

                [_settingsDataStore setObject:[NSNumber numberWithBool:((UISwitch *)sender).isOn] forKey:kShowAdvancedPriceCapture];
                break;
            }
        case 1: {
                ((UISwitch *)sender).isOn ? ANALYTIC(@"Turned Show Store Brands On") : ANALYTIC(@"Turned Show Store Brands Off");
                [_settingsDataStore setObject:[NSNumber numberWithBool:((UISwitch *)sender).isOn] forKey:kShowStoreBrands];
                break;
            }
        case 2: {
                ((UISwitch *)sender).isOn ? ANALYTIC(@"Turned Show Coupons On") : ANALYTIC(@"Turned Show Coupons Off");
                [_settingsDataStore setObject:[NSNumber numberWithBool:((UISwitch *)sender).isOn] forKey:kShowCoupons];
            }
            break;
        default:
            break;
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case UserInfo:
            switch (indexPath.row) {
                case SignInOutRow:
                    if ([[IKOAuthenticationManager sharedInstance]isAuthenticated]) {
                        [UIAlertView showWithTitle:@"Sign Out"
                        message:@"Sign Out of uGrocery?"
                        cancelButtonTitle:@"Cancel"
                        otherButtonTitles:@[@"OK"]
                        tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                    if (buttonIndex == 0) {
                        [self.tableView deselectRowAtIndexPath:indexPath animated:YES];
                    }
                    if (buttonIndex == 1) {
                        ANALYTIC(@"Signed Out");
                        [[IKOAuthenticationManager sharedInstance]signOut];
                        [self.tableView reloadData];
                    }
                }];
                    } else {
                        [self performSegueWithIdentifier:@"signIn" sender:self];
                    }
                    break;
                case SignUpRow:
                    [self performSegueWithIdentifier:@"signUp" sender:self];
                    break;
                default:
                    break;
            }
            break;
        case UserPreferences:
            switch (indexPath.row) {
                case 3:
                    [self performSegueWithIdentifier:@"changePassword" sender:self];
                    break;
                default:
                    break;
            }
        default:
            break;
    }
}

@end
