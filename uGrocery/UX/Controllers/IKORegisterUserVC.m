//
//  IKORegisterUserViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 8/5/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import "IKORegisterUserVC.h"
#import "UIViewController+Utilities.h"
#import "IKOLocationManager.h"
#import "IKORegex.h"
#import "UIAlertView+Blocks.h"
#import "IKOSDK.h"
#import "IKOAuthenticationManager.h"
#import "IKOServerResponse.h"
#import "IKOError.h"
#import "UIColor+uGrocery.h"
#import "IKOLocationManager.h"

@implementation IKORegisterUserVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [self buildUI];
}

- (void)buildUI {
    [self setupViewController];

    WEAK_SELF(weakSelf);
    [[IKOLocationManager sharedInstance]getPlacemark:^(NSError *error, CLPlacemark *placemark) {
        weakSelf.zipCode.text = placemark.postalCode;
    }];

    _emailAddress.delegate = _zipCode.delegate = _password.delegate = self;
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:NO];
    ANALYTIC(@"Opened Registration");
}

- (void)viewDidAppear:(BOOL)animated {
    [[IKOLocationManager sharedInstance]startUpdatingLocation];
    [self performSelector:@selector(becomeFirstResponder) withObject:_emailAddress afterDelay:1];
}

- (void)viewWillDisappear:(BOOL)animated {
    [self.navigationController setNavigationBarHidden:YES animated:NO];
}

#pragma mark - Validation

- (BOOL)isValid {
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:kValidateEmailRegex options:NSRegularExpressionCaseInsensitive error:nil];
    BOOL validEmail = [regex numberOfMatchesInString:_emailAddress.text options:NSMatchingReportProgress range:NSMakeRange(0, [_emailAddress.text length])] > 0;
    if (!validEmail) {
        [UIAlertView showWithTitle:@"Invalid Email"
        message:@"Please enter a valid Email Address."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }

    if (_zipCode.text.length != 5) {
        [UIAlertView showWithTitle:@"Invalid Zip Code"
        message:@"Please enter a 5 digit Zip Code."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }


    if (_password.text.length < 8) {
        [UIAlertView showWithTitle:@"Password"
        message:@"Password must be at least 8 characters (case sensitive)."
        cancelButtonTitle:@"OK"
        otherButtonTitles:nil
        tapBlock:nil];
        return NO;
    }
    
    return YES;
}

#pragma mark - Navigation

- (IBAction)submitRegistration:(id)sender {
    if ([self isValid]) {
        if ([_zipCode.text hasPrefix:@"80"] | [_zipCode.text hasPrefix:@"81"] ) {
            [self submit];
        } else {
            WEAK_SELF(weakSelf);
            [UIAlertView showWithTitle:@"Location Alert!"
                               message:@"uGrocery currently monitors prices in Colorado only.  A Colorado zip code is required to work properly.  You can still use the app, but prices and grocery stores are for Colorado."
                     cancelButtonTitle:@"Cancel"
                     otherButtonTitles:@[@"Continue"]
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 1) {
                                      [weakSelf submit];
                                  }
                              }];
        }
    } else {
        [self submit];
    }
}

- (void)submit {
    [[IKOSDK new]registerNewUser:_emailAddress.text
                       firstName:nil
                        lastName:nil
                        password:_password.text
                      postalCode:_zipCode.text
                       promoCode:nil
                     phoneNumber:nil
                      newsletter:NO
                      referredBy:nil
                        callback:^(IKOError *error, IKOServerResponse *response) {
                            if (response.success) {
                                ANALYTIC(@"Successfully Registered");
                                [UIAlertView showWithTitle:@"Thank You!"
                                                   message:@"Thank you for registering for uGrocery!"
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil
                                                  tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                                      if (buttonIndex == [alertView cancelButtonIndex]) {
                                                          [[IKOAuthenticationManager sharedInstance]authenticatedUser:response];
                                                          if (_completionBlock)
                                                              _completionBlock();
                                                      }
                                                  }];
                            } else {
                                ANALYTIC_ERROR(@"Error Registering User", error.error);
                                [UIAlertView showWithTitle:@"Unable to Register"
                                                   message:SF(@"We were unable to register your uGrocery account. %@", error.responseErrorDescription ? error.responseErrorDescription : @"")
                                         cancelButtonTitle:@"OK"
                                         otherButtonTitles:nil
                                                  tapBlock:nil];
                            }
                        }];

}

- (IBAction)cancel:(id)sender {
    if (_completionBlock)
        _completionBlock();
}

#pragma mark =- UITextFieldDelegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (textField == _emailAddress) {
        [_zipCode becomeFirstResponder];
    } else if(textField == _zipCode) {
        [_password becomeFirstResponder];
    } else if(textField == _password) {
        [self submitRegistration:self];
    }
    return YES;
}

@end
