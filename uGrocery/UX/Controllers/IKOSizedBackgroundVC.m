//
//  IKOSizedBackgroundVC.m
//  uGrocery
//
//  Created by Duane Schleen on 3/14/16.
//  Copyright © 2016 ikonomo. All rights reserved.
//

#import "IKOSizedBackgroundVC.h"
#import "UIScreen+Additions.h"

@interface IKOSizedBackgroundVC ()

@end

@implementation IKOSizedBackgroundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    UIImageView *background = [[UIImageView alloc] initWithImage:[self launchImage]];
    [self.view insertSubview:background atIndex:0];
}

- (UIImage*)launchImage {
    NSString *launchImageName;
    if([UIScreen mainScreen].bounds.size.height > 667.0f) {
        launchImageName = @"LaunchImage-800-736h"; // iphone6 plus
    }
    else if([UIScreen mainScreen].bounds.size.height > 568.0f) {
        launchImageName = @"LaunchImage-800-667h"; // iphone6
    }
    else if([UIScreen mainScreen].bounds.size.height > 480.0f){
        launchImageName = @"LaunchImage-700-568h";// iphone5/5plus
    } else {
        launchImageName = @"LaunchImage-700"; // iphone4 or below
    }
    return [UIImage imageNamed:launchImageName];
}


@end
