//
//  IKOZeroStateViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 9/3/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IKOZeroStateVC : UIViewController

@property (nonatomic) IBOutlet UIImageView *screenImage;
@property (nonatomic) IBOutlet UILabel *zeroStateTitle;
@property (nonatomic) IBOutlet UILabel *zeroStateMessage;

@end
