//
//  IKOBarcodeScanner.m
//  uGrocery
//
//  Created by Duane Schleen on 6/9/14.
//  Copyright (c) 2014 FullContact. All rights reserved.
//

#import "FMemCache.h"
#import "IKOAuthenticationManager.h"
#import "IKOBarcodeScanner.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOImageToastView.h"
#import "IKOPriceUpdate.h"
#import "IKOProduct.h"
#import "IKOProductDetailViewController.h"
#import "IKOProductNoDetailViewController.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIScreen+Additions.h"
#import "UIView+Toast.h"
#import "UIViewController+Utilities.h"
#import <AudioToolbox/AudioServices.h>
#import "IKOError.h"
#import "UIView+Animation.h"

@interface IKOBarcodeScanner () <AVCaptureMetadataOutputObjectsDelegate>

@property (nonatomic) AVCaptureSession *session;
@property (nonatomic) AVCaptureDevice *device;
@property (nonatomic) AVCaptureDeviceInput *input;
@property (nonatomic) AVCaptureMetadataOutput *output;
@property (nonatomic) AVCaptureVideoPreviewLayer *prevLayer;

@property (nonatomic) BOOL torchIsOn;

@property (nonatomic) IKOProductDataStore *productDataStore;
@property (nonatomic) IKOShoppingListDataStore *shoppingListDataStore;
@property (nonatomic) IKOFavoritesDataStore *myProductsDataStore;
@property (nonatomic) IKOSettingsDataStore *settingsDataStore;

@property (nonatomic) IKOProduct *lastDetectedProduct;
@property (nonatomic) IKOServerResponse *lastDetectedServerResponse;
@property (nonatomic) BOOL processing;

@property (nonatomic) UIBarButtonItem *lightButton;

@end

@implementation IKOBarcodeScanner

- (void)viewDidLoad {
    [super viewDidLoad];
    [self setupViewController];
    
    _productDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _shoppingListDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOShoppingListItem class]];
    _myProductsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOFavoriteItem class]];
    _settingsDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOObject class]];
    _torchIsOn = NO;
    [self capture];
    [self buildUI];
}

- (void)viewWillAppear:(BOOL)animated {

#if TARGET_IPHONE_SIMULATOR
    [_productDataStore getProductForUPC:@"TESTDATA" callback:^(IKOError *error, IKOProduct *product, IKOServerResponse *response) {
        _lastDetectedProduct = product;
        [self performSegueWithIdentifier:@"showNoProductDetails" sender:self];
    }];
    return;
#endif

    _spinner.hidden = YES;
    _lastDetectedProduct = nil;
    if (_session)
        [_session startRunning];
    if (![[_session outputs]containsObject:_output]) {
        [_session addOutput:_output];
    }
    self.barcodeGhost.alpha = 1.0f;
    ANALYTIC(@"Opened Scanner Screen");
}

- (void)viewDidAppear:(BOOL)animated {
    [IKOAnalytics logApptentiveEvent:@"Barcode_Scan" fromViewController:self];
    [UIView animateWithDuration:0.9f animations:^{
        self.barcodeGhost.alpha = 0.0f;
    }];
}

- (void)viewWillDisappear:(BOOL)animated {
    [_session stopRunning];
    _torchIsOn = NO;
    [self light:_torchIsOn];
}

- (void)buildUI {
    self.title = @"scan";
    UIBarButtonItem *closeButton = [[UIBarButtonItem alloc]initWithTitle:@"Close" style:UIBarButtonItemStylePlain target:self action:@selector(closeScanner:)];
    self.navigationItem.leftBarButtonItem = closeButton;

    _lightButton = [[UIBarButtonItem alloc]initWithTitle:@"Light" style:UIBarButtonItemStylePlain target:self action:@selector(toggleTorch:)];
    if (_device.hasTorch)
        self.navigationItem.rightBarButtonItem = _lightButton;

    [_autoAddSwitch setOnTintColor:[UIColor UGGreen]];
    [_autoAddMyProductsSwitch setOnTintColor:[UIColor UGGreen]];

    _highlightView.layer.borderColor = [UIColor whiteColor].CGColor;
    _highlightView.layer.borderWidth = 3;
    
    _optionsView.hidden = ![[IKOAuthenticationManager sharedInstance] isAdministrator];
}

- (void)capture {
    _session = [[AVCaptureSession alloc]init];
    _device = [AVCaptureDevice defaultDeviceWithMediaType:AVMediaTypeVideo];
    NSError *error = nil;

    AVCaptureDeviceInput *input = [AVCaptureDeviceInput deviceInputWithDevice:_device
      error:&error];
    if (!input) {
        DDLogError(@"Error: %@", error);
        return;
    }

    [_session addInput:input];

    //Turn on point autofocus for middle of view
    [_device lockForConfiguration:&error];
    CGPoint point = CGPointMake(0.5,0.5);
    [_device setFocusPointOfInterest:point];
    [_device setFocusMode:AVCaptureFocusModeContinuousAutoFocus];
    [_device unlockForConfiguration];

    //Add the metadata output device
    _output = [[AVCaptureMetadataOutput alloc]init];
    [_output setMetadataObjectsDelegate:self queue:dispatch_get_main_queue()];
    [_session addOutput:_output];

    _output.metadataObjectTypes = @[AVMetadataObjectTypeEAN13Code, AVMetadataObjectTypeEAN8Code, AVMetadataObjectTypeUPCECode];
    _output.rectOfInterest = self.previewView.bounds;


    AVCaptureVideoPreviewLayer *newCaptureVideoPreviewLayer = [[AVCaptureVideoPreviewLayer alloc]initWithSession:_session];
    newCaptureVideoPreviewLayer.frame = self.previewView.bounds;
    newCaptureVideoPreviewLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
    [self.previewView.layer insertSublayer:newCaptureVideoPreviewLayer above:self.previewView.layer];

    [_session startRunning];
}

- (void)captureOutput:(AVCaptureOutput *)captureOutput didOutputMetadataObjects:(NSArray *)metadataObjects fromConnection:(AVCaptureConnection *)connection {
    NSString *detectionString = nil;

    for (AVMetadataObject *metadata in metadataObjects) {
        if ([metadata.type isEqualToString:AVMetadataObjectTypeEAN13Code]) {
            NSString *code =[(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            NSLog(@"Read EAN13 %@", code);
            detectionString = code;
        }else if ([metadata.type isEqualToString:AVMetadataObjectTypeUPCECode]) {
            NSString *code =[(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            NSLog(@"Read UPC-E %@", code);
            detectionString = code;
        }else if ([metadata.type isEqualToString:AVMetadataObjectTypeEAN8Code]) {
            NSString *code =[(AVMetadataMachineReadableCodeObject *)metadata stringValue];
            NSLog(@"Read EAN8 %@", code);
            detectionString = code;
        }
    }

    if (detectionString != nil) {
        DDLogVerbose(@"Captured code: %@", detectionString);
        [self processCapture:detectionString];
    }
}

- (IBAction)toggleTorch:(id)sender {
    _torchIsOn = !_torchIsOn;
    [self light:_torchIsOn];
}

- (IBAction)closeScanner:(id)sender {
    [_session stopRunning];
    [_delegate closeScanner];
}

- (void)light:(BOOL)turnOn {
    NSError *deviceError;

    if (![_device hasTorch])
        return;

    if (!turnOn) {
        DDLogVerbose(@"Turning torch mode off");
        _lightButton.title = @"Light";
        [_device lockForConfiguration:&deviceError];
        [_device setTorchMode:AVCaptureTorchModeOff];
        [_device unlockForConfiguration];
    } else {
        DDLogVerbose(@"Turning torch mode on");
        _lightButton.title = @"No Light";
        [_device lockForConfiguration:&deviceError];
        [_device setTorchMode:AVCaptureTorchModeOn];
        [_device unlockForConfiguration];
    }
}

- (void)processCapture:(NSString *)detectionString {
    [_session removeOutput:_output];
    
    _spinner.hidden = NO;
    [_spinner spinWithDuration:15 rotations:1 repeat:10000];
    
    AudioServicesPlayAlertSound(kSystemSoundID_Vibrate);

    __weak __typeof__(self) weakSelf = self;
    
    [_productDataStore getProductForUPC:detectionString callback:^(IKOError *error, IKOProduct *product, IKOServerResponse *response) {
        self.lastDetectedProduct = product;
        self.lastDetectedServerResponse = response;
        
        if (product) {
            [self light:NO];
            DDLogVerbose(@"Found product match: %@ for code: %@", product.productDescription, detectionString);
            
            //if (_autoAddMyProductsSwitch.on)
            [weakSelf addProductToMyFavorites:product];
            
            if (_autoAddSwitch.on)
                [weakSelf addProductToShoppingList:product];
            
            
            if (!(_autoAddMyProductsSwitch.on || _autoAddSwitch.on))
                [weakSelf performSegueWithIdentifier:@"showProductDetails" sender:self];
            
            
            NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ? : @"", @"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
            ANALYTIC_PARAMS(@"Barcode Scanner Found Product", params);
        } else if (!product && !error) {
            self.lastDetectedProduct = [IKOProduct new];
            self.lastDetectedServerResponse = nil;
            self.lastDetectedProduct.upc = detectionString;
            [self performSegueWithIdentifier:@"showNoProductDetails" sender:self];
            NSDictionary *params = @{@"UPC": product.upc ? : @"", @"UPC-E": product.upce ? : @""};
            ANALYTIC_PARAMS(@"Barcode Scanner Did Not Find Product", params);
        } else {
            IKOImageToastView *toast = [[IKOImageToastView alloc]initWithFrame:CGRectMake(20, 100, 220, 48) andImage:nil andMessage:@"An error occurred.  Try Again."];
            int offset = ([[UIScreen mainScreen]bounds].size.height > 480) ? 0 : 88;
            [self.view.superview showToast:toast duration:3 position:[NSValue valueWithCGPoint:CGPointMake([UIScreen currentSize].width / 2, [UIScreen currentSize].height-offset)]];
        }
        _spinner.hidden = YES;
    }];
    
}

- (void)addProductToShoppingList:(IKOProduct *)product {
    //Animate Down
    IKOShoppingListItem *shoppingListItem = [[IKOShoppingListItem alloc]initWithProduct:product];
    [_shoppingListDataStore saveShoppingListItem:&shoppingListItem];
    [_session addOutput:_output];

    IKOImageToastView *toast = [[IKOImageToastView alloc]initWithFrame:CGRectMake(20, 100, 220, 48) andImage:nil andMessage:@"Added to Shopping List..."];
    int offset = ([[UIScreen mainScreen]bounds].size.height > 480) ? 0 : 88;
    [self.view.superview showToast:toast duration:3 position:[NSValue valueWithCGPoint:CGPointMake([UIScreen currentSize].width / 2, [UIScreen currentSize].height-offset)]];
}

- (void)addProductToMyFavorites:(IKOProduct *)product {
    IKOFavoriteItem *myProductItem = [[IKOFavoriteItem alloc]initWithProduct:product];
    [_myProductsDataStore saveMyProductItem:&myProductItem];
    NSDictionary *params = @{@"Product ID": product.productId ? : @"", @"Product Description": product.productDescription ?: @"", @"UPC": product.upc ?: @"", @"UPC-E": product.upce ?: @""};
    ANALYTIC_PARAMS(@"Added Scanned Product to Shopping List", params);
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    [_session stopRunning];
    if ([[segue identifier]isEqualToString:@"showProductDetails"]) {
        IKOProductDetailViewController *vc = [segue destinationViewController];
        vc.product = _lastDetectedProduct;
        vc.lastDetectedServerResponse = _lastDetectedServerResponse;
        if (![_settingsDataStore hasSettingForKey:kShowAdvancedPriceCapture])
            [vc updatePrice];
    } else if ([[segue identifier]isEqualToString:@"showNoProductDetails"]) {
        IKOProductNoDetailViewController *vc = [segue destinationViewController];
        vc.product = _lastDetectedProduct;
    }
}

@end
