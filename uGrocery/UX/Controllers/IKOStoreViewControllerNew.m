//
//  IKOStoreViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/26/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IAPHelper.h"
#import "IAPShare.h"
#import "IKOAuthenticationManager.h"
#import "IKOSDK.h"
#import "IKOStoreViewControllerNew.h"
#import "IKOZeroStateVC.h"
#import "NSData+Base64.h"
#import "PDKeychainBindings.h"
#import "UIAlertView+Blocks.h"
#import "UIView+Animation.h"
#import "UIScreen+Additions.h"

typedef NS_ENUM(NSInteger, StoreSections)
{
    Header = 0,
    SubscriptionOptions,
    Pitch,
    RestorePurchases
};


@interface IKOStoreViewControllerNew ()

@property (nonatomic) NSArray *products;
@property (nonatomic) UIView *waitView;

@end

@implementation IKOStoreViewControllerNew

- (void)viewDidLoad {
    [super viewDidLoad];
}

-(void)viewWillAppear:(BOOL)animated {
    [self loadData];
}

- (void)displayWaitIndicator:(BOOL)show {
    [_waitView removeFromSuperview];
    self.tableView.userInteractionEnabled = YES;
    if (show) {
        IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
        [vc loadView];
        vc.zeroStateTitle.text = @"";
        vc.zeroStateMessage.text = @"";
        vc.screenImage.image = [UIImage imageNamed:@"spinner"];
        [vc.screenImage spinWithDuration:15 rotations:1 repeat:100000];
        _waitView = vc.view;
        _waitView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
        _waitView.alpha = 0;
        _waitView.backgroundColor = [UIColor whiteColor];
        [self.view insertSubview:_waitView atIndex:999];
        [UIView animateWithDuration:0.2 animations:^{
            _waitView.alpha = 1;
        }];
        self.tableView.userInteractionEnabled = NO;
    } else {
        [_waitView removeFromSuperview];
        _waitView = nil;
        self.tableView.userInteractionEnabled = YES;
    }
}

- (void)loadData {
    if (![IAPShare sharedHelper].iap.products) {
        [[IAPShare sharedHelper].iap requestProductsWithCompletion:^(SKProductsRequest *request,SKProductsResponse *response)
         {
            NSMutableArray *products = [[IAPShare sharedHelper].iap.products mutableCopy];
            [products sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES]]];
            _products = products;
            [self.tableView reloadData];
         }];
    } else {
        NSMutableArray *products = [[IAPShare sharedHelper].iap.products mutableCopy];
        [products sortUsingDescriptors:@[[NSSortDescriptor sortDescriptorWithKey:@"price" ascending:YES]]];
        _products = products;
    }
}

#pragma mark - Table view data source

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case Header:
            return 82;
            break;
        case SubscriptionOptions: {
            return 60;
            break;
        }
        case Pitch : {
            return 336;
            break;
        }
        case RestorePurchases :
            return 56;
            break;
        default:
            return 44;
            break;
    }
}

-(NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section {
    if (section == SubscriptionOptions) {
        return @"Subscription Options";
    }
    return nil;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 4;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == SubscriptionOptions)
        return _products.count;
    return 1;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    switch (indexPath.section) {
        case Header: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"titleCell" forIndexPath:indexPath];
            UILabel *label = (UILabel *)[cell viewWithTag:666];
            if (![[IKOAuthenticationManager sharedInstance]isInTrialPeriod] && [[IKOAuthenticationManager sharedInstance] daysRemainingOnSubscription] > 0){
                label.text = [[IKOAuthenticationManager sharedInstance] getExpirationDateString];
            } else if ([[IKOAuthenticationManager sharedInstance]isInTrialPeriod]) {
                label.text = SF(@"Currently Trialing uGrocery Premium - %ld Days Left in Trial.  Go Premium Now!", [[IKOAuthenticationManager sharedInstance]trialPeriodRemaining] );
            } else {
                label.text = @"Always find the Lowest Bill with a Premium Subscription";
            }
            return cell;
            break;
        }
        case SubscriptionOptions: {
            IKOSubscriptionCell *cell = (IKOSubscriptionCell *)[tableView dequeueReusableCellWithIdentifier:@"subscriptionCell" forIndexPath:indexPath];
            [cell configureCellWith:_products[indexPath.row]];
            cell.delegate = self;
            if ([[IKOAuthenticationManager sharedInstance] daysRemainingOnSubscription] > 0) {
                [cell.subscriptionButton setTitle:@"Renew" forState:UIControlStateNormal];
            } else {
                [cell.subscriptionButton setTitle:@"Subscribe" forState:UIControlStateNormal];
            }
            return cell;
            break;
        }
        case Pitch: {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"pitchCell" forIndexPath:indexPath];
            return cell;
            break;
        }
        case RestorePurchases : {
            UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"restoreCell" forIndexPath:indexPath];
            UIButton *restorePurchasesButton = (UIButton *)[cell viewWithTag:666];
            [restorePurchasesButton addTarget:self action:@selector(restorePurchases:) forControlEvents:UIControlEventTouchUpInside];
            return cell;
            break;
        }
        default:
            return [UITableViewCell new];
            break;
    }
    
}

- (void)unlockContent:(SKPaymentTransaction *)trans {
    __weak __typeof__(self) weakSelf = self;
    [[IAPShare sharedHelper].iap checkReceipt:trans.transactionReceipt AndSharedSecret:kSS onCompletion:^(NSString *response, NSError *error) {
        NSDictionary *rec = [IAPShare toJSON:response];
        
        if([rec[@"status"] integerValue]==0) {
            NSString *productIdentifier = trans.payment.productIdentifier;
            [self setExpirationDate:trans.payment.productIdentifier];
            
            [[IAPShare sharedHelper].iap provideContent:productIdentifier];
            DDLogInfo(@"SUCCESS %@",response);
            DDLogInfo(@"Purchases %@",[IAPShare sharedHelper].iap.purchasedProducts);
            
            BOOL isProduction = [IAPShare sharedHelper].iap.production;
            
//            [[PDKeychainBindings sharedKeychainBindings]setObject:SF(@"%f", [NSDate timeIntervalSinceReferenceDate]) forKey:(NSString *)kFirstLaunchKey];
            
            [[IKOSDK new]premiumUpgrade:[IKOAuthenticationManager currentUser] expiration:[IKOAuthenticationManager sharedInstance].expirationDate receipt:[trans.transactionReceipt base64EncodedString] isSandboxReceipt:isProduction callback:^(IKOError *error, IKOServerResponse *response) {
            }];
        
            [UIAlertView showWithTitle:nil
                               message:@"Thank you for purchasing a subscription to uGrocery!"
                     cancelButtonTitle:@"OK"
                     otherButtonTitles:nil
                              tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                                  if (buttonIndex == 0) {
                                      [weakSelf.navigationController.tabBarController setSelectedIndex:0];
                                  }
                              }];
        } else {
            DDLogInfo(@"Failed purchasing product subscription");
        }
    }];
}

- (void)setExpirationDate:(NSString*)productIdentifier {
    if ([productIdentifier isEqualToString:IKO1MonthSubscription]) {
        [[IKOAuthenticationManager sharedInstance] purchaseSubscriptionWithMonths:1];
    } else if ([productIdentifier isEqualToString:IKO3MonthSubscription]) {
        [[IKOAuthenticationManager sharedInstance] purchaseSubscriptionWithMonths:3];
    } if ([productIdentifier isEqualToString:IKO1YearSubscription]) {
        [[IKOAuthenticationManager sharedInstance] purchaseSubscriptionWithMonths:12];
    }
}

- (IBAction)restorePurchases:(id)sender {
    [[IAPShare sharedHelper].iap restoreProductsWithCompletion:^(SKPaymentQueue *payment, NSError *error) {
        for (SKPaymentTransaction *transaction in payment.transactions) {
            NSString *purchased = transaction.payment.productIdentifier;
            if ([@[ALL_PRODUCT_IDS] containsObject:purchased]) {
                [self unlockContent:transaction];
            }
        }
    }];
}

#pragma mark - IKOPurchaseProductDelegate

- (void)purchaseProduct:(SKProduct *)product {
    if (!product) {
        [UIAlertView showWithTitle:@"Cannot Purchase"
                           message:@"There was an error purchasing your selected product.  Please try again later."
                 cancelButtonTitle:@"OK"
                 otherButtonTitles:nil
                          tapBlock:nil];
        return;
    }
    
    [self displayWaitIndicator:YES];
    
    [[IAPShare sharedHelper].iap buyProduct:product
       onCompletion:^(SKPaymentTransaction *trans){
           if (trans.error) {
               [self displayWaitIndicator:NO];
               [UIAlertView showWithTitle:@"Cannot Purchase"
                                  message:trans.error.localizedDescription
                        cancelButtonTitle:@"OK"
                        otherButtonTitles:nil
                                 tapBlock:nil];
           } else if(trans.transactionState == SKPaymentTransactionStatePurchased) {
               [self unlockContent:trans];
               [self displayWaitIndicator:NO];
           }else if(trans.transactionState == SKPaymentTransactionStateFailed) {
               [self displayWaitIndicator:NO];
               [UIAlertView showWithTitle:@"Cannot Purchase"
                                  message:@"There was a problem purchasing your subscription.  Please try again."
                        cancelButtonTitle:@"OK"
                        otherButtonTitles:nil
                                 tapBlock:nil];
           }
       }];
}


@end
