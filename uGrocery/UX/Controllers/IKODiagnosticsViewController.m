//
//  IKODiagnosticsViewController.m
//  uGrocery
//
//  Created by Duane Schleen on 7/29/15.
//  Copyright (c) 2015 ikonomo. All rights reserved.
//

#import "IKODiagnosticsViewController.h"
#import "IKOProductDataStore.h"
#import "IKOProductCategoryDataStore.h"
#import "IKOBrandDataStore.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOZeroStateVC.h"
#import "UIView+Animation.h"
#import "UIScreen+Additions.h"
#import "UIAlertView+Blocks.h"

@interface IKODiagnosticsViewController ()

@property (nonatomic) IKOProductCategoryDataStore *categoryDataStore;
@property (nonatomic) IKOProductDataStore *productDataStore;
@property (nonatomic) IKOBrandDataStore *brandDataStore;

@property (nonatomic) UIView *waitView;

@end

@implementation IKODiagnosticsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _categoryDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProductCategory class]];
    _productDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOProduct class]];
    _brandDataStore = [[IKODataStorageManager sharedInstance]storeForClass:[IKOBrand class]];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
     [self displayWaitIndicator:YES withTitle:@"Loading Diagnostics" andMessage:@"One moment please..."];
}

- (void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    [self setCounts];
    [self displayWaitIndicator:NO];
}

- (void)setCounts {
    _productCount.text = SF(@"%lu", (unsigned long)[_productDataStore allObjects].count);
    _brandCount.text = SF(@"%lu", (unsigned long)[_brandDataStore allObjects].count);
    _l1Count.text = SF(@"%lu", (unsigned long)[_categoryDataStore categoriesForLevel:@1].count);
    _l2Count.text = SF(@"%lu", (unsigned long)[_categoryDataStore categoriesForLevel:@2].count);
    _l3Count.text = SF(@"%lu", (unsigned long)[_categoryDataStore categoriesForLevel:@3].count);
}

- (void)displayWaitIndicator:(BOOL)show {
    [self displayWaitIndicator:show withTitle:nil andMessage:nil];
}

- (void)displayWaitIndicator:(BOOL)show withTitle:(NSString *)title andMessage:(NSString *)message {
    //[_waitView removeFromSuperview];
    self.tableView.userInteractionEnabled = YES;
    if (show) {
        self.navigationItem.hidesBackButton = YES;
        IKOZeroStateVC *vc = [[UIStoryboard storyboardWithName:@"iPhone" bundle:nil]instantiateViewControllerWithIdentifier:@"ikoZeroStateViewController"];
        [vc loadView];
        vc.zeroStateTitle.text = title;
        vc.zeroStateMessage.text = message;
        vc.screenImage.image = [UIImage imageNamed:@"spinner"];
        [vc.screenImage spinWithDuration:15 rotations:1 repeat:100000];
        _waitView = vc.view;
        _waitView.frame = CGRectMake(0, 0, [UIScreen currentSize].width, [[UIScreen mainScreen]bounds].size.height-113);
        _waitView.alpha = 0;
        _waitView.backgroundColor = [UIColor whiteColor];
        [self.view insertSubview:_waitView atIndex:999];
        [UIView animateWithDuration:0.2 animations:^{
            _waitView.alpha = 1;
        }];
        self.tableView.userInteractionEnabled = NO;
    } else {
        self.navigationItem.hidesBackButton = NO;
        [_waitView removeFromSuperview];
        _waitView = nil;
        self.tableView.userInteractionEnabled = YES;
    }
}


- (IBAction)seedBrands:(id)sender {
    [self displayWaitIndicator:YES withTitle:@"Seeding Brands" andMessage:@"Seeding Brands... this may take a moment..."];
    IKOBrandDataStore *brandDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOBrand class]];
    [brandDataStore seedBrands:IKOSeedTypeAdmin completion:^{
        [self setCounts];
        [self displayWaitIndicator:NO];
    }];
}

- (IBAction)seedCategories:(id)sender {
    [self displayWaitIndicator:YES withTitle:@"Seeding Categories" andMessage:@"Seeding Categories... this may take a moment..."];
    
    IKOProductCategoryDataStore *productCategoryDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOProductCategory class]];
    [productCategoryDataStore seedCategories:IKOSeedTypeAdmin completion:^{
        [self setCounts];
        [self displayWaitIndicator:NO];
    }];
}

- (IBAction)seedProducts:(id)sender {
    
    [UIAlertView showWithTitle:@"Seed Products?"
                       message:@"Loading admin seed products will take a bit of time - are you sure you'd like to proceed?"
             cancelButtonTitle:@"Cancel"
             otherButtonTitles:@[@"OK"]
                      tapBlock:^(UIAlertView *alertView, NSInteger buttonIndex) {
                          if (buttonIndex == 1) {
                              [self displayWaitIndicator:YES withTitle:@"Seeding Products" andMessage:@"This admin seed WILL take a quite awhile...Please wait until this screen disappears"];
                              
                              IKOProductDataStore *productDataStore = [[IKODataStorageManager sharedInstance] storeForClass:[IKOProduct class]];
                              [productDataStore seedProducts:^{
                                  [self setCounts];
                                  [self displayWaitIndicator:NO];
                              }];
                          }
                      }];
    
}

@end
