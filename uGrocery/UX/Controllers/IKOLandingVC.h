//
//  IKOLandingViewController.h
//  uGrocery
//
//  Created by Duane Schleen on 11/12/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "IKOSizedBackgroundVC.h"

@interface IKOLandingVC : IKOSizedBackgroundVC

- (IBAction)signin:(id)sender;
- (IBAction)signup:(id)sender;

@end
