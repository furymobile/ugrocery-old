//
//  AppDelegate.m
//  uGrocery
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//


#import "AppDelegate.h"
#import "DDFileLogger.h"
#import "DDTTYLogger.h"
#import "FFileFunctionLevelFormatter.h"
#import "IKOAuthenticationManager.h"
#import "IKODataStorageManager+uGrocery.h"
#import "IKOLocationManager.h"
#import "UIColor+uGrocery.h"
#import "UIFont+uGrocery.h"
#import "UIViewController+Utilities.h"
#import "UVStyleSheet.h"
#import "UserVoice.h"
#import "ATConnect.h"

@implementation AppDelegate

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    [IKONetworkManager connectedToNetwork];
    [self configureLogging];
    [self configureAppearance];
    [self configureApptentive];
    
    [application setStatusBarHidden:NO];
    //[application setStatusBarStyle:UIStatusBarStyleLightContent];
    
    [UserVoice initialize:[[IKOAuthenticationManager sharedInstance]userVoiceConfig]];
    [IKOAnalytics logEvent:ANALYTICS_EVENT_STARTED_APP withParameters:nil];
    
    return YES;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    [[IKODataStorageManager sharedInstance]flushDatabases];
    [[IKOLocationManager sharedInstance]stopUpdatingLocation];
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    [[IKODataStorageManager sharedInstance]flushDatabases];
    [[IKOLocationManager sharedInstance]stopUpdatingLocation];
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    [[IKOLocationManager sharedInstance]resumeUpdatingLocation];
}

- (void)applicationWillTerminate:(UIApplication *)application {
    [[IKODataStorageManager sharedInstance]flushDatabases];
    [[IKODataStorageManager sharedInstance]closeDatabases];
    [[IKOLocationManager sharedInstance]stopUpdatingLocation];
}

#pragma mark - Setup

- (void)configureLogging {
    FFileFunctionLevelFormatter *Formatter = [[FFileFunctionLevelFormatter alloc]init
      ];
#ifdef DEBUG
    [DDLog addLogger:[DDTTYLogger sharedInstance]];

    [[DDTTYLogger sharedInstance]setColorsEnabled:YES];
    [[DDTTYLogger sharedInstance]setForegroundColor:[UIColor IKOInfoLogColor] backgroundColor:nil forFlag:LOG_FLAG_INFO];
    [[DDTTYLogger sharedInstance]setForegroundColor:[UIColor IKOErrorLogColor] backgroundColor:nil forFlag:LOG_FLAG_ERROR];
    [[DDTTYLogger sharedInstance]setForegroundColor:[UIColor IKOWarnLogColor] backgroundColor:nil forFlag:LOG_FLAG_WARN];
    [[DDTTYLogger sharedInstance]setForegroundColor:[UIColor IKOVerboseLogColor] backgroundColor:nil forFlag:LOG_FLAG_VERBOSE];
    [[DDTTYLogger sharedInstance]setLogFormatter:Formatter];
#endif

    DDFileLogger *fileLogger = [[DDFileLogger alloc]init];
    fileLogger.rollingFrequency = 60 * 60 * 24; // 24 hour rolling
    fileLogger.logFileManager.maximumNumberOfLogFiles = 7;

    [fileLogger setLogFormatter:Formatter];

    [DDLog addLogger:fileLogger];


    NSString *appVersion = [NSString stringWithFormat:@"%@", [[NSBundle mainBundle]objectForInfoDictionaryKey:@"CFBundleShortVersionString"]];
    NSString *build = [[[NSBundle mainBundle]infoDictionary]objectForKey:(NSString *) kCFBundleVersionKey];
    DDLogInfo(@"Welcome to uGrocery %@ (%@)", appVersion, build);
}

- (void)configureAppearance {
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil]setDefaultTextAttributes:@{NSForegroundColorAttributeName: [UIColor UGDkGrey],NSFontAttributeName: [UIFont IKOHeavyFontOfSize:15.0]}];

    [[UINavigationBar appearance] setTintColor:[UIColor UGBlue]];
    [[UINavigationBar appearance] setBarTintColor:[UIColor UGWhite]];
    [[UITextField appearanceWhenContainedIn:[UISearchBar class], nil] setTextColor:[UIColor UGDkGrey]];

    [[UINavigationBar appearance]setTitleTextAttributes:
    @{ NSForegroundColorAttributeName:
       [UIColor UGBlue],
       NSFontAttributeName: 
       [UIFont IKOMediumFontOfSize:18]}];

    [[UIBarButtonItem appearance] setTitleTextAttributes:
    @{
        NSForegroundColorAttributeName: [UIColor UGBlue],
        NSFontAttributeName: [UIFont IKOLightFontOfSize:16.0]
    }
    forState:UIControlStateNormal];
    

    [[UINavigationBar appearanceWhenContainedIn:[ATNavigationController class], nil] setBarTintColor:[UIColor UGWhite]];
    [[UINavigationBar appearanceWhenContainedIn:[ATNavigationController class], nil] setTitleTextAttributes:@{ NSForegroundColorAttributeName:
                                                                                                                   [UIColor UGBlue],
                                                                                                               NSFontAttributeName: 
                                                                                                                   [UIFont IKOMediumFontOfSize:18]}];
    [[UITabBar appearance]setTintColor:[UIColor redColor]];
    [[UITabBar appearance]setBarTintColor:[UIColor UGDkGrey]];
    [[UITabBar appearance]setSelectedImageTintColor:[UIColor whiteColor]];

    _window.tintColor = [UIColor UGLtGrey];

    [UVStyleSheet instance].tableViewBackgroundColor = [UIColor UGReallyLtGrey];
}

- (void)configureApptentive {
    [ATConnect sharedConnection].apiKey = @"a0464100228cc3f0032fafae47f512efd2c0b806dfb64330061d547bac6ff228";
}

@end
