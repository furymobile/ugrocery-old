#!/usr/bin/php
<?php
set_time_limit(0);
ini_set('memory_limit', '512M');

/**
 * Usage ./fetch-seed.php <environment> <target>
 *   <environment> is one of { dev, staging, uat, prod }
 *   <target> is one of { admin, public }
 */
$sEnv = $argv[1];

$sTarget = 'public';
if(isset($argv[2]) && $argv[2] == 'admin')
    $sTarget = 'admin';

switch($sEnv) {
case 'dev':
    $sSeedUrl = 'http://admin.dev.ikonomo.com/dump-json.php?type=';
    break;
case 'staging':
    $sSeedUrl = 'https://api.staging.ugrocery.com/dump-json.php?type=';
    break;
case 'uat':
    $sSeedUrl = 'https://api.uat.ugrocery.com/dump-json.php?type=';
    break;
case 'prod':
    $sSeedUrl = 'https://api.ugrocery.com/dump-json.php?type=';
    break;
default:
    die("Please enter a valid environment <dev,staging,uat,prod>\n");
}
$sSeedTarget .= '&target=' . $sTarget;
$sSeedDir     = realpath(__DIR__ . '/../uGrocery/json') . '/';

$sBrandsUrl = $sSeedUrl . 'brands' . $sSeedTarget;
echo "Fetching brands seed with URL $sBrandsUrl\n";
file_put_contents($sSeedDir . 'brands.json', file_get_contents($sBrandsUrl));

$sCatUrl = $sSeedUrl . 'categories' . $sSeedTarget;
echo "Fetching categories seed with URL $sCatUrl\n";
file_put_contents($sSeedDir . 'categories.json', file_get_contents($sCatUrl));

$sCatJoinsUrl = $sSeedUrl . 'category_joins' . $sSeedTarget;
echo "Fetching category joins seed with URL $sCatJoinsUrl\n";
file_put_contents($sSeedDir . 'category_joins.json', file_get_contents($sCatJoinsUrl));

$sProductUrl = $sSeedUrl . 'products' . $sSeedTarget;
echo "Fetching products seed with URL $sProductUrl\n";
file_put_contents($sSeedDir . 'products.json', file_get_contents($sProductUrl));

$sZipUrl = $sSeedUrl . 'zips' . $sSeedTarget;
echo "Fetching colorado zips seed with URL $sZipUrl\n";
file_put_contents($sSeedDir . 'coloradozips.json', file_get_contents($sZipUrl));
