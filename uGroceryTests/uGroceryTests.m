//
//  uGroceryTests.m
//  uGroceryTests
//
//  Created by Duane Schleen on 6/10/14.
//  Copyright (c) 2014 ikonomo. All rights reserved.
//

#import <XCTest/XCTest.h>
#import "IKOAuthenticationManager.h"
#import "IKOSDK.h"
#import "IKOFLBRequestPayload.h"
#import "WaitSemaphore.h"

static NSString *const UserName = @"duane@furymobile.com";
static NSString *const Password = @"Asdf1234!";

@interface uGroceryTests : XCTestCase

@end

@implementation uGroceryTests


BOOL authenticated = NO;


- (void)setUp
{
    [super setUp];
    if(!authenticated) {
        [[IKOSDK new]loginUser:UserName
            password:Password
            callback:^(IKOError *error, IKOServerResponse *response) {
                if (response.success) {
                    [[IKOAuthenticationManager sharedInstance]authenticatedUser:response];
                    authenticated = YES;
                    [[WaitSemaphore sharedInstance] lift:@"setUp"];
                } else {
                    [[WaitSemaphore sharedInstance] lift:@"setUp"];
                }
            }
         ];
        [[WaitSemaphore sharedInstance] waitForKey:@"setUp"];
    }
}

- (void)tearDown
{
    [super tearDown];
}

- (void)testIsAdministrator
{
    XCTAssertTrue([[IKOAuthenticationManager sharedInstance] isAdministrator], "Expected user to be an administrator");
}

- (void)testFindLowestBillDetailViaProductId {
    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc]initWithUpc:@"070640034390"];
    payload.withCoupons = [NSNumber numberWithBool:[@"0" boolValue]];
    
    [[IKOSDK new] findLowestBillsDetailWithPayload:[payload JSONRepresentation] storeIds:@[@2,@4,@5,@7] user:[IKOAuthenticationManager currentUser] callback:^(IKOError *error, IKOServerResponse *responseParams) {
        XCTAssertTrue(responseParams.success, @"Expected Response to be Successful");
        [[WaitSemaphore sharedInstance] lift:@"testFindLowestBillDetailViaProductId"];
    }];
    [[WaitSemaphore sharedInstance] waitForKey:@"testFindLowestBillDetailViaProductId"];
}

- (void)testSearchForProduct {
    NSNumber *page  = [NSNumber numberWithInt:1];
    NSNumber *limit = [NSNumber numberWithInt:35];

    [[IKOSDK new]
        searchProducts:@""
        storeIds:@[@2,@4,@5,@7]
        l1CatIds:@[]
        l3CatIds:@[]
        brandIds:@[@2684]
     withCoupons:NO
 withStoreBrands:YES
            page:page
           limit:limit
            user:[IKOAuthenticationManager currentUser]
        callback:^(IKOError *error, IKOServerResponse *responseParams) {
            XCTAssertTrue(responseParams.success, @"Expected Response to be Successful");
            [[WaitSemaphore sharedInstance] lift:@"testSearchForProduct"];
        }];

    [[WaitSemaphore sharedInstance] waitForKey:@"testSearchForProduct"];
}

- (void)testFetchBulkData {

    [[IKOSDK new]
     fetchBulkData:@"admin"
     type:@"zips"
     callback:^(IKOError *error, IKOServerResponse *responseParams) {
         XCTAssertTrue(responseParams.success, @"Expected Response to be Successful");
         [[WaitSemaphore sharedInstance] lift:@"testFetchBulkData"];
     }];

    [[WaitSemaphore sharedInstance] waitForKey:@"testFetchBulkData"];
}

- (void) testSearchForDeals {
    IKOFLBRequestPayload *payload = [[IKOFLBRequestPayload alloc] init];

    [[IKOSDK new]
     findDealsTopWithPayload: [payload JSONRepresentation]
     storeIds:@[@2,@4,@5,@7]
     user:[IKOAuthenticationManager currentUser]
     callback:^(IKOError *error, IKOServerResponse *responseParams) {
         XCTAssertTrue(responseParams.success, @"Expected Response to be Successful");
         [[WaitSemaphore sharedInstance] lift:@"testSearchForProduct"];
     }];

    [[WaitSemaphore sharedInstance] waitForKey:@"testSearchForProduct"];
}

@end
