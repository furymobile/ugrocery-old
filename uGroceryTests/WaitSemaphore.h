#import <Foundation/Foundation.h>

@interface WaitSemaphore : NSObject

@property(strong, atomic) NSMutableDictionary *flags;

+ (WaitSemaphore *)sharedInstance;

- (BOOL)isLifted:(NSString *)key;
- (void)lift:(NSString *)key;
- (void)waitForKey:(NSString *)key;
- (void)purgeKeys;
- (void)purgeKey:(NSString *)key;

@end
